'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');
var async = require('async');
const rp = require("request-promise");
var logger = require('./../winstonlog'); // to force the initialization code (above) to run once
var wechatConfig = require("../config/wechatConfig");

var listByName = function (req, res) {
    res.status(200);
    var schools = [{
            "name": "John",
            "city": "Doe"
        },
        {
            "name": "Anna",
            "city": "Smith"
        },
        {
            "name": "Peter",
            "city": "Jones"
        }
    ];
    res.json(schools);
};

var updateMemberInfo = function (result, wechatResponse, callback) {
    if (result[0].nickname !== wechatResponse.nickname || result[0].avatar !== wechatResponse.headimgurl|| result[0].open_id !== wechatResponse.openid) {
        db.get().pool.query('update member SET avatar = ?,open_id = ?, sex = ? where id = ?', [wechatResponse.headimgurl,wechatResponse.openid, wechatResponse.sex, result[0].id], function (err, _) {
            if (err) {
                return callback(err);
            } else {
                console.log("update return a:" + result[0].id);
                var sql = 'update member SET nickname = ? where id = ?';
                //var values = [wechatResponse.nickname, 'qq', wechatResponse.unionid,1,1,1,1,1];
                db.get().pool.query(sql, [wechatResponse.nickname, result[0].id], function (err) {
                    return callback(null, result[0].id);
                });
            }
        });
    } else {
        console.log("update return b:" + result[0].id);
        return callback(null, result[0].id);
    }
}

var insertNewMemberInfo = function (result, wechatResponse, callback) {
    if (wechatResponse.introId === 'null') {
        wechatResponse.introId = 0;
    }
    var newItem = {
        nickname: '--',
        qq: 'qq',
        wechat: wechatResponse.unionid,
        avatar: wechatResponse.headimgurl,
        reg_ip: 1,
        open_id: wechatResponse.openid,
        sex: wechatResponse.sex,
        last_login_ip: 1,
        login_count: 1,
        reg_time: 1,
        last_login_time: 1,
        introducer: wechatResponse.introId || 0,
        loginPoints: table.loginPoints
    };
    console.log("insert....");
    var tasks = [
        function newMember(waterfallCallback) {
            var sql = 'INSERT INTO member SET ? ';
            db.get().pool.query(sql, newItem, function (err, currResult) {
                console.log("insert.... a");
                waterfallCallback(err, currResult.insertId);
            });
        },
        function updateUserNickName(insertId, waterfallCallback) {
            var sql = 'update member SET nickname = ? where id = ?';
            //var values = [wechatResponse.nickname, 'qq', wechatResponse.unionid,1,1,1,1,1];
            db.get().pool.query(sql, [wechatResponse.nickname, insertId], function (err) {
                waterfallCallback(err, insertId);
            });
        },
        function insertNewPointHistory(insertId, waterfallCallback) {
            if (newItem.introducer && newItem.introducer > 0 && insertId) {
                var newPointHistory = {
                    uid: newItem.introducer,
                    points: table.introUserPoints,
                    pointType: 1,
                    clientId: insertId
                }
                var sql = 'INSERT INTO memberPoints SET ? ';
                //var values = [wechatResponse.nickname, 'qq', wechatResponse.unionid,1,1,1,1,1];
                db.get().pool.query(sql, newPointHistory, function (err) {
                    if (err) {
                        console.error(err);
                    }
                    console.log("insert.... b");
                    waterfallCallback(err, {
                        insertId: insertId,
                        introducer: newItem.introducer
                    });
                });
            } else {
                console.log("insert.... c");
                waterfallCallback(null, {
                    insertId: insertId,
                    introducer: 0
                });
            }
        },
        function updateUserIntroPoint(item, waterfallCallback) {
            var sql = 'update member SET userIntroPoints = userIntroPoints + 100 where id = ?';
            //var values = [wechatResponse.nickname, 'qq', wechatResponse.unionid,1,1,1,1,1];
            db.get().pool.query(sql, [item.introducer], function (err) {
                waterfallCallback(err, item.insertId);
            });
        }
    ];
    async.waterfall(tasks, function (err, insertId) {
        if (err) {
            console.log("insert return b:" + err);
            console.log(err);
            return callback(err, insertId);
        } else {
            console.log("insert return b" + insertId);
            return callback(null, insertId);
        }
    });
}

var createNew = function (wechatResponse, callback) {
    console.log("createNew");
    db.get().pool.query(
        "SELECT id FROM member WHERE wechat = ?", [wechatResponse.unionid],
        function (err, result) {
            if (err) {
                console.error("createNew-Err:" + err);
                return callback(err);
            }
            if (result.length > 0) {
                updateMemberInfo(result, wechatResponse, callback);
                //console.log("update-c:");
            } else {
                insertNewMemberInfo(result, wechatResponse, callback);
                console.log("insert-c:");
            }
        }
    );
};
var getReferenceCode = function (uid, callback) {
    if (uid) {
        db.get().pool.query(
            "SELECT reference_code FROM member WHERE agentType = 1 and id = ?", [uid],
            function (err, result) {
                if (err) return callback(err);;
                if (result.length > 0) {
                    return callback(null, result[0]);
                } else {
                    return callback("invalid agent Id", {
                        reference_code: "520665"
                    });
                }
            }
        );
    } else {
        return callback("invalid agent Id", "520665");
    }
}
var readOne = function (uid, callback) {
    if (uid) {
        db.get().pool.query(
            "SELECT * FROM member WHERE id = ?", [uid],
            function (err, result) {
                if (err) throw err;
                if (result.length > 0) {
                    if (!result[0].reference_code) {
                        logger.debug(result[0]);
                        result[0].reference_code = false;
                    }
                    return callback(result[0]);
                } else {
                    return callback({});
                }
            }
        );
    }
};

var updateProfile = function (user, callback) {
    db.get().pool.query('update member SET phone = ?, email = ?, wechat_Id = ?, introducer = ? ,fullname = ?, agentType = ?, slogan = ? where id = ?', [user.phone, user.email, user.wechat_Id, user.introducer, user.fullname, user.agentType, user.slogan, user.uid], function (err, _) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, user.uid);
        }
    });
};
var updateNationId = function (user, callback) {
    db.get().pool.query('update member SET nation_Id = ? where id = ?', [user.nation_Id, user.uid], function (err, _) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, user);
        }
    });
};

var updateWechatQrcode = function (user, callback) {
    db.get().pool.query('update member SET wechat_qrcode = ? where id = ?', [user.wechat_qrcode, user.uid], function (err, _) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, user);
        }
    });
};
var getIdByReferenceCode = function (code, callback) {
    if (code) {
        db.get().pool.query(
            "SELECT id,avatar,fullname,phone,wechat_qrcode,reference_code FROM member WHERE agentType = 1 and reference_code = ?", [code],
            function (err, result) {
                if (err) return callback(err);;
                if (result.length > 0) {
                    return callback(null, result[0]);
                } else {
                    return callback("invalid code", {});
                }
            }
        );
    } else {
        return callback("invalid code", {});
    }
};

var getMemberOrders = function (uid, callback) {
    if (uid) {
        db.get().pool.query(
            "SELECT memberOrders.id,created_time,updated_time,productId,orderStatus,originalPrice,rewardpoints,title FROM memberOrders join product on memberOrders.productId = product.id  WHERE clientId = ?", [uid],
            function (err, result) {
                if (err) return callback(err);;
                if (result.length > 0) {
                    return callback(null, result);
                } else {
                    return callback("invalid user id", {});
                }
            }
        );
    } else {
        return callback("invalid user id", {});
    }
};
var insertNewOrder = function (newOrder, callback) {
    /*db.get().pool.query('update member SET phone = ?,fullname = ? where id = ?',
        [user.phone, user.fullname, user.uid], function (err, _) {
            if (err) {
                return callback(err);
            } else {
                return callback(null, user.uid);
            }
        });
        */
    // var tasks = [
    //function updateProfile(cb) {
    db.get().pool.query('update member SET phone = ?,fullname = ? where id = ?', [newOrder.phone, newOrder.fullname, newOrder.uid], function (err, _) {
        //      cb();
        var order = {
            clientId: newOrder.uid || 0,
            productId: newOrder.pid || 0,
            orderStatus: 4,
            agentId: newOrder.agentId || 0,
            reference_code: newOrder.ref_code || '0'
        }
        var sql = 'INSERT INTO memberOrders SET ? ';
        db.get().pool.query(sql, order, function (err, currResult) {
            return callback(err, currResult.insertId);
        });
    });
    // },
    // function insertNew(cb) {

    // }
    // ];
    /*async.waterfall(tasks, function (err, insertId) {
        return callback(err, insertId);
    });*/


};
var getMemo = function (orderId, callback) {
    db.get().pool.query(
        "SELECT comment, orderStatus, stamp from memo where orderId = ? order by stamp", [orderId],
        function (err, result) {
            if (err) return callback(err);;
            if (result.length > 0) {
                return callback(null, result);
            } else {
                return callback("no record found", {});
            }
        }
    );
};
var clients = function (uid, callback) {
    if (uid) {
        db.get().pool.query(
            "SELECT memberOrders.Id,clientId,orderStatus,memberOrders.created_time,nickname, fullname, avatar, title FROM memberOrders join member on memberOrders.clientId = member.id join product on memberOrders.productId = product.id WHERE agentId = ?", [uid],
            function (err, result) {
                return callback(err, result);
            }
        );
    }
};
var referees = function (uid, callback) {
    if (uid) {
        db.get().pool.query(
            "SELECT id,nickname,created_time,fullname,avatar FROM member WHERE introducer = ?", [uid],
            function (err, result) {
                return callback(err, result);
            }
        );
        //return callback(result);
    }
};
var points = function (uid, callback) {
    if (uid) {
        db.get().pool.query(
            "SELECT * FROM memberPoints WHERE uid = ? ORDER BY stamp DESC", [uid],
            function (err, result) {
                if (err) throw err;
                return callback(result);
            }
        );
    }
};
//var exchangeRate = function (uid, callback) {
//    if (uid) {
//        db.get().pool.query(
//            "SELECT * FROM memberPoints WHERE uid = ?", [uid],
//            function (err, result) {
//                if (err) throw err;
//                if (result.length > 0) {
//                    return callback(result);
//                }
//            }
//        );
//    }
//};

var pointSummary = function (uid, callback) {
    if (uid) {
        db.get().pool.query(
            "SELECT * FROM memberPoints WHERE uid = ?", [uid],
            function (err, result) {
                if (err) throw err;
                var map = {};
                if (result.length > 0) {
                    for (var i = result.length - 1; i >= 0; i--) {
                        var key = result[i].uid + "-" + result[i].pointType + "-" + result[i].stamp.toISOString().split("T")[0];
                        if (key in map) {
                            map[key].points += result[i].points;
                        } else {
                            map[key] = result[i];
                        }
                    }
                    var final_res = [];
                    for (key in map) {
                        final_res.push(map[key]);
                    }
                    return callback(final_res);
                }
            }
        );
    }
};

var list = function (req, res) {
    var parseUrl = url.parse(req.url, true);
    var typeId = parseUrl.query.type_id;
    if (typeId) {
        db.get().pool.query(
            "SELECT * FROM school WHERE type = ? ORDER BY national_rank", [typeId],
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    } else {
        db.get().pool.query(
            "SELECT * FROM school ORDER BY national_rank",
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    }
};

var wechat_auth = function (code, callback) {
        //console.log("auth:" + code);
        // console.log("type:" + req.params.type || "0");
        //var type = 0;
        // if (req.params && req.params.type){
        //     type = req.params.type || 0;
        // }
        //console.log("ty:" + type);

        // if(req.params.type){

        // }
        var rs_access_token = "";
        if (code) {
            var url1 = `https://api.weixin.qq.com/sns/oauth2/access_token?appid=${wechatConfig.appid}&secret=${wechatConfig.secret}&code=${code}&grant_type=authorization_code`;
            //var url1 = "http://localhost:8080/products/code/" + code;
            rp(url1).then(response => {
                    console.log("get access code");
                    var accessTokenResult = JSON.parse(response);
                    var accessToken = accessTokenResult.access_token;
                    var refreshToken = accessTokenResult.refresh_token;
                    var openId = accessTokenResult.openid;
                    var url2 = `https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=${wechatConfig.appid}&grant_type=refresh_token&refresh_token=${refreshToken}`;
                    //var url2 = "https://api.weixin.qq.com/sns/auth?access_token=" + accessToken + "&openid=" + openId;

                    return rp(url2);
                }).then(response => {
                    console.log("get access code step 2");
                    var accessTokenResult = JSON.parse(response);
                    var accessToken = accessTokenResult.access_token;
                    var refreshToken = accessTokenResult.refresh_token;
                    var openId = accessTokenResult.openid;
                    var url4 = `https://api.weixin.qq.com/sns/userinfo?access_token=${accessToken}&openid=${openId}`;
                    //var url3 = "http://localhost:8080/products/code3/ee";
                    //console.log(url3);
                    console.log("return:" + code + ":" + url4.errcode + url4.errmsg + "--" + accessToken + "-" + openId);
                    rs_access_token = accessToken;
                    return rp(url4);
                })
                .then(responseRaw => {
                    // do stuff after all requests
                    console.log("return 2 raw:" + responseRaw);
                    var response = JSON.parse(responseRaw);
                    console.log("return 2:" + code + ":<" + response + ">-->" + response.errcode + response.errmsg + response.nickname + "unionid:" + response.unionid + ":" + response.openid + "--" + response.subscribe);
                    if (response.unionid) {
                        var bytelike = unescape(encodeURIComponent(response.nickname));
                        response.nickname = decodeURIComponent(escape(bytelike)).replace(/[\uFF00-\uFFFF]/g, '');
                        console.log("new name:" + response.nickname);
                        response.introId = 0;
                        response.access_token = rs_access_token;
                        createNew(response, function (err, id) {
                            response.uid = id;
                            response.err = err;
                            console.log("membCtrl.createNew" + response + response.uid);
                            //response.token = token;
                            callback({result: response, err: null});
                        });
                    } else {

                        response.customErr = "unionid is null";
                        callback({result: null, err: response});
                    }
                    console.log(code + ":<");
                })
                .catch(err => {
                    console.log(err);
                    callback({result: null, err: err});

                }); // Don't forget to catch errors
        }
};

var getCredit = function(id, callback){
    var returnObj={};
    db.get().pool.query(
        "select IFNULL((select a.points/100 from (select uid, sum(points) as points from memberPoints where uid = ? GROUP BY uid) as a)  ,0) + 100 as available" , [id],
	    function (err, rows, fields) {
		    if (err) throw err;
            returnObj.available = rows[0].available;
            db.get().pool.query("select IFNULL((select (sum(points)/100) from memberPoints where pointType = 30 and uid = ? GROUP BY uid), 0 ) as withdrawn", [id], function(err, rows){
                 if (err) throw err;
                 returnObj.withdrew = rows[0].withdrawn;
                callback(returnObj);
            })
	    }
    );
};
var getClientsIntroducedByMe = function(id, callback){
    db.get().pool.query("select nickname, created_time from member where introducer = ?",[id], function(err, rows){
        if (err) throw err;
        callback(null ,rows);
    });
}
module.exports = {
    getClientsIntroducedByMe: getClientsIntroducedByMe,
    getCredit: getCredit,
    wechat_auth: wechat_auth,
    listByName: listByName,
    readOne: readOne,
    list: list,
    createNew: createNew,
    points: points,
    pointSummary: pointSummary,
    clients: clients,
    getIdByReferenceCode: getIdByReferenceCode,
    getMemberOrders: getMemberOrders,
    referees: referees,
    updateProfile: updateProfile,
    getMemo: getMemo,
    insertNewOrder: insertNewOrder,
    getReferenceCode: getReferenceCode,
    updateWechatQrcode: updateWechatQrcode,
    updateNationId: updateNationId
}