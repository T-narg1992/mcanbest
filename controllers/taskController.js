const async = require("async");
const db = require("../db");

var fn1 = function (cb2) {
    timeNow = new Date(Date.now() + 8 * 60 * 60 * 1000).toISOString().replace(/T/, ' ').replace(/\..+/, ''); //hard converted to china time...
    console.log(timeNow);
    db.get().pool.query("Select * from dailyTask where startDateTime<? and endDateTime>?", [timeNow, timeNow],
        function (err, rows, fields) {
            if (err) {
                cb2(err, null);
            }
            if (rows.length == 0) {
                cb2("No tasks at the moment!", null);

            } else {
                var tasklist = rows;
                cb2(null, tasklist);
            }
        });
};
var fn2 = function (tasklist, cb4) {
    var titles = [];
    var resultF;
    async.forEach(tasklist, function (task, cb3) {
        var module;
        var table;
        var id;
        if (task.module == 1) {
            table = 'article';
            module = 'article';
        } else if (task.module == 2) {
            table = 'school';
            module = 'school';
        } else if (task.module == 3) {
            table = 'question';
            module = 'question';
        } else if (task.module == 4) {
            table = 'product_new'
            module = 'product';
        }
        id = task.row_id;
        query = "select " + table + ".*, " + /*table + ".id," + table + ".title, " + table + ".promo_thumb, " + */"dt.id as dailyTaskId from " + table + " right join dailyTask dt on dt.row_id = " + table + ".id where " + table + ".id = " + parseInt(id);
        db.get().pool.query(query,
            function (err, rows, fields) {
                //console.log(rows[0]);
                rows[0].type = task.module == 1 ? (task.category == 1) ? "article" : "event" : module;
                titles.push(rows[0]);
                cb3(err, titles);
            });
    }, function (err) {
        if (err) {
            return err;
        }
        cb4(null, titles);
    });
};

var getMission = function(callback){
    async.waterfall(
        [fn1, fn2],
        function (err, resultF) {
            if (err) callback(null);
            else callback(resultF);
        }
    );
};

module.exports = {
    getMission: getMission
}