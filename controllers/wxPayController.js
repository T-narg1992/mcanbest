const payConfig = require("../config/wechatPayConfig").liunarPayConfig;
const payment = require("wechat-pay").Payment;

var wxPaymentSign = function(order, callback){
    var wxPayment = new payment(payConfig);
    wxPayment.getBrandWCPayRequestParams(order, function(err, payargs){
        if(err){
            console.log(err);
            callback("error", err);
        }else{
            callback(payargs, null);
        }
    });
};

module.exports = {
    wxPaymentSign: wxPaymentSign
}