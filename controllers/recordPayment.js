var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');
var async = require('async');
const request = require('superagent');

var selectTransaction = function(id, callback){
    db.get().pool.query(
        "SELECT * FROM Transactions WHERE id = ?",
        [parseInt(id)],
        function(err, item){
            if(err) console.log(err);
            callback(item);
        }
        )
};

var wxPayDownPayment = function(fee, outtrade, transactionid, timeend, openid, feetype, status, o_id){
    console.log("hit wxPayDownPayment");

    var saveTransaction = function(callback){
        db.get().pool.query(
            "INSERT INTO Transactions (orderId, fee, method, timestamp, wx_outtradeno, wx_transaction_id, wx_timeend, wx_openid, wx_feetype) VALUES (?,?,?,?,?,?,?,?,?)",
            [
            parseInt(o_id),
            fee,
            1,
            parseInt(new Date().getTime()),    
            outtrade,
            transactionid,
            timeend,
            openid,
            feetype,
            ],
            function(err, res3){
                if(err) throw err;
                console.log('before updateOrder');
                callback(null, res3.insertId, openid);
            }
            );
    }; 

    var updateOrder = function(trans_id ,openid ,callback){
        console.log('openid', openid);
        console.log('trans_id', trans_id);
        db.get().pool.query(
            "UPDATE memberOrders SET transaction_id = ?, orderStatus = ? where id= ? ", 
            [trans_id, status ,o_id],
            function(err, res){
                if(err) throw err;
                callback(null, trans_id);
            }
            )
    }

    var tasks = [saveTransaction, updateOrder];
    async.waterfall(
        tasks,
        function(err,result){
            if(err) throw err;
            selectTransaction(result, function(item){
                request.post(`http://ystage.liunar.com/account/wxpay?transactionId=${item[0].id}&openId=${item[0].wx_openid}`)
                .send({ transactionId: item[0].id, openId: item[0].wx_openid })
                .end(
                    function(err,res){
                        if (err) console.log(err);
                    }
                    )
            });
        }
        );
};

module.exports = {
    wxPayDownPayment: wxPayDownPayment
}