﻿'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');
var listByName = function (req, res) {
    res.status(200);
    var schools = [
        { "name": "John", "city": "Doe" },
        { "name": "Anna", "city": "Smith" },
        { "name": "Peter", "city": "Jones" }
    ];
    res.json(schools);
};

var readOne = function (req, res) {

    if (req.params && req.params.id) {
        db.get().pool.query(
            "SELECT * FROM school WHERE id = ?",
            [req.params.id],
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    } else {
        res.status(404);
        res.json({ "message": "school id is required." });
    }
};


module.exports = {
    listByName: listByName,
    readOne: readOne,
}