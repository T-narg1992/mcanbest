﻿'use strict';

exports.reviewsCreate = function (req, res) {
    res.status(201);
    //show new review data
    res.json({ "name": "review content", "id": 111 });
};
exports.updateOne = function (req, res) {
    if (req.params && req.params.reviewid < 10) {
        res.status(200);
        //show review data
        res.json({ "review": "review content", "schoolid": req.params.schoolid, "reviewid": req.params.reviewid });
    } else {
        res.status(404);
        res.json({ "message": "no review found" });
    }
};
exports.deleteOne = function (req, res) {
    if (req.params && req.params.reviewid < 10) {
        //instead of using 204, return 200 and send data back to the client
        res.status(200);
        res.json({ "message":"deleted!","review": "review content", "id": req.params.reviewid });
    } else {
        res.status(400);
        res.json({ "message": "bad request" });        
    }
};

exports.readOne = function (req, res) {
    if (req.params && req.params.reviewid < 10) {
        res.status(200);
        res.json({ "review": "review content", "schoolid": req.params.schoolid, "reviewid": req.params.reviewid });
    } else {
        res.status(404);
        res.json({ "message": "no review found" });
    }
};
exports.readAll = function (req, res) {
    if (req.params && req.params.schoolid < 10) {
        res.status(200);
        var reviews = [
            { "name": "John", "city": "Doe" },
            { "name": "Anna", "city": "Smith" },
            { "name": "Peter", "city": "Jones" }
        ];
        res.json({ "schoolid": req.params.schoolid, "reviews": reviews });
    } else {
        res.status(404);
        res.json({ "message": "no review found" });
    }
};