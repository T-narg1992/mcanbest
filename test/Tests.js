'use strict';

var chai = require('chai');
var schools = require('../controllers/schoolController');
var members = require('../controllers/memberController');

//router.get('/schools', schools.list);

var expect = chai.expect;

chai.should();

var hashCode = function (uId) {
    var parseUrl = {
        query : {
            type_id: "1",
            city: "2",
            ispublic : "3" 
        }
    };

    var q = schools.schoolFilterQuery(parseUrl);
    console.log(q.query);
    console.log(q.params);
    q.query.should.equal("SELECT * FROM school where  type = ?  and  city = ?  and  isPublic = ?  ORDER BY national_rank");
    q.params.length.should.equal(3);

    parseUrl = {
        query: {
            city: "2",
            ispublic: "3",
            limit: 123
        }
    };

    q = schools.schoolFilterQuery(parseUrl);
    console.log(q.query);
    console.log(q.params);
    q.query.should.equal("SELECT * FROM school where  city = ?  and  isPublic = ?  ORDER BY national_rank LIMIT ?");
    q.params.length.should.equal(3);

    parseUrl = {
        query: {
        }
    };

    q = schools.schoolFilterQuery(parseUrl);
    console.log(q.query);
    console.log(q.params);
    q.query.should.equal("SELECT * FROM school ORDER BY national_rank");
    q.params.length.should.equal(0);

    parseUrl = {
        query: {
            type_id: "1",
            city: "2"
        }
    };

    q = schools.schoolFilterQuery(parseUrl);
    console.log(q.query);
    console.log(q.params);
    q.query.should.equal("SELECT * FROM school where  type = ?  and  city = ?  ORDER BY national_rank");
    q.params.length.should.equal(2);

    return 65 * 177 + 123456;
};

function newUser() {
    var wechat = {
        "openid": "OPENID",
        "nickname": "NICKNAM2E",
        "sex": 1,
        "province": "PROVINCE",
        "city": "CITY",
        "country": "COUNTRY",
        "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
        "privilege": [
            "PRIVILEGE1",
            "PRIVILEGE2"
        ],
        "unionid": " o6_bmasc10rtdstaid6hMZOPfLbq",
        "introId": 1239
    };
    members.createNew(wechat, function (err, id) {
        wechat.uid = id;
        console.log("memb.createNew" + response + response.uid);
    });

}

function isEven(num){
    //return false;
    console.log("has:" + hashCode(65));
    newUser();
    return num % 2 === 0;
}

describe('isEven', function(){
	it('should return true when number is even', function(){
        //console.log("return 2:" + code + ":<" + response + ">-->" + response.errcode + response.errmsg + response.nickname + "unionid:" + response.unionid + ":" + response.openid + "--" + response.sex);
	    var nickname = "Alex Song";
        var bytelike = unescape(encodeURIComponent(nickname));
        nickname = decodeURIComponent(escape(bytelike)).replace(/[\uFF00-\uFFFF]/g, '');
        console.log("new name:" + nickname);
        isEven(4).should.be.true;
	});
	it('should return false when number is odd', function(){
		isEven(3).should.be.false;
		expect(isEven(3)).to.be.false;
	});
});


function add(num1, num2){
	return num1 + num2;
}

describe('add with setup/teardown',function(){
	var num;
	beforeEach(function(){
		num = 5;
	});
	
	afterEach(function(){
		
	});
	
	it('should be ten when adding 5 to 5', function(){
		num = add(num,5);
		num.should.equal(10);
	});
	
	xit('should be ten when adding 7 to 5', function(){
		num = add(num,7);
		num.should.equal(12);
	});
		
});

