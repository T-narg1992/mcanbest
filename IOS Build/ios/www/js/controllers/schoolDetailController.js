
angular.module('schoolDetailController',[])
.controller('schoolDetailController', function($document, $rootScope, $scope, schoolDetail, $stateParams, $sce,  $state, $ionicHistory, schoolComment, localStorage, $ionicPopup){
    
    $scope.school = schoolDetail;
    $scope.schoolComment = schoolComment;
    
    console.log(schoolDetail,$state,$ionicHistory,schoolComment);
    
    //wechatShare();
    $scope.mapModules = [
        {
            text: "街道地图",
            value: "google.maps.MapTypeId.ROADMAP"
        },
        {
            text: "实景街图",
            value: "streetView"
        },
        {
            text: "鸟瞰地图",
            value: "google.maps.MapTypeId.SATELLITE"
        }
    ];
    
    $scope.selected = $scope.mapModules[0];
    $scope.mapType = $scope.mapModules[0].value;
    
    $scope.select = function(item){
        console.log(item);
        $scope.selected = item;
        $scope.mapType = item.value;
    };
    
    console.log($scope.mapType);
    
    $scope.commentObj = {
        row_id : $stateParams.postId,
        content: "",
        module: 2   
    };
    
    $scope.leaveComment = function(){
        if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $state.go("app.userCenter");    
                }
            );
            //$state.go();
        }else{
            $scope.commentObj.uid = localStorage.getObject("canbestUserInfo").uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.getPostDetail();
                            }
                        )
                    }
                );
            }
        }
    };
    
})
.controller('campDetailController', function($scope, httpServices, $stateParams, campDetail, $sce){
    console.log(campDetail, $sce);
    $scope.school = campDetail;
    $scope.daily = $sce.trustAsHtml(campDetail["daily_schedule"]);
    $scope.detail = $sce.trustAsHtml(campDetail["personalize"])
    //console.log(Object.keys(campDetail), campDetail["daily_schedule"]);
})