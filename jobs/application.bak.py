import os
import flask
import MySQLdb

application = flask.Flask(__name__)
application.debug = True

@application.route('/')
def hello_world():
  storage = Storage()
  storage.populate()
  score = storage.score()
  print "Hello Beijing 123, %d!" % score

class Storage():
  def __init__(self):
    self.db = MySQLdb.connect(
      user   = 'alex',
      passwd = 'alexsong',
      db     = 'app',
      host   = '52.36.30.184',
      port   = 3306
    )

    cur = self.db.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS scores(score INT)")

  def populate(self):
    cur = self.db.cursor()
    cur.execute("INSERT INTO `memberPoints` (uid,points,pointType,productId,clientId) VALUES (61,33,1,NULL,9)")
    self.db.commit()

	
  def score(self):
    cur = self.db.cursor()
    cur.execute("SELECT * FROM memberPoints")
    row = cur.fetchone()
    return row[0]

if __name__ == "__main__":
  hello_world()	
