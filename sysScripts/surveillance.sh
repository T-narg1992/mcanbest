#!/bin/bash

#checks log files and sends email about irregularities
strindex() { 
  x="${1%%$2*}"
  [[ $x = $1 ]] && echo -1 || echo ${#x}
}
FILES=~/cronlogs/*
for f in $FILES    
do
    lastline=$(tail -1 $f | head -1)
    asd=$(strindex "$lastline" success)
    if [ $asd -eq -1 ]; then
        emailtitle="oh no, $f"
        emailbody="$lastline"
	echo "$emailbody" | mail -s "$emailtitle" roger@canbest.org
    fi
done 
