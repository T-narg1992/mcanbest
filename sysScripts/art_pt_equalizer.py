
import os
import MySQLdb
import MySQLdb.cursors
from time import gmtime, strftime, localtime

conn = MySQLdb.connect(host='liunar.com',user='alex',passwd='alexsong',port=3306, cursorclass=MySQLdb.cursors.DictCursor)

cur = conn.cursor()
cur.execute('use app')

d_memberpoints_articleViews = {}
d_article_articleViews = {}
d_pointsToInsert = {}

queries = {}
queries['getMemberPointsList'] = "select uid, sum(points) as totalpoints from memberPoints where pointType = 4 and uid > 0 group by uid"
queries['getArticlePointsList'] = "select uid, sum(view_count) as totalpoints from article where uid > 0 group by uid"
queries['params'] = []

cur.execute( queries['getArticlePointsList'] )
rows = cur.fetchall()
for row in rows:
    d_article_articleViews[row['uid']] = row['totalpoints']

cur.execute( queries['getMemberPointsList'] )
rows = cur.fetchall()
for row in rows:
    d_memberpoints_articleViews[row['uid']] = row['totalpoints']


memberPoints_userlist = d_memberpoints_articleViews.keys()
article_userList = d_article_articleViews.keys()
memberPoints_userlist.extend(article_userList)
allUniqueUsers = list(set(memberPoints_userlist))

for id in allUniqueUsers:
    if id in d_article_articleViews:
        pass
    else:
        d_article_articleViews[id] = 0
    if id in d_memberpoints_articleViews:
        pass
    else:
        d_memberpoints_articleViews[id] = 0
    difference = d_article_articleViews[id] - d_memberpoints_articleViews[id]
    if difference > 0:
        queries['params'].append((id, difference, 4))

for x in queries['params']:
    cur.execute("insert into memberPoints (uid, points, pointType) VALUES (%s,%s,%s)", (x))
    conn.commit()
conn.close()
print "members updated :: "+  str(len(queries['params']) + " " + strftime("%Y-%m-%d %H:%M:%S", localtime())
