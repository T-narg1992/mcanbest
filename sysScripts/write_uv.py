import os
import MySQLdb
import MySQLdb.cursors
from time import gmtime, strftime, localtime

f = open('/var/log/nginx/uv.log.1', 'r')
content = f.readlines()
list_unique = list(set(content))
config = {}
dictlist=[]
for line in list_unique:
        if line.find('pv.jpg') != -1:
                if line.find('referid=null') == -1:
                        temp = line[line.find('referid'):line.find('HTTP/')-1]
                        temp = temp.split('&')
                        if temp and len(temp) > 1:
                                try:
                                        refer_label = temp[0].split('=')[0]
                                        refer_val = temp[0].split('=')[1]
                                        type_label = temp[1].split('=')[0]
                                        type_val = temp[1].split('=')[1]
                                        if refer_val and refer_label and type_val and type_label :
                                                d = dict()
                                                d['sharer_id'] = refer_val
                                                d['type'] = type_label
                                                d['id'] = type_val
                                                d['cookie'] = line.split(':::')[1]
                                                if len(temp) > 2 and temp[2].split('=')[0] == 'sharetasktime':
                                                        d['sharetime'] = strftime("%Y-%m-%d %H:%M:%S", localtime(int(temp[2].split('=')[1])))
                                                if len(temp) > 3 and temp[3].split('=')[0] == 'taskid':
                                                        d['taskid'] = temp[3].split('=')[1]
                                        dictlist.append(d) #if err, do not add
                                except:
                                        print 'Except1:'+ line
                                        print "Unexpected error:", sys.exc_info()[0]

conn = MySQLdb.connect(host='localhost',user='root',passwd='Grant1234',port=3306, cursorclass=MySQLdb.cursors.DictCursor)
cur = conn.cursor()
cur.execute('use app')
d_sharing_points = {}
d_page_views = {}
print 'lines total count:',len(list_unique)
print 'share lines total count:' , len(dictlist)

for item in dictlist:

        if item['sharer_id'] in d_sharing_points:
                d_sharing_points[item['sharer_id']] += 1
        else:
                d_sharing_points[item['sharer_id']] = 1
        if 'taskid' in item:
                query = "select * from dailyTask where id='%s' and  startDateTime< '%s' and endDateTime > '%s';" % (str(item['taskid']),item['sharetime'],item['sharetime'])
                cur.execute(query)
                if not cur.rowcount:
                        print 'not a dailytask'
                else:
                        d_sharing_points[item['sharer_id']] +=1 #only for double points, for N times points=> +=N-1
print 'points summary: ' , d_sharing_points


for k,v in d_sharing_points.iteritems():
        cur.execute("Insert into memberPoints ( uid, points, pointType ) values ("+ str(k) + "," + str(v) + ",3)")
        conn.commit()
abc = {}
cur.execute("select uid, sum(points) as introp from memberPoints where pointType = 1 GROUP BY uid")
rows = cur.fetchall()
for row in rows:
        if row['uid'] in abc:
                abc[row['uid']]+= row['introp']
        else:
                abc[row['uid']] = row['introp']
for key, value in abc.iteritems():
        cur.execute("update member set userIntroPoints =" + str(value/10+100) +" where id =" + str(key))
        conn.commit()
abc.clear()

cur.execute("select uid, sum(points) as salesp from memberPoints where pointType = 2 GROUP BY uid")
rows = cur.fetchall()
for row in rows:
        if row['uid'] in abc:
                abc[row['uid']]+= row['salesp']
        else:
                abc[row['uid']] = row['salesp']
for key, value in abc.iteritems():
        cur.execute("update member set salePoints= "+ str(value) +" where id = "+ str(key))
        conn.commit()
abc.clear()


cur.execute("select uid, sum(view_count) as sharep from article where uid > 0 GROUP BY uid")
rows = cur.fetchall()
for row in rows:
        if row['uid'] in abc:
                abc[row['uid']]+= row['sharep']
        else:
                abc[row['uid']] = row['sharep']
for key, value in abc.iteritems():
        cur.execute("update member set sharePoints="+ str(value) +" where id = "+ str(key))
        conn.commit()
abc.clear()
cur.close()
print 'success:'+ strftime("%Y-%m-%d %H:%M:%S", localtime())
conn.close()
