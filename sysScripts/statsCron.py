import os
import MySQLdb 
from time import strftime, time, localtime
import json
import sys



string_joiner = ''

class King(object):

	def __init__(self):
		self.d = {}
		self._list = []

	def buildD(self, conn, cur, t_now): 
		# update member points
		cur.execute("select uid, sum(points) from memberPoints where pointType = 1 group by uid")
		for x in cur.fetchall():
			cur.execute("update member set userIntroPoints = '"+str(x[1])+"' where id = '"+str(x[0])+"'") 
			conn.commit();
		cur.execute("select uid, sum(points) from memberPoints where pointType = 2 group by uid")
		for x in cur.fetchall():
			cur.execute("update member set salePoints ='"+str(x[1])+"' where id = '"+str(x[0])+"'") 
			conn.commit();
		cur.execute("select uid, sum(points) from memberPoints where pointType = 3 group by uid")
		for x in cur.fetchall():
			cur.execute("update member set sharePoints ='"+str(x[1])+"' where id = '"+str(x[0])+"'") 
			conn.commit();
		print 'finished updating user points in member table!'
		self.d['totalMembers'] = '(Select count(*) as total from member where User_type != 99)'
		self.d['dailyMemberIncrease'] = '(Select count(*) as dailyIncrease from member where User_type != 99  and unix_created_time > %s)' % (t_now-24*60*60,)
		self.d['applyingAgentsTotal'] = '(Select count(*) from member where agentType = 2)'
		self.d['successfulApplicantsTotal'] = '(Select count(*) from member where agentType = 1)'
		self.d['dailyAgentIncrease'] = '(Select count(*) from member where agentType = 1 and unix_created_time > %s)' % (t_now-24*60*60,)
		self.d['sitePVTotal'] = '(Select sum(visits) as total from siteStats where unix_time > %s)' % (t_now-24*60*60,)
		self.d['shareNetworkPV'] = 'select IFNULL( (Select sum(visits) from siteStats where sharer_id > 0 and unix_time > %s) , 0) as total' % (t_now-24*60*60,)
		self.d['siteUVTotal'] = '(select count(distinct(cookie)) from siteStats where unix_time > %s)' % (t_now-24*60*60,)
	
		cur.execute("select concat(siteStats.type,'=', row_id), count(concat(siteStats.type,'=', row_id)) as c from siteStats where type ='article' group by concat(siteStats.type,'=', row_id) order by c desc limit 10" )
		for x in cur.fetchall():
			id = x[0].split('=')[1]
			query = 'select title from article where id = %s' % (id,)
			cur.execute(query) 
			title = cur.fetchone()
			url = 'http://www.liunar.net/#/app/mainhome/postdetail/'+id
			x=  {'title':title, 'url': url, 'count': x[1]}
			self._list.append(x)
		self.d['shareUrlTop10'] = json.dumps(self._list,ensure_ascii=False)

		self._list=[]
		cur.execute("select m.id, m.nickname, sum(mp.points) as s from member as m join memberPoints as mp on m.id = mp.uid where pointType = 3 and unix_stamp < "+str(t_now)+" group by m.id order by s desc limit 10;") 
		for x in cur.fetchall():
			x=  {'id':x[0], 'nickname': x[1], 'sharePoints': str(x[2])}
			self._list.append(x)
		self.d['sharePointsTop10'] = json.dumps(self._list,ensure_ascii=False) 

		self._list=[]
		cur.execute("select m.id, m.nickname, sum(mp.points) as s from member as m join memberPoints as mp on m.id = mp.uid where pointType = 2 and unix_stamp < "+str(t_now)+" group by m.id order by s desc limit 10;")
		for x in cur.fetchall():
			x=  {'id':x[0],'nickname':x[1], 'salePoints':str(x[2])}
			self._list.append(x)
		self.d['salePointsTop10'] = json.dumps(self._list,ensure_ascii=False)
		
		self._list=[]
		cur.execute("select m.id, m.nickname, sum(mp.points) as s from member as m join memberPoints as mp on m.id = mp.uid where pointType = 1 and unix_stamp < " +str(t_now)+" group by m.id order by s desc limit 10;")
		for x in cur.fetchall():
			x=  {'id':x[0],'nickname':x[1], 'introducePoints':str(x[2])}
			self._list.append(x)
		self.d['introducePointsTop10'] = json.dumps(self._list,ensure_ascii=False)
		

		self._list=[]
		cur.execute("select m.id, m.nickname, sum(mp.points) as s from member as m join memberPoints as mp on m.id = mp.uid where unix_stamp < "+str(t_now)+" group by m.id order by s desc limit 10;") 
		for x in cur.fetchall():
			x=  {'id':x[0],'nickname':x[1], 'points':str(x[2])}
			self._list.append(x)
		self.d['agentPointsTop10'] = json.dumps(self._list,ensure_ascii=False)


		self._list=[]
		cur.execute("select count(*) as new_users from member where unix_created_time > "+str(t_now-24*60*60))
		self.d['newWechatUsersDaily'] = cur.fetchone()[0]

		self._list=[]

		cur.execute("select m.id, m.nickname, sum(mp.points) as p from memberPoints mp join member m on m.id = mp.uid where unix_stamp <"+str(t_now)+" and unix_stamp >" +str(t_now-24*60*60*7) +" group by uid order by p desc limit 10")
		for x in cur.fetchall():
		 	self._list.append({'id':x[0],'nickname':x[1],'points':int(x[2])})
		self.d['fastestGrowingAgentTop10'] = json.dumps(self._list,ensure_ascii=False)


		query ="insert into site_stats_readable set "
		for k,v in self.d.items():
			if k == 'applyingAgentsTotal' or k == 'successfulApplicantsTotal' or k =='sitePVTotal' or k=='siteUVTotal' or k == 'shareNetworkPV' or k =='dailyMemberIncrease' or k=='totalMembers' or k=='dailyAgentIncrease':
		 		query += "%s = %s," % (k,v)
		 	else:	
		 		query += "%s = '%s'," % (k,v)
		query= query.strip(',')
		time_struct = localtime(t_now)
		time_formatted = strftime('%Y-%m-%d %H:%M:%S', time_struct)
		query+= ",create_time = '%s'" % (time_formatted,)
		cur.execute(query)
		conn.commit()
		print 'success:'+ strftime("%Y-%m-%d %H:%M:%S", localtime())

def main(): 
	tnow = time() #time now
	king = King()
	conn = MySQLdb.connect(host='localhost',user='root',passwd='Grant1234',port=3306)
	cur = conn.cursor()
	cur.execute('use app')	
	cur.execute('set names utf8')
	king.buildD(conn, cur, tnow)
	king._list = []
	king.d.clear()
	cur.close() 
	conn.close()
	sys.exit(1)

if __name__ == '__main__':
    main()
