var mysql = require('mysql');

var http = require('http');

var url = require('url');

var prefix = "o2c0_";

var config = require('./config')['development'];

var table = {
    userTable: prefix + "ucenter_member",
    newsTable: prefix + "news",
    newsTableDetail: prefix + "news_detail",
    newsCommentTable: prefix + "local_comment",
    pictureTable: prefix + "picture"
};

var dbC = mysql.createConnection({
    host: config.database.host,
    user: config.database.username,
    password: config.database.password,
    database: config.database.db
});

var dbC2 = mysql.createConnection({
   host: config.database.host,
   user: config.database.username,
   password: config.database.password,
   database: config.db2.db
});

dbC.connect();
dbC2.connect();

var getList = function(callback){
    dbC.query(
        "SELECT " + table.newsTable + ".*, " + table.userTable + ".username, " + table.pictureTable + ".path" + " FROM " + table.newsTable + " LEFT JOIN " + table.pictureTable + " ON " + table.newsTable + ".cover = " + table.pictureTable + ".id" + " LEFT JOIN " + table.userTable + " ON " + table.newsTable + ".uid = " + table.userTable + ".id LIMIT 5",
        function(err,result){
            if(err){
                callback(JSON.stringify(err), null);
            }else{
                callback(null, JSON.stringify(result));
                //dbC.end();
            }
        }
        );
};

var getPostDetail = function(callback,id){
    dbC.query(
        "SELECT " + table.newsTableDetail + ".content, " + table.newsTable + ".* ," + table.userTable + ".username " + "FROM " + table.newsTable + " LEFT JOIN " + table.newsTableDetail + " ON " + table.newsTable +".id = " + table.newsTableDetail + ".news_id " + " LEFT JOIN " + table.userTable + " ON " + table.newsTable + ".uid = " + table.userTable + ".id WHERE " + table.newsTable + ".id = ?",
        [id],
        function(err,result){
            if(err){
                //throw err;
                callback(JSON.stringify(err), null);
            }else{
                callback(null, JSON.stringify(result));
                //dbC.end();
            }
        }
        );
};

var getPostComment = function(callback, type, id){
  dbC.query(
      "SELECT " + table.newsCommentTable + ".*, " + table.userTable + ".username FROM " + table.newsCommentTable + " LEFT JOIN " + table.newsTable + " ON " + table.newsTable + ".id = " + table.newsCommentTable + ".row_id LEFT JOIN " + table.userTable + " ON " + table.userTable + ".id = " + table.newsCommentTable + ".uid WHERE " + table.newsCommentTable + ".row_id = ? AND " + table.newsCommentTable + ".app = ?",
      [id, type],
      function(err, result){
         if(err){
            throw err;
            callback(JSON.stringify(err), null);
        }else{
            callback(null, JSON.stringify(result));
                //dbC.end();
            }
        }
    );
};

var getSchoolDetail = function(callback, id){
    dbC2.query(
        "SELECT * FROM school WHERE id = ?",
        [id],
        function(err, result){
            if(err){
                throw err;
                callback(JSON.stringify(err), null);
            }else{
                callback(null, JSON.stringify(result));
            }
        }
        );
};

var getSchoolList = function(callback, id){
    dbC2.query(
        "SELECT * FROM school WHERE type = ? ORDER BY national_rank",
        [id],
        function(err, result){
            if(err){
                throw err;
                callback(JSON.stringify(err), null);
            }else{
                callback(null, JSON.stringify(result));
            }
        }
        );
};
var getArticles = function (callback, id) {
   dbC2.query(
    "SELECT * FROM article WHERE status = 1 ORDER BY create_time DESC LIMIT 50"
    , function (err, result) {
       if(err){
        throw err;
        callback(JSON.stringify(err), null);
    }else{
        callback(null, JSON.stringify(result));
    }
});
}
var getArticle = function (callback, id) {
 dbC2.query(
    "SELECT * FROM article WHERE id = ?"
    ,[id], function (err, result) {
       if(err){
        throw err;
        callback(JSON.stringify(err), null);
    }else{
        callback(null, JSON.stringify(result));
    }
});
}
http.createServer(function(request, response){
    var parseUrl =url.parse(request.url, true);
    response.setHeader("Access-Control-Allow-Origin","*");
        //console.log(parseUrl,request);
       // var query = JSON.stringify(parseUrl.query);
        var callback = function(err,result){
            response.writeHead(200);
            //response.setHeader("Access-Control-Allow-Origin","*");            response.write(err || result);
            response.end(err || result);
        };

       //console.log(parseUrl.query);
        if(request.method === "GET"){
            if(Object.prototype.hasOwnProperty.call(parseUrl.query,'GetList')){
                getList(callback);
            }else if(Object.prototype.hasOwnProperty.call(parseUrl.query,'GetPostDetail')){
                getPostDetail(callback,parseUrl.query.id.toString());
            }else if(Object.prototype.hasOwnProperty.call(parseUrl.query,'GetPostComments')){
                getPostComment(callback, parseUrl.query.app, parseUrl.query.id.toString());
            }else if(Object.prototype.hasOwnProperty.call(parseUrl.query,'GetSchoolList')){
                getSchoolList(callback, parseUrl.query.type_id.toString());
            }else if(Object.prototype.hasOwnProperty.call(parseUrl.query,'GetSchoolDetail')){
                getSchoolDetail(callback, parseUrl.query.id.toString());
            }
            else{
                response.end("need some request");
            }
        }

	/*else if(request.method === "POST"){
            var requestObject = {};
            request.on('data', function(chunk){
                var requestBody = chunk.toString('utf-8').split('&');
                for (var i = 0; i < requestBody.length; i++) {
                    var requestObject_key = requestBody[i].split("=");
                    requestObject[requestObject_key[0]] = requestObject_key[1];
                }
                console.log(requestObject);
                response.end(chunk);
            });
            console.log(request);
            response.end("POST");
        }*/
    }).listen(config.server.port);
