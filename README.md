#Environment Settings#
ubuntu 14.04
software: nginx > 1.10 , git, php, nodejs, npm, pm2, logrotate, mysql, redis

to replicate website:
install above software; then copy following data over to new server
database(mysql+redis),
offline scripts/crontab
nginx conf (add another access log for uv.log),
nginx vhosts,
log rotate conf,

set up: 
mailutils;
sudo apt-get update;
sudo apt-get install mailutils;
Fully Qualified Domain Name (FQDN): 'liunar.com';
sudo nano /etc/postfix/main.cf->myhostname = 'liunar.com';

**special considerations**
/var/log/nginx might be off limits, make sure all log files belong to www-data:adm;
add nopassword to visudo.tmp to make sure cronscripts can run

#Convert JS Files#
1. cd "/www/tasks" folder
2. run "gulp ng-js"
3. run "gulp scripts"