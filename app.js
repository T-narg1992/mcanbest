var express = require('express');
var db = require('./db');
var app = express();
var config = require('./config')['settings'];
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser');
app.use(cookieParser('shhhh, very secret'));
var session = require('express-session');
const wechatConfig = require("./config/wechatConfig");
const wechat_url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${wechatConfig.appid}&redirect_uri=http%3A%2F%2Fwww.liunar.com%2Fmission%2Fwechat_auth&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect`;
console.log(config);
app.set('secret', config.secret); // secret variable

app.use(session({
  secret: 'nginxGoneCrazy',
  resave: false,
  saveUninitialized: true,
}))

var discover = require('./routes/discover');
var tags = require('./routes/tags');
var survey = require('./routes/survey');
var pastcases = require('./routes/pastcases');
var products = require('./routes/products');
var products_new = require('./routes/product_new');
var articles = require('./routes/articles');
var questions = require('./routes/questions');
var users = require('./routes/users');
var school = require('./routes/school');
var posts = require('./routes/posts');
var images = require('./routes/images');
var comments = require('./routes/comments');
var routesApi = require('./routes/index');
var web = require('./webRoutes/index');
var likes = require('./routes/likes');
var stats = require('./routes/stats');
var square = require('./routes/square');
var picture_gernate = require("./routes/picturegernate");
var pay = require("./routes/pay");

//app.use(express.static('views'));
var engines = require('consolidate');
app.use(express.static('www'));
app.use('/img', express.static("www/img"));
app.use('/public', express.static("www/public"));
app.use('/uploads',express.static('uploads'));
app.use(express.static('wwwtest'));
app.use('/gernates/posters', express.static('gernates/posters'));
app.use('/posterGernate/assets/imgs', express.static('posterGernate/assets/imgs'))

app.set('views', __dirname + '/views');
//app.engine('html', engines.mustache);
app.set('view engine', 'ejs');

app.use(function (err, req, res, next) {
    console.log("app err:" + err.code);
    if (err.code === 'LIMIT_FILE_SIZE') {
        res.status(200);
        res.json({ err: 1001 });
        return;
    }

    // Handle any other errors
})
// Add headers
app.use(function (req, res, next) {
    var allowedOrigins = ["http://www.liunar.com","https://www.liunar.com","http://shop.liunar.com","https://shop.liunar.com","http://toutiao.liunar.com","https://toutiao.liunar.com", "http://yun.liunar.com", "http://localhost:59416"];
    var origin = req.headers.origin || 'http://www.liunar.com';
    // Website you wish to allow to connect
    
    if(allowedOrigins.indexOf(origin) > -1){
     res.setHeader('Access-Control-Allow-Origin', origin);
 }
    //res.setHeader('Access-Control-Allow-Origin', origin);

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,token');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    var salt = 'grantisawesome';

    var cookie = req.cookies.pv;
    if (cookie == undefined)
    {
        res.cookie('pv',  Date.now() + salt,{ path:'/', domain: config.cookie_domain});
    }
    // Pass to next layer of middleware
    return next();
});



function checkIfAuthNeeded(req, res, next) {
    //protected routes
    authRoutesArray=['/comments', '/square/post', '/square/delete', '/questions']; 
    regexRoutes=['\/schools\/[0-9]*\/reviews', '\/reviews\/[0-9]*','\/square\/post\/qr\/[0-9]*','\/usercenter\/user\/upload\/[0-9]*\/[0-9]*'];
    if(authRoutesArray.indexOf(req.path) == -1){ 
        return next();
    }
    if(['POST', 'DELETE','PUT'].indexOf(req.method) == -1 ){
        return next();
    }

    var token = req.headers['token'];
    if (token) {
        jwt.verify(token, app.get('secret'), function (err, decoded) {
            if (err) {
                console.log(err);
                return res.json({ success: false, message: 'Failed to authenticate token.' });    
            } else {
                req.decoded = decoded;
                console.log(decoded);
                return next();
            }
        });
    } else {
     return res.redirect(wechat_url);
 }
    // }
}
app.use(checkIfAuthNeeded);


// app.use(function (req, res, next) {
//   // check if client sent cookie
//   var cookie = req.cookies.LOG;
//   if (cookie === undefined)
//   {
//     // no: set a new cookie
//     var random=Math.random().toString() + Date.now();
//     res.cookie('cookieName',random, { maxAge: 31536000, httpOnly: true });// set to persist for 1 year
//     console.log('cookie created successfully');
//   } 
//   else
//   {
//     // yes, cookie was already present 
//     console.log('cookie exists', cookie);
//   } 
//   next(); 
// });

var middleware = require('wechat-pay').middleware;
var payConfig = require("./config/wechatPayConfig").liunarPayConfig;
var recordPayment = require("./controllers/recordPayment");

app.use("/paycallback", middleware(payConfig).getNotify().done(function(message, req, res, next) {
    var openid = message.openid;
    var order_id = message.out_trade_no;
    var attach = {};
    attach = JSON.parse(message.attach);
    console.log("paycall middle message:" + message);
    if(message.result_code == "SUCCESS" && (attach.type == "DP" || attach.type == "FP" || attach.type == "GP")){

       var reVal = recordPayment.wxPayDownPayment(
            message.total_fee, 
            message.out_trade_no,
            message.transaction_id, 
            message.time_end, 
            message.openid, 
            message.fee_type, 
            attach.type == "FP" || attach.type == "DP" || attach.type == "GP" ? 5 : 3,
            attach.o_id
            );
        console.log(reVal);
        //res.redirect('http://www.liunar.com/testpay/paycallback?v='+reVal)
            console.log("paycall middle message cc: -reval");
    }
  /**
   * 查询订单，在自己系统里把订单标为已处理
   * 如果订单之前已经处理过了直接返回成功
   */
   res.reply('success');

  /**
   * 有错误返回错误，不然微信会在一段时间里以一定频次请求你
   * res.reply(new Error('...'))
   */
}));

app.use(bodyParser.json({ limit: '50mb' })); // support json encoded bodies
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 })); // support encoded bodi
//var jsonParser = bodyParser.json({ limit: 1024 * 1024 * 20, type: 'application/json' });
//var urlencodedParser = bodyParser.urlencoded({ extended: true, limit: 1024 * 1024 * 20, type: 'application/x-www-form-urlencoding', parameterLimit: 50000 })

//app.use(jsonParser);
//app.use(urlencodedParser);

app.use('/', routesApi);
app.use('/usercenter', users);
app.use('/questions', questions);
app.use('/discover', discover);
app.use('/posts', posts);
app.use('/images',images);
app.use('/tags',tags);
app.use('/products', products);
app.use('/pastcases', pastcases);
app.use('/articles', articles);
app.use('/comments', comments);
app.use('/web', web);
app.use('/school', school);
app.use('/productnew', products_new);
app.use('/survey', survey);
app.use('/likes', likes);
app.use('/statistics', stats);
app.use('/square', square);
app.use('/createpng', picture_gernate.router);
app.use('/pay', pay);

db.connect(true, function(err) {
    if (err) {
        console.log("Error connecting database ... nn");
        process.exit(1);
    } else {
        app.listen(8080,'127.0.0.1', function() { //+ localhost as 2nd param for local binding only
            console.log('server engaged on 8080');
        });
    }
});

module.exports = app;