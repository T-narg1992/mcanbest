'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var http = require('http');
var url = require('url');
const async = require('async');
const isMobile = require('../utils/detectMobile').isMobile;

var getHomePromo = function(callback){
    db.get().pool.query(
        "SELECT * from placeholder WHERE promo_group = 0 ORDER BY promo_position desc LIMIT 6",
        function (err, rows, fields) {
            if (err) callback(err, null);
            //console.log("line 21:",typeof rows, rows instanceof Array, rows.length);
            callback(null, {
                slider: rows[0].content_html,
                qandA: rows[4].content_html,
                banner: rows[2].content_html
            });
        }
        );
};

var getHomeActivities = function(callback){
    db.get().pool.query(
        "SELECT special_event, effective_time, expire_time, create_time, update_time, promo_thumb, have_thumbnail, title, view_count, id, thumbnail FROM article WHERE special_event = 1 and site =0 ORDER BY update_time DESC LIMIT 4",
        function(err, result){
            if (err) callback(err, null);
            callback(null, result);
        }
        );
};

var getChildActivities = function(callback){
    if(!this.params.child){
        callback(null, []);
        return;
    }
    //where ag.location = ?
    db.get().pool.query(
        "select ag.location, a.special_event, a.effective_time, a.expire_time, a.create_time, a.update_time, a.promo_thumb, a.have_thumbnail, a.title, a.view_count, a.id, a.thumbnail from agency_event  a_e join article a on a_e.event_id = a.id join agencies ag on  a_e.agency_id = ag.id where ag.location = ? order by a.expire_time desc limit 4", [this.params.child],
        function(err, result){
            if (err) callback(err, null);
            callback(null, result);
        }
        );
};


var getPromoArticle = function(callback){
    db.get().pool.query("SELECT * FROM article WHERE promo_position is not null and channel < 3 and special_event = 0 and (site <> 1 or site is null) ORDER BY promo_position desc, create_time desc LIMIT 4",
        function (err, rows, fields) {
            if (err) callback(err, null);
            callback(null, rows);
        });
};

var getHomeProducts = function (callback) {
    var list0 = [], list1 = [];
    var master = [];
    db.get().pool.query(
        "select id, title,type, country,msrp, price,age_min,age_max,currency,start_date,promo_thumb, promo_group from product_new where promo_group in (0,1,2,3,4) order by promo_position desc",
        function (err, item) {
            if (err) callback(err, null);
            for (var i = 0; i < item.length; i++) {
                if (item[i]['promo_group'] == 0 && list0.length < 4) {
                    list0.push(item[i]);
                } else if (item[i]['promo_group'] == 1 && list1.length < 4) {
                    list1.push(item[i]);
                }
            }
            master.push(list0);
            master.push(list1);
            callback(null, master);
        }
        );
}

var fetchDatas = function (req, res) {
    var getRegionActivities;
    var region =  typeof req.params.child == 'undefined' ? '' : req.params.child;
    getRegionActivities = getChildActivities.bind(req);

    region = typeof req.params.child  == 'undefined'? "":req.params.child;
    async.parallel(
        [
        getHomePromo,
        getHomeActivities,
        getRegionActivities,
        getPromoArticle,
        getHomeProducts
        ],
        function(err, result){
            if(err) throw err;
            var not_expire = [];
            result[1].forEach(
                function(item){
                    if(not_expire.length <= 4){
                        if(item.expire_time * 1000 > new Date().getTime()){
                            not_expire.push(item);
                        }
                    }
            })
            result[2].forEach(
                function(item){
                    if(not_expire.length <= 4){
                        if(item.expire_time * 1000 > new Date().getTime()){
                            not_expire.push(item);
                        }
                    }
            })
            var renderObj = {
                slider: result[0].slider,
                qandA: result[0].qandA,
                banner: result[0].banner,
                activities: not_expire,
                articles: result[3],
                products: result[4],
                region: region 
            };
            if(isMobile(req.headers['user-agent']) === true){
                res.render("home/home", renderObj);
            }else{
                res.render("home/d_home", renderObj);
            }
        }
        );   
    
};


module.exports = {
    fetchDatas: fetchDatas
}