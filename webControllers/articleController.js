'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var http = require('http');
var url = require('url');
const isMobile = require('../utils/detectMobile').isMobile;



var getUid = function(refer_code, callback){
    db.get().pool.query(
        "SELECT id, headerImage, footerImage, headerHtml, footerHtml, publisher FROM member WHERE reference_code = ? AND publisher IS NOT NULL",
        [parseInt(refer_code)],
        function(err, data){
            if(err) throw err;
            callback(data[0]);
        }
        )
};

var getReferCode = function(id){
    db.get().pool.query(
        "SELECT reference_code FROM member where id = ? AND publisher IS NOT NULL",
        [parseInt(id)],
        function(err, data){
            console.log(data);
            //return data[0];
        }
        )
};


var readOne = function (req, res) {
    if(isMobile(req.headers['user-agent']) === true){
        var showBaidu = "false";
    }else{
        var showBaidu = "true";
    }
    var articleQuery = "";
    var articleParams = "";
    var isLiunarSystem = false; 

    if (req.params.id && req.params.refercode && parseInt(req.params.refercode) === 0) {
        articleQuery = "SELECT * FROM article WHERE article.id = ?";
        articleParams = [parseInt(req.params.id)];
        isLiunarSystem = true;
    }
    getUid(req.params.refercode, function(user){
        if(user && user.id){
            articleQuery = "SELECT * FROM article WHERE article.id = ? AND article.uid = ?";
            articleParams = [parseInt(req.params.id), parseInt(user.id)];
        }
        db.get().pool.query(articleQuery, articleParams,
            function (err, item) {
                if (err) throw err;
                var taskId = req.params.taskid || '';
                if(item.length){
                    var promoPrdList=[];
                    var queryPrds="";
                    var p = [];
                    if(item[0].prd_or_customjson == 'prd'){
                        queryPrds ="SELECT id, msrp,currency, title, country, age_min, age_max from product_new where id = ?";
                        p = [item[0].recommended_prd];
                        if(item[0].prd_or_customjson2 =='prd2'){
                            queryPrds+= ' or id = ?';
                            p.push(item[0].recommended_prd2);
                        }
                    }
                    db.get().pool.query(queryPrds ,p, function(err, prd){
                        if(prd){
                            for (var i = 0; i< prd.length; i++) {
                                var prd_obj = {};
                                prd_obj.msrp = prd[i].msrp;
                                prd_obj.age_min = prd[i].age_min;
                                prd_obj.age_max = prd[i].age_max ;
                                prd_obj.country = prd[i].country;
                                prd_obj.currency = prd[i].currency;
                                prd_obj.title =  prd[i].title;
                                prd_obj.url = "http://shop.liunar.com/"+ prd[i].id;
                                prd_obj.absPathCover = "http://cms.liunar.com/uploadimage/products/"+prd[i].id+".jpg";
                                promoPrdList.push(prd_obj);
                            }
                        }
                        function processRenderData(callbackResult, isLiunar){

                            if(isLiunar){
                              callbackResult[0].source = "留哪儿热点";
                              callbackResult[0].taskid = taskId;
                              callbackResult[0].img = "http://cms.liunar.com/uploadimage/articles/" + item[0].promo_thumb;
                          }else {
                              callbackResult[0].source = user.publisher;
                              callbackResult[0].header = user.headerHtml;
                              callbackResult[0].footer = user.footerHtml;
                              callbackResult[0].img ="http://yun.liunar.com/uploadimage/articles/" + item[0].thumbnail;
                          }
                          return {
                              title: callbackResult[0].title,
                              img: callbackResult[0].img,
                              date: callbackResult[0].update_time,
                              source: callbackResult[0].source,
                              data: callbackResult[0].content_html,
                              id: callbackResult[0].id,
                              tags: callbackResult[0].tags,
                              taskid: callbackResult[0].taskid, 
                              header: callbackResult[0].header,
                              footer: callbackResult[0].footer,
                              desc: callbackResult[0].description,
                              views: callbackResult[0].view_count,
                              refercode: req.params.refercode,
                              haveRecommend: callbackResult[0].prd_or_customjson,
                              promoPrdList: promoPrdList,
                              source_url:callbackResult[0].source_url,
                              showBaidu: showBaidu
                          }
                      }
                      var renderObj = processRenderData(item, isLiunarSystem);
                      if(isLiunarSystem){
                          res.render('article/article_detail', renderObj);
                      }else{
                        res.render('article/user_article_detail', renderObj);
                      }
                  })
                }else{
                    res.sendStatus(404);
                }
            });
});
}
var comment = function(req, res){
    if (req.params.id && req.params.refercode) {
        db.get().pool.query(
            "SELECT title, id FROM article WHERE article.id = ?",
            [parseInt(req.params.id)],
            function (err, item) {
                if (err) throw err;
                res.render('article/article_comment',{
                    title: item[0].title,
                    id: item[0].id,
                    refercode: req.params.refercode
                })
            }
            );
    }else{
        res.render('article/article_comment', {
            title: '找不到该文章',
            id: "无效id"
        });
    }
};

var testOne = function (req, res) {
    db.get().pool.query(
        "SELECT * FROM article WHERE article.id = 34888",
        function (err, item) {
            if (err) throw err;
            //console.log(item);
            var taskId = req.params.taskid || '';
            if (item.length) {
                res.render('article/article_detail_test', {
                    title: item[0].title,
                    date: item[0].update_time,
                    source: "留哪儿热点",
                    data: item[0].content_html,
                    id: item[0].id,
                    tags: item[0].tags,
                    taskid: taskId,
                    desc: item[0].description,
                    img: item[0].have_thumbnail == 1 ? "http://www.liunar.com" + item[0].thumbnail : "http://cms.liunar.com/uploadimage/articles/" + item[0].promo_thumb,
                    views: item[0].view_count,
                    refercode: req.params.refercode
                });
            } else {
                res.sendStatus(404);
            }
        }
        );
};

var getArticleByUser = function(uid, callback){
    db.get().pool.query(
        "SELECT create_time, update_time, description, promo_thumb, have_thumbnail, title,view_count, id,thumbnail FROM article WHERE special_event = 0 AND uid = ? AND publish_status = 0 ORDER BY create_time DESC LIMIT 0, 15",
        [parseInt(uid)],
        function(err, data){
            if(err) throw err;
            callback(data);
        }
        )
};

var getList = function(req, res){
    console.log(req.params, req.params.refercode == '0', !req.params.refercode);
    if(!req.params.refercode || req.params.refercode == '0'){
        console.log("not 达人");
        db.get().pool.query(
            "SELECT uid, site, create_time, update_time, description, promo_thumb, have_thumbnail, title,view_count, id, thumbnail from article WHERE publish_status = 0 AND special_event = 0 AND ((site <> 1 OR site IS NULL) OR ((site = 1 OR site IS NULL) and status = 1 and submit_for_toutiao = 0)) ORDER BY promo_position desc, create_time desc limit 0, 15",
            function(err, data){
                if (err) throw err;
                var renderObj = {
                    title: "留哪儿热点",
                    articles: data,
                    refercode: 0,
                    getCode: getReferCode
                };
                if(isMobile(req.headers['user-agent']) === true){
                    res.render('article/article_list', renderObj);
                }else{
                    res.render('article/d_article_list', renderObj);
                }
                // res.render('article/article_list', renderObj);
            }
            );
    }else if(req.params.refercode && req.params.refercode != '0'){
        getUid(req.params.refercode, function(user){
            if(user && user.id){
                getArticleByUser(user.id, function(list){
                    var obj = {
                        title: user.publisher,
                        articles: list,
                        refercode: req.params.refercode,
                    };
                    if(isMobile(req.headers['user-agent']) === true){
                        res.render('article/user_article_list', obj);
                    }else{
                        res.render('article/user_article_list', obj);
                    }
                });
            }else{
                res.send("没有refercode");
            }
        })
    }
};

module.exports = {
    readOne: readOne,
    comment: comment,
    getList: getList,
    testOne: testOne
}
