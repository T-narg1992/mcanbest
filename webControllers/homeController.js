﻿'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');
var listByName = function (req, res) {
    res.status(200);
    var schools = [
        { "name": "John", "city": "Doe" },
        { "name": "Anna", "city": "Smith" },
        { "name": "Peter", "city": "Jones" }
    ];
    res.json(schools);
};

var readOne = function (req, res) {

    if (req.params && req.params.id) {
        db.get().pool.query(
            "SELECT * FROM school WHERE id = ?",
            [req.params.id],
            function (err, result) {
                if (err) throw err;
                if (result.length > 0) {
                    res.render('schools/index', {
                        title: table.defaultTitle + result[0].ch_name + " " + result[0].en_name,
                        schoolName: result[0].ch_name + result[0].en_name,
                        schoolId: result[0].id
                    });
                }
            }
        );
    } else {
        res.status(404);
        res.render('schools/index.html', {
            title: 'Articles',
        });
    }
};

var list = function (req, res) {
    var parseUrl = url.parse(req.url, true);
    var typeId = parseUrl.query.type_id;
    if (typeId) {
        db.get().pool.query(
            "SELECT * FROM school WHERE type = ? ORDER BY national_rank",
            [typeId],
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    } else {
        db.get().pool.query(
            "SELECT * FROM school ORDER BY national_rank",
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    }
};

var get = function (req, res) {

    //router.post('/wechatSign', function (req, res) {
    //    //var url = req.protocol + '://' + req.host + req.path;
    //    var url = req.body.url; //获取当前url
    //    console.log("get url->:" + url);
    //});
    db.get().pool.query(
        "SELECT * FROM member",
        function (err, result) {
            if (err) throw err;

            //res.status(200);
            //res.json(signatureMap);
            if (result.length > 0) {
                var shapeObje = {
                    title: 'here is title from js sdk',
                    desc: 'I put any desc here',
                    link: 'http://api.liunar.net/web/agents',
                    imgUrl: 'http://www.liunar.net/img/hwyx.png',
                }
                res.render('home/index', {
                    title: "用户列表",
                    list: result,

                });
            } else {
                res.render('home/index', {
                    title: "sss",
                });
            }


        }
    );

    /*
    var parseUrl = url.parse(req.url, true);
    var typeId = parseUrl.query.type_id;
    res.render('agent/index', {
        title: "asdfasd",
    });
    /*
    if (typeId) {
        db.get().pool.query(
            "SELECT * FROM school WHERE type = ? ORDER BY national_rank",
            [typeId],
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    } else {
        db.get().pool.query(
            "SELECT * FROM school ORDER BY national_rank",
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    }*/
};

module.exports = {
    listByName: listByName,
    readOne: readOne,
    list: list,
    get: get
}