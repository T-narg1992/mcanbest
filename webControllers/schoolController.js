﻿'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');
const isMobile = require('../utils/detectMobile').isMobile;

var readOne = function (req, res) {
    if (req.params && req.params.id) {
        db.get().pool.query(
            "SELECT * FROM school_new WHERE id = ?",
            [req.params.id],
            function (err, result) {
                if (err) throw err;
                if (result.length > 0) {
                    if(isMobile(req.headers['user-agent']) === true){
                        res.render('schools/school_detail', {
                            title: result[0].ch_name == "" ? result[0].en_name : result[0].ch_name,
                            schoolName: result[0].ch_name + result[0].en_name,
                            schoolId: result[0].id,
                            school: result[0]
                        });
                    }else{
                        res.render('schools/d_school_detail', {
                            title: result[0].ch_name == "" ? result[0].en_name : result[0].ch_name,
                            schoolName: result[0].ch_name + result[0].en_name,
                            schoolId: result[0].id,
                            school: result[0]
                        });
                    }
                }else{
                    res.send("找不到学校");
                }
            }
            );
    } else {
        res.sendStatus(404);
        /*res.render('schools/index.html', {
            title: 'school',
        });*/
    }
};
var getListUS = function(req,res){
 var reqToQuery = function (requestUrl) {
     var params = [];
     var query = "SELECT id,ch_name,en_name,tag, rank, state, description FROM us_schools where rank is not null";
     var urlObj = url.parse(requestUrl, true);

     query+= " order by rank asc limit 0, 10";
     return {query: query, params : params}
 }
 var query =reqToQuery(req.url);
 db.get().pool.query(
    query.query,
    query.params,
    function (err, result) {
        if (err) throw err;
        if(isMobile(req.headers['user-agent']) === true){
            res.render('schools/US/list', {schools:result});
        }else{
           res.render("schools/US/d_list",{
            schools: result
        });
       }
   });   
};
var getListApplySquareSchools = function(req,resp){
     var reqToQuery = function (requestUrl) {
     var params = [];
     var query = "SELECT intro, id,ch_name,en_name,tag,"+escape(req.params.ranking)+", country FROM apply_square_schools  where "+req.params.ranking+" is not null";
     if(req.params.country){
        query += ' and countryCode = ?';
        params.push(req.params.country);
     }
     query+= " order by "+escape(req.params.ranking)+" asc";
     return {query: query, params : params}
 }
 var query = reqToQuery(req.url);
 db.get().pool.query(
    query.query,
    query.params,
    function (err, result) {
        if (err) resp.send('出错了！', 404);
            if(isMobile(req.headers['user-agent']) === true){
            resp.render('schools/applysquare/list', 
                {
                    schools:result,
                    type: 5,
                    ranktype: req.params.ranking
                });
        }else{
           resp.render("schools/applysquare/d_list",
           {
            schools: result,
            type: 5,
            ranktype: req.params.ranking
        });
       }
   })
};

var readOneApplySquare = function (req, res) {
    if (req.params.id) {
        db.get().pool.query(
            "SELECT * FROM apply_square_schools WHERE id = ?",
            [req.params.id],
            function (err, result) {
                if (err) throw err;
                var json;
                if (result.length > 0) {
                    if(isMobile(req.headers['user-agent']) === true){
                        res.render('schools/applysquare/details', {
                            school:result[0],
                            err:null,
                            isMobile:'yes'
                        });
                    }else{
                         res.render('schools/applysquare/d_details', {
                            school:result[0],
                            err:null,
                            isMobile:'no'
                        });
                    }
                }
            }
            );
    } else {
        res.render('schools/US/details', {
            err: 'Not found',
        });
    }
};
var readOneUS = function (req, res) {
    if (req.params.id) {
        db.get().pool.query(
            "SELECT ch_name,en_name, id, tag, rank, description, state ,json_content FROM us_schools WHERE id = ?",
            [req.params.id],
            function (err, result) {
                if (err) throw err;
                var json;
                if (result.length > 0) {
                    if(result[0].json_content){
                        json = result[0].json_content
                        json = JSON.parse(json);
                    }
                    if(isMobile(req.headers['user-agent']) === true){
                        res.render('schools/US/details', {
                            title:result[0].ch_name + " " + result[0].en_name,
                            name: result[0].ch_name +'/'+ result[0].en_name,
                            id: result[0].id,
                            rank: result[0].rank,
                            tag: result[0].tag,
                            json_content: json,
                            cover: result[0]['tag']+'-cover.jpg',
                            description: result[0]['description'],
                            err:null
                        });
                    }else{
                         res.render('schools/US/d_details', {
                            school:result[0],
                            title:result[0].ch_name + " " + result[0].en_name,
                            name: result[0].ch_name +'/'+ result[0].en_name,
                            id: result[0].id,
                            rank: result[0].rank,
                            tag: result[0].tag,
                            json_content: json,
                            cover: result[0]['tag']+'-cover.jpg',
                            description: result[0]['description'],
                            err:null
                        });
                    }
                }else{
                    res.render('schools/US/details', {
                        err: 'Not found',
                    });
                }
            }
            );
    } else {
        res.render('schools/US/details', {
            err: 'Not found',
        });
    }
};

var list = function (req, res) {
    if(!req.params.type){
        res.sendStatus(404);
    }
    var filterOn;
    var reqToQuery = function (request) {
        var sharetitle ="加拿大";
       var params = [];
       var query = "SELECT en_name, ch_name, national_rank,id,detail_info, city,isPublic,school_category, five_year_rank FROM school_new";
       var urlObj = url.parse(request.url, true);
       params.push(parseInt(req.params.type));
       query += " where type = ? ";
       if ( req.params.type != '1' && req.params.type !='2'){ //not 特色，大学
      
        if(urlObj.query.isPublic){
            params.push(parseInt(urlObj.query.isPublic));
            query += " and isPublic = ? ";
        } 
        if(request.params.province){
            params.push(request.params.province);
            query += " and province = ? ";
            sharetitle += (request.params.province+'省');
        }else{
            res.send("中小学必须有省");
        } 
        if(urlObj.query.city){
            params.push(urlObj.query.city);
            query += " and city = ? ";
            sharetitle += (urlObj.query.city+'市');
        }
          if(req.params.type == '3'){
            sharetitle += '中学排名';
        }else if(req.params.type == '4'){
            sharetitle += '小学排名';
        }
        filterOn = 'true';
    }else{
        if(req.params.type == '1'){
            sharetitle += "大学排名";
        }else if(req.params.type != '2'){
            sharetitle += "特色学院排名";
        }
        filterOn = 'false';
    }
    query+= "order by cast(substring_index(national_rank,'/',1) as unsigned) limit 10";
    return {query: query, params : params, sharetitle:sharetitle}
}
var queryObj = reqToQuery(req);

db.get().pool.query(queryObj.query, queryObj.params,function(err, results){
    if (err){ 
        throw (err)
    }else{
        if(results.length){
            if(isMobile(req.headers['user-agent']) === true){
                res.render("schools/school_fliter_list",{
                    schools: results,
                    city: req.query.city ? req.query.city : "all",
                    isPublic: req.query.isPublic ? req.query.isPublic : "all",
                    province: req.params.province ? req.params.province : '',
                    type: parseInt(req.params.type),
                    filterOn:filterOn,
                    sharetitle:queryObj.sharetitle
                });
            }else{
                res.render("schools/d_school_fliter_list",{
                    schools: results,
                    city: req.query.city ? req.query.city : "all",
                    isPublic: req.query.isPublic ? req.query.isPublic : "all",
                    province: req.params.province ? req.params.province : '',
                    type: parseInt(req.params.type),
                    filterOn:filterOn
                });
            }
        }else{
            if(isMobile(req.headers['user-agent']) === true){
                res.render("schools/school_fliter_list",{
                    schools: [],
                    city: req.query.city ? req.query.city : "all",
                    isPublic: req.query.isPublic ? req.query.isPublic : "all",
                    province: req.params.province ? req.params.province : '',
                    type: parseInt(req.params.type),
                    filterOn:filterOn,
                    sharetitle:queryObj.sharetitle
                });
            }else{
                res.render("schools/d_school_fliter_list",{
                    schools: [],
                    city: req.query.city ? req.query.city : "all",
                    isPublic: req.query.isPublic ? req.query.isPublic : "all",
                    province: req.params.province ? req.params.province : '',
                    type: parseInt(req.params.type),
                    filterOn:filterOn
                });
            }
        }
    }
}
);

};

var cate = function(req, res){
    if(isMobile(req.headers['user-agent']) === true){
        res.render('schools/home',{

        })
    }else{
        db.get().pool.query(
            "SELECT en_name, ch_name, national_rank,id,detail_info, city,isPublic,school_category, five_year_rank FROM school_new WHERE type = 1 ORDER BY cast(substring_index(national_rank,'/',1) as unsigned) limit 10",
            function(err, data){
                if(err) res.send(404);
                res.render("schools/d_school_fliter_list", {
                    schools: data,
                    city: req.query.city ? req.query.city : "all",
                    isPublic: req.query.isPublic ? req.query.isPublic : "all",
                    province: req.params.province ? req.params.province : '',
                    type: 1,
                    title: "留哪儿院校库",
                    filterOn: "false"
                })
            }
            )
    }
};

module.exports = {
    readOne : readOne,
    list : list,
    readOneUS:readOneUS,
    getListUS:getListUS,
    cate: cate,
    appSqList: getListApplySquareSchools,
    appSqOne: readOneApplySquare,
}