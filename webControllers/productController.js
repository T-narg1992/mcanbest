'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var http = require('http');
var url = require('url');
const async = require('async');
var request = require('request');
const isMobile = require('../utils/detectMobile').isMobile;
var pdf = require('html-pdf');


var product_map = { 0: "冬夏令营", 1: "背景提升", 2: "中小学", 3: "特色学院", 4: "知名大学", 5: "团购产品" };

var localGetList = function (category, callback) {
    db.get().pool.query(
        "select id,description,title,duration, currency, period,tags,country, price ,age_min,age_max, promo_thumb,promo_position from product_new where type = ? order by promo_position desc limit 6 ",
        product_map[category],
        function (err, data) {
            if (err) throw err;
            var msg;
            if (data == null) {
                msg = "No products available";
            } else {
                msg = null;
            }
            callback(data, msg);
        });
};
var home = function (req, res) {

    if (isMobile(req.headers['user-agent']) === true) {
        var list0 = [], list1 = [], list2 = [], list3 = [], list4 = [], list5 = []; // see product map above
        var master = [];
        db.get().pool.query("select id, title,type, country,msrp, deposit, group_purchase_msrp , price,age_min,age_max,currency,start_date,promo_thumb, promo_group from product_new where promo_group in (0,1,2,3,4, 5) order by promo_position desc",
            function (err, item) {
                if (err) throw err;
                for (var i = 0; i < item.length; i++) {
                    if (item[i]['promo_group'] == 0 && list0.length < 4) {
                        list0.push(item[i]);
                    } else if (item[i]['promo_group'] == 1 && list1.length < 4) {
                        list1.push(item[i]);
                    } else if (item[i]['promo_group'] == 2 && list2.length < 4) {
                        list2.push(item[i]);
                    } else if (item[i]['promo_group'] == 3 && list3.length < 4) {
                        list3.push(item[i]);
                    } else if (item[i]['promo_group'] == 4 && list4.length < 4) {
                        list4.push(item[i]);
                    } else if (item[i]['promo_group'] == 5 && list5.length < 4) {
                        list5.push(item[i]);
                    }
                }
                master.push(list0);
                master.push(list1);
                master.push(list2);
                master.push(list3);
                master.push(list4);
                if(req.query.test == 'gaopin_list'){
                    master.push(list5);
                }
                var region = typeof req.params.child  == 'undefined'? "":req.params.child;
                if(req.params.child){
                    db.get().pool.query("select a.location, p.id, p.title, p.type, p.country,p.msrp, p.price, p.age_min, p.age_max, p.currency, p.start_date, p.promo_thumb from agency_prds apf join agencies a on apf.agency_id = a.id join product_new p on apf.product_id = p.id where location = ?", req.params.child , function(err, data){
                       res.render("products/home", {
                        promotion: data,
                        list: master,
                        region: region
                    });
                   })
                }else{
                    res.render("products/home", {
                        list: master,
                        promotion: '',
                        region: region,
                    });
                }


            });

    } else {
        localGetList(0, function (data, msg) {
            if(req.query.fromyun && req.query.fromyun == "true"){
                res.render("products/yun_list", {
                    title: "留哪儿精选",
                    products: data,
                    msg: msg,
                    category: 0,
                    categoryStr: product_map[0]
                });
            }else{
                res.render("products/d_shop_list", {
                    title: "留哪儿精选",
                    products: data,
                    msg: msg,
                    category: 0,
                    categoryStr: product_map[0]
                });
            }
        })
    }
};

var pdfPage = function (req,res) {
 db.get().pool.query("SELECT * FROM product_new WHERE product_new.id = ?",
    [parseInt(req.params.id)],
    function (err, item) {
        if (err) throw err;
        var matches;
        if(item[0]){
            matches = item[0].body_tab1.match(/http:\/\/cms.liunar.com\/ueditor\/net\/upload\/image\/[0-9]*\/[0-9]*.png/gi);
            matches = matches.slice(0,5); //first 5 pictures
        }

        res.render('products/pdf', {
            headerImages: matches,
            title:item[0].title,
            theme: item[0].theme
        },
        function(err,html){
            var options = {
                "format": "Letter",    
                "orientation": "portrait",
                "height": "11in",  
                "width": "8.5in"
            }
            pdf.create(html, options).toFile('/public/test1.pdf', function(err, data) {
              if (err){
                console.log(err);
                throw err;
            } 
            res.send(html);
        });
        }
        )
    });
};

var readTest = function(req, res){
    db.get().pool.query(
        "SELECT * FROM product_new WHERE product_new.id = 99",
        [parseInt(req.params.id)],
        function (err, item) {
            if (err) throw err;
            var itenary = JSON.parse(item[0].multiple_itenary);
            var option = 0;
            var schedule = 0;
            if (itenary) {
                option = itenary.options;
                schedule = itenary.schedules
            }
            item[0].pastcase = null;
            if (isMobile(req.headers['user-agent']) === true){
                res.render('products/prd_test', {
                    prd: item[0],
                    options: option,
                    schedules: schedule,
                    img: "http://cms.liunar.com/uploadimage/products/" + item[0].promo_thumb
                });
            }else{
                res.render('products/d_prd_test', {
                    prd: item[0],
                    options: option,
                    schedules: schedule,
                    img: "http://cms.liunar.com/uploadimage/products/" + item[0].promo_thumb
                });
            }
        }
        );
};

var readOne = function (req, res) {
    console.log("issub:" + req.session.isSubscribe);
    if (!req.session.isSubscribe) {
        req.session.isSubscribe = false;
        var id = 0;
        var gid = 0;
        if (req.params.id) {
            id = req.params.id;
        }

        if (req.params.group_id) {
            gid = req.params.group_id;
        }

    }
    if(req.session.isVisit) {
        req.session.isVisit++;
    } else {
        req.session.isVisit = 1;
    }
    if ((!req.cookies.isSubscribe || req.cookies.isSubscribe !== '1') && req.params && req.params.pay){
        res.redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxca0568e7f29d0e45&redirect_uri=http%3A%2F%2Fwww.liunar.com%2Fusercenter%2Fwechat_auth_pay%2F' + id + '%2F' + gid + '%2F' + '&payType='+payType+'&response_type=code&scope=snsapi_base&state=wxlogin&connect_redirect=1#wechat_redirect');
    }
    async.waterfall([
        function (callback) {
            if (req.params && req.params.id && req.params.group_id) {
                db.get().pool.query("select * from (SELECT p.*, gm.product_id, gm.id as gm_id, gm.group_id as grouphash, gm.created_at, m.id as uid,m.nickname, m.fullname, m.open_id from group_members gm join member m on gm.open_id = m.open_id join product_new p on p.id = gm.product_id WHERE p.id = ? and gm.id = ?) a join (select count(group_id) as currentCount, group_id from group_members group by group_id) b on a.grouphash = b.group_id;",
                    [parseInt(req.params.id), req.params.group_id],
                    function (err, item) {
                        if (err) throw err;
                        if(!item[0]){
                            callback("no group and product combination found", null);

                        }else{
                            console.log('here');
                            item[0].spaces_left = item[0].group_size - item[0].currentCount;
                            db.get().pool.query("select m.avatar from member m join group_members gm on m.open_id = gm.open_id where group_id= ? order by created_at asc ", [item[0].grouphash], function(err, avatars){
                                item[0].avatars = avatars;
                                callback(null, item[0]);
                            })
                        }
                    });
            }else if(req.params && req.params.id){
             db.get().pool.query("SELECT * FROM product_new WHERE product_new.id = ?",
                [parseInt(req.params.id), req.params.group_id],
                function (err, item) {
                    if (err) throw err;
                    if(null){
                        callback("no group and product combination found", null);
                    }
                    callback(null, item[0]);
                });
         }else{
            callback("no product found with id", null);
        }
    },
    function (item, callback) {
        if (item['pastcase'] != null) {
            db.get().pool.query("SELECT id, authorid, type ,title, view_count, have_thumbnail, promo_thumb FROM article WHERE article.id IN (?)",
                [item['pastcase'].split(',')],
                function (err, articles) {
                    if (err) throw err;
                    item['pastcase'] = articles;
                    callback(null, item);
                });
        } else {
            callback(null, item);
        }
    }
    ], function (error, result) {
        if (error) {
            res.status(404);
            res.send(error)
        } else {
            if (result.type == '冬夏令营' || result.type == '夏令营') {
                var itenary = JSON.parse(result.multiple_itenary);
                var option = 0;
                var schedule = 0;
                if (itenary) {
                    option = itenary.options;
                    schedule = itenary.schedules
                }
                if (isMobile(req.headers['user-agent']) === true){
                    res.render('products/camps', {
                        prd: result,
                        options: option,
                        schedules: schedule,
                        isVisit: req.session.isVisit,
                        isSubscribe: req.cookies.isSubscribe,
                        img: "http://cms.liunar.com/uploadimage/products/" + result.promo_thumb
                    });
                }else{
                    res.render('products/d_camps', {
                        prd: result,
                        options: option,
                        schedules: schedule,
                        img: "http://cms.liunar.com/uploadimage/products/" + result.promo_thumb
                    });
                }
            } else if (result.type == '知名大学' || result.type == '中小学' || result.type == '特色学院' || result.type == '背景提升') {
                if (isMobile(req.headers['user-agent']) != true){
                    res.render('products/d_studyOverseas', {
                        prd: result,
                        img: "http://cms.liunar.com/uploadimage/products/" + result.promo_thumb
                    });
                }else{
                 res.render('products/studyOverseas', {
                    prd: result,
                    img: "http://cms.liunar.com/uploadimage/products/" + result.promo_thumb
                });
             }

         } else {
            res.render('products/studyOverseas', {
                title: 'not found'
            });
        }
    }
});
};



var getList = function (req, res) {

    var category = product_map[req.params.category];
    db.get().pool.query("select id,title,duration, description, currency, period,tags,country, msrp, price ,age_min,age_max,promo_group, promo_thumb,promo_position from product_new where type = ? and promo_group is not null order by promo_position desc limit 6 ",
        category,
        function (err, data) {
            if (err) throw err;
            var msg;
            if (data == null) {
                msg = "No products available";
            } else {
                msg = null;
            }
            if(isMobile(req.headers['user-agent']) === true){
                res.render('products/list', {
                    category: category,
                    data: data,
                    msg: msg
                });
            }else{
                if(req.query.fromyun && req.query.fromyun == "true"){
                    res.render("products/yun_list", {
                        title: "留哪儿精选",
                        products: data,
                        msg: msg,
                        category: req.params.category,
                        categoryStr: category
                    });
                }else{
                    res.render("products/d_shop_list", {
                        title: "留哪儿精选",
                        products: data,
                        msg: msg,
                        category: req.params.category,
                        categoryStr: category
                    });
                }
            }
        });
};

var getMoreProductDetails = function (httpreq, httpres) {
    async.parallel([
        function (callback) {
            db.get().pool.query(
                "SELECT id,title, commission,referral_fee_process_time,sale_instruction,market_instruction from product_new where id = ?",
                [httpreq.params.prod_id],
                function (err, productresult) {
                    //console.log(err, productresult);
                    if (err) callback(err, null);
                    callback(null, productresult);
                }
                );
        },
        function (callback) {
            db.get().pool.query(
                "SELECT title from attachment where row_id = ? and module= 4 and type = 2",
                [httpreq.params.prod_id],
                function (err, attachmentresult) {
                    //console.log(err, attachmentresult);
                    if (err) callback(err, null);
                    callback(null, attachmentresult);
                }
                );
        }
        ], function (err, result) {
            if (err) res.sendStatus(404);
            httpres.render('products/yunying', { prd: result[0][0], files: result[1], valid: true });
        });
};

var getYunYingInfo = function (req, res) {
    var params = '/' + req.params.uid;
    if (req.params.code) {
        params += '/' + req.params.code;
    }
    request('http://www.liunar.com/api/productnew/authorizeClient' + params, function (error, response, body) {
        if (response.statusCode == 200) {
            var body = JSON.parse(body);
            if (body.code != 'true') {
                var prd = { title: '您没有权利访问此页面，请回吧' }
                var files = [];
                res.render('products/yunying', { prd: prd, files: files, valid: false });
            } else {
                getMoreProductDetails(req, res);
            }
        } else {
            var prd = { title: '您没有权利访问此页面，请回吧' }
            var files = [];
            res.render('products/yunying', { prd: prd, files: files, valid: false });
        }
    });
};

var getGaoPinProduct = function(req,res){
    res.cookie('isSubscribe', '0', {maxAge: 60 * 1000});
    console.log('co:' + res.cookie.isSubscribe);
    //var grp_id = req.params.group_id;
    var prd_id = 138;
    //joint call group_member, product_new, member table
    db.get().pool.query("select * from (select p.msrp,p.id, p.start_date, p.end_date, p.age_max, p.age_min, p.group_size , p.description, p.promo_thumb, p.title from product_new p)  a join (select count(productId) as totalbought, productId  from memberOrders group by productId) b on a.id = b.productId where productId =?;", [prd_id ],
        function (err, group_product) {
            if (err) throw err;
            if(!group_product[0]){
                res.send("no group and product combination found", null);
            }else{
                db.get().pool.query("select m.avatar from member m join memberOrders mo on m.open_id = mo.open_id where productId = ? order by mo.created_time desc limit 6", [prd_id], function(err, avatars){
                    if (err) throw err;
                    group_product[0].avatars = avatars;
                    res.render('products/gaopin', { group_product: group_product[0] });
                })
            }
        });
}

var getGaoPinProductV2 = function(req,res){
    res.cookie('isSubscribe', '0', {maxAge: 60 * 1000});
    console.log('co:' + res.cookie.isSubscribe);
    //var grp_id = req.params.group_id;
    var prd_id = req.params.prd_id;
    //joint call group_member, product_new, member table

    db.get().pool.query("select * from (select p.msrp,p.deposit, p.group_purchase_msrp ,p.id, p.start_date, p.end_date, p.age_max, p.age_min, p.group_size , p.description, p.promo_thumb, p.title from product_new p)  a join (select count(productId) as totalbought, productId  from memberOrders group by productId) b on a.id = b.productId where productId =?;", [prd_id ],
        function (err, group_product) {
            if (err) throw err;
            if(!group_product[0]){
                db.get().pool.query("select p.msrp,p.deposit, p.group_purchase_msrp ,p.id, p.start_date, p.end_date, p.age_max, p.age_min, p.group_size , p.description, p.promo_thumb, p.title from product_new p where id = ?;", [prd_id ],
                    function (err, prd_only) {
                        if (err) throw err;
                        console.log(group_product.avatars);
                        res.render('products/gaopin_v2', { group_product: prd_only[0] });
                    });
            }else{
                db.get().pool.query("select m.avatar from member m join memberOrders mo on m.open_id = mo.open_id where productId = ? order by mo.created_time desc limit 7", [prd_id], function(err, avatars){
                    if (err) throw err;
                    group_product[0].avatars = avatars;
                    res.render('products/gaopin_v2', { group_product: group_product[0] });
                })
            }
        });
}


var getPaymentPage = function(req,res){

    var o_id = req.params.o_id;
    var prd_id = req.params.prd_id;

    console.log('smallone',req.query, req.params);
    var isSubscribe = req.query.pay ==  prd_id.toString() + o_id;
    if(isSubscribe) {
        res.cookie('isSubscribe', '1', {maxAge: 60 * 1000});
    }
    console.log('co:' + res.cookie.isSubscribe);

    if (!isSubscribe &&(!req.cookies.isSubscribe || req.cookies.isSubscribe !== '1')){
        res.redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxca0568e7f29d0e45&redirect_uri=http%3A%2F%2Fwww.liunar.com%2Fusercenter%2Fwechat_auth_pay%2F' + prd_id + '%2F' + o_id + '%2F' + '&response_type=code&scope=snsapi_base&state=wxlogin&connect_redirect=1#wechat_redirect');
    }else {
        db.get().pool.query("select * from (select m.avatar, m.nickname, m.fullname, p.title, p.id as pid , p.msrp, p.group_size, p.promo_thumb, mo.* from product_new p inner join memberOrders mo on p.id = mo.productId join member m on m.open_id = mo.open_id where p.id = ? and mo.id = ?) a join (select count(productId) as totalbought, productId from memberOrders where productId = ? group by productId) b on a.productId = b.productId;", [prd_id, o_id,prd_id],
            function (err, prd) {
                if (err) throw err;
                if (!prd[0]) {
                 res.send("no such product found 2!");
             } else {
              var price = 0;
              if(prd[0].payType.toLocaleLowerCase() == 'fp'){
                price = prd[0].msrp;
            }else if(prd[0].payType.toLocaleLowerCase() == 'gp'){
             price = prd[0].group_purchase_msrp;
         }else if(prd[0].payType.toLocaleLowerCase() == 'dp'){
            price = prd[0].deposit;
        }
        res.render('products/payment_v2', {prd: prd[0], o_id: o_id, price: price});
    }
});
    }
}

var getGaoPinSharePage = function (req,res) {
   var o_id = req.params.o_id;
   db.get().pool.query("select * from  (select p.title,p.group_purchase_msrp, p.msrp, p.deposit, p.description,p.end_date, p.id as pid, p.group_size, p.promo_thumb, mo.* from product_new p inner join memberOrders mo on p.id = mo.productId where mo.id = ? and mo.group_id is not null and orderStatus = 5) a join (select count(group_id) as currentCount , group_id from memberOrders where orderStatus = 5 group by group_id) b on a.group_id = b.group_id;", [o_id],
    function (err, prd) {
        if (err) throw err;
        if(!prd[0]){
            res.send("has no id, no group, or is unpaid!");
        }else{

            prd[0].spaces_left = prd[0].group_size - prd[0].currentCount;
            console.log(prd[0].spaces_left, prd[0].group_size, prd[0].currentCount);
            db.get().pool.query("select * from (select count(productId) as totalbought, productId from memberOrders where productId = ? group by productId) a join (select m.avatar, m.fullname, m.nickname, mo.created_time, mo.productId from member m join memberOrders mo on m.open_id = mo.open_id where group_id=? and orderStatus = 5) b on a.productId = b.productId order by created_time asc", [prd[0].productId,prd[0].group_id], function(err, jointUsers){
             res.render('products/gaopin_share_v2', { prd: prd[0], jointUsers: jointUsers });
         })

        }
    });

}

module.exports = {
    readTest: readTest,
    readOne: readOne,
    home: home,
    getList: getList,
    getYunYingInfo: getYunYingInfo,
    pdfPage: pdfPage,
    getGaoPinProduct: getGaoPinProduct,
    getGaoPinProductV2:getGaoPinProductV2,
    getPaymentPage: getPaymentPage,
    getGaoPinSharePage: getGaoPinSharePage
}

