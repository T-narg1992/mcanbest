'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var http = require('http');
var url = require('url');
const isMobile = require('../utils/detectMobile').isMobile;

function getFeedList(callback){
    db.get().pool.query(
        "SELECT id, title, date, description FROM fankui",
        function(err, result){
            if(err) throw err;
            callback(result);
        }
    );
};

function getFeedDetail(id, callback){
    db.get().pool.query(
        "SELECT * FROM fankui WHERE id = ?",
        [parseInt(id)],
        function(err, result){
            if(err) throw err;
            callback(result);
        }
    );
};

var fetchList = function(req, res){
    getFeedList(function(data){
        res.render("feedback/d_feedback", {
            streams: data,
        })
    });
};

var getOne = function(req, res){
    if(req.params && req.params.id){
        getFeedDetail(
            req.params.id, 
            function(data){
                if(data && data.length > 0){
                    res.render("feedback/d_feedback_detail", {
                        feed: data[0],
                    });
                }else{
                    res.send(404);
                }
            }
        );
    }
};

module.exports = {
    fetchList: fetchList,
    getOne: getOne
}

