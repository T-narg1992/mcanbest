'use strict';
var express = require('express');
var router = express.Router();
var db = require('../db');
const request = require("request");
const async = require('async');
const mission = require("../controllers/taskController");
const member = require("../controllers/memberController");
const isMobile = require('../utils/detectMobile').isMobile;
const wechatConfig = require("../config/wechatConfig");

const wechat_url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${wechatConfig.appid}&redirect_uri=http%3A%2F%2Fwww.liunar.com%2Fmission%2Fwechat_auth&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect`;

var getMember = function(id, callback){
    member.readOne(
        id, 
        function(user){
            if(user.id){
                member.getCredit(user.id, function(credit){
                    callback(null, {userInfo: user, credit: credit});
                })
            }else{
                callback({user_err: "err"}, null);
            }
        }
    );
};

var missionList = function(callback){
    mission.getMission(
        function(result){
            if(result){
                callback(null, result);
            }else{
                callback(null, null);
            }
        }
    );
};

var combine_member_mission = function(id, callback){
    async.parallel(
        {
            member: function (next) {
                getMember(id, next);
            },
            clientsIntroducedByMe: function(next){
                member.getClientsIntroducedByMe(id, next);
            },
            missions: function (next) {
                missionList(next);
            },
        },
        function (err, data) {
            if (err) {
                callback(err);
            }else{
                callback(data);
            }
        }
    )
}

var wechat_auth = function(req, res){
    var code = req.query.code;
    var state = req.query.state;
    if(code){
        member.wechat_auth(code, function(response){
            console.log("mission_auth => " + JSON.stringify(response));
            if(response.result){
                res.cookie("user", `{"uid": ${response.result.uid}}`);
                res.redirect("http://www.liunar.com/mission");
            }else{
                res.redirect(wechat_url);
            }
        })
    }else{
        res.redirect("http://www.liunar.com");
    }
}


var getMission = function (req, res) {
    console.log(req.cookies);
    if(req.cookies.user && JSON.parse(req.cookies.user).uid){
        combine_member_mission(
            JSON.parse(req.cookies.user).uid,
            function(dataPack){ 
                var missions = "";
                var clientsIntroducedByMe ="";
                if(!dataPack.user_err){
                    if(dataPack.missions){
                        missions = dataPack.missions;
                    }
                    if(dataPack.clientsIntroducedByMe){
                        clientsIntroducedByMe = dataPack.clientsIntroducedByMe;
                    }
                    res.render("mission/mission", {
                        missions: missions,
                        user: dataPack.member.userInfo,
                        clientsIntroducedByMe: clientsIntroducedByMe,
                        credit: dataPack.member.credit,
                        showInstruction: req.cookies.educated ? false : true
                    });
                }else{
                    res.redirect(wechat_url);
                }
            }
        );
    }else{
        res.redirect(wechat_url);
    }
};


module.exports = {
    getMission: getMission,
    wechat_auth: wechat_auth
}