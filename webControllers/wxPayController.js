'use strict';
const request = require('superagent');
const isMobile = require('../utils/detectMobile').isMobile;
const async = require("async");
const user = require("../controllers/memberController");
//const wechatConfig = require('../config/wechatConfig');
var md5 = require('md5');
const wechatConfig = { //liunar config
    appid: "wxca0568e7f29d0e45",
    secret: "68c4006e18b9faa88822b7a1445ac736"
};
// const wechatConfig = { //chunteng config
//     appid: "wx9dee29ecdd3ba1fd",
//     secret: "0f0fa53e31f1a5ef0079b4255f5a15f0"
// };

const wechatPayController = require('../controllers/wxPayController');

const db = require('../db');

var paymentType_FieldMap = {
    'DP': 'deposit', 
    'FP': 'msrp',
    'GP': 'group_purchase_msrp'
};

var getOpenId = function (code, callback) {
    if (code) {
        var tokenUrl = `https://api.weixin.qq.com/sns/oauth2/access_token?appid=${wechatConfig.appid}&secret=${wechatConfig.secret}&code=${code}&grant_type=authorization_code`;
        request.get(
            tokenUrl,
            function (err, res) {
                if (err) callback(err)
                    callback(res.text);
            }
            )
    } else {

    }
};

var getProductInfo = function (id, callback) {
    db.get().pool.query(
        "SELECT * FROM product_new WHERE product_new.id = ?", [parseInt(id)],
        function (err, item) {
            if (err) callback("error");
            if (item.length < 1) callback("error");
            callback(item);
        }
        );
};


var renderError = function (title, body, res) {
    console.log('product_pay error', title, body)
    var message = {};
    message.title = title;
    message.body = body;
    res.render("wxpay/product_pay", {
        message: message,
        getArgs: "fail",
    });
};

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var saveOrder = function(req, res){
    console.log('save order requests', req.query);
    var isGroupPurchase = req.query.isGP; //if creating/joining a group
    var joiningGroup = req.query.joiningGroup; // joining group
    var payType = req.query.payType;
    var verified = req.query.verified || 0;
    //route params
    // var phone = isNaN(parseInt(req.params.phone)) ? '': req.params.phone;
    var pid = parseInt(req.params.pid);

    var orderItem = { clientId:0, orderStatus:4, verified: verified};
    if(pid){ //phone && pid
        if(isGroupPurchase){
            if(!joiningGroup){ 
                var group_hash = md5(Date.now() + pid); 
                orderItem.group_id = group_hash;
                console.log("group_hash:", group_hash);
            }else{
               console.log("already have gid:", joiningGroup);
               orderItem.group_id = joiningGroup;
           }
       }
       // orderItem.phone = phone;
       orderItem.productId = pid, orderItem.payType = payType;
        // check if full, if full then tell user to create new group
        db.get().pool.query('SELECT count(group_id) as count from memberOrders where group_id = ? and productId = ? and orderStatus = 5', 
            [orderItem.group_id, orderItem.productId],
            function(err, countObj){
                if(parseInt(countObj[0].count) > 2){
                    res.json({status: 1003 ,mo_id: 0, msg: 'group '+orderItem.group_id+' full'});
                    return;
                }else{
                  console.log("inserting into memberOrders 2381: "+ JSON.stringify(orderItem));
                  db.get().pool.query(
                    "INSERT INTO memberOrders SET ?",
                    orderItem,
                    function(err, order){
                      if(err) { 
                        res.json({status:999, msg: err});
                    }else{
                        res.json({status: 0 , mo_id: order.insertId});
                    }
                    
                })
              }
          });
    }else{
     res.json('no product #');
     return;
 }
}

var textConfirm = function(req, res){
    var user_code = req.params.code;
    var verify_id = req.params.verify_id;
    db.get().pool2.query(
        "select * from verification_records where id = ?",
        verify_id,
        function(err, record){
          if(err) throw err;
          console.log(record[0].code, user_code);
          if( user_code == record[0].code){
              db.get().pool2.query(
                "UPDATE verification_records SET verified = 1 where id = ?",
                verify_id,
                function(err, v_record){
                    console.log("before json answer", v_record);
                    res.json(
                    {
                        message: null, 
                        success: 1,
                    });
                });
          }else{
            res.json(
            {
                message: "code doesn't match", 
                success: 0,
            });
        }
    }
    )
};

var getMemberOrder = function(){

}
var productPay = function(req, res) {

    console.log('inside productPay')
    var p_id = req.query.p_id;
    // var referId = req.query.rid;
    // var ref_code = req.query.ref_code || '';
    // var uId = req.query.uid || 0;
    var code = req.query.code || '';
    var o_id = req.query.o_id;
    var payType = req.query.type;
    // var verified = req.query.verified;
    // console.log('entered with verified: ', verified);
    var message = {};
    getProductInfo(p_id, function(result) {
        if (result !== "error") {
            if (code) {
                console.log("o_id : " + o_id, "type : " + payType, "productId : " + p_id);
                var sendPaymentRequest = function(_order) {
                    if(_order.id){
                        o_id = _order.id;
                        payType = _order.payType;
                        // verified = _order.verified;
                    }
                    var price = result[0][paymentType_FieldMap[payType]] * 100;
                    var payTypes = ["DP", "FP", "GP"];
                    if( payTypes.indexOf(payType) == -1){
                        renderError("出错啦", 'pay type not supported', res);
                    }
                    getOpenId(req.query.code, function(data) {
                        var dataPack = JSON.parse(data);
                        var addition = {
                            type: payType,
                            o_id: o_id,
                        };

                        if (dataPack.openid) {
                            var order = {
                                openid: dataPack.openid,
                                body: result[0].title,
                                attach: JSON.stringify(addition),
                                out_trade_no: 'prd' + (+new Date),
                                total_fee: parseInt(price),
                                spbill_create_ip: req.ip,
                                trade_type: 'JSAPI'
                            };

                            wechatPayController.wxPaymentSign(
                                order,
                                function(sign, errDetail) {
                                    if (sign !== "error") {
                                        console.log('sign not error');
                                        message.title = result[0].title;
                                        message.body = result[0].description;
                                        var wxPayViewData = {
                                            message: message,
                                            getArgs: "success",
                                            price: (price / 100).toFixed(2),
                                            appid: sign.appId,
                                            timestamp: sign.timestamp,
                                            signType: sign.signType,
                                            package: sign.package,
                                            paySign: sign.paySign,
                                            nonceStr: sign.nonceStr,
                                            o_id: o_id,
                                        };
                                        if (req.query.fromGaoPin == '1' || _order.id > 0 ) {
                                           var getPaymentPage = function(req, res) {
                                            console.log('big one ---', 'order: ', order);
                                            var prd_id = req.query.p_id;
                                            var isSubscribe = req.query.pay == prd_id.toString() + o_id;
                                            if (isSubscribe) {
                                                res.cookie('isSubscribe', '1', {
                                                    maxAge: 60 * 1000
                                                });
                                            }

                                            if (!isSubscribe && (!req.cookies.isSubscribe || req.cookies.isSubscribe !== '1')) {
                                             console.log('before redirect to wechat_auth_pay');
                                             res.redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxca0568e7f29d0e45&redirect_uri=https%3A%2F%2Fwww.liunar.com%2Fusercenter%2Fwechat_auth_pay%2F' + prd_id + '%2F' + o_id + '%2F'+ payType + '%2F' + '?response_type=code' +'&scope=snsapi_base&state=wxlogin&connect_redirect=1#wechat_redirect');
                                         } else {
                                            console.log('already suscribed');
                                            db.get().pool.query(
                                                "UPDATE memberOrders SET open_id = ? ,reference_code = (select reference_code from member where open_id = ?) where id= ?;", [order.openid, order.openid, o_id],
                                                function(err, _) {
                                                    if (err) throw err;
                                                    db.get().pool.query("select * from (select m.avatar, m.nickname, m.fullname, p.title, p.group_purchase_msrp, p.msrp, p.deposit, p.id as pid, p.group_size, p.promo_thumb, mo.* from product_new p inner join memberOrders mo on p.id = mo.productId join member m on m.open_id = mo.open_id where p.id = ? and mo.id = ?) a join (select count(productId) as totalbought, productId from memberOrders where productId = ? group by productId) b on a.productId = b.productId;", [prd_id, o_id, prd_id],
                                                        function(err, prd) {
                                                            if (err) throw err;
                                                            if (!prd[0]) {
                                                                res.send("no such product found 1!");
                                                            } else {
                                                              wxPayViewData.prd = prd[0];
                                                              wxPayViewData.price = prd[0][paymentType_FieldMap[payType]];
                                                              // console.log('verified before check','verified:', verified)
                                                            //   if(prd[0].verified == 1){
                                                            //     db.get().pool.query('UPDATE member set phone = ? where open_id = ?', [prd[0].phone, prd[0].open_id], function(err, insert){
                                                            //         console.log('verified: ', prd[0].phone, prd[0].open_id);
                                                            //         res.render('products/payment_v2', wxPayViewData);
                                                            //     });
                                                            // }else{
                                                            //     console.log('not verified');
                                                            res.render('products/payment_v2', wxPayViewData);
                                                            // }


                                                        }
                                                    });
                                                });
                                        }
                                    }
                                    getPaymentPage(req, res);
                                } else {
                                    console.log('not gaopin');
                                    res.render("wxpay/product_pay", wxPayViewData);
                                }
                            } else {
                                console.log('sign error');
                                var err_string = "";
                                for (var property in errDetail) {
                                    if (errDetail.hasOwnProperty(property)) {
                                        err_string += (property + ":" + errDetail[property] + " | ");
                                    }
                                }
                                renderError("出错啦", errDetail, res);
                            }
                        }
                        )
} else {
    console.log('no datapack.openid');
    var errDetail = "";
    for (var property in dataPack) {
        if (dataPack.hasOwnProperty(property)) {
            errDetail += (property + ": " + dataPack[property]);
        }
    }
    renderError("出错啦", errDetail, res);
}
});
}
if(o_id){
    db.get().pool.query('select * from memberOrders where id = ?', [o_id], function(err, order){
        if(order[0] && order[0].id){
            sendPaymentRequest(order[0]);
        }
    });
}else{
 sendPaymentRequest();
}

} else {
                // is unsigned
                console.log('no wx code ');
                var fromGaoPin = req.query.fromGaoPin;
                if (p_id && fromGaoPin != "1") {
                    var orderItem = {
                        productId: parseInt(p_id),
                        phone: parseInt(tel) || 11111111,
                        referId: isNaN(parseInt(referId)) ? 0 : referId,
                        reference_code: ref_code == ref_code || "",
                        orderStatus: 4
                    };

                    console.log("before addOrder");
                    var requestPaymentSignature = function() {
                        var addOrder = function() {
                            console.log("inserting into memberOrders!");
                            console.log("group_id:", orderItem.group_id);
                            db.get().pool.query(
                                "INSERT INTO memberOrders SET ?",
                                orderItem,
                                function(err, res2) {
                                    if (err) throw err;
                                    res.render("wxpay/pay_redirect", {
                                        type: payType,
                                        o_id: res2.insertId,
                                        p_id: p_id,
                                        verified: verified
                                    });
                                }
                                )
                        };
                        if (req.header("Referer") && req.header("Referer") != "undefined") {
                            addOrder();
                        } else {
                            res.send('some foreign request');
                        }
                    }
                    requestPaymentSignature();

                } else if (req.query.fromGaoPin == "1") {
                    res.render("wxpay/pay_redirect", {
                        type: 'DP',
                        o_id: o_id,
                        p_id: p_id,
                        fromGaoPin: 1,
                        verified: verified
                    });
                } else {
                    renderError("出错啦", "缺少必要的参数", res);
                }
            }
        } else {
            renderError("出错啦", "找不到该产品", res);
        }
    })
};

var sendMsg = function (req, res) {
    var o_id = 419;
    var prd_id = 138;
    db.get().pool.query("select * from (select m.avatar, m.nickname, m.fullname, p.title, p.group_purchase_msrp, p.msrp, p.deposit, p.id as pid, p.group_size, p.promo_thumb, mo.* from product_new p inner join memberOrders mo on p.id = mo.productId join member m on m.open_id = mo.open_id where p.id = ? and mo.id = ?) a join (select count(productId) as totalbought, productId from memberOrders where productId = ? group by productId) b on a.productId = b.productId;", [prd_id, o_id, prd_id],
        function(err, prd) {
            if (err) throw err;
            if (!prd[0]) {
                res.send("no such product found 1!");
            } else {
                var wxPayViewData = {};
                wxPayViewData.prd = prd[0];
                wxPayViewData.getArgs = 'success';
                wxPayViewData.price = prd[0][paymentType_FieldMap[prd[0].payType]];
                console.log('patttttttttyy:::::', paymentType_FieldMap[prd[0].payType]);
                res.render('products/payment_v2', wxPayViewData);
            }
        });
};

var howtoshare = function (req, res) {
    console.log("howtoshare referrer::" + req.header("Referer"));
    var o_id = req.query.o_id || 0;
    if(o_id){
       db.get().pool.query(
        "select p.title,p.description, p.id as pid , p.msrp,p.deposit,p.group_purchase_msrp, p.group_size, p.promo_thumb,p.end_date, mo.* from product_new p join memberOrders mo on p.id = mo.productId where mo.id= ?"
        , [parseInt(o_id)],
        function (err, order) {
            if (err) throw err;
            db.get().pool.query('select count(group_id) as count from memberOrders where group_id = ? and orderStatus = 5', [order[0].group_id], 
                function(err, count){
                 if (err) throw err;
                 var payType = paymentType_FieldMap[order[0].payType];
                 console.log(payType);
                 res.render("products/howtoshare", {
                    order: order[0],
                    count: count[0],
                    price: order[0][payType],
                    payType: order[0].payType,
                });
             });
        });
   }else{
    renderError('出错啦', '没有订单号', res);
}
};

var redirectToGaopinShare = function( req,res ){
    var o_id = req.query.o_id || 0;
    res.redirect('http://shop.liunar.com/gaopin/share/'+o_id);
}

module.exports = {
    sendMsg: sendMsg,
    productPay: productPay,
    saveOrder: saveOrder,
    textConfirm:textConfirm,
    howtoshare : howtoshare,
    redirectToGaopinShare: redirectToGaopinShare,
}