﻿'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');
var memberCtrl = require('../controllers/memberController');
var signature = require('../controllers/wechatSignature');
var wechat_cfg = require('../config/wechatConfig');
var config = require('../config/wechatConfig');
var listByName = function (req, res) {
    res.status(200);
    var schools = [
        { "name": "John", "city": "Doe" },
        { "name": "Anna", "city": "Smith" },
        { "name": "Peter", "city": "Jones" }
    ];
    res.json(schools);
};
var insertNewPoint = function (req, res) {
    var newItem = {
        uid: 0,
        points: 0,
        pointType: 0,
        clientId: 0
    }
    //newItem.stamp = 
    if (req.body.uid) {
        newItem.uid = req.body.uid;
    }
    if (req.body.clientId) {
        newItem.clientId = req.body.clientId;
    }
    if (req.body.points) {
        newItem.points = req.body.points;
    }
    if (req.body.pointType) {
        newItem.pointType = req.body.pointType;
    }
    insertNewPointRec(newItem, function (id) {
        res.render('agent/created', {
            nickname: newItem.points
        });
    });
}

var updateUserPointsSummary = function (callback) {

}
var insertNewPointRec = function (newItem, callback) {

    //newItem.stamp = 
    var sql = 'INSERT INTO memberPoints SET ? ';
    //var values = [wechatResponse.nickname, 'qq', wechatResponse.unionid,1,1,1,1,1];
    db.get().pool.query(sql, newItem, function (err, row, fields) {
        if (err) {

            //console.error(err + ":" + wechatResponse.nickname + ":" + wechatResponse.unionid + ":" + wechatResponse.errcode);
        } else {
            console.log(row);
            return callback(row.insertId);
        }
    });

}

var wx = function (req, res) {
    if (req.params && req.params.code) {
        var code = req.params.code;
        res.redirect('yun.liunar.com/account/wx?code=' + code);
    }
};

var createNew = function (req, res) {
    var wechatResponse = {
        "openid": "OPENID",
        "nickname": "NICKNAME",
        "sex": 1,
        "province": "PROVINCE",
        "city": "CITY",
        "country": "COUNTRY",
        "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
        "privilege": [
            "PRIVILEGE1",
            "PRIVILEGE2"
        ],
        "unionid": " o6_bm"
    };
    wechatResponse.unionid = new Date();
    if (req.body.name) {
        wechatResponse.nickname = req.body.name;

    } else {
        wechatResponse.nickname = new Date();
    }
    wechatResponse.unionid = wechatResponse.unionid + wechatResponse.nickname;
    wechatResponse.introId = req.body.agent;
    memberCtrl.createNew(wechatResponse, function (id) {
        res.render('agent/created', {
            nickname: wechatResponse.nickname
        });
    });

};
var readOne = function (req, res) {

    if (req.params && req.params.id) {
        db.get().pool.query(
            "SELECT * FROM school WHERE id = ?",
            [req.params.id],
            function (err, result) {
                if (err) throw err;
                if (result.length > 0) {
                    res.render('agent/index', {
                        title: table.defaultTitle + result[0].ch_name + " " + result[0].en_name,
                        schoolName: result[0].ch_name + result[0].en_name,
                        schoolId: result[0].id
                    });
                }
            }
        );
    } else {
        res.status(404);
        res.render('schools/index.html', {
            title: 'Articles',
        });
    }
};

var list = function (req, res) {

    //router.post('/wechatSign', function (req, res) {
    //    //var url = req.protocol + '://' + req.host + req.path;
    //    var url = req.body.url; //获取当前url
    //    console.log("get url->:" + url);
    //});
    db.get().pool.query(
        "SELECT * FROM member",
        function (err, result) {
            if (err) throw err;
            signature.sign("http://api.liunar.net/web/agents", function (signatureMap) {
                signatureMap.appId = wechat_cfg.appid;
                signatureMap.nonceStr = config.noncestr;
                //res.status(200);
                //res.json(signatureMap);
                if (result.length > 0) {
                   var shapeObje = {
                        title: 'here is title from js sdk',
                        desc: 'I put any desc here',
                        link: 'http://api.liunar.net/web/agents',
                        imgUrl: 'http://www.liunar.net/img/hwyx.png',
                    }
                    res.render('agent/index', {
                        title: "用户列表",
                        list: result,
                        signPackage: signatureMap,
                        shapeObje: shapeObje,
                    });
                } else {
                    res.render('agent/index', {
                        title: "sss",
                    });
                }
            });


        }
    );

    /*
    var parseUrl = url.parse(req.url, true);
    var typeId = parseUrl.query.type_id;
    res.render('agent/index', {
        title: "asdfasd",
    });
    /*
    if (typeId) {
        db.get().pool.query(
            "SELECT * FROM school WHERE type = ? ORDER BY national_rank",
            [typeId],
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    } else {
        db.get().pool.query(
            "SELECT * FROM school ORDER BY national_rank",
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    }*/
};
module.exports = {
    listByName: listByName,
    readOne: readOne,
    list: list,
    wx: wx,
    createNew: createNew,
    insertNewPointRec: insertNewPointRec,
    insertNewPoint: insertNewPoint
}