﻿'use strict';
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');
var listByName = function (req, res) {
    res.status(200);
    var schools = [
        { "name": "John", "city": "Doe" },
        { "name": "Anna", "city": "Smith" },
        { "name": "Peter", "city": "Jones" }
    ];
    res.json(schools);
};

var readOne = function (req, res) {
    if (req.params && req.params.id) {
        db.get().pool.query("SELECT article.* ," +
            table.userTable + ".username " + "FROM article LEFT JOIN " +
            table.userTable + " ON article.uid = " + table.userTable + ".id WHERE " +
            "article.id = ?",
            [parseInt(req.params.id)],
            function (err, item) {
                if (err) throw err;
                res.render('post/index', {
                    title: item[0].title,
                    id: item[0].id
                });
            });
    } else {
        res.status(404);
        res.render('post/index', {
            title: 'not found',
        });
    }
};

var list = function (req, res) {
    var parseUrl = url.parse(req.url, true);
    var typeId = parseUrl.query.type_id;
    if (typeId) {
        db.get().pool.query(
            "SELECT * FROM school WHERE type = ? ORDER BY national_rank",
            [typeId],
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    } else {
        db.get().pool.query(
            "SELECT * FROM school ORDER BY national_rank",
            function (err, result) {
                if (err) throw err;
                res.status(200);
                res.json(result);
            }
        );
    }
};
module.exports = {
    listByName: listByName,
    readOne: readOne,
    list: list
}