'use strict';
const config = require("../config")['settings'];
var express = require('express');
var router = express.Router();
var db = require('../db');
const jwt = require("jsonwebtoken");
const request = require("superagent");
const async = require('async');
var http = require('http');
var url = require('url');
const rp = require("request-promise");
const mission = require("../controllers/taskController");
const member = require("../controllers/memberController");
const isMobile = require('../utils/detectMobile').isMobile;
const isWechat = require('../utils/detectMobile').isWechat;
const wechatConfig = require("../config/wechatConfig");

const wechat_url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${wechatConfig.appid}&redirect_uri=https%3A%2F%2Fwww.liunar.com%2Fusercenter%2Fwechat_auth&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect`;
const wechat_url_2 = "https://open.weixin.qq.com/connect/qrconnect?appid=wxa9717b409b6a35a2&redirect_uri=https%3A%2F%2Fwww.liunar.com%2Fusercenter%2Fwechat_auth%2F&response_type=code&scope=snsapi_login#wechat_redirect";

var logger = require('./../winstonlog');
var isAdmin = function (id, callback) {
    var obj = {};
    request.post(
        `http://yun.liunar.com/agent/agencyinfo/?uid=${id}`,
        function (err, res) {
            if (err) obj.admin = false, obj.AgencyName = null;
            else obj.admin = res.body.Admin, obj.AgencyName = res.body.AgencyName;
            callback(obj);
        }
        )
};

var getMember = function (id, callback) {
    member.readOne(
        id,
        function (user) {
            if (user.id) {
                member.getCredit(user.id, function (credit) {
                    callback({
                        userInfo: user,
                        credit: credit
                    });
                })
            } else {
                callback({
                    user_err: "no user found by given id"
                });
            }
        }
        );
};
var wechat_subinfo = function (req, res) {
    var code = req.query.code;
    var openid = req.query.openid;
    var state = req.query.state;
    if (openid) {

        var url1 = `http://ystage.liunar.com/account/WxSubscribe?openid=${openid}`;
        console.log(url1);
        var options = {
            method: 'POST',
            uri: url1,
            body: {
                some: 'payload'
            },
            json: true // Automatically stringifies the body to JSON
        };

        rp(options).then(response => {
            console.log(response);
            var accessTokenResult = response;
            console.log("get access code" + accessTokenResult.subscribe + accessTokenResult.errmsg);
            res.render("wxpay/pay_result", {
                title: "这是结果",
                msg: "支付成功！",
                subscribe: accessTokenResult.subscribe,
                product: 10000,
            })
        }).
        catch(err => {
            console.log("Err:" + err);
            res.render("wxpay/pay_result", {
                title: "这是结果",
                msg: "支付成功！",
                product: 12444333,
            })

        })
        ;
    } else {
        console.log("else: 88");

        res.render("wxpay/pay_result", {
            title: "这是结果",
            msg: "支付成功！",
            product: 12444,
        })
    }
};
var wechat_subinfo_pay = function (req, res) {
  var code = req.query.code;
  var openid = req.query.openid;
  // var verified = req.query.verified;
  // console.log('in wechat_subinfo_pay---verified', verified);

  var payType = req.query.payType;
  var state = req.query.state;
  var id = req.query.id;
  var oid = req.query.oid;
  console.log("oid1: --", oid);
    // req.session.isSubscribe = 0;
    var product_page = "https://shop.liunar.com/gaopin/product/" + id;
    if (openid) {

        var url1 = `http://ystage.liunar.com/account/WxSubscribe?openid=${openid}`;
        console.log(url1);
        var options = {
            method: 'POST',
            uri: url1,
            body: {
                some: 'payload'
            },
            json: true // Automatically stringifies the body to JSON
        };

        rp(options).then(response => {
            console.log("response:" , response);
            console.log("oid2: --", oid);

            var accessTokenResult = response;
            console.log("get access code subscribe" + accessTokenResult.subscribe + accessTokenResult.errmsg);
            if (!accessTokenResult.errmsg) {
            // req.session.isSubscribe = accessTokenResult.subscribe;
            // req.session.save();
            if (accessTokenResult.subscribe === '1') {
                // console.log('wechat-subinfo-pay--209', verified);
                var val = id + oid;
                res.render("wxpay/pay_redirect",{
                    p_id: id,
                    type: payType,
                    o_id: oid,
                    fromGaoPin: 1,
                    pay: val,
                    // verified: verified
                })
                // res.redirect("http://liunar.com/testpay/payproduct?type=DP&o_id="+ oid + "&pay=" + val+"&p_id="+ id);
            } else {
                db.get().pool.query(
                    "UPDATE memberOrders SET open_id = ? where id= ? ",
                    [openid, parseInt(oid)],
                    function (err, _) {
                        if (err) throw err;
                        // console.log('update logssssss--', verified);
                        res.redirect("https://www.liunar.com/follow");
                    }
                    )            
            }
        }else {
            res.redirect(product_page);
        }
    }).catch(err => {
        console.log("Err:" + err);
        res.redirect(product_page);

    });
}else {
    res.redirect(product_page);
}
};
var wechat_subscribe_page = function (req, res) {
    var id = req.params.id;
    var gid = req.params.gid;

    res.cookie('product_id', id, {maxAge: 60 * 1000});
    res.cookie('gid', gid, {maxAge: 60 * 1000});

    res.render("wxpay/pay_result", {
        title: "这是结果",
        msg: "支付成功！",
        product: id + "--" + gid,
    })
}
var wechat_continue_pay = function (req, res) {
    /*if(req.cookies.product_id) {
     var id = req.cookies.product_id;

     res.redirect("http://shop.liunar.com/"+ id + "/" + gid + "&pay=test");
 }*/
}

var wechat_auth_pay = function (req, res) {
    var code = req.query.code;
    var state = req.query.state;
    var payType = req.params.payType;
    // var verified = req.params.verified;
    // console.log('in wechat_auth_pay with verified #########', verified);
    console.log(req.params, req.query);
    var id = req.params.id;
    var oid = req.params.oid;
    var product_page = "https://shop.liunar.com/gaopin/product/" + id;

    if (code) {
        console.log('if code aaa--', code);
        member.wechat_auth(code, function (response) {
            console.log("base:" + response.result);
            if (response.result) {
             console.log('if code bbb--', response.result.openid);
                // user session begins
                console.log('wechat auth: ' + response.result.openid + ' : ' + response.result.subscribe);
                var url1 = `http://ystage.liunar.com/account/WxSubscribe?openid=${response.result.openid}`;
                res.redirect("https://www.liunar.com/usercenter/wechat_subinfo_pay?openid=" + response.result.openid + "&id=" + id + "&payType="+ payType +'&code='+ code  + "&oid=" + oid);
            } else {
                res.redirect(product_page);
            }
        })
    } else {
        res.redirect(product_page);
    }
};

var wechat_auth_base = function (req, res) {

    console.log('in wechat_auth_base');
    var code = req.query.code;
    var state = req.query.state;
    if (code) {
        member.wechat_auth(code, function (response) {
            if (response.result) {
                // user session begins
                var url1 = `http://ystage.liunar.com/account/WxSubscribe?openid=${response.result.openid}`;
                res.redirect("https://www.liunar.com/usercenter/wechat_subinfo?openid=" + response.result.openid);

                // var url1 = `https://api.weixin.qq.com/cgi-bin/user/info?access_token=${response.result.access_token}&openid=${response.result.openid}&lang=zh_CN `;
                // rp(url1).then(response => {
                // var accessTokenResult = JSON.parse(response);
                // console.log("get access code" + accessTokenResult.subscribe + accessTokenResult.errmsg);
                //
                // res.render("wxpay/pay_result", {
                //     title: "这是结果",
                //     msg: "支付成功！",
                //     product: 10000,
                // })

            } else {
                res.render("wxpay/pay_result", {
                    title: "这是结果",
                    msg: "支付成功！",
                    product: 1,
                })
            }
        })
    } else {
        res.render("wxpay/pay_result", {
            title: "这是结果",
            msg: "支付成功！",
            product: 12444,
        })
    }
};

var wechat_auth = function (req, res) {
    console.log('in wechat_auth');
    var code = req.query.code;
    var state = req.query.state;
    if (code) {
        member.wechat_auth(code, function (response) {
            if (response.result) {
                // user session begins
                logger.debug('wechat auth: ' + response.result.uid + ' : ' + response.result.nickname)
                req.session.user = {
                    uid: response.result.uid,
                    fullname: response.result.nickname
                };
                var token = jwt.sign({
                    data: response.result.uid
                }, req.app.get('secret'), {
                    expiresIn: '365d'
                });
                res.cookie("user", `{"uid": ${response.result.uid}}`, {
                    path: '/',
                    domain: ".liunar.com"
                });
                res.cookie("token", `{"token": ${token}}`, {
                    path: '/',
                    domain: ".liunar.com"
                });
                res.redirect("https://www.liunar.com/usercenter");
            } else {
                res.redirect(wechat_url);
            }
        })
    } else {
        res.redirect(wechat_url);
    }
};

var logout = function (req, res) {
    delete req.session.user;
    //document.cookie = "user=; domain=.liunar.com; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/"
    res.cookie('user', "", {
        domain: "www.liunar.com",
        path: "/",
        expires: new Date(Date.now() - 900000)
    });
    res.cookie('user', "", {
        domain: ".liunar.com",
        path: "/",
        expires: new Date(Date.now() - 900000)
    })
    res.clearCookie('user', {
        domain: "www.liunar.com",
        path: "/"
    });
    res.clearCookie('user', {
        domain: ".liunar.com",
        path: "/"
    });
    res.redirect("https://www.liunar.com/");
};

function parseUserCookie(json, err) {
    ;
    var parsed = {};
    try {
        parsed = JSON.parse(json);
        return parsed;
    } catch (e) {
        return parsed.uid = null;
    }
};

var userRecommend = function (req, res) {
    logger.debug('got request 4 userRecommend');
    var viewObj = {};
    var user = req.session.user;
    if (user && user.uid) {
        logger.debug(user.uid + '-' + user.fullname)
        db.get().pool.query("select id, reference_code from member where id = ?", [user.uid], function (err, rows) {
            if (err) throw err;
            viewObj.uid = user.uid;
            viewObj.fullname = user.fullname;
            viewObj.ref_code = rows[0].reference_code;
            logger.debug("grabbed refCode for uid " + user.uid + "  -- ref_code = " + viewObj.ref_code);
            res.render('mobile/users/user_recommend', {
                uid: viewObj.uid,
                fullname: viewObj.fullname,
                ref_code: viewObj.ref_code
            });
        });
    } else {
        res.redirect(wechat_url);
    }
}

var userPage = function (req, res) {

    if (isWechat(req.headers['user-agent']) === true) console.log("wechat=> ", req.cookies);
    else console.log("regular ", req.cookies);
    if (req.session.user && req.session.user.uid > 0) {
        var uid = req.session.user.uid;
        logger.debug('session user:' + uid)
        getMember(
            uid,
            function (user) {
                if (!user.user_err) {

                    isAdmin(uid, function (admin) {

                        res.render("mobile/users/user_center", {
                            uid: uid,
                            currentCredit: user.credit.available,
                            withdrew: user.credit.withdrew,
                            avatar: user.userInfo.avatar,
                            name: user.userInfo.fullname,
                            yunId: user.userInfo.yunId,
                            ref_code: user.userInfo.reference_code,
                            isAdmin: admin
                        })
                    });
                } else {

                    res.redirect(wechat_url);
                }
            }
            );
    } else {
        logger.debug('no session user')
        res.redirect(wechat_url);
    }
};

var cashPage = function (req, res) {
    if (req.cookies.user && parseUserCookie(req.cookies.user).uid) {
        var uid = parseUserCookie(req.cookies.user).uid;
        getMember(
            uid,
            function (user) {
                if (!user.user_err) {
                    res.render("mobile/users/user_cash", {
                        currentCredit: user.credit.available,
                        withdrew: user.credit.withdrew,
                        avatar: user.userInfo.avatar,
                        name: user.userInfo.nickname
                    })
                } else {
                    res.redirect(wechat_url);
                }
            }
            );
    } else {
        res.redirect(wechat_url);
    }
}

module.exports = {
    userPage: userPage,
    userRecommend: userRecommend,
    cashPage: cashPage,
    logout: logout,
    wechat_auth: wechat_auth,
    wechat_auth_base: wechat_auth_base,
    wechat_subinfo: wechat_subinfo,
    wechat_auth_pay: wechat_auth_pay,
    wechat_subinfo_pay: wechat_subinfo_pay,
    wechat_subscribe_page: wechat_subscribe_page,
    wechat_continue_pay: wechat_continue_pay
}
