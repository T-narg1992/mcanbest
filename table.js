﻿var prefix = "o2c0_";
var table = {
    userTable: prefix + "ucenter_member",
    newsTable: prefix + "news",
    newsTableDetail: prefix + "news_detail",
    newsCommentTable: prefix + "local_comment",
    pictureTable: prefix + "picture",
    defaultTitle: "www.liunar.net: ",
    loginPoints: 100,
    introUserPoints: 1000,
    sharePoints: 1,
    salePoints: 2000
};

module.exports = table;