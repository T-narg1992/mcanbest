DROP TABLE IF EXISTS `product` ;

CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `price` DECIMAL(13,4),
  `description` varchar(255),
  `title` varchar(255),
  `recommended` tinyint DEFAULT 0,
  `school_id` int(5) unsigned NOT NULL COMMENT 'if related to school-> id else 0',
  `weight` int DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
