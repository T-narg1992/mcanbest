DROP TABLE IF EXISTS `survey_question`;
DROP TABLE IF EXISTS `survey_question_option` ;
DROP TABLE IF EXISTS `survey_user_answer` ;

CREATE TABLE `survey_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `question_text` varchar(514),
  `question_type` tinyint COMMENT '1:single answer multiple choice',
  `survey_formId` int unsigned,
  `sort_order` int unsigned
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `survey_question_option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `question_id` int unsigned,
  `option_text` varchar(255),
  `sort_order` int unsigned
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `survey_user_answer` (
  `question_id` int unsigned NOT NULL,
  `uid` int unsigned NOT NULL,
  `answer_text` varchar(255),
  `answer` int
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;









