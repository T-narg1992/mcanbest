DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
    id  Int,
    country Int,
    province    Int,
    city    Varchar(30),
    street_1    nvarchar(100),
    street_2    nvarchar(100),
    zipcode Varchar(30),
    lng Decimal(11,8) COMMENT  'Longitude',
    lat Decimal(11,8)  COMMENT 'latitude',
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
