DROP TABLE IF EXISTS `goodnews` ;

CREATE TABLE `goodnews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `school_id` int(4) unsigned,
  `name` varchar(50) NOT NULL,
  `event` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '{1:university, 2:college, 3:Secondary + primary}',
  `weight` int DEFAULT 0,
  `extra_info` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


