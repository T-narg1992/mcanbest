﻿
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `module` tinyint(3) unsigned DEFAULT '0' COMMENT '{1:article, 2:school}',
  `row_id` int(11) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `create_time` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `ip` bigint(20) DEFAULT NULL,
  `area` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO `comment` VALUES (1,1,1,31,'足够的资金准备',1446752301,1,1179631198,'不列颠哥伦比亚Richmond'),(2,1,1,31,'想好以后会告诉你的！',1446962548,1,1179631198,'不列颠哥伦比亚Richmond'),(3,1,1,31,'我家孩子不想出国读书',1447009292,1,1179631198,'不列颠哥伦比亚Richmond'),(4,109,1,9,'一般工作签给的时间是少于上学时间的，而且工作签不能续的哟',1447011470,1,1179631198,'不列颠哥伦比亚Richmond'),(5,113,1,39,'表格格式不太好',1447022412,1,1119327973,'不列颠哥伦比亚Richmond'),(6,0,1,2,'好文章',1447694316,1,3639137920,'不列颠哥伦比亚Richmond'),(7,125,1,49,'37900是学生的费用吗？大人的费用怎么算？',1447799101,1,1179601933,'不列颠哥伦比亚Vancouver'),(8,125,1,48,'看来想让孩子接受好的教育，就一个字，有钱。',1447799166,1,1179601933,'不列颠哥伦比亚Vancouver'),(9,125,1,47,'工作快到我碗里来',1447799256,1,1179601933,'不列颠哥伦比亚Vancouver'),(10,125,1,5,'呵呵呵',1447799571,1,1179601933,'不列颠哥伦比亚Vancouver'),(11,0,1,5,'这篇不错。',1448066990,1,1179601933,'不列颠哥伦比亚Vancouver'),(12,112,1,2,'貌似不贵',1449080120,1,1179626865,'不列颠哥伦比亚Richmond'),(13,1,1,376,'我有意见',1455314366,1,2919666246,'不列颠哥伦比亚Richmond'),(14,104,1,455,'美得不要不要的',1458859256,1,1179601933,'不列颠哥伦比亚Vancouver');
