﻿# Host: 52.36.30.184  (Version 5.7.12)
# Date: 2016-06-08 18:11:26
# Generator: MySQL-Front 5.3  (Build 5.39)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "memberPoints"
#

DROP TABLE IF EXISTS `memberPoints`;
CREATE TABLE `memberPoints` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0',
  `points` int(11) unsigned NOT NULL DEFAULT '0',
  `pointType` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '积分类别: 0 - 首次微信授权, 1 - 引进新用户, 2 - 销售, 3 - 分享',
  `stamp` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发生时间',
  `productId` int(11) unsigned DEFAULT NULL COMMENT '销售产品的ID',
  `clientId` int(11) unsigned DEFAULT NULL COMMENT '客户/消费者 ID',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='用户积分明细表';

#
# Data for table "memberPoints"
#

