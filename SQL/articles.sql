DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '作者id',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `description` varchar(255) COMMENT '描述',
  `category` tinyint NOT NULL COMMENT '{0:old data,1：海外规划,2：留学申请,3：校园内外,4：本地生活,5：实习就业,6：职业移民}',
  `view_count` int(11) DEFAULT 0 COMMENT '阅读量',
  `status` tinyint(2) NOT NULL COMMENT '状态 {0:fail, 1:pass, 2:pending}',
  `reason` varchar(100) COMMENT '审核失败原因',
  `comment_count` int(11) DEFAULT 0 COMMENT '评论量',
  `content_html` text NOT NULL COMMENT '文章内容',
  `content_text` text COMMENT '纯文本内容',
  `collection_count` int(11) DEFAULT 0 COMMENT '收藏量',
  `share_count` int(11) DEFAULT 0 COMMENT '分享量',
  `is_huma` tinyint COMMENT '是否是虎妈学堂文章 0:不是 1： 是',
  `channel` tinyint COMMENT 'see article form for this fields options',
  `source_url` varchar(200) DEFAULT NULL COMMENT '来源',
  `create_time` int(11)  COMMENT '创建时间',
  `update_time` int(11)  COMMENT '更新时间',
  `thumbnail` varchar(255) DEFAULT NULL COMMENT '缩略图',
  `image_array` varchar(50) DEFAULT NULL COMMENT 'store as [1,2,3..etc]',
  PRIMARY KEY (`id`),
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 COMMENT='文章列表';


INSERT INTO article (id,uid, title, description,category,content_text, view_count, comment_count, collection_count, source_url, thumbnail, status, content_html,create_time, update_time)
SELECT id, uid, title, description,0, '',view, comment, collection, source, cover, 1, content, create_time, update_time
FROM o2c0_news INNER JOIN o2c0_news_detail ON o2c0_news.id = o2c0_news_detail.news_id


