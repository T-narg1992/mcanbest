﻿# Host: 52.36.30.184  (Version 5.7.12)
# Date: 2016-06-08 18:09:24
# Generator: MySQL-Front 5.3  (Build 5.39)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "member"
#

DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(5000) CHARACTER SET utf8 DEFAULT NULL,
  `qq` char(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'qq号',
  `address_Id` int(10) unsigned DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wechat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Wechat_qrcode` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'avatar jpg path',
  `User_type` tinyint(4) DEFAULT NULL COMMENT '1: Normal, 2:Agent, 3:parents, 4:student',
  `reference_Id` int(10) unsigned DEFAULT NULL,
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `introducer` int(11) DEFAULT NULL COMMENT '用户第一次登录留哪儿的介绍人',
  `intro_time` int(10) unsigned DEFAULT NULL,
  `loginPoints` int(11) unsigned DEFAULT NULL COMMENT '用户首次微信授权积分',
  `userIntroPoints` int(11) unsigned DEFAULT NULL COMMENT '新增用户积分',
  `salePoints` int(11) unsigned DEFAULT NULL COMMENT '销售积分',
  `sharePoints` int(11) unsigned DEFAULT NULL COMMENT '分享积分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "member"
#

INSERT INTO `member` VALUES (61,NULL,'Alex Song','qq',NULL,NULL,'oGKxKv33wlK1T6FbUiT3lg5qcKRs',NULL,NULL,NULL,NULL,1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL),(62,NULL,'Jo)AnNa','qq',NULL,NULL,'oGKxKvwFh75x2dhPsn2Sz7vYyp4U',NULL,NULL,NULL,NULL,1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL);
