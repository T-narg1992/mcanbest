CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='文章分类';

CREATE TABLE `tag_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int NOT NULL,
  `target_id` int NOT NULL,
  `module` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB