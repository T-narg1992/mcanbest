CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `phone`   Varchar(15),
  `nickname` char(32) NOT NULL DEFAULT '' COMMENT '昵称',
  `qq` char(30) DEFAULT '' COMMENT 'qq号',
  `address_Id` Int unsigned,
  `email` Varchar(50),
  `wechat`  Varchar(50),
  `wechat_qrcode` Varchar(40),
  `avatar`  Varchar(100) COMMENT 'avatar jpg path',
  `User_type` Tinyint COMMENT '1: Normal, 2:Agent, 3:parents, 4:student',
  `reference_Id`  Int unsigned,
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;






