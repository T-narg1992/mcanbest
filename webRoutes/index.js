﻿var express = require('express');
var app = express();
var router = express.Router();
var schools = require('../webControllers/schoolController');
var agents = require('../webControllers/agentController');
var articles = require('../webControllers/articleController');
var home = require('../webControllers/homeController');
var products = require('../webControllers/productController');
var newHome = require('../webControllers/indexController');
var feedback = require('../webControllers/feedbackController');
var wxPay = require('../webControllers/wxPayController');
var mission = require("../webControllers/missionController");
const user_center = require("../webControllers/userController");
const db = require('../db');
var fs = require('fs');


router.get('/home/mission', mission.getMission);

router.get('/home/mission/wechat_auth', mission.wechat_auth);

router.get('/home/about', function(req, res){
    res.render("about/d_about", {});
});
router.get('/home/about_en', function(req, res){
    res.render("about/d_about_en", {});
});
router.get('/home/contact', function(req, res){
    res.render("about/d_contact", {});
});

router.get('/home/achievement', function(req, res){
    res.render("about/d_achievement", {});
});

router.get('/home/staffs', function(req, res){
    res.render("about/d_staffs", {});
});

router.post('/home/test', function(req, res){
    console.log(req);
    res.render("about/d_staffs", {})
});

router.get('/home/logout', user_center.logout);
router.get('/home/follow', function(req, res){
    res.render("home/follow");
});
router.get('/home/usercenter', user_center.userPage);
router.get('/home/userrecommend', user_center.userRecommend);
router.get('/home/usercenter/wechat_auth', user_center.wechat_auth);
router.get('/home/usercenter/wechat_auth_base', user_center.wechat_auth_base);
router.get('/home/usercenter/wechat_subinfo', user_center.wechat_subinfo);
router.get('/home/usercenter/wechat_auth_pay/:id(\\d+)/:oid(\\d+)/:payType', user_center.wechat_auth_pay);
router.get('/home/usercenter/wechat_subinfo_pay', user_center.wechat_subinfo_pay);
router.get('/home/usercenter/wechat_continue_pay', user_center.wechat_continue_pay);
router.get('/home/usercenter/wechat_subscribe_page', user_center.wechat_subscribe_page);
router.get('/home/usercenter/cashflow', user_center.cashPage);
router.get('/home/feedback', feedback.fetchList);
router.get('/home/feedback/:id(\\d+)', feedback.getOne);
router.get('/home/testpay/payproduct', wxPay.productPay);
router.get('/home/testpay/paycallback', wxPay.howtoshare);
router.get('/home/testpay/paycallbackredirect', wxPay.redirectToGaopinShare);
router.get('/product/saveOrder/:pid', wxPay.saveOrder);
router.get('/product/textConfirm/:code/:verify_id', wxPay.textConfirm);
router.get('/home/baidu_verify_76HsmXaWf5.html', function(req, res){
    fs.readFile('www/baidu_verify_76HsmXaWf5.html', 'utf8', function (err,data) {
        if (err) {
            res.render(err);
            return console.log(err);
        }
        res.send(data);
    })
});

router.get('/home/MP_verify_47NF2BSZ09JJUaqU.txt', function(req, res){
    fs.readFile('www/public/weixin/MP_verify_47NF2BSZ09JJUaqU.txt', 'utf8', function (err,data) {
        if (err) {
            res.render(err);
            return console.log(err);
        }
        res.send(data);
    })
});
router.get('/home/test', function(req, res){
        res.render("home/test");
});

router.get('/home/hoOuxdCwQI.txt', function(req, res){ //小程序webview域名白名单
    fs.readFile('www/public/weixin/hoOuxdCwQI.txt', 'utf8', function (err,data) {
        if (err) {
            res.render(err);
            return console.log(err);
        }
        res.send(data);
    })
});

router.get('/home/:child?', newHome.fetchDatas); //has to be last in /home routes section

router.get('/education', schools.cate);
router.get('/education/list/:type(\\d+)/:province?', schools.list);
//router.get('/education/list', schools.list);
router.get('/education/:id(\\d+)', schools.readOne);
router.get('/education/us_cate', function(req, res){
    res.render("schools/us_cate", {

    })
});
router.get('/education/global_cate', function(req, res){
    res.render("schools/global_cate", {

    })
});
router.get('/education/world/:id(\\d+)', schools.appSqOne);
router.get('/education/world/:ranking?/:country?', schools.appSqList);
router.get('/product/:id(\\d+)', products.readOne);
router.get('/product/:id(\\d+)/:group_id?', products.readOne);
router.get('/product/test', products.readTest);
router.get('/product/:id(\\d+)/PDF', products.pdfPage);
router.get('/product/:child?', products.home);
router.get('/product/list/:category(\\d+)', products.getList);
router.get('/product/gaopin/review/:prd_id/:o_id?', products.getGaoPinProduct);
router.get('/product/gaopin/product/:prd_id/:o_id?', products.getGaoPinProductV2);
router.get('/product/gaopin/howtoshare', wxPay.howtoshare);
router.get('/product/gaopin/purchase/:prd_id/:o_id', products.getPaymentPage);
router.get('/product/gaopin/test', wxPay.sendMsg);
router.get('/product/gaopin/share/:o_id', products.getGaoPinSharePage);
router.get('/product/YunYingInfo/:prod_id(\\d+)/:uid(\\d+)/:code(\\d+)', products.getYunYingInfo);
router.get('/product/MP_verify_JkoJAfr0TcNDwQrj.txt', function(req, res){
    fs.readFile('www/public/weixin/MP_verify_JkoJAfr0TcNDwQrj.txt', 'utf8', function (err,data) {
        if (err) {
            res.render(err);
            return console.log(err);
        }
        res.send(data);
    })
});
router.get('/product/MP_verify_47NF2BSZ09JJUaqU.txt', function(req, res){
    fs.readFile('www/public/weixin/MP_verify_47NF2BSZ09JJUaqU.txt', 'utf8', function (err,data) {
        if (err) {
            res.render(err);
            return console.log(err);
        }
        res.send(data);
    })
});

router.get('/article/:refercode(\\d+)?', articles.getList);
router.get('/article/test/34888', articles.testOne);
router.get('/article/:refercode(\\d+)/:id(\\d+)/:taskid?', articles.readOne);
router.get('/article/MP_verify_JkoJAfr0TcNDwQrj.txt', function(req, res){
    fs.readFile('www/public/weixin/MP_verify_JkoJAfr0TcNDwQrj.txt', 'utf8', function (err,data) {
        if (err) {
            res.render(err);
            return console.log(err);
        }
        res.send(data);
    })
});
router.get('/article/MP_verify_47NF2BSZ09JJUaqU.txt', function(req, res){
    fs.readFile('www/public/weixin/MP_verify_47NF2BSZ09JJUaqU.txt', 'utf8', function (err,data) {
        if (err) {
            res.render(err);
            return console.log(err);
        }
        res.send(data);
    })
});
router.get('/article/article_comment/:refercode(\\d+)/:id(\\d+)', articles.comment);
router.get('/agents', agents.list);
router.get('/agents/wx', agents.wx);
router.post('/agents/createNew', agents.createNew);
router.post('/agents/insertNewPoint', agents.insertNewPoint);

module.exports = router;


