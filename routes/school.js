
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');
var http = require('http');
var url = require('url');

router.get('/cities', function (req,res){
  db.get().pool.query(
    "select distinct(city) from school_new WHERE type IN (3,4)",
    function (err, result) {
      if (err) throw err;
      if (result.length > 0) {
       res.json(result);
     }
   });
});

router.get('/:type/:entries_per_page(\\d+)/:page_index(\\d+)', function (req, res) {

 var reqToQuery = function (requestUrl, count, page) {

   var params = [];
   var query = "SELECT en_name, ch_name, national_rank,id,detail_info, city,isPublic,school_category, five_year_rank FROM school_new";
   var urlObj = url.parse(requestUrl, true);

   params.push(parseInt(req.params.type));
   query += " where type = ? ";
       if ( req.params.type != '1' && req.params.type !='2'){ //not 特色，大学
        if(urlObj.query.isPublic){
          params.push(parseInt(urlObj.query.isPublic));
          query += " and isPublic = ? ";
        } 
        if(urlObj.query.province){
          params.push(urlObj.query.province);
          query += " and province = ? ";
        } 
        if(urlObj.query.city){
          params.push(urlObj.query.city);
          query += " and city = ? ";
        } 
      }
      query+= "order by cast(substring_index(national_rank,'/',1) as unsigned) limit " + (page * count) + " , " + count;
      return {query: query, params : params}
    }

    var query = reqToQuery(req.url, req.params.entries_per_page, req.params.page_index);
    if(req.params.type && req.params.entries_per_page && req.params.page_index){

     db.get().pool.query(
      query.query,
      query.params,
      function (err, result) {
        if (err) throw err;
        if(result.length && result.length > 0){
          res.json(result);
        }else{
          res.json("page requested is not available");
        }
      }
      );   
   } else {
    res.sendStatus(404);
  }
});

router.get('/us/:type/:index(\\d+)/:per_page(\\d+)', function (req, res) {

 var reqToQuery = function (requestUrl) {
   var params = [];
   var query = "SELECT id,ch_name,en_name,tag,rank,description FROM us_schools where rank is not null"; //elems and secondaries dont have ranking values
   var urlObj = url.parse(requestUrl, true);

   if(req.params.type && req.params.index && req.params.per_page){
     params.push(parseInt(req.params.type));
     query += " and type = ? ";
     params.push(parseInt(req.params.index));
     params.push(parseInt(req.params.per_page));
     query+= "order by rank asc limit ?,?";
   } else {
    res.sendStatus(404);
  }
  return {query: query, params : params}
}
var query =reqToQuery(req.url);
db.get().pool.query(
  query.query,
  query.params,
  function (err, result) {
    if (err) throw err;
    res.json(result);
  });   

});

router.get('/ap_sq/tags', function (req, res) {
    db.get().pool.query(
      "SELECT count(CONCAT(countryCode, '-', country)) as count,CONCAT(countryCode, '-', country) as country FROM apply_square_schools  group by CONCAT(countryCode, '-', country) order by count desc",
      function (err, results1) {
        if (err) res.sendStatus(404);
        res['countries'] = results1;
        db.get().pool.query("SELECT * FROM apply_square_ranking_map order by display_weight desc", 
          function (err, results2) {
            res['rankings'] = results2;
              res.json({'ranking':res['rankings'], 'countries':res['countries']});
        }) 
        })
});

router.get('/school_standard/:id', function (req, res) {
  if (req.params && req.params.id) {
    db.get().pool.query(
      "SELECT * FROM school_new WHERE id = ?",
      [req.params.id],
      function (err, result) {
        if (err) throw err;
        if (result.length > 0) {
          res.json(result);
        }
      }
      );
  } else {
    res.sendStatus(404);
  }
});

module.exports =router;