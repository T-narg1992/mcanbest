var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var request = require('request');
var db = require('../db');
var fs = require('fs');
var multer  = require('multer');
var uploadQR = multer({ dest: 'uploads/qr' });
var comment= require('./comments');
//select count(cm.row_id), s.* from comment cm right join square s on s.id = cm.row_id where module = 5 group by cm.row_id
var getListDefault = function(req,res,cb){
	var entries_per_page = parseInt(req.params.entries_per_page);
	var page_index = parseInt(req.params.page_index);
	var offset = entries_per_page* page_index;
	var count = entries_per_page;
	db.get().pool.query("select c.*, s.*,m.avatar,m.nickname from (select count(cm.row_id) as commentCount,cm.row_id from comment cm where cm.module = 5 group by cm.row_id) c right outer join square s on s.id = c.row_id left join member m on m.id = s.uid order by created_time desc limit "+ offset + ',' + count,
		function (err, data) {
			if (err) cb(err,null,res);
			cb(null,data,res);
		});
};
var getMyList = function(req,res,cb){
	var entries_per_page = parseInt(req.params.entries_per_page);
	var page_index = parseInt(req.params.page_index);
	var offset = entries_per_page* page_index;
	var count = entries_per_page;
	db.get().pool.query("select c.*, s.*,m.avatar,m.nickname from (select count(cm.row_id) as commentCount,cm.row_id from comment cm where cm.module = 5 group by cm.row_id) c right outer join square s on s.id = c.row_id left join member m on m.id = s.uid where uid = ? order by created_time desc limit "+ offset + ',' + count, [req.params.uid],
		function (err, data) {
			if (err) {cb(err,null,res);}
			cb(null,data,res);
		});
};
var getExpiringList = function(req,res,cb){
	var entries_per_page = parseInt(req.params.entries_per_page);
	var page_index = parseInt(req.params.page_index);
	var offset = entries_per_page* page_index;
	var count = entries_per_page;
	var now = Math.floor(Date.now() / 1000);
	db.get().pool.query("select c.*, s.*,m.avatar,m.nickname from (select count(cm.row_id) as commentCount,cm.row_id from comment cm where cm.module = 5 group by cm.row_id) c right outer join square s on s.id = c.row_id left join member m on m.id = s.uid where deadline > ? order by deadline asc limit "+ offset + ',' + count, [now],
		function (err, data) {
			if (err) cb(err,null,res);
			cb(null,data,res);
		});
};
var getExpiredList = function(req,res,cb){
	var entries_per_page = parseInt(req.params.entries_per_page);
	var page_index = parseInt(req.params.page_index);
	var offset = entries_per_page* page_index;
	var count = entries_per_page;
	var now = Math.floor(Date.now() / 1000);
	db.get().pool.query("select c.*, s.*,m.avatar,m.nickname from (select count(cm.row_id) as commentCount,cm.row_id from comment cm where cm.module = 5 group by cm.row_id) c right outer join square s on s.id = c.row_id left join member m on m.id = s.uid where deadline < ? order by deadline asc limit "+ offset + ',' + count, [now],
		function (err, data) {
			if (err) cb(err,null,res);
			cb(null,data,res);
			
		});
};
var getOne = function (httpreq,httpres,cb) {
	db.get().pool.query("SELECT sq.*, m.nickname, m.avatar from square sq left join member m on m.id = sq.uid where sq.id = ?",[httpreq.params.id],
		function (err, res) { 
			if (err || res.length<1) cb(err,null, httpres);
			cb(null,res,httpres);
		});
};
var postOne = function (httpreq,httpres) {
	if(!httpreq.body.uid || !httpreq.body.title || !httpreq.body.deadline){
		httpres.sendStatus(404);
	}else{
		db.get().pool.query("INSERT INTO square SET ?", httpreq.body, function (err, res) {
			if (err) throw err;
			httpres.status(201);
			httpres.json({code:'success'});
		});
	}
};

var getComments = function (httpreq,cb) {
	request('http://www.liunar.com/api/comments/5/'+httpreq.params.id, function (error, response, body) {
		if (error) cb(error, null);
		if (!error && response.statusCode == 200) {
			cb(null, body);
		}
	});
};
var getDetails = function (req,res) {
	async.parallel([
		function(cb){
			db.get().pool.query("SELECT sq.*, m.nickname, m.avatar from square sq left join member m on m.id = sq.uid where sq.id = ?",[req.params.id],
				function (err, res) {
					if (err || res.length < 1) {
						cb ('no record found',null);
					}else{
						cb(null,res);	
					}
					
				});
		},
		function(cb){
			getComments(req,cb);
		}
		], function (err, dataArray) {
			if (err){
				res.sendStatus(404);
			}else{
				res.json(dataArray);
			}
		});
};



var callback = function(err,res,httpres){
	if(err) {
		console.log(err);
		httpres.sendStatus(404);
	}
	else{
		httpres.json(res);
	}
};

//definitions above

router.post('/post', function (httpreq,httpres) {
	postOne(httpreq, httpres);
});
router.post('/testdelete', function (httpreq,httpres) {
	console.log(JSON.stringify(httpreq.body.id));
});
router.post('/delete', function(httpreq,httpres){
	async.parallel([
		function(cb){
			var id = httpreq.body.id;
			db.get().pool.query("DELETE FROM square where id =?",
				[id], 
				function(err,data){
					if (err) {
						cb(err, null);
					}else{
						cb(null, data);
					}
				});
		},
		function(cb){
			db.get().pool.query('DELETE FROM comment where module = 5 and row_id = ?', [httpreq.body.id], 
				function(err,result){
					if (err) {cb( err, null);
					}else{
						cb(null, result);
					}
				});
		}],
		function(err, cbresult, httpreq){
			if (err) {
				console.log(err);
			}else{
				if(cbresult[0].affectedRows == 1){
					httpres.json({code:1 ,desc:"successfully deleted post and "+ cbresult[1].affectedRows + " attached comments"});
				}else if (cbresult[0].affectedRows == 0) {
					httpres.json({code:0 ,desc:"post doesnt exist or delete failed; please try again"});
				}
			}
		}
		);
});


router.get('/get/:id(\\d+)', function(httpreq, httpres) {
	getDetails(httpreq,httpres);
});

router.get('/get/list/:entries_per_page(\\d+)/:page_index(\\d+)',function(httpreq,httpres){
	getListDefault(httpreq,httpres,callback);
});

router.get('/get/my/:uid(\\d+)/:entries_per_page(\\d+)/:page_index(\\d+)',function(httpreq,httpres){
	getMyList(httpreq,httpres,callback);
});

router.get('/get/expiring/:entries_per_page(\\d+)/:page_index(\\d+)',function(httpreq,httpres){
	getExpiringList(httpreq,httpres, callback);
});
router.get('/get/expired/:entries_per_page(\\d+)/:page_index(\\d+)',function(httpreq,httpres){
	getExpiredList(httpreq,httpres, callback);
});
var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './uploads/qr');
	},
	filename: function (req, file, cb) {
		var uid = req.params.uid;
		console.log(JSON.stringify(file));
		req.serverFileName = 'qr-'+uid+'.jpg';
		cb(null, req.serverFileName);
	}
});
var limits = { fileSize: 1024 * 1024 * 1024 };
var upload = multer({ //multer settings
	storage: storage,
	limits: limits
}).single('file');

router.post('/post/qr/:uid(\\d+)', function (req, res) {
	upload(req, res, function (err) {
		if (err) {
			console.log("upload:" + err);
			res.json({ err: err });
			return;
		}else{
			var relpath='/uploads/qr/'+req.serverFileName;
			res.json({ path: relpath });
		}
	});
});

module.exports = router;