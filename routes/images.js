var express = require('express');
var router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs')
var mysql = require('mysql');

const TO_PATH = '/home/devops/pictures/schools/'; //set absolute path here
const FROM_PATH = '/home/devops'; //pictures relative path starts with /Uploads.., prepend directory to make path complete
var picture = function (index, picid, skoolid , path) {
    this.index= index;
    this.pictureID= picid;
    this.schoolID = skoolid;
}
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '23812531',
  database : 'app'
});

var picture_list= {};
router.get('/dev_data_convert', function(req, response){
    connection.query('SELECT id,pic_array from school', function(err, rows, fields) {
        if (!err){
            for (var i = 0; i < rows.length; i++) {
                console.log(rows[i].pic_array)
                var pictures = JSON.parse(JSON.stringify(rows[i].pic_array.split(',')));
                for (var j = 0; j < pictures.length; j++) {
                    pic = new picture(j, pictures[j], rows[i].id);//adds a index to the images- {0:cover, 1:logo, 2:other image}
                    picture_list[pic.pictureID] = pic;
                }
            }
        }
        else{
            console.log(err);
            connection.end();
        }
    });
    connection.query('SELECT id,path from picture', function(err, rows, fields) {
        if (!err){
            for (var i = 0; i < rows.length; i++) {
                var index = rows[i].id;
                if(picture_list[index] && index !== 0){
                    picture_list[index]['path'] = rows[i].path;
                    mkdirp(TO_PATH + picture_list[index]['schoolID'], function(err) {
                        if(err !== null) console.log(err);
                    });
                    var path = picture_list[index]['path'];
                    var pic_index = picture_list[index]['index'];
                    var school = picture_list[index]['schoolID'];
                    var new_file_path = TO_PATH + school + '/'+ pic_index +'.png';

                    fs.rename(FROM_PATH + path, new_file_path,
                        function(err){
                            if(!err){
                            }else{
                                console.log(err );
                            }
                            response.end();
                        });
                }
            }
        }  else{
            console.log(err);
            connection.end();
        }
    });
});

module.exports = router;
