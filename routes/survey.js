var express = require('express');
var app = express();
var router = express.Router();
var db = require('../db');
var bulkInsertArray = [];
router.post('/submit', function (req, res) {

    var questionArray = req.body.answers.split(',');
    console.log(questionArray);
    for (var i = 0; i < questionArray.length; i++) {
        console.log(i);
        //creates tempArray with : [uid, qid, answer]
        var tempArray = [req.body.uid, questionArray[i], questionArray[i+1]];
        i+=1;

        bulkInsertArray.push(tempArray);
    }
    console.log(bulkInsertArray);
    db.get().pool.query("INSERT INTO survey_user_answer (uid, question_id, answer) VALUES ?", [bulkInsertArray], function(err, rows, fields){
       if (err) throw err;
       res.status(201);
       res.json(rows);
   });
});

module.exports = router;
