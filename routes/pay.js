var express = require('express');
var router = express.Router();
var wxPayController = require("../controllers/wxPayController");
var db = require("../db");

router.post('/wxPayinfo', function (req, res) {
    console.log(req.body);
    res.json(req.body);
});

router.get('/wxPayinfo', function (req, res) {
    console.log(req.query);
    res.json(req.query);
});

var getProductInfo = function (id, callback) {
    db.get().pool.query(
        "SELECT id, deposit, msrp, group_purchase_msrp, title FROM product_new WHERE product_new.id = ?", 
        [parseInt(id)],
        function (err, item) {
            if (err) callback("error");
            if (item.length < 1) callback("error");
            callback(item);
        }
        );
};

router.post('/createPayUrl', function (req, res) {
    var pid = req.body.pid;
    var name = req.body.name;
    var tel = req.body.tel;
    var payType = req.body.paytype;
    var referId = req.body.referId;
    console.log('TOBEPAID:', payType);

    if (payType && pid && name && tel) {
        getProductInfo(pid, function (item) {
            var payment = {"DP": item[0].deposit, "FP": item[0].msrp, "GP":item[0].group_purchase_msrp};
            var addition = {
                type: payType,
                pid: pid,
                who: name,
                tel: tel,
                rid: referId,
                oid: ""
            };
            var order = {
                product_id: item[0].id,
                body: item[0].title,
                attach: JSON.stringify(addition),
                out_trade_no: 'camp' + (+new Date),
                total_fee: parseInt(payment[payType]) * 100,
                spbill_create_ip: req.ip,
                trade_type: 'NATIVE'
            };

            wxPayController.wxPaymentSign(
                order,
                function (payargs) {
                    res.send(payargs);
                }
                );
        });
    } else {
        res.send("缺少必要参数");
    }
})

module.exports = router;