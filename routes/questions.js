var db = require('../db');
var table = require('../table');

var express = require('express');
var router = express.Router();
var mysql = require('mysql');

router.get('/list/:orderby/:limit(\\d+)', function (req, res) {
    var orderby = '';
    if(req.params.orderby == 'newest'){
        orderby = "create_time";
    }else if(req.params.orderby == 'most-answered'){
         orderby = "answer_num";
    }else{
        res.json('nope,nope');
    }
    db.get().pool.query("SELECT q.*, c.title from question q left join category c on c.id=q.category where q.source = '留哪儿' ORDER BY "+ orderby +" desc LIMIT ?;",
        [ parseInt(req.params.limit)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});

router.get('/promo', function (req, res) {
    db.get().pool.query("SELECT * from question WHERE promo_position is not null ORDER BY promo_position desc LIMIT 4",
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});

router.get('/promo/:category(\\d+)/:limit(\\d+)', function (req, res) {
    db.get().pool.query("SELECT * from question WHERE category = ? ORDER BY promo_position desc, create_time desc LIMIT ?",
        [parseInt(req.params.category),parseInt(req.params.limit)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});


router.post('/', function (req, res) {
    req.body.create_time = Date.now()/1000;
    req.body.update_time = Date.now()/1000;
    db.get().pool.query("INSERT INTO question SET ?",
        [req.body],
        function (err, result) {
            if (err) throw err;
            res.json(result.insertId);
        });
});

router.get('/:id(\\d+)', function (req, res) {
    if (req.params && req.params.id) {
        var id = req.params.id;
        db.get().pool.query('SELECT q.*, m.nickname,m.fullname,m.avatar from question q left join member m on m.id = q.uid where q.id = ?', id, function (err, rows, fields) {
            if (err) throw err;
            res.json(rows);
        });
    } else {
        throw "id is required.";
    }
});

router.get('/answers/:qid(\\d+)', function (req, res) {
    if (req.params && req.params.qid) {
        var qid = req.params.qid;
        db.get().pool.query('SELECT a.*, m.nickname,m.fullname,m.avatar from answer a left join member m on m.id = a.uid where question_id = ? order by update_time desc', [qid], function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
    } else {
        throw "qid is required.";
    }
});

module.exports = router;