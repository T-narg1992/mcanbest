var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var db = require('../db');

router.get('/list/:type(\\d+)', function (req, res) {
    db.get().pool.query("SELECT * from goodnews where goodnews.type = ? LIMIT 3", [req.params.type],
        function (err, rows, fields) {
            if (err) throw err;
            res.send(rows);
        });
});

router.get('/list/all/:limit(\\d+)', function (req, res) {
    db.get().pool.query("SELECT * from goodnews LIMIT ?", [req.params.limit],
        function (err, rows, fields) {
            if (err) throw err;
            res.send(rows);
        });
});

module.exports = router;