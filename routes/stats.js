var db = require('../db');
var table = require('../table');
const mission = require("../controllers/taskController");

var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var http = require('http');
var url = require('url');
var async = require('async');
// 文章传播度  individual 
var stats = [];

var callback = function(err,result){
	if(err){
		console.log(err);
	}
	stats.push(result);
};


//达人数
var getCurrentApplications = function(callback){
	db.get().pool.query("Select count(*) as total from member where agentType = 2",
		function (err, rows, fields) {
			if (err) {
				return callback(err, null);
			}
			var currentApplications = {currentApplications:rows[0].total};
			return callback(null, currentApplications);
		});
}
//达人申请数
var getSuccessfulApplications = function(callback){
	db.get().pool.query("Select count(*) as total from member where agentType = 1",
		function (err, rows, fields) {
			if (err) {
				return callback(err, null);
			}
			var getSuccessfulApplications = {getSuccessfulApplications:rows[0].total};
			return callback(null, getSuccessfulApplications);		
		});
}
//pv
var getWholeSitePV = function(callback){
	db.get().pool.query("Select sum(visits) as total from siteStats",
		function (err, rows, fields) {
			if (err) throw err;
			var wholeSitePV = {wholeSitePV:rows[0].total};
			return callback(null, wholeSitePV);
		});

}
// var getItemsPVTotal = function(callback){
// 	db.get().pool.query("select sum(url) as total from siteStats",
// 		function (err, rows, fields) {
// 			if (err) throw err;
// 			var itemsPVTotal= {itemsPVTotal: rows};
// 			callback(null, itemsPVTotal);
// 		});
// 
// }

router.get('/get', function (req, res) {
	async.parallel([
		function(callback){
			getCurrentApplications(callback);	
		},
		function(callback){
			getSuccessfulApplications(callback);
		},
		function(callback){
			getWholeSitePV(callback);
		}],
		function(err, results) {
			if(err){
				console.log(err)
				res.json(err);
			}
			console.log(results)
			res.json(results);
		});
});

router.get('/get/dailyTasks', function (req, res) {
	mission.getMission(
		function(missions){
			if(missions){
				res.json(missions)
			}else{
				res.json({err: "No tasks today!"});
			}
		}
	);
});



router.get('/get/list/urlPV', function (req, res) {
//indivdual url pv
db.get().pool.query("select url ,count(url) as total from siteStats group by url order by count(url) desc limit 10",
	function (err, rows, fields) {
		if (err) throw err;
		res.json(rows);
	});
})

router.get('/get/points/:id(\\d+)', function (req, res) {
//indivdual points
db.get().pool.query("select id, nickname, withdrewCredits, (userIntroPoints + (sharePoints/100)) as credits from member where id = ?", [req.params.id] ,
	function (err, rows, fields) {
		if (err) throw err;
		res.json(rows[0]);
	});
})


router.get('/get/list/sharerPV', function (req, res) {
// 达人PV
db.get().pool.query("Select sharer_id, count(sharer_id) as total from siteStats group by sharer_id order by count(sharer_id) desc limit 10",
	function (err, rows, fields) {
		if (err) throw err;
		res.json(rows);
	});	
})

router.get('/get/list/salePoints', function (req, res) {
// 销售最佳达人 
db.get().pool.query("select id, nickname, salePoints from member order by salePoints desc limit 10",
	function (err, rows, fields) {
		if (err) throw err;
		res.json(rows);
	});
})

router.get('/get/list/sharePoints', function (req, res) {
//分享最佳达人
db.get().pool.query("select id, nickname, sharePoints from member order by sharePoints desc  limit 10",
	function (err, rows, fields) {
		if (err) throw err;
		res.json(rows);
	});
})


module.exports = router;