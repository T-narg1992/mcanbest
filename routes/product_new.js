var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var request = require('request');
var db = require('../db');
var _ = require('underscore');

router.get('/detail/:id(\\d+)', function (req, res) {
	db.get().pool.query("SELECT * from product_new where id = ?", [req.params.id],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});
router.get('/list/:channel(\\d+)', function (req, res) {
	db.get().pool.query("SELECT * from product_new where status = 0 and channel = ? LIMIT 2", [req.params.channel],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/list/all/:limit(\\d+)', function (req, res) {
	db.get().pool.query("SELECT id, country, promo_thumb, title, currency,referral_fee_process_time, referral_fee FROM product_new WHERE status = 0 ORDER BY order_in_list desc, promo_position desc LIMIT ?", [parseInt(req.params.limit)],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});


router.get('/list/promo/:promoGroup(\\d+)', function (req, res) {
	db.get().pool.query("SELECT id, promo_thumb from product_new WHERE status = 0 and promo_position is not null and promo_position > 0 and promo_group = ? ORDER BY promo_position desc", [parseInt(req.params.promoGroup)],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/list/promo/:promoGroup(\\d+)', function (req, res) {
	db.get().pool.query("SELECT id, promo_thumb from product_new WHERE status = 0 and promo_position is not null and promo_position > 0 and promo_group = ? ORDER BY promo_position desc", [parseInt(req.params.promoGroup)],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/list/promo/all', function (req, res) {
	db.get().pool.query("SELECT id, promo_thumb,promo_group, currency from product_new WHERE status = 0 and promo_position is not null and promo_position > 0 ORDER BY promo_position desc",
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});
router.get('/list/country/:country', function(req, res){
	db.get().pool.query(
		"SELECT id, country, promo_thumb FROM product_new WHERE country = ?",
		[req.params.country],
		function(err, result, fields){
			if (err) throw err;
			res.send(result);
		}
		);
});
router.get('/liunarID/:yunId', function(req, res){
	db.get().pool.query(
		"SELECT id FROM member WHERE yunId = ?",
		[req.params.yunId],
		function(err, result, fields){
			if (err) throw err;
			res.send(result);
		}
		);
});

router.get('/authorizeClient/:uid(\\d+)/:code(\\d+)?', function (req, res) {
	if(!req.params.code){// no refer code given, check db for member reference_code, if exists, continue to request cms api for permissions; else return false or error msg;
		db.get().pool.query(
			"SELECT id, reference_code from member WHERE id = ?",
			[req.params.uid],
			function(err, result, fields){

				if (err) throw err;
				if ( !result[0]) {
					res.json({code:false,reason:'Member id not found'});
				}
				if(!result[0].reference_code){
					res.json({code:false,reason:'Member refcode not found'});
				}
				if(result[0].reference_code){
					request('http://yun.liunar.com/Agent/boolCheckProductsViewPermission?refcode='+result[0].reference_code, function (error, response, body) {
						if (!error) {
							if(body.toString() == 'true'){
								res.json({code:'true'});	
							}else{
								res.json({code:'false', reason:"matching refer code not found"});	
							}
						}else{
							res.json(error);
						}
					});
				}
			});
	}else{
//		console.log("with refer code");
request('http://yun.liunar.com/Agent/boolCheckProductsViewPermission?refcode='+req.params.code, function (error, response, body) {

//			console.log(error, response.statusCode, "body:"+body);
if(body == 'true'){
	res.json({code:'true'});	
}else{
	res.json({code:'false', reason:"matching refer code not found"});	
}

});
}

});

var getMoreProductDetails = function(httpreq, httpres){
	async.parallel([
		function(callback){ 
			db.get().pool.query(
				"SELECT id,title, commission,referral_fee_process_time,sale_instruction,market_instruction from product_new where id = ?",
				[httpreq.params.prod_id],
				function(err, productresult){
					//console.log(err, productresult);
					if (err) callback(err,null);
					callback(null,productresult);
				}
				);
		},
		function(callback){ 
			db.get().pool.query(
				"SELECT title from attachment where row_id = ? and module= 4 and type = 2",
				[httpreq.params.prod_id],
				function(err, attachmentresult){
					//console.log(err, attachmentresult);
					if (err) callback(err, null);
					callback(null,attachmentresult);
				}
				);
		}
		], function (err, result) {
			if (err) res.sendStatus(404);
			httpres.json(result);
		});
};

router.get('/get/memberOnlyDetails/:prod_id(\\d+)/:uid(\\d+)/:code(\\d+)?', function(req, res){
	var params='/'+req.params.uid;
	if (req.params.code){
		params += '/'+req.params.code;
	}
	request('http://www.liunar.com/api/productnew/authorizeClient'+params, function (error, response, body) {
		if( response.statusCode== 200){
			var body = JSON.parse(body);
			if(body.code != 'true'){
				res.json({code:'false', reason:"matching refer code not found"});
			}else{
				getMoreProductDetails(req,res);
			}
		}else{
			res.json({code:'false', reason:error.toString()});	
		}
	});
});
var product_map = { 0:"冬夏令营", 1:"背景提升", 2:"中小学", 3:"特色学院", 4:"知名大学"}

var getPrdIdList = function(list, cb){
	console.log('filter activated');
	console.log(list);
	if(list.length < 1) cb(null, list);
	db.get().pool.query("select row_id from contentTags where module = 4 and tagId in (?)",
		[list],
		function (err, data) {
			if (err) throw err;
			var listIds = [];
			for (var i = 0; i < data.length; i++) {
				listIds.push(data[i].row_id)
			}
			cb(null, listIds);
		});
}
router.post('/list', function(req,res){
	console.log(req.body);
	console.log('got request',req.body.json);
	var query;
	var params;
	var json;
	if(req.body.json){ 
		json = JSON.parse(req.body.json);
		async.map(json, getPrdIdList, function(err,result){
			if (err) {
				console.log(err);
				return;
			}
			var fullMembersOnly = [];
			for(i=0;i<result.length;i++){
				fullMembersOnly.push(result[i]);
			}
			var intersection =_.intersection.apply(_, fullMembersOnly);
			if(intersection.length > 0){
				console.log('intersection:',intersection)
				query = "select id,title,duration,msrp, description, period,country, tags,currency ,price ,age_min,age_max, promo_thumb,promo_position from product_new where type = ? and promo_group is not null and id in (?) order by promo_position desc limit ?,?";
				params = [req.body.type, intersection, parseInt(req.body.index), parseInt(req.body.count)];
				db.get().pool.query(query, params,
					function (err, data) {
						if (err) throw err;
						if(data.length == 0){
							data = {err:"off the shelf product found"};
						}
						res.json(data);
					});
			}else{
				console.log('no intersection');
				res.json({err: 'no intersection'});
			}

		});
	}else{
		console.log('no filters used');
		query = "select id,title,duration, description, msrp, period,country,currency, price,tags,age_min,age_max, promo_thumb,promo_position from product_new where type = ? and promo_group is not null order by promo_position desc limit ?, ?";
		params = [req.body.type, parseInt(req.body.index), parseInt(req.body.count)];
		db.get().pool.query(query, params,
			function (err, data) {
				if (err) throw err;
				res.json(data);
			});
	};
});

router.get('/filter/:type(\\d+)',function(req,res){
	var master = {};
	var query = 'select distinct(tagid),tag, category, tagCategories.id as cateId from contentTags right join tags on id = tagid join tagCategories on tagCategories.id =tagCategory join product_new on row_id = product_new.id where module = 4 and promo_position is not null and type = ?';
	var params = [product_map[req.params.type]];
	db.get().pool.query(query, params, function (err, data) {
		if (err) throw err;
		for(var i = 0; i<data.length; i++){
			tagObj = {'tagString':data[i].tag, 'tagid': data[i].tagid, 'cateid': data[i].cateId}
			if (!master[data[i].category]){
				master[data[i].category] = [{'tagString':'不限', 'tagid':0, 'cateid': data[i].cateId}];
			}
			master[data[i].category].push(tagObj);
		}
		console.log(params);
		res.json(master);
	});
})
module.exports = router;