var db = require('../db');
var express = require('express');
var router = express.Router();
var mysql = require('mysql');



router.get('/:type/:id(\\d+)', function (req, res) {
    db.get().pool.query("select comment.*, member.avatar, member.nickname from comment LEFT JOIN member ON member.id = comment.uid where (module = ? AND row_id = ?) ORDER BY comment.create_time DESC",
        [req.params.type, req.params.id],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});

router.post('/', function (req, res) {
    req.body.create_time = Math.floor(Date.now() / 1000);
    db.get().pool.query("INSERT INTO comment SET ?", req.body, function (err, rows, fields) {
        if (err) throw err;
        res.status(201);
        res.json(rows);
    });
});

module.exports = router;