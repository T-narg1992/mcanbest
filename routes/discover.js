var db = require('../db');
var express = require('express');
var router = express.Router();
var mysql = require('mysql');

router.get('/list/:limit(\\d+)', function (req, res) {
    db.get().pool.query("SELECT id, title, description,'3' as channel, create_time, '0' as thumbnail, 'question' as category FROM o2c0_question UNION ALL SELECT id, title, description, channel, create_time,thumbnail, category FROM article ORDER BY create_time desc limit ?",[parseInt(req.params.limit)],function (err, rows, fields) {
        if (err) throw err;
        res.status(200);
        res.json(rows);
    });
});

module.exports = router;