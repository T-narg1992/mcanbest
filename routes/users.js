var express = require('express');
var router = express.Router();
var request = require("request");
var rp = require('request-promise');
var memberCtrl = require('../controllers/memberController');
var busboy = require('connect-busboy');
var path = require('path');
var fs = require('fs-extra');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var multer = require('multer');
var jwt = require('jsonwebtoken');
var mysql = require('mysql');
var db = require('../db');
var table = require('../table');

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        req.serverFileName = req.userId + "-" + file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];
        cb(null, req.serverFileName);
    }
});

var limits = { fileSize: 1024 * 1024 * 1024 }
var upload = multer({ //multer settings
    storage: storage,
    limits: limits
}).single('file');



router.get('/', function (req, res) {
    var wechat = {
        "openid": "OPENID",
        "nickname": "NICKNAM2E",
        "sex": 1,
        "province": "PROVINCE",
        "city": "CITY",
        "country": "COUNTRY",
        "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
        "privilege": [
        "PRIVILEGE1",
        "PRIVILEGE2"
        ],
        "unionid": " 1_bmasc10rtdstaid62sgVi7hMZOPf3",
        "introId": 1239
    };
    memberCtrl.createNew(wechat, function (err, id) {
        //response.end(res);
        wechat.uid = id;
        res.json(wechat);

    });

    db.get().pool.query(
        "SELECT * FROM member WHERE introducer is not null",
        function (err, result) {
            if (err) {
                console.error("createNew-Err:" + err);
            }
            if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {
                    var introId = result[i].introducer;
                    var uId = result[i].id;
                    (function (introId, uId) {
                        db.get().pool.query(
                            "SELECT * FROM memberPoints WHERE uid = ? and clientId = ?",
                            [introId, uId],
                            function (err2, result2) {
                                if (result2.length > 0) {
                                    console.log("found:" + result2);
                                } else {
                                    insertNewPointHistory(uId, introId, printError);
                                }
                            });
                    }
                    )(introId, uId);
                }
                //updateMemberInfo(result, wechatResponse, callback);
                //console.log("update-c:");
            }
        }
        );



    // res.json(m);

});
function printError(err, result) {
    console.log("err" + err + "res:" + result.insertId + "**" + result.introducer);
}
function insertNewPointHistory(insertId, introducer, waterfallCallback) {

    var newPointHistory = {
        uid: introducer,
        points: table.introUserPoints,
        pointType: 1,
        clientId: insertId
    }
    var sql = 'INSERT INTO memberPoints SET ? ';
    //var values = [wechatResponse.nickname, 'qq', wechatResponse.unionid,1,1,1,1,1];
    db.get().pool.query(sql, newPointHistory, function (err) {
        if (err) {
            console.error(err);
        }
        console.log("insert.... b");
        waterfallCallback(err, { insertId: insertId, introducer: introducer });
    });
}
var post = function (req, res) {
    var book = new Book(req.body);
    if (req.body.title) {
        console.log(book);
        book.save();
        res.status(201);
        res.send(book);
    } else {
        res.status(400);
        res.send('Title is required');
    }

}

router.post('/user/profile/:uid', function (req, res) {
    var user = {
        uid: req.params.uid,
        fullname: req.body.fullname,
        phone: req.body.phone,
        email: req.body.email,
        slogan: req.body.slogan || " ",
        wechat_Id: req.body.wechat_Id || " ",
        agentType: req.body.agentType || 2,
        introducer: req.body.introducer || 0
        //var wechat_qrcode = "ss";
    };
    memberCtrl.updateProfile(user, function (err, userData) {
        res.status(200);
        res.json({ err: err, data: userData });
    });
});

router.post('/user/upload/:type/:uid', function (req, res) {
    req.userId = req.params.uid;
    var type = req.params.type;
    upload(req, res, function (err) {
        if (err) {
            console.log("upload:" + err);
            res.json({ err: err });
            return;
        }
        if (type === "2") {
            var user = {
                nation_Id: req.serverFileName,
                uid: req.userId
            }

            memberCtrl.updateNationId(user, function (err, userData) {
                console.log("update nationId:" + err);
                res.status(200);
                res.json({ err: err, file: req.serverFileName });
            });
        } else {
            res.json({ file: req.serverFileName });
        } //console.log(req.serverFileName);
    })
    /*
    var file = req.files.file;
    if (file) {
        console.log(file.name);
        console.log(file.type);
        var stream = fs.createWriteStream(__dirname + '/upload/' + file.name);
        file.pipe(stream);
        stream.on('close', function () {
            console.log('File ' + filename + ' is uploaded');
            res.json({
                filename: filename
            });
        });
    }
    /*
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
        var stream = fs.createWriteStream(__dirname + '/upload/' + filename);
        file.pipe(stream);
        stream.on('close', function () {
            console.log('File ' + filename + ' is uploaded');
            res.json({
                filename: filename
            });
        });
    });*/
});


router.get('/user/clients/:uid', function (req, res) {
    if (req.params && req.params.uid) {
        var uid = req.params.uid;
        memberCtrl.clients(uid, function (err, userData) {
            res.status(200);
            if (err) {
                res.json({ err: err });
            } else {
                res.json(userData);
            }

        });
    }
}
);
router.get('/user/referees/:uid', function (req, res) {
    if (req.params && req.params.uid) {
        var uid = req.params.uid;
        memberCtrl.referees(uid, function (err, userData) {
            res.status(200);
            if (err) {
                res.json({ err: err });
            } else {
                res.json(userData);
            }
        });
    }
}
);
router.get('/user/pointSummary/:uid', function (req, res) {
    if (req.params && req.params.uid) {
        var uid = req.params.uid;
        memberCtrl.pointSummary(uid, function (userData) {
            res.status(200);
            res.json(userData);
        });
    }
}
);
router.get('/order/:unionid/:productid', function (req, res) {
    res.status(200);

    if (req.params && req.params.unionid && req.params.productid) {
            // console.log("found:" + req.params.unionid + req.params.productid);
            db.get().pool.query(
                "select * from memberOrders join member on memberOrders.open_id = member.open_id where productid = ? and member.wechat = ? and orderStatus = 5 limit 1",
                [parseInt(req.params.productid), req.params.unionid],
                function (err, result) {
                    if (result.length > 0) {
                        //console.log("found:" + result);
                        res.json({ "err": "", data : result });
                    } else {
                        res.json({ "err": "no order record found" });
                    }
                });
        }else{
            res.json({ "err": "unionid and productid are required." });
        }
    }
);

router.get('/user/exchangeRate', function (req, res) {
    res.status(200);
    res.json({ "rate": 10 });
    //if (req.params && req.params.uid) {
    //    var uid = req.params.uid;
    //    memberCtrl.pointSummary(uid, function (userData) {
    //        res.status(200);
    //        res.json(userData);
    //    });
    //}
}
);
router.get('/user/points/:uid', function (req, res) {
    if (req.params && req.params.uid) {
        var uid = req.params.uid;
        memberCtrl.points(uid, function (userData) {
            res.status(200);
            res.json(userData);
        });
    }
}
);

router.get('/user/:uid', function (req, res) {
    if (req.params && req.params.uid) {
        var uid = req.params.uid;
        memberCtrl.readOne(uid, function (userData) {
            res.status(200);
            res.json(userData);
        });
    }
}
);
router.get('/user/getByReferCode/:code', function (req, res) {
    var code = req.params.code;
    memberCtrl.getIdByReferenceCode(code, function (err, user) {
        if (err) {
            res.status(200);
            res.json({ err: err });
        } else {
            res.status(200);
            res.json(user);
        }
    });

});
router.get('/user/getReferCode/:uid', function (req, res) {
    memberCtrl.getReferenceCode(req.params.uid, function (err, user) {
        if (err) {
            res.status(200);
            res.json({ err: err });
        } else {
            res.status(200);
            res.json(user);
        }
    });

});
router.get('/user/orders/:uid', function (req, res) {
    var uid = req.params.uid;
    memberCtrl.getMemberOrders(uid, function (err, data) {
        if (err) {
            res.status(200);
            res.json({ err: err });
        } else {
            res.status(200);
            res.json(data);
        }
    });

});
router.post('/user/neworder/:uid/:pid', function (req, res) {
    var newOrder = {
        uid: req.params.uid,
        pid: req.params.pid,
        ref_code: req.body.ref_code,
        fullname: req.body.fullname,
        phone: req.body.phone,
        agentId: req.body.agentId
    };
    memberCtrl.insertNewOrder(newOrder, function (err, data) {
        if (err) {
            res.status(200);
            res.json({ err: err });
        } else {
            res.status(200);
            res.json(data);
        }
    });

});
router.get('/user/memo/:orderid', function (req, res) {
    var id = req.params.orderid;
    memberCtrl.getMemo(id, function (err, data) {
        res.status(200);
        res.json({ err: err, data: data });
    });

});
router.get('/wx/:code/:uid', function (req, res, next) {

    if (req.params && req.params.code) {
        var code = req.params.code;
        var url1 = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx0933bfa899eedcaf&secret=adcf9376ed914f531b13a395c1ae3355&code=" + code + "&grant_type=authorization_code";
        //var url1 = "http://localhost:8080/products/code/" + code;
        rp(url1).then(response => {
            console.log("get access code");
            var accessTokenResult = JSON.parse(response);
            var accessToken = accessTokenResult.access_token;
            var refreshToken = accessTokenResult.refresh_token;
            var openId = accessTokenResult.openid;
            var url2 = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=wx0933bfa899eedcaf&grant_type=refresh_token&refresh_token=" + refreshToken;
            //var url2 = "https://api.weixin.qq.com/sns/auth?access_token=" + accessToken + "&openid=" + openId;

            return rp(url2);
        }).then(response => {
            console.log("get access code step 2");
            var accessTokenResult = JSON.parse(response);
            var accessToken = accessTokenResult.access_token;
            var refreshToken = accessTokenResult.refresh_token;
            var openId = accessTokenResult.openid;
            var url4 = "https://api.weixin.qq.com/sns/userinfo?access_token=" + accessToken + "&openid=" + openId;
            //var url3 = "http://localhost:8080/products/code3/ee";
            //console.log(url3);
            console.log("return:" + code + ":" + url4.errcode + url4.errmsg + "--" + accessToken + "-" + openId);
            return rp(url4);
        })
        .then(responseRaw => {
                // do stuff after all requests
                // res.json(response);
                console.log("return 2 raw:" + responseRaw);
                var response = JSON.parse(responseRaw);
                console.log("return 2:" + code + ":<" + response + ">-->" + response.errcode + response.errmsg + response.nickname + "unionid:" + response.unionid + ":" + response.openid + "--" + response.sex);
                console.log("referid:" + req.params.uid);
                if (response.unionid) {
                    var bytelike = unescape(encodeURIComponent(response.nickname));
                    response.nickname = decodeURIComponent(escape(bytelike)).replace(/[\uFF00-\uFFFF]/g, '');
                    console.log("new name:" + response.nickname);
                    if (req.params.uid) {
                        response.introId = req.params.uid;
                    } else {
                        response.introId = 0;
                    }
                    if (req.body.uid) {
                        response.introId = parseInt(req.body.uid);
                    }
                    memberCtrl.createNew(response, function (err, id) {
                        response.uid = id;
                        response.err = err;
                        console.log("membCtrl.createNew" + response + response.uid);
                        var token = jwt.sign({data:response.uid}, req.app.get('secret'), {
                            expiresIn: '365d'
                        });
                        response.token = token;
                        res.json(response);
                    });
                } else {

                    response.err = "unionid is null";
                    res.json(response);
                }
                console.log(code + ":<");


            })
        .catch(err => {
            console.log(err);
            res.json({ err: err});

        }
            ); // Don't forget to catch errors

    }
});

router.get('/weixin/', function (req, res) {
    if (req.params && req.params.url) {

    }

});

module.exports = router;