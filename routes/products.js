var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var config = require('../config')['development'];
var crypto = require("crypto");

var db = require('../db');

router.get('/list/:channel(\\d+)', function (req, res) {
	db.get().pool.query("SELECT * from product where status = 0 and channel = ? LIMIT 2", [req.params.channel],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/list/all/:limit(\\d+)', function (req, res) {
	db.get().pool.query("SELECT id, country, promo_thumb, title, referral_fee_process_time, referral_fee FROM product WHERE status = 0 ORDER BY order_in_list desc, promo_position desc LIMIT ?", [parseInt(req.params.limit)],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});


router.get('/list/promo/:promoGroup(\\d+)', function (req, res) {
	db.get().pool.query("SELECT id, promo_thumb from product WHERE status = 0 and promo_position is not null and promo_position > 0 and promo_group = ? ORDER BY promo_position desc", [parseInt(req.params.promoGroup)],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/list/promo/:promoGroup(\\d+)', function (req, res) {
	db.get().pool.query("SELECT id, promo_thumb from product WHERE status = 0 and promo_position is not null and promo_position > 0 and promo_group = ? ORDER BY promo_position desc", [parseInt(req.params.promoGroup)],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/list/promo/all', function (req, res) {
	db.get().pool.query("SELECT id, promo_thumb,promo_group from product WHERE status = 0 and promo_position is not null and promo_position > 0 ORDER BY promo_position desc",
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/:id(\\d+)', function (req, res) {
	db.get().pool.query("SELECT * from product where id = ?", [req.params.id],
		function (err, rows, fields) {
			if (err) throw err;
			res.send(rows);
		});
});

router.get('/list/country/:country', function(req, res){
	db.get().pool.query(
		"SELECT id, country, promo_thumb FROM product WHERE country = ?",
		[req.params.country],
		function(err, result, fields){
			if (err) throw err;
			res.send(result);
		}
		);
});

router.get('/cms/autocomplete', function (req, serverres) {
	async.parallel([
		function(){ db.get().pool.query(
			"SELECT distinct(provider) FROM product",
			[req.params.country],
			function(err, result, fields){
				if (err) callback(err);
				callback(null,result)
			}
			); },
		function(){ 
			db.get().pool.query(
				"SELECT distinct(currency) FROM product",
				[req.params.country],
				function(err, result, fields){
					if (err) callback(err);
					callback(null,result)
				}
				); },
			function(){ 
				db.get().pool.query(
					"SELECT distinct(country) FROM product",
					[req.params.country],
					function(err, result, fields){
						if (err) callback(err);
						callback(null,result)
					}
					);  }
				], function(err, resfinal){
					if (err) throw err;
					serverres.send(resfinal);
				});
});

router.get('/code/:prd_id(\\d+)/:n(\\d+)?', function(req, res){
	var code = crypto.randomBytes(5).toString('hex');
	console.log(code);
	var n = parseInt(req.params.n);	
	var codes = []; 
	if( n > 0 ){
		while(codes.length < n){
			var code = (Math.random() + 1).toString(36).substring(7);
			if(codes.indexOf())
			codes.push('')
		}
	}
	res.send(code);
});


module.exports = router;