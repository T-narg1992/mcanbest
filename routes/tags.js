var db = require('../db');
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var bodyParser = require('body-parser');


router.get('/mylist/:uid(\\d+)', function (req, res) {
    db.get().pool.query("SELECT ut.*, t.tag from userTags ut join tags t on ut.tag_id = t.id where uid = ?",
        [ parseInt(req.params.uid)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});


router.get('/count/:tagid(\\d+)', function (req, res) {
    db.get().pool.query("SELECT count(uid) as userCount from userTags where tag_id = ? group by tag_id",
        [ parseInt(req.params.tagid)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows[0]);
        });
});

router.post('/follow', function (req, res) {
    db.get().pool.query("INSERT INTO userTags VALUES ('?','?');", [parseInt(req.body.tagid), parseInt(req.body.uid)],
       function (err, rows, fields) {
        try {
            res.status(201);
            res.json(rows.affectedRows);
        } catch (err) {
            res.json(err);        
        } 
    });
});

router.post('/unfollow', function (req, res) {
    var query ="delete from userTags where uid = ? and tag_id = ?";
    db.get().pool.query(query,
       [parseInt(req.body.uid), parseInt(req.body.tagid)],
       function (err, rows, fields) {
           try {
            res.status(200);
            res.json(rows.affectedRows);
        } catch (err) {
            res.json(err);        
        } 
    }); 
});
module.exports = router;