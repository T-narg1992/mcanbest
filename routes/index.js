var express = require('express');
var app = express();
var router = express.Router();
var schools = require('../controllers/schoolController');
var reviews = require('../controllers/reviewController');
var wechat_cfg = require('../config/wechatConfig');
var http = require('http');
var cache = require('memory-cache');
var sha1 = require('sha1'); //签名算法
//var url = require('url');
var signature = require('../controllers/wechatSignature');

router.post('/schools/:schoolid/reviews', reviews.reviewsCreate);
router.get('/schools/:schoolid/reviews', reviews.readAll);


router.get('/reviews/:reviewid', reviews.readOne);
router.put('/reviews/:reviewid', reviews.updateOne);
//router.post('/schools/:schoolid/reviews/:reviewid', reviews.updateOne);
router.delete('/reviews/:reviewid', reviews.deleteOne);



router.post('/wechatSign',function(req,res){
		//var url = req.protocol + '://' + req.host + req.path;
		var url = req.body.url; //获取当前url
       console.log("get url->:" + url);
       signature.sign(url,req.app, function (signatureMap){
        signatureMap.appId = wechat_cfg.appid;
        res.status(200);
        res.json(signatureMap);
    });
   });

/*

*/

module.exports = router;
