var db = require('../db');
var table = require('../table');
var express = require('express');
var router = express.Router();

//get likes by uid
router.get('/:uid(\\d+)', function (req, res) {
	db.get().pool.query("SELECT * FROM likes WHERE uid = ? ", [parseInt(req.params.uid)],
		function (err, rows, fields) {
			if (err) throw err;
			res.status(200);
			res.json(rows);
		});
});

router.post('/like', function (req, res) {
	console.log(req.body);
	req.body.create_time = Math.floor(Date.now() / 1000);
	db.get().pool.query("INSERT INTO likes SET ?", req.body, function (err, rows, fields) {
		if (err) throw err;
		res.status(201);
		res.json(rows);
	});
});
router.get('/total/:module(\\d+)/:row_id(\\d+)', function (req, res) {

    db.get().pool.query("SELECT count(*) as likes FROM likes WHERE module = ? and row_id = ?",
        [parseInt(req.params.module),parseInt(req.params.row_id)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});
router.post('/unlike', function (req, res) {
	console.log(req.body);
	db.get().pool.query('DELETE FROM likes WHERE module = ? AND row_id=? AND uid=?', [req.body.module,req.body.row_id,req.body.uid], function (err, rows, fields) {
		if (err) throw err;
		res.status(201);
		res.json(rows);
	});
});
module.exports = router;

