var express = require('express');
var router = express.Router();
var pictureGernate = require('../posterGernate/pictureGernateController');

router.post('/create', function (req, res) {
	var content = req.body;
	console.log(content);
	if(content.author && content.body){
		pictureGernate.createPNG(content.author, content.avatar, content.type, content.title, content.body, content.qrcode, content.expire_time, function(data){
			if(data.err){
				res.json({"err": data.err});
			}else{
				res.json({"img": data});
			} 
		});
	}else{
		res.json({"err": "Message can't be blank."});
	}	
})

module.exports = {
	router: router,
}