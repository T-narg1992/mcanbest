var db = require('../db');
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var http = require('http');
var url = require('url');
var async = require('async');

router.get('/view/:id', function (req,res) {
   db.get().pool.query("update article set view_count = view_count + 1 where id = ?",
      [parseInt(req.params.id)], 
      function (err, rows, fields) {
        if (err) throw err;
        res.status(200);
        res.json(rows);
    });
})

router.get('/promo', function (req, res) {
    db.get().pool.query("SELECT * FROM article WHERE promo_position is not null and channel < 3 and special_event = 0 ORDER BY promo_position desc, create_time desc LIMIT 4",
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});


router.get('/placeholders/detail/:id(\\d+)', function (req, res) {
    db.get().pool.query("SELECT * from placeholder Where id=?",
        [parseInt(req.params.id)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});


router.get('/placeholders/:promoGroup(\\d+)', function (req, res) {
    db.get().pool.query("SELECT * from placeholder WHERE promo_group = ? ORDER BY promo_position desc LIMIT 6",
        [parseInt(req.params.promoGroup)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});

router.get('/list/huma/:limit(\\d+)', function (req, res) {
    db.get().pool.query("SELECT * from article WHERE channel = 2 ORDER BY create_time desc LIMIT ?", [parseInt(req.params.limit)],
        function (err, rows, fields) {
            if (err) throw err;
            res.status(200);
            res.json(rows);
        });
});
router.get('/list/:entries_per_page(\\d+)/:page_index(\\d+)', function (req, res) {
    var startIndex = req.params.entries_per_page *  req.params.page_index;
    db.get().pool.query("SELECT uid, site, create_time, update_time, description, promo_thumb, have_thumbnail, title,view_count, id, thumbnail from article WHERE publish_status = 0 AND special_event = 0 AND ((site <> 1 OR site IS NULL) OR ((site = 1 OR site is NULL) and status = 1 and submit_for_toutiao = 0)) ORDER BY promo_position desc, create_time desc limit " + parseInt(startIndex) + ", ?",
        [parseInt(req.params.entries_per_page)],
        function (err, result, fields) {
            if (err) throw err;
            res.status(200);
            res.json(result);
        });
});

router.get('/events/', function(req, res){
    db.get().pool.query(
        "SELECT special_event, effective_time, expire_time, create_time, update_time, promo_thumb, have_thumbnail, title, view_count, id, thumbnail FROM article WHERE special_event = 1 ORDER BY update_time DESC LIMIT 4",
        function(err, result){
            if (err) throw err;
            res.status(200);
            res.json(result);
        }
        );
});

router.get('/events/all/:count(\\d+)/:index(\\d+)', function(req, res){
    var startIndex = req.params.count * req.params.index;
    db.get().pool.query(
        "SELECT special_event, effective_time, expire_time, create_time, update_time, promo_thumb, have_thumbnail, title, view_count, id, thumbnail FROM article WHERE special_event = 1 ORDER BY update_time DESC LIMIT " + parseInt(startIndex) + ", ?",
        [parseInt(req.params.count)],
        function(err, result){
            if(err) throw err;
            res.status(200);
            res.json(result);
        }
        );
});

var getArticleTopicTag = function(id){
 return function(callback){
    db.get().pool.query("SELECT * from article WHERE article.id = ?",
        [parseInt(id)],
        function (err, rows, fields) {
            if (err){ 
                callback(err,null)
            }else if(rows.length == 0){
                callback('No article found with this id',null);
            }else{  
                callback(null, rows[0]);
            };            
        });
};
}

router.get('/coverInfo/:id(\\d+)', function (req, serverres) {
   db.get().pool.query("SELECT id, title, thumbnail, view_count from article WHERE article.id = ?",
    [parseInt(req.params.id)],
    function (err, rows, fields) {
        if (err) throw err;
        serverres.json(rows[0]);
    });
});

//fix this waterfall
var getArticleWithTopicTag = function(cb1resp,callback){
    var query='';
    if (cb1resp.hasTopicTag == 0){
        callback(cb1resp);
    }else{
        query = "select tagId, tag from contentTags ct left join tags on ct.tagId = tags.id where module = 1 and row_id = ? and tagCategory = 18 ";
        db.get().pool.query(query,
            [parseInt(cb1resp.id)],
            function (err, rows, fields) {
                cb1resp.topicTags =JSON.stringify(rows);
                callback(cb1resp);
            });
    }
}

router.get('/detail/:id(\\d+)', function (req, serverres) {
    async.waterfall([
        getArticleTopicTag(req.params.id),
        getArticleWithTopicTag
        ], function (error,res) {
            if (error) {
                serverres.json(error)
            }else{
                serverres.json(res)
            }
        });
});



module.exports = router;