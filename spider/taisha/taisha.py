#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re

sys.path.append('/home/janie/spider/common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS
from newsitem import NewsItem
from util import removeContentInTag

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds 
socket.setdefaulttimeout(timeout) 

#socket.setdefaulttimeout(5.0)
#global variables defined here
spider_name = 'liunar'
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A'
output_file = '/home/janie/spider/taisha/taisha.json'

jsonfile=open('/home/janie/spider/taisha/fetchedURLs.json')
fetchedURL = json.load(jsonfile)
jsonfile.close()
#fetchedURL = []

allowed_domain = [
        "taisha.org"
    ]

content_length_limit = 100

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def isAllowedChannel(self, url):
        if ( url.find('/canada/') < 0 ):
            return False
        if ( not (url.endswith('.html')) ):
            return False
        return True
    
    def getLinks(self,page,page_url):
        links = []
        parser = LinkExtract()
        parser.feed(page)

        parser.urls = set(parser.urls)

        for eachlink in parser.urls:
            if ( eachlink.rfind('#') >= 0 or eachlink.find('taisha.org/canada') <= 0 ):
                continue
            if not eachlink.startswith('http://'):
                eachlink = urlparse.urljoin(page_url,eachlink)
            if not self.isAllowedDomain(eachlink):
                continue
            if not self.isAllowedChannel(eachlink):
                continue
            links.append(eachlink)

        return links

    def getHTMLContent(self,content):
        start_tag = "<div class=\"txt_content\">"
        end_tag = "<p style=\"font-stretch: normal;\"><span style=\"FONT-SIZE: 14px\">"

        start = content.find(start_tag)
        if start == -1:
            return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            end_tag = "<div class=\"page\">"
            end = content.find(end_tag)
            if end ==-1:
                return ''

        origin_content = content[start:end]

        return removeContentInTag(origin_content)

    def parsePage(self,page,page_url):# here to get useful info section
                
        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)  
        
        r = dom.xpath("//div[@class='txt_title']/h2")
        v.title = r[0].text.encode('utf8')

        r = dom.xpath("//div[@class='txt_title']//span/text()")
        v.create_time = r[0].replace('互联网', '').replace('来源：', '').strip().encode('utf8')

        crumbs = ''
        r = dom.xpath("//article[@id='nav_location']//*")
        #for i in range(1, len(r)):
        crumbs =  crumbs+ r[len(r)-1].text.encode('utf8')
        v.category = crumbs
        
        contents = self.getHTMLContent(page).strip()
	    contents = contents.split(u'【太傻留学热门专题推荐】'.encode('utf-8'))[:-1]
        v.content = ''.join(contents).encode('utf8')
        v.textContent = v.content.replace('<p>', '').replace('</p>','')

        desc = v.content.split('</p>')[0].replace('<p>', '')
        v.description = desc.replace('　', '') + '...'

        v.source = u'太傻网'.encode('utf-8')
        
        if len(v.textContent) > content_length_limit:
            self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent': v.textContent, 'thumbnail': v.imagesource, 'source': v.source})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        links = self.getLinks(page,url)
            
        for l in links:
            if(l not in self._queue and l not in fetchedURL):
                   self._queue.append(l)
        
    def getStart_url(self):
        start_url = [
            "http://www.taisha.org/canada/application/",
            "http://www.taisha.org/canada/career/",
            "http://www.taisha.org/canada/cost/",
            "http://www.taisha.org/canada/education/",
            "http://www.taisha.org/canada/immigrate/",
            "http://www.taisha.org/canada/impression/",
            "http://www.taisha.org/canada/news/",
            "http://www.taisha.org/canada/prepare/",
            "http://www.taisha.org/canada/school/",
            "http://www.taisha.org/canada/specialty/",
            "http://www.taisha.org/canada/visa/"
        ]

        for i in range(2,40):
            start_url.append("http://www.taisha.org/canada/application/"+str(i)+".html")

        for i in range(2,20):
            start_url.append("http://www.taisha.org/canada/career/"+str(i)+".html")

        for i in range(2,28):
            start_url.append("http://www.taisha.org/canada/cost/"+str(i)+".html")

        for i in range(2,3):
            start_url.append("http://www.taisha.org/canada/education/"+str(i)+".html")

        for i in range(2,7):
            start_url.append("http://www.taisha.org/canada/immigrate/"+str(i)+".html")

        for i in range(2,24):
            start_url.append("http://www.taisha.org/canada/impression/"+str(i)+".html")

        for i in range(2,37):
            start_url.append("http://www.taisha.org/canada/news/"+str(i)+".html")

        for i in range(2,20):
            start_url.append("http://www.taisha.org/canada/prepare/"+str(i)+".html")

        for i in range(2,28):
            start_url.append("http://www.taisha.org/canada/school/"+str(i)+".html")

        for i in range(2,9):
            start_url.append("http://www.taisha.org/canada/specialty/"+str(i)+".html")

        for i in range(2,30):
            start_url.append("http://www.taisha.org/canada/visa/"+str(i)+".html")

        return start_url

    def start(self):
        start_url = self.getStart_url()
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            url = self._queue.pop()
            try:
                self.getPage(url)
                fetchedURL.append(url)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue

        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        f = open('/home/janie/spider/taisha/fetchedURLs.json', 'w')
        json.dump(fetchedURL, f, indent=4, ensure_ascii=False)
        f.close()
        sys.exit(1)

if __name__ == '__main__':
    main()

