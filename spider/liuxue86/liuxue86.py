#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re

sys.path.append('common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds 
socket.setdefaulttimeout(timeout) 

#global variables defined here
spider_name = 'liunar'
user_agent = 'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16'
output_file = 'liuxue86/liuxue86.json'

#jsonfile=open('liuxue86/fetchedURLs.json')
#fetchedURL = json.load(jsonfile)
#jsonfile.close()
fetchedURL = []

allowed_domain = [
        "liuxue86.com"
    ]

class NewsItem(object): #container of video meta info
    
    def __init__(self):
        self.pageurl = ''
        self.title = ''
        self.category = ''
        self.create_time = ''
        self.description = ''
        self.content = ''
        self.textContent = ''

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def isAllowedChannel(self, url):
        if ( url.find('/a/') < 0 and url.find('/v/') < 0 ):
            return False
        if ( not (url.endswith('.html')) ):
            return False
        return True
    
    def getLinks(self,page,page_url):
        links = []
        parser = LinkExtract()
        parser.feed(page)

        parser.urls = set(parser.urls)

        for eachlink in parser.urls:
            if ( eachlink.rfind('#') >= 0 or eachlink.find('liuxue86.com/a') <= 0 ):
                continue
            if not eachlink.startswith('http://'):
                eachlink = urlparse.urljoin(page_url,eachlink)
            if not self.isAllowedDomain(eachlink):
                continue
            if not self.isAllowedChannel(eachlink):
                continue
            links.append(eachlink)

        return links

    def getHTMLContent(self,content):
        start_tag = "<div id=\"endtext\">"
        end_tag = "<div class=\"ye_780_four4 top20\">"

        start = content.find(start_tag)
        if start == -1:
            start_tag = "<div class=\"main_zhengw\">"
            start = content.find(start_tag)
            if start == -1:
                return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            end_tag = "<div class=\"moreclass\">"
            end = content.find(end_tag)
            if end ==-1:
                end_tag = "</div>"
                end = content.find(end_tag)
                if end ==-1:
                    return ''

        inTag = False
        new_content = list()
        origin_content = content[start:end].replace('<p>','liuxue86_para_mark1').replace('</p>','liuxue86_para_mark2')
        for c in origin_content:
            if c == '<':
                inTag = True
                continue
            if c == '>':
                inTag = False
                continue
            if inTag == True:
                continue
            new_content.append(c)

        return ''.join(new_content).replace('liuxue86_para_mark1','<p>').replace('liuxue86_para_mark2','</p>').replace('\n', '').replace('\r', '').replace(' ','').encode('utf8')

    def parsePage(self,page,page_url):# here to get useful info section
                
        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)
        
        r = dom.xpath("//h1")
        v.title = r[0].text.encode('utf8')

        r = dom.xpath("//h2/span")
        if not r:
            r = dom.xpath("//div[contains(@class, 'conter_main_one_nav')]/p")
        v.create_time = r[0].text.encode('utf8')

        '''
        crumbs = ''
        r = dom.xpath("//a[@class='keyword']")
        if r:
            crumbs = r[0].text.encode('utf8')
        else:
            r = dom.xpath("//div[@class=\"nav_logo clearfix\"]//*/text()")
            if r:
                crumbs = r[0].encode('utf8')
            else:
                crumbs = dom.xpath("//div[@class='one_nav']//*/text()")[2].encode('utf8')
        v.category = crumbs
        '''
        
        contents = self.getHTMLContent(page)
        v.content = contents.encode('utf8')
        v.textContent = v.content.replace('<p>', '').replace('</p>','')

        desc = ''
        r = dom.xpath("//div[@id='digest']/text()")
        if r:
            v.description = r[0].encode('utf8')
        else:
            desc = v.content.split('</p>')[0].replace('<p>', '')
            v.description = desc.replace('　', '') + '...'

        self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent:': v.textContent})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        links = self.getLinks(page,url)
            
        for l in links:
            if(l not in self._queue and l not in fetchedURL):
                   self._queue.append(l)
        
    def getStart_url(self):
        start_url = [
            "http://jianada.liuxue86.com/feiyong/",
            "http://jianada.liuxue86.com/jiangxuejin/",
            "http://jianada.liuxue86.com/tiaojian/",
            "http://visa.liuxue86.com/jianada/qianzhengzixun/",
            "http://visa.liuxue86.com/jianada/huzhaobanli/",
            "http://visa.liuxue86.com/jianada/banliliucheng/",
            "http://visa.liuxue86.com/jianada/jiqiao/",
            "http://jianada.liuxue86.com/yuanxiaozhuanye/guojia/",
            "http://jianada.liuxue86.com/yuanxiaozhuanye/jiaoyutixi/",
            "http://jianada.liuxue86.com/yuanxiaozhuanye/zhuanyezixun/",
            "http://jianada.liuxue86.com/yuanxiaozhuanye/remenzhuanye/",
            "http://jianada.liuxue86.com/exam/kaoshixinxi/" 
        ]
        
        for i in range(2,11):
            start_url.append("http://jianada.liuxue86.com/feiyong/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://jianada.liuxue86.com/jiangxuejin/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://jianada.liuxue86.com/tiaojian/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://visa.liuxue86.com/jianada/qianzhengzixun/"+str(i)+".html")

        for i in range(2,3):
            start_url.append("http://visa.liuxue86.com/jianada/huzhaobanli/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://visa.liuxue86.com/jianada/banliliucheng/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://visa.liuxue86.com/jianada/jiqiao/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://jianada.liuxue86.com/yuanxiaozhuanye/guojia/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://jianada.liuxue86.com/yuanxiaozhuanye/jiaoyutixi/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://jianada.liuxue86.com/yuanxiaozhuanye/zhuanyezixun/"+str(i)+".html")

        for i in range(2,11):
            start_url.append("http://jianada.liuxue86.com/yuanxiaozhuanye/remenzhuanye/"+str(i)+".html")
        
        return start_url

    def start(self):
        start_url = self.getStart_url()
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            url = self._queue.pop()
            try:
                self.getPage(url)
                fetchedURL.append(url)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue

        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        f = open('liuxue86/fetchedURLs.json', 'w')
        json.dump(fetchedURL, f, indent=4, ensure_ascii=False)
        f.close()
        for c in categories:
            print c
        sys.exit(1)

if __name__ == '__main__':
    main()

