#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
sys.path.append('../common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

socket.setdefaulttimeout(5.0)
#global variables defined here
spider_name = 'liunar'
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47'
output_file = 'kaoshixinxi.log'
start_url = [
        "http://jianada.liuxue86.com/exam/kaoshixinxi/"
    ]
allowed_domain = [
        "liuxue86.com"
    ]

class NewsItem(object): #container of video meta info
    
    def __init__(self):
        self.pageurl = ''
        self.title = ''
        self.category = ''
        self.create_time = ''
        self.description = ''
        self.content = ''
    
    def toString(self):
        buf = 'pageurl:'+self.pageurl+os.linesep
        buf = buf+'title:'+self.title.strip()+os.linesep
        buf = buf+'category:'+self.category+os.linesep
        buf = buf+'create_time:'+self.create_time+os.linesep
        buf = buf+'description:'+self.description+os.linesep
        buf = buf +'content:'+self.content+os.linesep
        return buf.encode('utf8')

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def isAllowedChannel(self, url):
        if ( url.find('/a/') < 0 ):
            return False
        if ( not (url.endswith('.html')) ):
            return False
        return True
    
    
    def getLinks(self,page,page_url):
        links = []
        parser = LinkExtract()
        parser.feed(page)

       	parser.urls = set(parser.urls)

        for eachlink in parser.urls:
            if ( eachlink.rfind('#') >= 0 or eachlink.find('liuxue86.com/a') <= 0 ):
                continue
            if not eachlink.startswith('http://'):
                eachlink = urlparse.urljoin(page_url,eachlink)
            if not self.isAllowedDomain(eachlink):
                continue
            if not self.isAllowedChannel(eachlink):
                continue
            links.append(eachlink)

        return links

    def parsePage(self,page,page_url):# here to get useful info section
                
        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)  
        
        r = dom.xpath("//h1")
        v.title = r[0].text.encode('utf8')
        
        r = dom.xpath("//div[contains(@class, 'conter_main_one_nav')]/p")
        v.create_time = r[0].text.encode('utf8')
        
        desc = ''
        contents = ''
        r = dom.xpath("//div[@class='main_zhengw']//*/text()")

        for i in range(len(r)-1):
            if u'推荐阅读'.encode('utf-8') in r[i].encode('utf8'):
                break
            elif 'liuxue86.com' in r[i].encode('utf8'):
                break
            else:
    	        contents = contents + r[i].encode('utf8')
        v.description = desc.encode('utf8')
        v.content = contents.encode('utf8')
        
        crumbs = ''
        r = dom.xpath("//div[@class='nav_logo']/a")
        for i in range(len(r)):
            if i==0:
                continue
            else:
                crumbs = crumbs + '>' +  r[i].text.encode('utf8')
        v.category = crumbs

        self._file.write(v.toString())
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        links = self.getLinks(page,url)
        for l in links:
        	if(l not in self._queue):
            	   self._queue.append(l)
        

    def start(self):
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            url = self._queue.pop()
            try:
                self.getPage(url)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue

        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        sys.exit(1)

if __name__ == '__main__':
    main()

