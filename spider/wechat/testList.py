#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
import md5

sys.path.append('common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS
from BeautifulSoup import BeautifulSoup

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds
socket.setdefaulttimeout(timeout)

#global variables defined here
spider_name = 'liunar'
user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
output_file = 'wechat/testList.json'

#jsonfile=open('wechat/testListURLs.json')
#testListURL = json.load(jsonfile)
#jsonfile.close()
testListURL = []

allowed_domain = [
        "/s?timestamp="
    ]

class NewsItem(object): #container of video meta info
    
    def __init__(self):
        self.pageurl = ''
        self.title = ''
        self.category = ''
        self.create_time = ''
        self.description = ''
        self.thumbnail = ''
        self.content = ''
        self.textContent = ''

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        self._images = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def getLinks(self,page,page_url):
        links = []
        parser = LinkExtract()
        parser.feed(page)

        parser.urls = set(parser.urls)

        for eachlink in parser.urls:
            if ( eachlink.rfind('#') >= 0 ):
                continue
            if not eachlink.startswith('http://'):
                eachlink = urlparse.urljoin("http://mp.weixin.qq.com",eachlink)
            if not self.isAllowedDomain(eachlink):
                continue
            links.append(eachlink)

        return links

    def getHTMLContent(self,content, hex_url):
        start_tag = "<div class=\"rich_media_content \" id=\"js_content\">"
        end_tag = "var first_sceen__time"

        start = content.find(start_tag)
        if start == -1:
            print "start is not found"
            return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            print "end is not found"
            return ''

        origin_content = content[start:end].replace('<p>','wechat_para_mark1').replace('</p>','wechat_para_mark2').replace('<img','wechat_para_img<img').replace('<br>', 'wechat_para_br')
        html_content = self.traversalContent(origin_content).replace('wechat_para_mark1','<p>').replace('wechat_para_mark2','</p>').replace('wechat_para_br', '<br>').replace('\n', '').replace('\r', '').replace('\t','').encode('utf8')

        k = html_content.count('wechat_para_img')

        for g in range(k):
            html_content = html_content.replace('wechat_para_img', '<img src=\"/articles/' + hex_url + '/' + hex_url + str(g) + "." + self._images[g]+ '\" />', 1)

        return html_content

    def traversalContent(self, originContent):
        inTag = False
        newContent = list()
        for c in originContent:
            if c == '<':
                inTag = True
                continue
            if c == '>':
                inTag = False
                continue
            if inTag == True:
                continue
            newContent.append(c)

        return ''.join(newContent)

    def parsePage(self,page,page_url):# here to get useful info section
                
        md = md5.new()
        md.update(page_url)
        hex_url = md.hexdigest()

        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)
        
        r = dom.xpath("//h2[@id='activity-name']")
        v.title = r[0].text.replace('\n', '').replace('\r', '').replace(' ', '').encode('utf8')

        r = dom.xpath("//em[@id='post-date']")
        v.create_time = r[0].text.encode('utf8')

        crumbs = u'本地生活'.encode('utf-8')
        v.category = crumbs

        r = dom.xpath("//div[@id='js_content']//img")
        if r:
            img_dir = "wechat/images/" + hex_url
            if not os.path.exists(img_dir):
               os.makedirs(img_dir)
            for m,image in enumerate(r):
                if image.attrib['data-src']:
                    image_suffix = re.search('(?<=fmt=)\w+', image.attrib['data-src']).group(0)
                    if image_suffix:
                        self._images.append(image_suffix)
                        urllib.urlretrieve(image.attrib['data-src'], img_dir + "/" + hex_url +str(m)+ "." + self._images[-1])
        else:
            print "No image on this page."

        thumbnail = '/articles/' + hex_url + '/' + hex_url + str(m-1) + '.png'
        v.thumbnail = thumbnail

        contents = self.getHTMLContent(page, hex_url).strip(' ')
        v.content = contents.encode('utf8')
        v.textContent = self.traversalContent(v.content).encode('utf8')

        v.description = v.textContent.split('。')[0].encode('utf8')

        self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent': v.textContent, 'thumbnail': v.thumbnail})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        links = self.getLinks(page,url)
            
        for l in links:
            if(l not in self._queue and l not in testListURL):
                   self._queue.append(l)
        
    def getStart_url(self):
        start_url = "http://weixin.sogou.com/weixin?type=1&query=inteligent_study&ie=utf8&_sug_=y&_sug_type_=&w=01015002&oq=in&ri=0&sourceid=sugg&stj=0%3B0%3B0%3B0&stj2=0&stj0=0&stj1=0&hp=32&hp1=&sut=5436&sst0=1465841683530&lkt=2%2C1465841679670%2C1465841679753"
        request = urllib2.Request(start_url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+ start_url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),start_url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (start_url,fp.getcode())
            return
        page = fp.read()

        dom = soupparser.fromstring(page)
        r = dom.xpath("//div[@id='sogou_vr_11002301_box_0']/@href")

        return r[0]

    def start(self):
        start_url = self.getStart_url()
        print "********************"
        print start_url
        self.fetchlinks(start_url)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            url = self._queue.pop()
            try:
                self.getPage(url)
                testListURL.append(url)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue
            
        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
    robot = Crawler()
    robot.start()
    f = open('wechat/testListURLs.json', 'w')
    json.dump(testListURL, f, indent=4, ensure_ascii=False)
    f.close()
    sys.exit(1)

if __name__ == '__main__':
    main()

