#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
import md5

sys.path.append('common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS
from BeautifulSoup import BeautifulSoup

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds
socket.setdefaulttimeout(timeout)

#global variables defined here
spider_name = 'liunar'
user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
output_file = 'wechat/testContent.json'

jsonfile=open('wechat/fetchedURLs.json')
fetchedURL = json.load(jsonfile)
jsonfile.close()
#fetchedURL = []

allowed_domain = [
        "weixin.qq.com"
    ]

class NewsItem(object): #container of video meta info
    
    def __init__(self):
        self.pageurl = ''
        self.title = ''
        self.category = ''
        self.create_time = ''
        self.description = ''
        self.thumbnail = ''
        self.content = ''
        self.textContent = ''
        self.source = ''

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        self._images = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def getHTMLContent(self,content, hex_url):
        start_tag = "<div class=\"rich_media_content \" id=\"js_content\">"
        end_tag = "var first_sceen__time"

        start = content.find(start_tag)
        if start == -1:
            print "start is not found"
            return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            print "end is not found"
            return ''

        origin_content = content[start:end].replace('<p>','wechat_para_mark1').replace('</p>','wechat_para_mark2').replace('<img','wechat_para_img<img').replace('<br>', 'wechat_para_br').replace('<h2>', 'wechat_para_mark3').replace('</h2>', 'wechat_para_mark4')
        html_content = self.traversalContent(origin_content).replace('wechat_para_mark1','<p>').replace('wechat_para_mark2','</p>').replace('wechat_para_br', '<br>').replace('wechat_para_mark3', '<h2>').replace('wechat_para_mark4', '</h2>').replace('\n', '').replace('\r', '').replace('\t','').encode('utf8')

        k = html_content.count('wechat_para_img')

        for g in range(k):
            address = 'wechat/images/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]
            if os.path.getsize(address) < 10000:
                html_content = html_content.replace('wechat_para_img', '', 1)
            else:
                html_content = html_content.replace('wechat_para_img', '<img src=\"http://www.liunar.net/articles/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]+ '\" />', 1)

        return html_content

    def traversalContent(self, originContent):
        inTag = False
        newContent = list()
        for c in originContent:
            if c == '<':
                inTag = True
                continue
            if c == '>':
                inTag = False
                continue
            if inTag == True:
                continue
            newContent.append(c)

        return ''.join(newContent)

    def parsePage(self,page,page_url):# here to get useful info section
                
        md = md5.new()
        md.update(page_url)
        hex_url = md.hexdigest()

        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)
        
        r = dom.xpath("//h2[@id='activity-name']")
        v.title = r[0].text.replace('\n', '').replace('\r', '').replace(' ', '').encode('utf8')

        r = dom.xpath("//em[@id='post-date']")
        v.create_time = r[0].text.encode('utf8')

        crumbs = u'留学申请'.encode('utf-8')
        v.category = crumbs

        r = dom.xpath("//a[@id='post-user']")
        v.source = r[0].text.encode('utf8')

        r = dom.xpath("//div[@id='js_content']//img")
        if r:
            img_dir = "wechat/images/" + hex_url
            if not os.path.exists(img_dir):
               os.makedirs(img_dir)
            for m,image in enumerate(r):
                if image.attrib['data-src']:
                    if 'fmt' not in image.attrib['data-src']:
                        image_suffix = 'gif'
                    else:
                        image_suffix = re.search('(?<=fmt=)\w+', image.attrib['data-src']).group(0)
                    self._images.append(image_suffix)
                    urllib.urlretrieve(image.attrib['data-src'], img_dir + "/" + hex_url + "-" + str(m)+ "." + self._images[-1])
        else:
            print "No image on this page."

        contents = self.getHTMLContent(page, hex_url).strip(' ')
        #contents = contents.split('</p>')[1:-2]
        #v.content = ''.join(contents).encode('utf8')
        v.content = contents.encode('utf8')
        v.textContent = self.traversalContent(v.content).encode('utf8')

        v.description = v.textContent.split('。')[0].encode('utf8')

        start = v.content.find('/articles/')
        end = v.content.find('" />')
        thumbnail = v.content[start:end]
        v.thumbnail = thumbnail

        self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent': v.textContent, 'thumbnail': v.thumbnail, 'source': v.source})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url)
        fp.close()
        
    def getStart_url(self):
        start_url = "http://weixin.sogou.com/weixin?type=1&query=AmericanLifeNews&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1710&sst0=1469052388072&lkt=0%2C0%2C0"
        request = urllib2.Request(start_url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+ start_url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),start_url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (start_url,fp.getcode())
            return
        page = fp.read()

        dom = soupparser.fromstring(page)
        r = dom.xpath("//a[@id='sogou_vr_11002301_link_first_0']/@href")
        if r:
            return r[0]
        else:
            print "cannot find first article!"
            sys.exit(1)

    def start(self):
        url = self.getStart_url()
        try:
            self.getPage(url)
            fetchedURL.append(url)
        except KeyboardInterrupt:
            print 'KeyboardInterrupt raised, so quit the crawler'
        except:
            print sys.exc_info()[0],sys.exc_info()[1]
            print 'Exception raised getting %s' % url
            
        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        f = open('wechat/fetchedURLs.json', 'w')
        json.dump(fetchedURL, f, indent=4, ensure_ascii=False)
        f.close()
        sys.exit(1)

if __name__ == '__main__':
    main()

