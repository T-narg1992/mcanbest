#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
import md5
import string

sys.path.append('common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS
from BeautifulSoup import BeautifulSoup
from newsitem import NewsItem

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds
socket.setdefaulttimeout(timeout)

#global variables defined here
spider_name = 'canbest'
user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
output_file = 'wechat/mgc_1.json'

jsonfile=open('wechat/fetchedArticles.json')
fetchedArticle = json.load(jsonfile)
jsonfile.close()
#fetchedArticle = []

exclude = set(string.punctuation)
exclude = list(exclude) + ['？', '！', '，', '。', '“', '”', '：', '|','【','】', ' ', '🌟', '、', '（', '）', '＂']

allowed_domain = [
        "weixin.qq.com"
    ]

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        self._images = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def getHTMLContent(self,content, hex_url):
        start_tag = "<div class=\"rich_media_content \" id=\"js_content\">"
        end_tag = "var first_sceen__time"

        start = content.find(start_tag)
        if start == -1:
            print "start is not found"
            return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            print "end is not found"
            return ''

        origin_content = content[start:end].replace('<p>','wechat_para_mark1').replace('</p>','wechat_para_mark2').replace('<img','wechat_para_img<img').replace('<br>', 'wechat_para_br').replace('<h2>', 'wechat_para_mark3').replace('</h2>', 'wechat_para_mark4')
        html_content = self.traversalContent(origin_content).replace('wechat_para_mark1','<p>').replace('wechat_para_mark2','</p>').replace('wechat_para_br', '<br>').replace('wechat_para_mark3', '<h2>').replace('wechat_para_mark4', '</h2>').replace('\n', '').replace('\r', '').replace('\t','').encode('utf8')

        k = html_content.count('wechat_para_img')

        for g in range(k):
            address = 'wechat/images/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]
            if os.path.getsize(address) < 16000:
                html_content = html_content.replace('wechat_para_img', '', 1)
            else:
                html_content = html_content.replace('wechat_para_img', '<img src=\"http://www.liunar.net/articles/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]+ '\" />', 1)

        return html_content

    def traversalContent(self, originContent):
        inTag = False
        newContent = list()
        for c in originContent:
            if c == '<':
                inTag = True
                continue
            if c == '>':
                inTag = False
                continue
            if inTag == True:
                continue
            newContent.append(c)

        return ''.join(newContent)

    def parsePage(self,page,page_url, i):# here to get useful info section
        md = md5.new()
        md.update(page_url)
        hex_url = md.hexdigest()

        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)
        
        r = dom.xpath("//h2[@id='activity-name']")
        v.title = r[0].text.replace('\n', '').replace('\r', '').replace(' ', '').replace('🌟', '').encode('utf8')
        fetchedArticle.append(v.title)

        r = dom.xpath("//em[@id='post-date']")
        v.create_time = r[0].text.encode('utf8')

        r = dom.xpath("//a[@id='post-user']")
        v.source = r[0].text.encode('utf8')

        r = dom.xpath("//div[@id='js_content']//img")
        if r:
            img_dir = "wechat/images/" + hex_url
            if not os.path.exists(img_dir):
               os.makedirs(img_dir)
            for m,image in enumerate(r):
                if image.attrib['data-src']:
                    if 'wx_fmt' in image.attrib['data-src']:
                        image_suffix = re.search('(?<=fmt=)\w+', image.attrib['data-src']).group(0)
                    else:
                        image_suffix = 'png'
                    if image_suffix:
                        self._images.append(image_suffix)
                        urllib.urlretrieve(image.attrib['data-src'], img_dir + "/" + hex_url + "-" + str(m)+ "." + self._images[-1])
        else:
            print "No image on this page."

        contents = self.getHTMLContent(page, hex_url).strip(' ')

        if i == 20: #加拿大留学
            contents = contents.split('</p>')[1:-11]
            contents = ''.join(contents).encode('utf8')
            v.category = u'留学申请'.encode('utf-8')
        if i == 19: #加拿大留学中心
            #contents = contents.split('</p>')[1:-6]
            contents = contents[116:]
            v.category = u'海外规划'.encode('utf-8')
        if i == 18: #启德加拿大
            contents = contents.split('</p>')[4:-6]
            contents = ''.join(contents).encode('utf8').replace(u'点击标题下「EIC启德加拿大」可快速关注'.encode('utf-8'),"").split('以下文章也值得关注')[:-1]
            contents = ''.join(contents).encode('utf8').split('<p>')[:-2]
            v.category = u'本地生活'.encode('utf-8')
        if i == 17: #加拿大家园
            contents = contents.split('</p>')[4:-6]
            v.category = u'本地生活'.encode('utf-8')
        if i == 16: #旭飞加拿大留学
            contents = contents.split('</p>')[:-6]
            v.category = u'留学申请'.encode('utf-8')
        if i == 15: #小枫叶义工联盟
            contents = contents.split('</p>')[:]
            v.category = u'本地生活'.encode('utf-8')
        if i == 14: #金吉列留学加拿大事业部
            contents = contents.split('</p>')[:]
            v.category = u'留学申请'.encode('utf-8')
        if i == 13: #加拿大留学堂
            contents = contents.split('<p>')[:]
            v.category = u'本地生活'.encode('utf-8')
        if i == 12: #少年商学院
            contents = contents.split('</p>')[:-9]
            v.category = u'本地生活'.encode('utf-8')
        if i == 11: #加拿大留学播报
            #contents = contents.split('</p>')[1:-3]
            v.category = u'本地生活'.encode('utf-8')
        if i == 10: #红媒教育
            contents = contents.split('</p>')[5:-14]
            v.category = u'校园内外'.encode('utf-8')
        if i == 9: #VPEA北美留学
            contents = contents.split('VPEA')[:]
            v.category = u'本地生活'.encode('utf-8')
        if i == 8: #盒子传媒
            #contents = contents.split('</p>')[2:-1]
            v.category = u'本地生活'.encode('utf-8')
        if i == 7: #魁北克PEQ
            #contents = contents.split('</p>')[:-2]
            v.category = u'本地生活'.encode('utf-8')
        if i == 6: #言道馆
            #contents = contents.split('</p>')[2:]
            v.category = u'海外规划'.encode('utf-8')
        if i == 5: #加拿大留学联盟
            contents = contents.split('</p>')[:-39]
            contents = ''.join(contents).encode('utf8')
            contents = contents.split('<p>')[1:]
            v.category = u'海外规划'.encode('utf-8')
        if i == 4: #加拿大教育中心
            contents = contents.split('</p>')[:-11]
            contents = ''.join(contents).split(u'注意！注意！'.encode('utf8'))[:-1]
            v.category = u'本地生活'.encode('utf-8')
        if i == 3: #嘉诚海外
            #contents = contents.split('</p>')[:]
            v.category = u'职业移民'.encode('utf-8')
        if i == 2: #德和衡枫叶助手
            contents = contents.split('</p>')[:-15]
            v.category = u'海外规划'.encode('utf-8')
        if i == 1: #梦工厂教育
            contents = contents.split('</p>')[:-2]
            v.category = u'海外规划'.encode('utf-8')
    
        v.content = ''.join(contents).encode('utf8')
        v.textContent = self.traversalContent(v.content).encode('utf8')

        v.description = v.textContent.split('。')[0].encode('utf8')

        start = v.content.find('/articles/')
        end = v.content.find('" />')
        thumbnail = v.content[start:end]
        v.thumbnail = thumbnail

        self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent': v.textContent, 'thumbnail': v.thumbnail, 'source': v.source})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url, i):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url, i)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+ url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (start_url,fp.getcode())
            return
        page = fp.read()

        dom = soupparser.fromstring(page)
        r = dom.xpath("//a[@id='sogou_vr_11002301_link_first_0']")
        if r:
            a = ''.join(ch for ch in r[0].text if ch not in exclude)
            for art in fetchedArticle:
                e = ''.join(ch for ch in art if ch not in exclude)
                if a == e:
                    print "article already exists!"
                    return
            self._queue.append(r[0].attrib["href"])
	    return
        else:
            print "cannot find first article!"
            print page
            return
        
    def getStart_url(self):
        start_url = [
            "http://weixin.sogou.com/weixin?type=1&query=mgc_group&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2425&sst0=1466631076450&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=go_canada&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1887&sst0=1466623562833&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=gasheng1989&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=3547&sst0=1466633221210&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=gh_ce65ae1d1c1f&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=2631&sst0=1466636948723&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=CA_Edu_Alliance&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1925&sst0=1466635454857&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=Tenglong-yandaoguan&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=18422&sst0=1466637026595&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=+jia-yimin&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2098&sst0=1466638062253&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=vanboxmedia&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2490&sst0=1466638523533&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=Vpea-2013&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1869&sst0=1466638872621&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=cdhongmei&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1912&sst0=1466639526004&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=edu_canada&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2256&sst0=1466640026381&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=youthmba&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1941&sst0=1466641773869&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=bailitop-can&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2321&sst0=1466642262825&lkt=0%2C0%2C0"
            "http://weixin.sogou.com/weixin?type=1&query=canada-jjl&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1742&sst0=1466642739469&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=gh_db70bb298b16&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2260&sst0=1466643355875&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=escxf_lx&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2014&sst0=1466703592042&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=iAsk-ca&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1907&sst0=1465515142004&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=EICCANADA&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=15323&sst0=1465839206355&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=liuxuejianada&ie=utf8&_sug_=n&_sug_type_=",
            "http://weixin.sogou.com/weixin?type=1&query=ca-xinquan&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=3298&sst0=1466025520712&lkt=0%2C0%2C0"
        ]
        return start_url

    def start(self):
        start_url = self.getStart_url()
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            self._images = []
            url = self._queue.pop()
            try:
                self.getPage(url, len(self._queue)+1)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue
            
        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        f = open('wechat/fetchedArticles.json', 'w')
        json.dump(fetchedArticle, f, indent=4, ensure_ascii=False)
        f.close()
        sys.exit(1)

if __name__ == '__main__':
    main()

