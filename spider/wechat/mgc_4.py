#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
import md5
import string

sys.path.append('common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS
from BeautifulSoup import BeautifulSoup
from newsitem import NewsItem

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds
socket.setdefaulttimeout(timeout)

#global variables defined here
spider_name = 'canbest'
user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
output_file = 'wechat/mgc_4.json'

jsonfile=open('wechat/fetchedArticles.json')
fetchedArticle = json.load(jsonfile)
jsonfile.close()
#fetchedArticle = []

exclude = set(string.punctuation)
exclude = list(exclude) + ['？', '！', '，', '。', '“', '”', '：', '|','【','】', ' ', '🌟', '、', '（', '）', '＂']

allowed_domain = [
        "weixin.qq.com"
    ]

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        self._images = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def getHTMLContent(self,content, hex_url):
        start_tag = "<div class=\"rich_media_content \" id=\"js_content\">"
        end_tag = "var first_sceen__time"

        start = content.find(start_tag)
        if start == -1:
            print "start is not found"
            return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            print "end is not found"
            return ''

        origin_content = content[start:end].replace('<p>','wechat_para_mark1').replace('</p>','wechat_para_mark2').replace('<img','wechat_para_img<img').replace('<br>', 'wechat_para_br').replace('<h2>', 'wechat_para_mark3').replace('</h2>', 'wechat_para_mark4')
        html_content = self.traversalContent(origin_content).replace('wechat_para_mark1','<p>').replace('wechat_para_mark2','</p>').replace('wechat_para_br', '<br>').replace('wechat_para_mark3', '<h2>').replace('wechat_para_mark4', '</h2>').replace('\n', '').replace('\r', '').replace('\t','').encode('utf8')

        k = html_content.count('wechat_para_img')

        for g in range(k):
            address = 'wechat/images/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]
            if os.path.getsize(address) < 16000:
                html_content = html_content.replace('wechat_para_img', '', 1)
            else:
                html_content = html_content.replace('wechat_para_img', '<img src=\"http://www.liunar.net/articles/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]+ '\" />', 1)

        return html_content

    def traversalContent(self, originContent):
        inTag = False
        newContent = list()
        for c in originContent:
            if c == '<':
                inTag = True
                continue
            if c == '>':
                inTag = False
                continue
            if inTag == True:
                continue
            newContent.append(c)

        return ''.join(newContent)

    def parsePage(self,page,page_url, i):# here to get useful info section
        md = md5.new()
        md.update(page_url)
        hex_url = md.hexdigest()

        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)
        
        r = dom.xpath("//h2[@id='activity-name']")
        v.title = r[0].text.replace('\n', '').replace('\r', '').replace(' ', '').replace('🌟', '').encode('utf8')
        fetchedArticle.append(v.title)

        r = dom.xpath("//em[@id='post-date']")
        v.create_time = r[0].text.encode('utf8')

        r = dom.xpath("//a[@id='post-user']")
        v.source = r[0].text.encode('utf8')

        r = dom.xpath("//div[@id='js_content']//img")
        if r:
            img_dir = "wechat/images/" + hex_url
            if not os.path.exists(img_dir):
               os.makedirs(img_dir)
            for m,image in enumerate(r):
                if image.attrib['data-src']:
                    if 'wx_fmt' in image.attrib['data-src']:
                        image_suffix = re.search('(?<=fmt=)\w+', image.attrib['data-src']).group(0)
                    else:
                        image_suffix = 'png'
                    if image_suffix:
                        self._images.append(image_suffix)
                        urllib.urlretrieve(image.attrib['data-src'], img_dir + "/" + hex_url + "-" + str(m)+ "." + self._images[-1])
        else:
            print "No image on this page."

        contents = self.getHTMLContent(page, hex_url).strip(' ')

        if i == 9: #北美生活
            #contents = contents.split('</p>')[:]
            v.category = u'本地生活'.encode('utf-8')
        if i == 8: #温房网
            contents = contents.split('</p>')[4:-15]
            v.category = u'本地生活'.encode('utf-8')
        if i == 7: #加拿大移民家园
            contents = contents.split('</p>')[3:-5]
            v.category = u'职业移民'.encode('utf-8')
        if i == 6: #加拿大AEG英才教育
            contents = contents.split('</p>')[:-6]
            v.category = u'留学申请'.encode('utf-8')
        if i == 5: #留留学
            contents = contents.split('<p>')[:-2]
            v.category = u'留学申请'.encode('utf-8')
        if i == 4: #加拿大留学专家
            contents = contents.split('</p>')[:-13]
            v.category = u'留学申请'.encode('utf-8')
        if i == 3: #灯塔EDU
            contents = contents.split('</p>')[2:-1]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 2: #鲸Media
            contents = contents.split('<p>')[2:-1]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 1: #多知网
            contents = contents.split('</p>')[:-2]
            v.category = u'青少年教育'.encode('utf-8')
        # if i == 60: #九宫八卦
        #     contents = contents.split('</p>')[:-12]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 59: #米妈正面管教沙龙
        #     contents = contents.split('</p>')[1:-13]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 58: #留学语言培训行业观察
        #     contents = contents.split('</p>')[:-3]
        #     v.category = u'海外规划'.encode('utf-8')
        # if i == 57: #带娃去郊游
        #     contents = contents.split('</p>')[1:]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 56: #BetterRead
        #     contents = contents.split('</p>')[:-7]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 55: #铅笔头情商
        #     contents = contents.split('</p>')[1:-9]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 54: #游美体验营
        #     contents = contents.split('</p>')[:-11]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 53: #启发童书馆
        #     contents = contents.split('</p>')[3:-4]
        #     v.category = u'青少年教育'.encode('utf-8')
        # # if i == 53: #教育圆桌
        # #     contents = contents.split('</p>')[:-7]
        # #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 52: #小花生网
        #     contents = contents.split('<p>')[:]
        #     #contents = contents.split('</p>')[:-20]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 51: #DE未来训练营
        #     contents = contents.split('</p>')[:-4]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 50: #芥末堆看教育
        #     contents = contents.split('</p>')[:-5]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 49: #童乐岛
        #     contents = contents.split('</p>')[:-2]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 48: #携程游学
        #     contents = contents.split('</p>')[2:-9]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 47: #新东方师训讲堂
        #     contents = contents.split('</p>')[2:-3]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 46: #SAT备考
        #     contents = contents.split('</p>')[:-7]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 45: #爸妈营
        #     contents = contents.split('</p>')[1:-4]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 44: #加拿大华人圈
        #     contents = contents.split('</p>')[1:-2]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 43: #德拉学院
        #     contents = contents.split('</p>')[:-2]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 42: #根基成长教育
        #     contents = contents.split('</p>')[:-10]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 41: #好玩的数学
        #     contents = contents.split('</p>')[:-8]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 40: #外滩教育
        #     contents = contents.split('</p>')[5:-22]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 39: #世青国际教育研究中心
        #     contents = contents.split('<p>')[:]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 38: #择学堂
        #     contents = contents
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 37: #爱读童书妈妈小莉
        #     contents = contents.split('</p>')[3:-16]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 36: #蓝橡树
        #     contents = contents.split('</p>')[1:-17]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 35: #李开复
        #     contents = contents.split('</p>')[:-2]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 34: #寓教于游
        #     contents = contents.split('</p>')[:-25]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 33: #佳亨温哥华学生联盟
        #     contents = contents.split('</p>')[3:-14]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 32: #奴隶社会
        #     contents = contents.split('</p>')[:-20]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 31: #红泥巴微学堂
        #     contents = contents.split('</p>')[:-12]
        #     v.category = u'青少年教育'.encode('utf-8')
        # if i == 30: #北京新东方雅思
        #     contents = contents[51:]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 29: #天下家
        #     contents = contents[49:].split('</p>')[:-2]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 28: #加拿大头条
        #     contents = contents.split('</p>')[2:-13]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 27: #加拿大留学咨询
        #     contents = contents.split('</p>')[2:-12]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 26: #加拿大名校留学
        #     contents = contents.split('<p>')[:-5]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 25: #加拿大名校留学
        #     contents = contents.split('</p>')[:-3]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 24: #优途加拿大留学网
        #     contents = contents.split('</p>')[:]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 23: #新东方前途加拿大留学
        #     contents = contents.split('<p>')[3:-17]
        #     contents = ''.join(contents).encode('utf8')
        #     contents = contents.split('</p>')[2:]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 22: #加拿大留学吧
        #     contents = contents.split('<p>')[:-1][116:]
        #     v.category = u'海外规划'.encode('utf-8')
        # if i == 21: #加拿大移民
        #     contents = contents.split('<p>')[1:-2]
        #     v.category = u'职业移民'.encode('utf-8')
        # if i == 20: #加拿大留学
        #     contents = contents.split('</p>')[1:-11]
        #     contents = ''.join(contents).encode('utf8')
        #     contents = contents.split('<p>')[:-2]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 19: #加拿大留学中心
        #     #contents = contents.split('</p>')[1:-6]
        #     contents = contents[116:]
        #     v.category = u'海外规划'.encode('utf-8')
        # if i == 18: #启德加拿大
        #     contents = contents.split('</p>')[4:-6]
        #     contents = ''.join(contents).encode('utf8')
        #     contents = contents.split('<p>')[:-2].replace("点击标题下「EIC启德加拿大」可快速关注","")
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 17: #加拿大家园
        #     contents = contents.split('</p>')[4:-6]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 16: #旭飞加拿大留学
        #     contents = contents.split('</p>')[:-6]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 15: #小枫叶义工联盟
        #     contents = contents.split('</p>')[:]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 14: #金吉列留学加拿大事业部
        #     contents = contents.split('</p>')[:]
        #     v.category = u'留学申请'.encode('utf-8')
        # if i == 13: #加拿大留学堂
        #     contents = contents.split('<p>')[:-5]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 12: #少年商学院
        #     contents = contents.split('</p>')[:-9]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 11: #加拿大留学播报
        #     contents = contents.split('</p>')[2:-5]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 10: #红媒教育
        #     contents = contents.split('</p>')[5:-14]
        #     v.category = u'校园内外'.encode('utf-8')
        # if i == 9: #VPEA北美留学
        #     contents = contents.split('VPEA')[:]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 8: #盒子传媒
        #     contents = contents.split('</p>')[2:-3]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 7: #魁北克PEQ
        #     contents = contents.split('</p>')[:-2]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 6: #言道馆
        #     contents = contents.split('</p>')[2:]
        #     v.category = u'海外规划'.encode('utf-8')
        # if i == 5: #加拿大留学联盟
        #     contents = contents.split('</p>')[:-39]
        #     contents = ''.join(contents).encode('utf8')
        #     contents = contents.split('<p>')[1:]
        #     v.category = u'海外规划'.encode('utf-8')
        # if i == 4: #加拿大教育中心
        #     contents = contents.split('</p>')[:-11]
        #     v.category = u'本地生活'.encode('utf-8')
        # if i == 3: #嘉诚海外
        #     contents = contents.split('</p>')[:]
        #     v.category = u'职业移民'.encode('utf-8')
        # if i == 2: #德和衡枫叶助手
        #     contents = contents.split('</p>')[:-15]
        #     v.category = u'海外规划'.encode('utf-8')
        # if i == 1: #梦工厂教育
        #     contents = contents.split('</p>')[:-2]
        #     v.category = u'海外规划'.encode('utf-8')
    
        v.content = ''.join(contents).encode('utf8')
        v.textContent = self.traversalContent(v.content).encode('utf8')

        v.description = v.textContent.split('。')[0].encode('utf8')

        start = v.content.find('/articles/')
        end = v.content.find('" />')
        thumbnail = v.content[start:end]
        v.thumbnail = thumbnail

        self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent': v.textContent, 'thumbnail': v.thumbnail, 'source': v.source})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url, i):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url, i)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+ url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (start_url,fp.getcode())
            return
        page = fp.read()

        dom = soupparser.fromstring(page)
        r = dom.xpath("//a[@id='sogou_vr_11002301_link_first_0']")
        if r:
            a = ''.join(ch for ch in r[0].text if ch not in exclude)
            for art in fetchedArticle:
                e = ''.join(ch for ch in art if ch not in exclude)
                if a == e:
                    print "article already exists!"
                    return
            self._queue.append(r[0].attrib["href"])
	    return
        else:
            print "cannot find first article!"
            print page
            return
        
    def getStart_url(self):
        start_url = [
            # "http://weixin.sogou.com/weixin?type=1&query=mgc_group&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2425&sst0=1466631076450&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=go_canada&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1887&sst0=1466623562833&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=gasheng1989&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=3547&sst0=1466633221210&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=gh_ce65ae1d1c1f&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=2631&sst0=1466636948723&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=CA_Edu_Alliance&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1925&sst0=1466635454857&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=Tenglong-yandaoguan&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=18422&sst0=1466637026595&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=+jia-yimin&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2098&sst0=1466638062253&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=vanboxmedia&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2490&sst0=1466638523533&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=Vpea-2013&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1869&sst0=1466638872621&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=cdhongmei&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1912&sst0=1466639526004&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=edu_canada&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2256&sst0=1466640026381&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=youthmba&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1941&sst0=1466641773869&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=bailitop-can&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2321&sst0=1466642262825&lkt=0%2C0%2C0"
            # "http://weixin.sogou.com/weixin?type=1&query=canada-jjl&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1742&sst0=1466642739469&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=gh_db70bb298b16&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2260&sst0=1466643355875&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=escxf_lx&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2014&sst0=1466703592042&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=iAsk-ca&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1907&sst0=1465515142004&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=EICCANADA&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=15323&sst0=1465839206355&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=liuxuejianada&ie=utf8&_sug_=n&_sug_type_=",
            # "http://weixin.sogou.com/weixin?type=1&query=ca-xinquan&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=3298&sst0=1466025520712&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=fushidapei360&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2310&sst0=1466031934011&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=jndlxb&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=2091&sst0=1466104360538&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=canadaxdf&ie=utf8&_sug_=n&_sug_type_=",
            # "http://weixin.sogou.com/weixin?type=1&query=YoutuCanada&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2509&sst0=1466534997483&lkt=1%2C1466534997379%2C1466534997379",
            # "http://weixin.sogou.com/weixin?type=1&query=gh_422697792ad3&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=3044&sst0=1466537478094&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=JEM-CANADA&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1866&sst0=1466544919511&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=jiaduoedu&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2289&sst0=1466550311263&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=Canadanews&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=3696&sst0=1466552377134&lkt=1%2C1466552374369%2C1466552374369",
            # "http://weixin.sogou.com/weixin?type=1&query=gh_0c609dd0233a&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2485&sst0=1466555759800&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=nosyasi&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2619&sst0=1468024095301&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=hongnibaweixuetang&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=955&sst0=1468521470259&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=nulishehui&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1076&sst0=1468531673972&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=Guangson-2008&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1087&sst0=1468533079993&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=learner-traveler&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=3078&sst0=1468535336449&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=kaifu&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=2840&sst0=1468536134497&lkt=5%2C1468536132525%2C1468536132967",
            # "http://weixin.sogou.com/weixin?type=1&query=blue_oak&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1081&sst0=1468536406456&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=xiaolijianggushi&ie=utf8&_sug_=n&_sug_type_=",
            # "http://weixin.sogou.com/weixin?type=1&query=zexuetang_com&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=2566&sst0=1468537478716&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=BWYA01&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=847&sst0=1468537896847&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=TBEducation&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1061&sst0=1468538873741&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=mathfun&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=908&sst0=1468539890953&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=Rootedu&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=955&sst0=1468540256737&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=Drosophila_Academy&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=876&sst0=1468540636405&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=ca-huaren&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=765&sst0=1468954069897&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=bamaying&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=764&sst0=1468954707923&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=satbeikao&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1231&sst0=1468956844840&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=xdf_sxjt&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1019&sst0=1468962230585&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=ctripstudy&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=967&sst0=1468962884263&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=tongledao2011&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=763&sst0=1468963719243&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=jiemoedu&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=995&sst0=1468964309984&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=EE_edu&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=788&sst0=1468964965724&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=xiaohuasheng99&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=754&sst0=1468965326870&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=xiaohuasheng99&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=754&sst0=1468965326870&lkt=0%2C0%2C0",
            # #"http://weixin.sogou.com/weixin?type=1&query=jiaoyuyuanzhuo&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=396&sst0=1468965983781&lkt=1%2C1468965983678%2C1468965983678",
            # "http://weixin.sogou.com/weixin?type=1&query=qifatongshu&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=852&sst0=1468967581198&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=statesidecamp&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1042&sst0=1468968103381&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=qianbitour&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1502&sst0=1468972510086&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=BetterRead&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=743&sst0=1468973285574&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=daiwaqujiaoyou&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1023&sst0=1468973925496&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=mima--pdbj&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=878&sst0=1469037099753&lkt=0%2C0%2C0",
            # "http://weixin.sogou.com/weixin?type=1&query=zy622005118&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1239&sst0=1469039719535&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=duozhiwang&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=921&sst0=1469040223714&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=jmedia360&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=848&sst0=1469040899674&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=dengtaEDU&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1032&sst0=1469042863480&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=LetEduCanada&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1251&sst0=1469043341672&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=im66xue&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=800&sst0=1469048840636&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=AEG_Education&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=944&sst0=1469049765948&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=canjiayuan&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=918&sst0=1469050391887&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=wenfangwang&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=982&sst0=1469051513659&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=AmericanLifeNews&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1710&sst0=1469052388072&lkt=0%2C0%2C0"
        ]
        return start_url

    def start(self):
        start_url = self.getStart_url()
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            self._images = []
            url = self._queue.pop()
            try:
                self.getPage(url, len(self._queue)+1)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue
            
        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        f = open('wechat/fetchedArticles.json', 'w')
        json.dump(fetchedArticle, f, indent=4, ensure_ascii=False)
        f.close()
        sys.exit(1)

if __name__ == '__main__':
    main()

