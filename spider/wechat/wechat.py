#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import sys,os
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

driver = webdriver.Firefox()

time.sleep(3)

#open sougou wechat search u'加拿大'.encode('utf-8')
driver.get("http://weixin.sogou.com/weixin?query=%E5%8A%A0%E6%8B%BF%E5%A4%A7&_sug_type_=&_sug_=y&type=2&page=1&ie=utf8&w=01&dr=1")
search_wechat = driver.window_handles[0]
print search_wechat

#search wechat public group account
driver.find_element_by_xpath("//*[@id='searchForm']/div/input[2]").click()

#click first account and open a new window
driver.find_element_by_xpath("//*[@id='sogou_vr_11002301_box_0']/div[2]/h3").click()
time.sleep(3)
article_list = driver.window_handles[1]
print article_list

#switch to newly opened window
driver.switch_to_window(article_list)

time.sleep(3)

#get list of articles in the public group account
title = driver.find_element_by_xpath("//*[@id='WXAPPMSG406798341']/div/h4")
if title:
	print 'Title: ' + title.text.encode('utf8')

description = driver.find_element_by_xpath("//p[@class='weui_media_desc']")
if description:	
	print 'Description: ' + description.text.encode('utf8')

thumbnail_link = driver.find_element_by_xpath("//*[@id='WXAPPMSG406845079']/span").get_attribute("style")
if thumbnail_link:	
	thumbnail_link = re.findall(r'"([^"]*)"', thumbnail_link)
	print 'thumbnail_link: ' + thumbnail_link[0]

time.sleep(3)

#get content of first article
title.click()
article_detail = driver.window_handles[1]
print article_detail

#switch to newly opened window
driver.switch_to_window(article_detail)

#get create time and content of article
create_time = driver.find_element_by_xpath("//*[@id='post-date']")
if create_time:	
	print 'create_time: ' + create_time.text.encode('utf8')

content = driver.find_elements_by_xpath("//div[@id='js_content']/p")
print len(content)
i = 0
for p in content:
	content_img = p.find_element_by_xpath("//img")
	if content_img:
		print "content_image " + str(i) + ": " + content_img.get_attribute("data-src")
	print p.text.encode('utf8')
	i += 1

driver.quit()

