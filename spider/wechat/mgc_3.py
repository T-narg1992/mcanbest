#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
import md5
import string

sys.path.append('common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS
from BeautifulSoup import BeautifulSoup
from newsitem import NewsItem

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds
socket.setdefaulttimeout(timeout)

#global variables defined here
spider_name = 'canbest'
user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
output_file = 'wechat/mgc_3.json'

jsonfile=open('wechat/fetchedArticles.json')
fetchedArticle = json.load(jsonfile)
jsonfile.close()
#fetchedArticle = []

exclude = set(string.punctuation)
exclude = list(exclude) + ['？', '！', '，', '。', '“', '”', '：', '|','【','】', ' ', '🌟', '、', '（', '）', '＂']

allowed_domain = [
        "weixin.qq.com"
    ]

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        self._images = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def getHTMLContent(self,content, hex_url):
        start_tag = "<div class=\"rich_media_content \" id=\"js_content\">"
        end_tag = "var first_sceen__time"

        start = content.find(start_tag)
        if start == -1:
            print "start is not found"
            return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            print "end is not found"
            return ''

        origin_content = content[start:end].replace('<p>','wechat_para_mark1').replace('</p>','wechat_para_mark2').replace('<img','wechat_para_img<img').replace('<br>', 'wechat_para_br').replace('<h2>', 'wechat_para_mark3').replace('</h2>', 'wechat_para_mark4')
        html_content = self.traversalContent(origin_content).replace('wechat_para_mark1','<p>').replace('wechat_para_mark2','</p>').replace('wechat_para_br', '<br>').replace('wechat_para_mark3', '<h2>').replace('wechat_para_mark4', '</h2>').replace('\n', '').replace('\r', '').replace('\t','').encode('utf8')

        k = html_content.count('wechat_para_img')

        for g in range(k):
            address = 'wechat/images/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]
            if os.path.getsize(address) < 16000:
                html_content = html_content.replace('wechat_para_img', '', 1)
            else:
                html_content = html_content.replace('wechat_para_img', '<img src=\"http://www.liunar.net/articles/' + hex_url + '/' + hex_url + "-" + str(g) + "." + self._images[g]+ '\" />', 1)

        return html_content

    def traversalContent(self, originContent):
        inTag = False
        newContent = list()
        for c in originContent:
            if c == '<':
                inTag = True
                continue
            if c == '>':
                inTag = False
                continue
            if inTag == True:
                continue
            newContent.append(c)

        return ''.join(newContent)

    def parsePage(self,page,page_url, i):# here to get useful info section
        md = md5.new()
        md.update(page_url)
        hex_url = md.hexdigest()

        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)
        
        r = dom.xpath("//h2[@id='activity-name']")
        v.title = r[0].text.replace('\n', '').replace('\r', '').replace(' ', '').replace('🌟', '').encode('utf8')
        fetchedArticle.append(v.title)

        r = dom.xpath("//em[@id='post-date']")
        v.create_time = r[0].text.encode('utf8')

        r = dom.xpath("//a[@id='post-user']")
        v.source = r[0].text.encode('utf8')

        r = dom.xpath("//div[@id='js_content']//img")
        if r:
            img_dir = "wechat/images/" + hex_url
            if not os.path.exists(img_dir):
               os.makedirs(img_dir)
            for m,image in enumerate(r):
                if image.attrib['data-src']:
                    if 'wx_fmt' in image.attrib['data-src']:
                        image_suffix = re.search('(?<=fmt=)\w+', image.attrib['data-src']).group(0)
                    else:
                        image_suffix = 'png'
                    if image_suffix:
                        self._images.append(image_suffix)
                        urllib.urlretrieve(image.attrib['data-src'], img_dir + "/" + hex_url + "-" + str(m)+ "." + self._images[-1])
        else:
            print "No image on this page."

        contents = self.getHTMLContent(page, hex_url).strip(' ')

        if i == 20: #九宫八卦
            contents = contents.split('</p>')[:-12]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 19: #米妈正面管教沙龙
            contents = contents.split('</p>')[1:-13]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 18: #留学语言培训行业观察
            contents = contents.split('</p>')[:-3]
            v.category = u'海外规划'.encode('utf-8')
        if i == 17: #带娃去郊游
            contents = contents.split('</p>')[:]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 16: #BetterRead
            contents = contents.split('</p>')[:-7]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 15: #铅笔头情商
            contents = contents.split('</p>')[1:-9]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 14: #游美体验营
            contents = contents.split('</p>')[:-11]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 13: #启发童书馆
            contents = contents.split('</p>')[3:-4]
            v.category = u'青少年教育'.encode('utf-8')
        # if i == 53: #教育圆桌
        #     contents = contents.split('</p>')[:-7]
        #     v.category = u'青少年教育'.encode('utf-8')
        if i == 12: #小花生网
            contents = contents.split('<p>')[:]
            contents = ''.join(contents).split(u'相关文章'.encode('utf-8'))[:-20]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 11: #DE未来训练营
            contents = contents.split('</p>')[:-4]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 10: #芥末堆看教育
            contents = contents.split('</p>')[:-5]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 9: #童乐岛
            contents = contents.split('</p>')[:-2]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 8: #携程游学
            contents = contents.split('</p>')[2:-7]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 7: #新东方师训讲堂
            contents = contents.split('</p>')[2:-3]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 6: #SAT备考
            contents = contents.split('</p>')[:-7]
            v.category = u'留学申请'.encode('utf-8')
        if i == 5: #爸妈营
            contents = contents.split('</p>')[1:-4]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 4: #加拿大华人圈
            contents = contents.split('</p>')[1:-2]
            v.category = u'本地生活'.encode('utf-8')
        if i == 3: #德拉学院
            contents = contents.split('</p>')[:-2]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 2: #根基成长教育
            contents = contents.split('</p>')[:]
            v.category = u'青少年教育'.encode('utf-8')
        if i == 1: #好玩的数学
            contents = contents.split('</p>')[:]
            v.category = u'青少年教育'.encode('utf-8')
    
        v.content = ''.join(contents).encode('utf-8')
        v.textContent = self.traversalContent(v.content).encode('utf-8')

        v.description = v.textContent.split('。')[0].encode('utf-8')

        start = v.content.find('/articles/')
        end = v.content.find('" />')
        thumbnail = v.content[start:end]
        v.thumbnail = thumbnail

        self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent': v.textContent, 'thumbnail': v.thumbnail, 'source': v.source})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url, i):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url, i)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+ url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (start_url,fp.getcode())
            return
        page = fp.read()

        dom = soupparser.fromstring(page)
        r = dom.xpath("//a[@id='sogou_vr_11002301_link_first_0']")
        if r:
            a = ''.join(ch for ch in r[0].text if ch not in exclude)
            for art in fetchedArticle:
                e = ''.join(ch for ch in art if ch not in exclude)
                if a == e:
                    print "article already exists!"
                    return
            self._queue.append(r[0].attrib["href"])
	    return
        else:
            print "cannot find first article!"
            print page
            return
        
    def getStart_url(self):
        start_url = [
            "http://weixin.sogou.com/weixin?type=1&query=mathfun&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=908&sst0=1468539890953&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=Rootedu&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=955&sst0=1468540256737&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=Drosophila_Academy&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=876&sst0=1468540636405&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=ca-huaren&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=765&sst0=1468954069897&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=bamaying&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=764&sst0=1468954707923&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=satbeikao&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1231&sst0=1468956844840&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=xdf_sxjt&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1019&sst0=1468962230585&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=ctripstudy&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=967&sst0=1468962884263&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=tongledao2011&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=763&sst0=1468963719243&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=jiemoedu&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=995&sst0=1468964309984&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=EE_edu&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=788&sst0=1468964965724&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=xiaohuasheng99&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=754&sst0=1468965326870&lkt=0%2C0%2C0",
            #"http://weixin.sogou.com/weixin?type=1&query=jiaoyuyuanzhuo&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=396&sst0=1468965983781&lkt=1%2C1468965983678%2C1468965983678",
            "http://weixin.sogou.com/weixin?type=1&query=qifatongshu&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=852&sst0=1468967581198&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=statesidecamp&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1042&sst0=1468968103381&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=qianbitour&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1502&sst0=1468972510086&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=BetterRead&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=743&sst0=1468973285574&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=daiwaqujiaoyou&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=1023&sst0=1468973925496&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=liuxueguancha&ie=utf8&_sug_=y&_sug_type_=&w=01019900&sut=48539&sst0=1469230488459&lkt=0%2C0%2C0"
            "http://weixin.sogou.com/weixin?type=1&query=mima--pdbj&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=878&sst0=1469037099753&lkt=0%2C0%2C0",
            "http://weixin.sogou.com/weixin?type=1&query=zy622005118&ie=utf8&_sug_=n&_sug_type_=&w=01019900&sut=1239&sst0=1469039719535&lkt=0%2C0%2C0"
        ]
        return start_url

    def start(self):
        start_url = self.getStart_url()
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            self._images = []
            url = self._queue.pop()
            try:
                self.getPage(url, len(self._queue)+1)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue
            
        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        f = open('wechat/fetchedArticles.json', 'w')
        json.dump(fetchedArticle, f, indent=4, ensure_ascii=False)
        f.close()
        sys.exit(1)

if __name__ == '__main__':
    main()

