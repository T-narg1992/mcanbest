#!/usr/bin/python
# -*- coding: utf-8 -*-
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
import md5

sys.path.append('/home/janie/spider/common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS
from newsitem import NewsItem
from util import removeContentInTag

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

import socket
timeout=300 # in seconds 
socket.setdefaulttimeout(timeout) 

#global variables defined here
spider_name = 'liunar'
user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
output_file = '/home/janie/spider/liuxue/liuxue.json'

jsonfile=open('/home/janie/spider/liuxue/fetchedURLs.json')
fetchedURL = json.load(jsonfile)
jsonfile.close()
#fetchedURL = []

allowed_domain = [
        "liuxue.com"
    ]

content_length_limit = 100

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False

    def isAllowedChannel(self, url):
        if ( url.find('canada') < 0 ):
            return False
        if ( not (url.endswith('.html')) ):
            return False
        return True
    
    def getLinks(self,page,page_url):
        links = []
        parser = LinkExtract()
        parser.feed(page)

        parser.urls = set(parser.urls)

        for eachlink in parser.urls:
            if ( eachlink.rfind('#') >= 0 or eachlink.find('liuxue.com') <= 0 ):
                continue
            if not eachlink.startswith('http://'):
                eachlink = urlparse.urljoin(page_url,eachlink)
            if not self.isAllowedDomain(eachlink):
                continue
            if not self.isAllowedChannel(eachlink):
                continue
            links.append(eachlink)

        return links

    def getHTMLContent(self,content):
        start_tag = "<div class=\"article-infomation\">"
        end_tag = "<div class=\"detail-bottom-tips\">"

        start = content.find(start_tag)
        if start == -1:
            return ''
        start += len(start_tag)
        end = content.find(end_tag)
        if end == -1:
            return ''

        origin_content = content[start:end]

        return removeContentInTag(origin_content)

    def parsePage(self,page,page_url):# here to get useful info section
        
	md = md5.new()
        md.update(page_url)
        hex_url = md.hexdigest()
        
        v = NewsItem()
        v.pageurl = page_url

        dom = soupparser.fromstring(page)
        
        r = dom.xpath("//h1")
        v.title = r[0].text.encode('utf8')

        r = dom.xpath("//p[@class='time-tips']/span")
        v.create_time = r[0].text.encode('utf8')

        crumbs = ''
        r = dom.xpath("//p[@class='bread-crumbs']//*")
        crumbs =  crumbs+ r[len(r)-2].text.encode('utf8')
        v.category = crumbs
        
        imagesource = ''
        r = dom.xpath("//div[@class='artile-image-tip']//img/@src")
        if r:
            imagesource = r[0].encode('utf8')
	    urllib.urlretrieve(imagesource,  "/home/devops/pictures/articles/thumbnails/" + hex_url + "." + ''.join(imagesource.split('.')[-1:]))
	    v.imagesource = "/articles/thumbnails/" + hex_url + "." + ''.join(imagesource.split('.')[-1:])

        contents = self.getHTMLContent(page)
	    contents = contents.split(u'相关推荐阅读'.encode('utf-8'))[:-1]
        v.content = ''.join(contents)
        v.textContent = v.content.replace('<p>', '').replace('</p>','')

        desc = ''
        r = dom.xpath("//div[@class='detail-summary']//*/text()")
        if r:
            for m in r:
                desc += m.strip().encode('utf8')
        else:
            desc = v.content.split('</p>')[0].replace('<p>', '')
            v.description = desc.replace('　', '') + '...'
        v.description = desc.encode('utf8')

        v.source = u'留学网'.encode('utf-8')

        if len(v.textContent) > content_length_limit:
            self._data.append({'pageurl':v.pageurl, 'title':v.title, 'category':v.category, 'create_time':v.create_time, 'description':v.description, 'content': v.content, 'textContent': v.textContent, 'thumbnail': v.imagesource, 'source': v.source})
        self._count += 1
        print 'total processed %d pages' % self._count
        
    def getPage(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        self.parsePage(page,url)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        links = self.getLinks(page,url)
            
        for l in links:
            if(l not in self._queue and l not in fetchedURL):
                   self._queue.append(l)
        
    def getStart_url(self):
        start_url = [
            "http://canada.liuxue.com/apply/",
            "http://canada.liuxue.com/cost/",
            "http://canada.liuxue.com/graduate/",
            "http://canada.liuxue.com/highschool/",
            "http://canada.liuxue.com/major/",
            "http://canada.liuxue.com/news/",
            "http://canada.liuxue.com/preparatory/",
            "http://canada.liuxue.com/scholarship/",
            "http://canada.liuxue.com/school/",
            "http://canada.liuxue.com/trip/",
            "http://canada.liuxue.com/undergraduate/",
            "http://canada.liuxue.com/visa/" 
        ]
        
        for i in range(2,22):
            start_url.append("http://canada.liuxue.com/apply/p"+str(i))

        for i in range(2,11):
            start_url.append("http://canada.liuxue.com/cost/p"+str(i))

        for i in range(2,4):
            start_url.append("http://canada.liuxue.com/graduate/p"+str(i))

        for i in range(2,9):
            start_url.append("http://canada.liuxue.com/highschool/p"+str(i))

        for i in range(2,16):
            start_url.append("http://canada.liuxue.com/major/p"+str(i))

        for i in range(2,33):
            start_url.append("http://canada.liuxue.com/news/p"+str(i))

        for i in range(2,4):
            start_url.append("http://canada.liuxue.com/preparatory/p"+str(i))

        for i in range(2,5):
            start_url.append("http://canada.liuxue.com/scholarship/p"+str(i))

        for i in range(2,15):
            start_url.append("http://canada.liuxue.com/school/p"+str(i))

        for i in range(2,7):
            start_url.append("http://canada.liuxue.com/trip/p"+str(i))

        for i in range(2,6):
            start_url.append("http://canada.liuxue.com/undergraduate/p"+str(i))

        for i in range(2,13):
            start_url.append("http://canada.liuxue.com/visa/p"+str(i))

        return start_url

    def start(self):
        start_url = self.getStart_url()
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)

        while len(self._queue) > 0:
            url = self._queue.pop()
            try:
                self.getPage(url)
                fetchedURL.append(url)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue

        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        f = open('/home/janie/spider/liuxue/fetchedURLs.json', 'w')
        json.dump(fetchedURL, f, indent=4, ensure_ascii=False)
        f.close()
        sys.exit(1)

if __name__ == '__main__':
    main()

