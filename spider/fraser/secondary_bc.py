#!/usr/bin/env python
import getopt
import sys,os
import urllib
import urllib2
import urlparse
import socket
import json
import re
sys.path.append('../common')

from link_extract import LinkExtract
from ignored_ext import IGNORED_EXTENSIONS

import socket
timeout=120 # in seconds
socket.setdefaulttimeout(timeout)

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

import lxml.html.soupparser as soupparser

socket.setdefaulttimeout(5.0)
#global variables defined here
spider_name = 'fraser'
user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
output_file = 'fraser_bc_secondary.json'
start_url = [
        "http://britishcolumbia.compareschoolrankings.org/secondary/SchoolsByRankLocationName.aspx"
    ]
allowed_domain = [
        "compareschoolrankings.org"
    ]

class SchoolCard(object):
    
    def __init__(self):
        self.current_rank = ''
        self.five_year_rank = ''
        self.school_name = ''
        self.school_type = ''
        self.school_address = ''
        self.school_phone = ''
        self.school_district = ''
        self.city = ''
        self.rating = ''
        self.five_year_rating = ''
        self.trend = ''
        self.g12_enrollment = ''
        self.esl_percentage = ''
        self.special_needs_percentage = ''
        self.french_percentage = ''
        self.parent_income = ''
        self.website = ''
        
        self.ave_exam_mark = []
        self.percentage_of_exams_failed = []
        self.school_vs_exam_mark_difference = []
        self.english_gender_gap = []
        self.math_gender_gap = []
        self.graduation_rate = []
        self.delayed_advancement_rate = []
        self.overall_rating = []
    
    def toString(self):
        pass

class Crawler(object): #manage entire crawling process

    def __init__(self):
        self._queue = []
        self._file = open(output_file,'w')
        self._count = 0
        self._data = []
        
    def isAllowedDomain(self,url):
        for d in allowed_domain:
            if ( urlparse.urlparse(url)[1].endswith(d) ):
                return True
        return False
    
    def getLinks(self,page,page_url):
        links = []
        parser = LinkExtract()
        parser.feed(page)
        for eachlink in parser.urls:
            if ( eachlink.rfind('/Report_Card.aspx') >= 0 ):
                if not eachlink.startswith('http://'):
                    eachlink = urlparse.urljoin(page_url,eachlink)

                links.append(eachlink)

        return links

    def parsePage(self,page,page_url):# here to get useful info section
        
        v = SchoolCard()

        dom = soupparser.fromstring(page)

        basic_info_start = '<span id="ctl00_ContentPlaceHolder1_SchoolInfoDisplay"><strong>'
        start = page.find(basic_info_start)
        if start < 0:
            print "Error: can not find basic school info"
            return
        start += len(basic_info_start)
        end = page.find('</strong><br/>',start)
        if end < 0 :
            print "Error:can not find basic school info"
            return

        val = page[start:end]
        v.school_name = val

        start = end + len('</strong><br/>')
        end = page.find('<br/>',start)
        if end < 0 :
            print "Error: can not find basic school info"
            return
        val = page[start:end]
        v.school_type = val

        start = end + len('<br/>')
        end = page.find('<br/>',start)
        if end < 0:
            return
        end += len('<br/>')
        end = page.find('<br/>',end)
        if end < 0:
            return
        val = page[start:end]
        v.school_address = val.replace(' <br/>', ',')
        v.city = v.school_address.split(',')[1]

        start = end + len('<br/>')
        end = page.find('<br/>',start)
        if end < 0:
            return
        val = page[start:end]
        v.school_phone = val.replace('Phone Number: ', '')

        start = end + len('<br/><br/>')
        end = page.find('<br/>',start)
        if end < 0:
            return
        val = page[start:end]
        v.school_district = val.replace('School District: ','')

        r = dom.xpath("//table[@id='ctl00_ContentPlaceHolder1_detailedReportCard_tblSchoolRank']/tr")
        v.current_rank = r[0].xpath(".//td")[1].text
        v.five_year_rank = r[1].xpath(".//td")[1].text

        r = dom.xpath("//a[@id='ctl00_ContentPlaceHolder1_hlSchoolWebsite']")
        if r:
            v.website = r[0].text
        
        r = dom.xpath("//table[@id='ctl00_ContentPlaceHolder1_detailedReportCard_SchoolProperties1_tblProps']/tr[@class='tdreportcard_info']")
        v.g12_enrollment = r[0].xpath(".//td")[1].text
        v.esl_percentage = r[1].xpath(".//td")[1].text
        v.special_needs_percentage = r[2].xpath(".//td")[1].text
        v.french_percentage = r[3].xpath(".//td")[1].text
        v.parent_income = r[4].xpath(".//td")[1].text

        r = dom.xpath("//table[@id='ctl00_ContentPlaceHolder1_detailedReportCard_tblReportCard']/tr[starts-with(@class,'tdreportcard_')]")

        tds = [] 
        for i in range(8):
            row = []
            temp = r[i].xpath(".//td")
            for j in range(1,7):
                if j == 6:
                    if not temp[j].text:
                        row.append(temp[j].xpath(".//img/@alt")[0].encode('utf8'))
                        continue
                row.append(temp[j].text)
            tds.append(row)

        v.ave_exam_mark = tds[0]
        v.percentage_of_exams_failed = tds[1]
        v.school_vs_exam_mark_difference = tds[2]
        v.english_gender_gap = tds[3]
        v.math_gender_gap = tds[4]
        v.graduation_rate = tds[5]
        v.delayed_advancement_rate = tds[6]
        v.overall_rating = tds[7]

        self._data.append({'school_name':v.school_name, 'school_type':v.school_type, 'school_address':v.school_address, 'city': v.city, 'school_phone':v.school_phone, 'website': v.website,   \
            'school_district':v.school_district, 'current_rank': v.current_rank, 'five_year_rank': v.five_year_rank, 'g12_enrollment': v.g12_enrollment, 'esl_percentage': v.esl_percentage,  \
            'special_needs_percentage':v.special_needs_percentage, 'french_percentage': v.french_percentage, 'parent_income': v.parent_income, 'ave_exam_mark': v.ave_exam_mark, 'percentage_of_exams_failed': v.percentage_of_exams_failed,  \
            'school_vs_exam_mark_difference':v.school_vs_exam_mark_difference, 'english_gender_gap':v.english_gender_gap, 'math_gender_gap':v.math_gender_gap, 'graduation_rate':v.graduation_rate,  \
            'delayed_advancement_rate':v.delayed_advancement_rate, 'overall_rating':v.overall_rating})

        #self._file.write(v.toString())
        self._count += 1
        print 'total processed %d pages' % self._count

    def getPage(self,url):

        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()
        
        self.parsePage(page,url)
        fp.close()

    def fetchlinks(self,url):
        request = urllib2.Request(url)
        request.add_header('User-Agent',user_agent)
        print 'begin to fetch '+url
        fp = urllib2.urlopen(request)
        print 'http code is %d for %s' % (fp.getcode(),url)
        if ( fp.getcode() != 200 ):
            print 'fail to get %s,code %d' % (url,fp.getcode())
            return
        page = fp.read()

        links = self.getLinks(page,url)
        for l in links:
            self._queue.append(l)
        

    def start(self):
        
        for u in start_url:
            self.fetchlinks(u)
        print 'queue elements :%d ' % len(self._queue)
        while len(self._queue) > 0:
            url = self._queue.pop()
            try:
                self.getPage(url)
            except KeyboardInterrupt:
                print 'KeyboardInterrupt raised, so quit the crawler'
                break
            except:
                print sys.exc_info()[0],sys.exc_info()[1]
                print 'Exception raised getting %s' % url
                continue

        json.dump(self._data, self._file, indent=4, ensure_ascii=False)
        self._file.close()

def main():
        robot = Crawler()
        robot.start()
        sys.exit(1)

if __name__ == '__main__':
    main()

