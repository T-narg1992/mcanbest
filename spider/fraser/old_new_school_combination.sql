#add column en_name_match to filter out same schools
alter table testSpider.school_test add en_name_match text
update testSpider.school_test set en_name_match = en_name

#simplify school English name for comparison
UPDATE testSpider.school_test SET en_name_match = LOWER(en_name_match);
UPDATE testSpider.school_test SET en_name_match = REPLACE(en_name_match, ' ', '');
UPDATE testSpider.school_test SET en_name_match = REPLACE(en_name_match, '\'', '');
UPDATE testSpider.school_test SET en_name_match = REPLACE(en_name_match, 'school', '');
UPDATE testSpider.school_test SET en_name_match = REPLACE(en_name_match, 'academy', '');
UPDATE testSpider.school_test SET en_name_match = REPLACE(en_name_match, 'secondary', '');
UPDATE testSpider.school_test SET en_name_match = REPLACE(en_name_match, 'elementary', '');

#find old_new school pair
select n2.id, n1.id FROM testSpider.school_test n1, testSpider.school_test n2 WHERE n1.id > n2.id AND n1.en_name_match = n2.en_name_match and n1.`type` = n2.`type` and n2.id < 366;