#encoding=utf-8
import getopt
import sys,os
import re
import MySQLdb
import glob
import xml.dom.minidom
import json, re

# default_encoding = 'utf-8'
# if sys.getdefaultencoding() != default_encoding:
#     reload(sys)
#     sys.setdefaultencoding(default_encoding)

def readFromDB(old, sql):

    doc_id = 0
    total = 0

    try:
        conn = MySQLdb.connect(host='52.36.30.184',user='janie',passwd='shao',port=3306)
        
        cur = conn.cursor()
        cur.execute('use testSpider')
        cur.execute('set names utf8')

        for sql_query in sql:
            cur.execute(sql_query)

        for old_id in old:
            cur.execute('delete from testSpider.school_test where id = '+ str(old_id) + ';')

        conn.commit()
        cur.close()
        conn.close()
        print 'Successfully updated testSpider.school_test!'

    except MySQLdb.Error,e:
        print 'updating:' + sql_query
        print "Mysql Error %d: %s" % (e.args[0],e.args[1])

def main():
    jsonfile = open('/Users/Janie/Desktop/1v1pair.json')
    data = json.load(jsonfile)
    jsonfile.close()

    #print data

    old = []
    sql = []
    for p in data:
        sql.append("update testSpider.school_test set " + \
        "ch_name = (select ch_name from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "school_category = (select school_category from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "teacher_num = (select teacher_num from testSpider.school_old where id = " + str(p[0]) + "),"  \
        "student_num = (select student_num from testSpider.school_old where id = " + str(p[0]) + "),"  \
        "int_student_num = (select int_student_num from testSpider.school_old where id = " + str(p[0]) + "),"+   \
        "website = (select website from testSpider.school_old where id = " + str(p[0]) + ")," +  \
        "tuition = (select tuition from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "apply_fee = (select apply_fee from testSpider.school_old where id = " + str(p[0]) + "),"+  \
        "apply_info = (select apply_info from testSpider.school_old where id = " + str(p[0]) + "),"+  \
        "detail_info = (select detail_info from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "core_courses = (select core_courses from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "specialty_courses = (select specialty_courses from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "programs = (select programs from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "ap_ib = (select ap_ib from testSpider.school_old where id = " + str(p[0]) + ")," + \
        "pic_array = (select pic_array from testSpider.school_old where id = " + str(str(p[0])) + ") " + \
        " where id = " + str(p[1]) + " ;")

        old.append(p[0])

    readFromDB(old, sql)

    sys.exit(0)


if __name__ == '__main__':
    main()