#encoding=utf-8
import getopt
import sys,os
import re
import MySQLdb
import glob
import xml.dom.minidom
import json, re

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

def readFromDB():

    doc_id = 0
    total = 0

    try:
        conn = MySQLdb.connect(host='liunar.com',user='alex',passwd='alexsong',port=3306, charset="utf8")
    
        cur = conn.cursor()
        cur.execute('use app')
        cur.execute('set names utf8')
		#jsonfiles = ['fraser_ab_high.json', 'fraser_bc_secondary.json', 'fraser_on_secondary.json', 'fraser_elementary_ab.json', 'fraser_elementary_bc.json', 'fraser_elementary_on.json']
        jsonfiles = ['fraser_bc_secondary.json']
			
        for f in range(len(jsonfiles)):
            jsonfile = open(jsonfiles[f])
            if 'high' in jsonfiles[f]:
                school_type = 5
            elif 'secondary' in jsonfiles[f]:
                school_type = 3
            elif 'elementary' in jsonfiles[f]:
                school_type = 4
            else:
                print 'Wrong school_type!'
                return
            data = json.load(jsonfile)
            jsonfile.close()
            f = 1
            for i in range(len(data)):
                if f == 0:
                    #high school in AB
                    other = 'g12_enrollment: ' + data[i]["g12_enrollment"] + ';'
                    other += 'ave_exam_mark: ' + ','.join(data[i]["ave_exam_mark"]) + ';'
                    other += 'percentage_of_exams_failed: ' + ','.join(data[i]["percentage_of_exams_failed"]) + ';'
                    other += 'school_vs_exam_mark_difference: ' + ','.join(data[i]["school_vs_exam_mark_difference"]) + ';'
                    other += 'english_gender_gap: ' + ','.join(data[i]["english_gender_gap"]) + ';'
                    other += 'math_gender_gap: ' + ','.join(data[i]["math_gender_gap"]) + ';'
                    other += 'courses_taken_per_student: '+ ','.join(data[i]["courses_taken_per_student"]) + ';'
                    other += 'diploma_completion_rate: ' + ','.join(data[i]["diploma_completion_rate"]) + ';'
                    other += 'delayed_advancement_rate: ' + ','.join(data[i]["delayed_advancement_rate"])
                    french_percentage = data[i]["french_percentage"] 

                if f == 1 :
                    #secondary school in BC
                    other = 'gr12_enrollment: ' + data[i]["g12_enrollment"] + ';'
                    other += 'ave_exam_mark: ' + ','.join(data[i]["ave_exam_mark"]) + ';'
                    other += 'percentage_of_exams_failed: ' + ','.join(data[i]["percentage_of_exams_failed"]) + ';'
                    other += 'school_vs_exam_mark_difference: ' + ','.join(data[i]["school_vs_exam_mark_difference"]) + ';'
                    other += 'english_gender_gap: ' + ','.join(data[i]["english_gender_gap"]) + ';'
                    other += 'math_gender_gap: ' + ','.join(data[i]["math_gender_gap"]) + ';'
                    other += 'graduation_rate: ' + ','.join(data[i]["graduation_rate"]) + ';'
                    other += 'delayed_advancement_rate: ' + ','.join(data[i]["delayed_advancement_rate"]) + ';'
                    french_percentage = data[i]["french_percentage"] 

                if f == 2:
                    #secondary school in ON
                    other = 'OSSLT_count: ' + data[i]["OSSLT_count"] + ';'
                    other += 'ave_gr9_math_acad: ' + ','.join(data[i]["ave_gr9_math_acad"]) + ';'
                    other += 'ave_gr9_math_apld: ' + ','.join(data[i]["ave_gr9_math_apld"]) + ';'
                    other += 'OSSLT_passed_percentage_FTE: ' + ','.join(data[i]["OSSLT_passed_percentage_FTE"]) + ';'
                    other += 'OSSLT_passed_percentage_PE: ' + ','.join(data[i]["OSSLT_passed_percentage_PE"]) + ';'
                    other += 'tests_below_standard_percentage: ' + ','.join(data[i]["tests_below_standard_percentage"]) + ';'
                    other += 'gender_gap_level_math: ' + ','.join(data[i]["gender_gap_level_math"]) + ';'
                    other += 'gender_gap_OSSLT: ' + ','.join(data[i]["gender_gap_OSSLT"]) + ';'
                    other += 'gr9_tests_not_written_percentage: ' + ','.join(data[i]["gr9_tests_not_written_percentage"]) + ';'
                    french_percentage = ''

                if f == 3:
                    #elementary school in AB
                    other = 'gr6_enrollment: ' + data[i]["gr6_enrollment"] + ';'
                    other += 'gr3_ave_test_mark_lang_arts: ' + ','.join(data[i]["gr3_ave_test_mark_lang_arts"]) + ';'
                    other += 'gr3_ave_test_mark_math: ' + ','.join(data[i]["gr3_ave_test_mark_math"]) + ';'
                    other += 'gr6_ave_test_mark_lang_arts: ' + ','.join(data[i]["gr6_ave_test_mark_lang_arts"]) + ';'
                    other += 'gr6_ave_test_mark_math: ' + ','.join(data[i]["gr6_ave_test_mark_math"]) + ';'
                    other += 'gr6_ave_test_mark_science: ' + ','.join(data[i]["gr6_ave_test_mark_science"]) + ';'
                    other += 'gr6_ave_test_mark_social_studies: ' + ','.join(data[i]["gr6_ave_test_mark_social_studies"]) + ';'
                    other += 'gr6_gender_gap_lang_arts: ' + ','.join(data[i]["gr6_gender_gap_lang_arts"]) + ';'
                    other += 'gr6_gender_gap_math: ' + ','.join(data[i]["gr6_gender_gap_math"]) + ';'
                    other += 'percentage_of_tests_failed: ' + ','.join(data[i]["percentage_of_tests_failed"]) + ';'
                    other += 'tests_not_written_percentage: ' + ','.join(data[i]["tests_not_written_percentage"]) + ';'
                    french_percentage = data[i]["french_percentage"] 

                if f == 4:
                    #elementary school in BC
                    other = 'gr4_enrollment: ' + data[i]["gr4_enrollment"] + ';'
                    other += 'gr4_avg_score_reading: ' + ','.join(data[i]["gr4_avg_score_reading"]) + ';'
                    other += 'gr4_avg_score_writing: ' + ','.join(data[i]["gr4_avg_score_writing"]) + ';'
                    other += 'gr4_avg_score_numeracy: ' + ','.join(data[i]["gr4_avg_score_numeracy"]) + ';'
                    other += 'gr7_ave_score_reading: ' + ','.join(data[i]["gr7_ave_score_reading"]) + ';'
                    other += 'gr7_avg_score_writing: ' + ','.join(data[i]["gr7_avg_score_writing"]) + ';'
                    other += 'gr7_avg_score_numeracy: ' + ','.join(data[i]["gr7_avg_score_numeracy"]) + ';'
                    other += 'gr7_gender_gap_reading: ' + ','.join(data[i]["gr7_gender_gap_reading"]) + ';'
                    other += 'gr7_gender_gap_numeracy: ' + ','.join(data[i]["gr7_gender_gap_numeracy"]) + ';'
                    other += 'below_expectations_percentage: ' + ','.join(data[i]["below_expectations_percentage"]) + ';'
                    other += 'tests_not_written_percentage: ' + ','.join(data[i]["tests_not_written_percentage"]) + ';'
                    french_percentage = data[i]["french_percentage"] 
                if f == 5:
                    #elementary school in ON
                    other = 'gr6_enrollment: ' + data[i]["gr6_enrollment"] + ';'
                    other += 'gr3_ave_level_reading: ' + ','.join(data[i]["gr3_ave_level_reading"]) + ';'
                    other += 'gr3_ave_level_writing: ' + ','.join(data[i]["gr3_ave_level_writing"]) + ';'
                    other += 'gr3_ave_level_math: ' + ','.join(data[i]["gr3_ave_level_math"]) + ';'
                    other += 'gr6_ave_level_reading: ' + ','.join(data[i]["gr6_ave_level_reading"]) + ';'
                    other += 'gr6_ave_level_writing: ' + ','.join(data[i]["gr6_ave_level_writing"]) + ';'
                    other += 'gr6_ave_level_math: ' + ','.join(data[i]["gr6_ave_level_math"]) + ';'
                    other += 'gender_gap_level_reading: ' + ','.join(data[i]["gender_gap_level_reading"]) + ';'
                    other += 'gender_gap_level_math: ' + ','.join(data[i]["gender_gap_level_math"]) + ';'
                    other += 'tests_below_standard_percentage: ' + ','.join(data[i]["tests_below_standard_percentage"]) + ';'
                    other += 'tests_not_written_percentage: ' + ','.join(data[i]["tests_not_written_percentage"]) + ';'
                    french_percentage = ''

                if data[i]["school_type"] == 'public' or data[i]["school_type"] == 'Public':
                    isPublic = 1
                else: 
                    isPublic = 0
                #insert val param
                #valuesTuple = (data[i]["school_name"],str(school_type) , data[i]["city"] , str(isPublic) , data[i]["current_rank"] ,data[i]["website"] ,data[i]["school_name"] ,data[i]["esl_percentage"] ,data[i]["school_phone"], data[i]["school_address"] ,','.join(data[i]["overall_rating"]),  french_percentage , data[i]["school_district"] , data[i]["five_year_rank"] , data[i]["special_needs_percentage"] , other)
                #cur.execute("insert into app.school_new (en_name,type,city,isPublic,national_rank,website,title,esl,contact,address,overall_rating,french_percentage,school_district,five_year_rank,special_needs_percentage,other) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", valuesTuple)
                #update val param
                vals = (data[i]["five_year_rank"],data[i]["current_rank"], ','.join(data[i]["overall_rating"]),other,data[i]["school_name"],  data[i]["city"], str(school_type))
                cur.execute('update school_new set five_year_rank = %s, national_rank = %s, overall_rating=%s, other=%s where en_name=%s and city=%s and type=%s', vals)

                   

        conn.commit()
        cur.close()
        conn.close()
        print 'Successfully inserted into mysql!'

    except MySQLdb.Error,e:
        #print 'Scrapping page:' + data[i]["school_name"].encode('utf8')
        print "Mysql Error %d: %s" % (e.args[0],e.args[1])

def main():
    readFromDB()
    sys.exit(0)


if __name__ == '__main__':
    main()