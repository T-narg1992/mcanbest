#encoding=utf-8
import getopt
import sys,os
import re
import MySQLdb
import glob
import xml.dom.minidom
import json, re

default_encoding = 'utf-8'
if sys.getdefaultencoding() != default_encoding:
    reload(sys)
    sys.setdefaultencoding(default_encoding)

def readFromDB():

    doc_id = 0
    total = 0

    try:
        conn = MySQLdb.connect(host='www.liunar.net',user='janie',passwd='shao',port=3306)

        cur = conn.cursor()
        cur.execute('use app')
        cur.execute('set names utf8')

        jsonfiles = ['/home/janie/spider/taisha/taisha.json', '/home/janie/spider/liuxue/liuxue.json']#,'/home/janie/spider/liuxue86/liuxue86.json']
	#jsonfiles = ['/home/janie/spider/wechat/mgc_1.json']#, '/home/janie/spider/wechat/mgc_2.json', '/home/janie/spider/wechat/mgc_3.json', '/home/janie/spider/wechat/mgc_4.json']
        # 0:old data,1：海外规划,2：留学申请,3：校园内外,4：本地生活,5：实习就业,6：职业移民,7: 青少年教育
        category1 = [u'海外规划'.encode('utf-8'), u'加拿大签证'.encode('utf-8'), u'留学签证'.encode('utf-8'), u'行前准备'.encode('utf-8'), u'加拿大预科'.encode('utf-8'), u'加拿大高中'.encode('utf-8'), u'加拿大研究生'.encode('utf-8')]
        category2 = [u'留学申请'.encode('utf-8'), u'加拿大专业'.encode('utf-8'), u'加拿大院校'.encode('utf-8'), u'留学准备'.encode('utf-8'), u'留学动态'.encode('utf-8'), u'留学印象'.encode('utf-8'), u'留学费用'.encode('utf-8'), \
            u'申请指南'.encode('utf-8'), u'大学排名'.encode('utf-8'), u'奖学金'.encode('utf-8'), u'热门专业'.encode('utf-8'), u'留学申请'.encode('utf-8')]
        category3 = [u'校园内外'.encode('utf-8'), u'加拿大教育'.encode('utf-8'), u'加拿大本科'.encode('utf-8')]
        category4 = [u'留学新闻'.encode('utf-8'), u'本地生活'.encode('utf-8')]
        category5 = [u'实习就业'.encode('utf-8')]
        category6 = [u'职业移民'.encode('utf-8'), u'加拿大移民'.encode('utf-8'), u'加拿大就业'.encode('utf-8')]
        category7 = [u'青少年教育'.encode('utf-8')]
	categories = [category1, category2, category3, category4, category5, category6, category7]

        for f in range(len(jsonfiles)):
            jsonfile = open(jsonfiles[f])
            data = json.load(jsonfile)
            jsonfile.close()

            for i in range(len(data)):
                description = data[i]["description"]
                if len(description)>=100:
                    description = description[:96] + '...'

                category = data[i]["category"]
                for j in range(0,7):
                    if category in categories[j]:
                        category = j+1
                        break

                if data[i]["thumbnail"].split(".")[-1] in ['jpg', 'jpeg', 'png', 'gif']:
                    have_thumbnail = 1
                else:
		    data[i]["thumbnail"] = ''
                    have_thumbnail = 0
	
                cur.execute("insert into app.article (title, uid, description, category, view_count, source_url, source, create_time, update_time, status, content_html, content_text, thumbnail, have_thumbnail) VALUES ('" \
                    + data[i]["title"].encode('utf-8') + "', 0, '" + description.encode('utf8') + "', " + str(category) + ", FLOOR(1 + RAND()*50), '" + data[i]["pageurl"].encode('utf8') + "', '" + data[i]["source"].encode('utf8') + "', UNIX_TIMESTAMP('" \
                    + data[i]["create_time"].encode('utf-8') + "')" + ", UNIX_TIMESTAMP(CURRENT_TIME())" + ", 2, '" + data[i]["content"].encode('utf8') + "', '" + data[i]["textContent"].encode('utf8') + "', '" + data[i]["thumbnail"].encode('utf-8') + "', " + str(have_thumbnail) + ");")
        
	cur.execute("DELETE n1 FROM app.article n1, app.article n2 WHERE n1.id < n2.id AND n1.title = n2.title;")
	cur.execute("update app.article set content_text = SUBSTRING_INDEX(content_text, '【太傻留学热门专题推荐】', 1);")
	cur.execute("update app.article set content_html = SUBSTRING_INDEX(content_html, '【太傻留学热门专题推荐】', 1);")
        conn.commit()
	cur.close()
        conn.close()
	print "Successfully inserted new articles!"
    except MySQLdb.Error,e:
        print "Mysql Error %d: %s" % (e.args[0],e.args[1])
	print 'current record:' + data[i]["title"].encode('utf-8')

def main():
    readFromDB()
    sys.exit(0)


if __name__ == '__main__':
    main()
