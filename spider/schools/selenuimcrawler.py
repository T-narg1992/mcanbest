
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import json

driver = webdriver.Chrome()
driver.get("http://timesrank.uker.net/")
elems = driver.find_elements_by_xpath("//li[contains(@class,'li1') or contains(@class,'li2') ]")
alist=[]
count = 1
for elem in elems:
	count+=1
	d={}
	try:
		d['rank-2017'] = (elem.find_element_by_xpath(".//p[contains(@class, 'bgp2') and contains(@class, 'bgli1')]").text).encode('utf-8')
		d['rank-2016'] = (elem.find_element_by_xpath(".//p[contains(@class, 'bgp2') and contains(@class, 'bgli2')]").text).encode('utf-8')
		d['name'] = (elem.find_element_by_xpath(".//p[@class='bgp14']").text).encode('utf-8').split('\n')[0]
		d['location'] = (elem.find_element_by_xpath(".//p[@class='bgp14']").text).encode('utf-8').split('\n')[1]
		alist.append(d)
	except Exception as e:
		print count
with open('timesrankresults.json','w') as f:
	json.dump(alist,f,ensure_ascii=False, indent=4)
f.close()
# _list=[]
# assert "No results found." not in driver.page_source
driver.close()

