#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import json
import io
import unittest
import re
import urllib
import os.path
import MySQLdb

BaseUrl = 'https://www.applysquare.com'

def FileExists(fullpath):
	is_file = os.path.exists(fullpath)
	return is_file

def GetFile(url, relpath, filename):
	urllib.urlretrieve(url,relpath+ '/'+ filename)

class ApplySquareList(object):
	def __init__(this):
		this.PageMax = 34
		this.SchList = []

	def LoadJsonObj(this,fname):#'RankingListPages.json'
		with open(fname,'r') as f:
			this.SchList = json.load(f)
		f.close()
		return this.SchList

	def ParseListPage(this):
		for c in xrange(1,this.PageMax+1): ##
			r  = requests.get(this.GetListUrl(c))
			parsedPage = BeautifulSoup(r.text, 'lxml')
			schoolsOnThisPage =  parsedPage.findAll('div', class_="institute-lite")
			for xa in schoolsOnThisPage:
				sch = ApplySquareSch()
				url = BaseUrl +xa.find('a')['href']
				schTag = xa.find('a')['href'].split('/')[2]
				country = xa.findAll('p', class_='ellipsis')[1].text.encode('utf-8').strip()
				setattr(sch, 'url', url)
				setattr(sch, 'tag', schTag)
				setattr(sch, 'country', country)
				logErr( 'on school ' + getattr(sch,'tag'))
				if(FileExists('./pictures'+'/'+getattr(sch,'tag')+'-logo.jpg')):
					pass
				else:
					img = xa.find('img')
					if(img):
						GetFile(img['src'], './pictures', getattr(sch,'tag')+'-logo.jpg')
				setattr(sch, 'ch_name', xa.find('span',class_='institute-school-name').text.encode('utf-8'))
				setattr(sch, 'en_name', xa.find('span',class_='institute-school-name').next_sibling.text.encode('utf-8'))
				writeToJson(sch.ParseDetailPage(url))
			print 'finished page '+str(c)
 		print 'finished all!'
	def GetListUrl(this,incrementor):
		this.listurl = 'https://www.applysquare.com/ranking-cn/?ranking_key=qs&page='+str(incrementor)
		return this.listurl

class ApplySquareSch(object):
	def __init__(self):
		self= self
		
	map= {'公立/私立': 'isPublic', '校园位置':'location', '地理位置':'areaDescription','校园':'campus','宿舍':'dorms', '历史文化':'historyCulture', '知名校友':'famousAlumni', '体育':'sports', '事实':'facts','犯罪率（犯罪数量/学生数量）':'crimeRate', '州外学生比例':'foreignRatio','本科生数量':'undergradCount','研究生数量':'gradCount','学生组织数量':'clubsCount','学术族裔分布(Hispanic)':'hispanicRatio','学术族裔分布(Black)':'blackRatio', '学术族裔分布(White)':'whiteRatio','学术族裔分布(Asian)':'asianRatio', '录取难度':'entryDifficulty', '录取率':'admissionRate', '录取后入学比例':'admitAndBegin','申请者高中成绩前50%比例':'top50GradApplyRatio', '申请者高中成绩前25%比例':'top25GradApplyRatio', '申请者高中成绩前10%比例':'top10GradApplyRatio', 'SAT语言':'scoreSat', 'SAT数学':'satMath', 'SAT写作':'satWriting', 'ACT综合':'satOverall', 'ACT英语':'actEnglish', 'ACT数学':'actMath', 'ACT写作':'actWriting', '学杂费（州内）':'instateGeneralTuition', '课本费（州内）':'instateTextBooksTuition', '总体花费（州内）':'instateTotalTuition', '学杂费（州外）':'foreignGeneralTuition', '课本费（州外）':'foreignTextBooksTuition', '总体花费（州外）':'foreignTotalTuition', '姐妹会数量':'sororities', '兄弟会数量':'fraternities', '地址':'address'}

	def ParseDetailPage(self, url):
		r  = requests.get(url)
		parsedPage = BeautifulSoup(r.text, 'lxml')
		for br in parsedPage.find_all('br'):
			br.extract()
		contentboxes = parsedPage.findAll('div',class_="box")
		for i in range( len(contentboxes)):
			contentbox = contentboxes[i]
			img = contentbox.find('img',class_="hidden-xs")
			try:
				if(FileExists('./pictures'+'/'+getattr(self,'tag')+'-cover.jpg')):
					pass
				else:
					if(img):
						GetFile(img['src'], './pictures', getattr(self,'tag')+'-cover.jpg')
			except Exception as ex: 
				logErr('couldnt get cover img for ' + getattr(self,'tag'))
				raise ex
			try:		
				if(contentbox.find('div', class_="br-gain-height")):#is description 
					self.intro = contentbox.find('div', class_="br-gain-height").get_text().encode('utf-8')
			except Exception as e: 
				logErr('couldnt get description for' + getattr(self,'tag'))
				pass
			try:
				header = contentbox.previous_sibling.get_text().encode('utf-8')
				if header == '院校排名':
					for atag in contentbox.findAll('h1'):
						href = atag.parent['href']
						rank_label = href[href.index('ranking:')+8:]
						rank = atag.text.encode('utf-8')
						setattr(self, rank_label, rank)
				elif header == '校园':
					for x in contentbox.findAll('strong'): 
						enKey = self.map[x.text.encode('utf-8')]
						find = '(\[\d+\])'
						replaced = ''
						s= x.next_sibling.strip().encode('utf-8')
						content = re.sub(find, replaced, s) # get rid of ie:[32]
						setattr(self, enKey, content)
				elif header == '学生组成' or header == '入学统计' or header == '标准化考试' or header == '学费花销':
					for x in contentbox.findAll('strong'): 
						enKey = self.map[x.text.encode('utf-8')]
						content= x.next_sibling.strip().encode('utf-8')
						setattr(self, enKey, content)
				elif header == '历史文化':
					for x in contentbox.findAll('strong'): 
						enKey = self.map[x.text.encode('utf-8')]
						content= x.next_sibling.strip().encode('utf-8')
						setattr(self, enKey, content)
				elif header == '知名校友':
					enKey = self.map['知名校友']
					v =  contentbox.text.encode('utf-8').replace("知名校友", "").replace('.', '.<br>').strip()
					setattr(self, enKey, v)
				elif  header == '体育' or header == '事实':
					for x in contentbox.findAll('strong'): 
						enKey = self.map[x.text.encode('utf-8')]
						content= x.next_sibling.strip().encode('utf-8')
						setattr(self, enKey, content)
			except Exception, e:
				logErr('couldnt get '+header+' info '+ getattr(self,'tag'))
				pass
		return self

class DBWriter(object):

	def __init__(self):
		self.conn = MySQLdb.connect(host='www.liunar.com',user='alex',passwd='alexsong',port=3306,charset='utf8')
		self.cur = self.conn.cursor()
		self.queries = []
		self.testQuery =''
	def GenerateQueryList(self):

  		pass
  	def GenerateTestQuery(self, schlist):
  		self.cur.execute('use app')
		self.cur.execute('set names utf8')

  		for x in xrange(len(schlist)):
  			testquery = ''
			cols =''
		  	vals = []
		  	param_holder_string=''
  			sch = schlist[x]
	  		for k, v in sch.iteritems():
				cols += ','+k
				vals.extend((v,))
				param_holder_string+=',%s'
		  	testquery = 'INSERT INTO apply_square_schools ('+cols[1:]+') VALUES ('+param_holder_string[1:]+')'
		  	try:
		  		self.Insert(testquery, tuple(vals))
		  	except Exception, e:
		  		logErr(str(e))
  				
		self.cur.close()
		self.conn.close()

	def Insert(self, query, params):
		self.cur.execute(query, params)
		self.conn.commit()


def writeToJson(obj):
	with open('testaa.json','a') as f:
		json.dump(obj.__dict__,f,ensure_ascii=False, indent=4)
		f.write(',')
	f.close()

def logErr(err):
	with open('err.log','a') as f:
		f.write(err+'\n')
	f.close()

l = ApplySquareList()
db = DBWriter()
# db.GenerateTestQuery(l.LoadJsonObj('RankingListPages.json'))
