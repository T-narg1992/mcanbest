#!/usr/bin/env python
import getopt
import sys,os

class UrlDB(object):
    def __init__(self,db_file):
        self._count = 0
        self._set = set()
        self._db_file = db_file
        if ( os.path.exists(db_file) ):
            fp = open(db_file)
            for line in fp:
                line = line.replace('\n','')
                self._set.add(line)
            fp.close()
        
    def fetched(self,url):
        if url not in self._set :
            return False
        else:
            print '%s fetched already, db size:%d' %(url,len(self._set))
            return True
    
    def addurl(self,url):
        self._set.add(url)
        ++self._count
        if ( self._count % 1000 ) == 0:
            self.dump2file()

    def dump2file(self):
        fp = open(self._db_file,'w')
        for url in self._set:
            fp.write(url+os.linesep)
        fp.close()
