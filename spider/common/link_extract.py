import urllib2
from sgmllib import SGMLParser

class LinkExtract(SGMLParser):

    def reset(self):                           
        SGMLParser.reset(self)
        self.urls = []

    def start_a(self, attrs):                   
        href = [v for k, v in attrs if k=='href']
        if href:
            self.urls.extend(href)

def main(): # test funciton
    f = urllib2.urlopen("http://www.youku.com/")
    if f.code == 200:
        parser = LinkExtract()
        parser.feed(f.read())
        f.close()
        for url in parser.urls:
            print url

if __name__ == '__main__':
    main()
