class NewsItem(object): #container of video meta info
    
    def __init__(self):
        self.pageurl = ''
        self.title = ''
        self.category = ''
        self.create_time = ''
        self.description = ''
        self.imagesource = ''
        self.content = ''
        self.textContent = ''
        self.source = ''
        self.thumbnail = ''