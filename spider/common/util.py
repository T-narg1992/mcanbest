def removeContentInTag(origin_content):
    inTag = False
    new_content = list()
    origin_content = origin_content.replace('<p>','liuxue_para_mark1').replace('</p>','liuxue_para_mark2')
    for c in origin_content:
        if c == '<':
            inTag = True
            continue
        if c == '>':
            inTag = False
            continue
        if inTag == True:
            continue
        new_content.append(c)

    return ''.join(new_content).replace('liuxue_para_mark1','<p>').replace('liuxue_para_mark2','</p>').replace('\n', '').replace('\r', '').replace('\t','').encode('utf8')