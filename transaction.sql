CREATE TABLE Orders(
    id INT NOT NULL AUTO_INCREMENT,
    productId INT NOT NULL,
    price INT NOT NULL COMMENT '以分为单位', #以分为单位
    liunarId INT COMMENT '微信认证用户，或者云招宝用户购买', #微信认证用户，或者云招宝用户购买;
    referId INT,
    yunId INT COMMENT '当用户支付成功后，云招宝先产生一条用户线索，再获取用户线索的ID', #当用户支付成功后，云招宝先产生一条用户线索，再获取用户线索的ID
    name varchar(50) NOT NULL COMMENT '姓名必填',
    phone BIGINT NOT NULL COMMENT '电话必填',
    wechat varchar(50),
    PRIMARY KEY (id),
    FOREIGN KEY (userId) REFERENCES member(id)
)

CREATE TABLE Transactions(
    id INT NOT NULL AUTO_INCREMENT,
    orderId INT NOT NULL,
    fee INT NOT NULL COMMENT '以分为单位', #以分为单位
    method INT NOT NULL COMMENT '1: wechat; 2: AliPay; 3: cashier;', # 1: wechat; 2: AliPay; 3: cashier;
    timestamp INT NOT NULL,
    wx_outtradeno varchar(100),
    wx_transaction_id varchar(100),
    wx_timeend INT,
    wx_openid varchar(100),
    wx_feetype varchar(15),
    PRIMARY KEY (id),
    FOREIGN KEY (orderId) REFERENCES Orders(id)
) 