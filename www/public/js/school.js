angular.module("school",['liunarBackend','directives','factories','chart.js'])
.controller("schoolCate", function($scope, wechatShare){
    wechatShare("留哪儿院校排名榜","http://www.liunar.com/img/school-default.png","");
})
.controller('ApplySquareListController', function($http, $scope,wechatShare,$rootScope){
    $scope.wechatshare_title = ''

    var url = $rootScope.serverUrlPrefix+ "school/ap_sq/tags";
        $.ajax({
            type: "get",
            url: url,
            success: function(data){
                $scope.countriesfull = data.countries
                $scope.rankingsfull = data.ranking.slice(1);
                $scope.$apply(function(){
                    $scope.countries = data.countries.slice(0,9);
                    $scope.rankings = data.ranking.slice(1,5);
                })
            }
            });
       $scope.search = function(){
         $scope.country = $('input[name=country]:checked').val()
         $scope.ranking = $('input[name=ranking]:checked').val()
        var url;
        if( $scope.country == 'all' || !$scope.country ){
            url = '/schools/world/'+$scope.ranking;
        }else{
            url = '/schools/world/'+$scope.ranking+'/'+$scope.country
        }
        window.location.href = url;
     };

        $('#MoreRanks').click(function() {
             $scope.$apply(function(){
                    $scope.rankings = $scope.rankingsfull;
                })
              $("input[name='ranking'][value='"+$scope.country+"']").prop("checked", true);
             $(this).hide();
        })
                $('#MoreCountries').click(function() {
             $scope.$apply(function(){
                   $scope.countries = $scope.countriesfull;
                });
              $("input[name='country'][value='"+$scope.country+"']").prop("checked", true);
              $(this).hide();
        });
                angular.element(document).ready(function () {
                    var path = location.pathname;
                    var pathParamsBegin = path.indexOf('world')+'world'.length;
                    wechatShare($scope.wechatshare_title, 'http://www.liunar.com/img/worldwechatshare.jpg', "留哪儿权威大学排名榜");    

                    var BindScopeVals = function(){
                        var pathParams = path.substring(pathParamsBegin);
                        if(pathParams.split('/')[1] && pathParams.split('/')[1]!= 'undefined'){
                            console.log(pathParams.split('/')[1] )
                           $scope.ranking = pathParams.split('/')[1];
                        }
                         if(pathParams.split('/')[2] && pathParams.split('/')[1]!= 'undefined'){
                            console.log(pathParams.split('/')[1] )
                              $scope.country=pathParams.split('/')[2];
                    }
                };
                BindScopeVals();
                    if($scope.country){
                        $("input[name='country'][value='"+$scope.country+"']").prop("checked", true);
                    }else{
                        $("input[name='country'][value='all']").prop("checked", true);
                    }
                    if($scope.ranking){
                        $("input[name='ranking'][value='"+$scope.country+"']").prop("checked", true);
                    }else{
                        $("input[name='ranking'][value='qs']").prop("checked", true);
                    }
                console.log($scope.country,$scope.ranking);
});
          
})
.controller("schoolFliterList", function($location, $rootScope, $http, $scope, wechatShare, en_ch){
        $scope.initShare = function(t, i, d){
            console.log(t);
        wechatShare(t,i,d);
    };
    $scope.city = "Surrey";
    $scope.fliter = {
        // type: "",
        // city: "",
        // public: "",
        // province: "",
    };
    if($scope.filterOn){
        $scope.allCities = en_ch.cities;    
    }
    
    $scope.schoolListsRank = [];
    $scope.page = 1;

    $scope.allCities = en_ch.cities;

    var isArray = function(arg) {
        return Object.prototype.toString.call(arg) === '[object Array]';
    };

    function getSchools(type, city, public, province, num, page, callback){
        var baseUrl = $rootScope.serverUrlPrefix + "school/" + type + "/" + num + "/" + page;
        if($scope.filterOn){
            baseUrl +="?city=" + city + "&isPublic=" + public + "&province=" + province;
        }
        $http({
            url:  baseUrl,
            method: "GET",
                //cache: true,
            }).then(
            function(res){
                //console.log(res.data);
                if(isArray(res.data) == true && res.data.length){
                    callback(res.data);
                }else{
                    callback('noMore');
                }
            }
            );
        };

        $scope.initSchool = function(){
            getSchools($scope.fliter.type,  $scope.fliter.city == "all" ? '':$scope.fliter.city, $scope.fliter.public == "all" ? '' : $scope.fliter.public, $scope.fliter.province, 10, $scope.page, function(res){
                $scope.schoolListsRank = [].concat($scope.schoolListsRank, res);
            });
        };

        $scope.haveMore = true;
        $scope.getMoreSchool = function(){
            $scope.page ++;

            if($scope.haveMore == true){
                getSchools($scope.fliter.type,  $scope.fliter.city == "all" ? '':$scope.fliter.city, $scope.fliter.public == "all" ? '' : $scope.fliter.public, $scope.fliter.province, 10, $scope.page, function(res){
                    if(res != 'noMore'){
                        $scope.schoolListsRank = [].concat($scope.schoolListsRank, res);
                    }else{
                        $scope.haveMore = false;
                    }
                });
            }else{
                console.log("end");
            }
        };

        $scope.search = function(){
            var string = "?&search=search";
            if($scope.fliter.public && $scope.fliter.public != "all"){
                string += "&isPublic=" + $scope.fliter.public;
            }
            if($scope.fliter.city && $scope.fliter.city != "all"){
                string += "&city=" + $scope.fliter.city;
            }
            window.location.search = string;
        }
    })
.controller('USListController',function($location, $rootScope, $http, $scope, wechatShare){
    wechatShare("美国院校排名大全", 'http://www.liunar.com/img/americanflag.jpg', "美国大学权威排名榜");    

    $scope.page = 1;
    $scope.index = 10;
    $scope.haveMore = true;
    $scope.USList = [];
    $scope.lock = false;
    $scope.getMoreSchool = function(){
        if($scope.lock == false){
            $scope.lock = true; //locked when getmoreschool called to prevent multiloading;
        var baseUrl = 'http://www.liunar.com/api/school/us/1'; //1 = type for universities  
        baseUrl+= '/' + $scope.page * $scope.index + '/' + $scope.index; 
        if($scope.haveMore == true){
         $.ajax({
            type: "get",
            url: baseUrl,
            success: function(data){
                $scope.$apply(function(){
                 $scope.page ++;
                 if(data.length < 10){
                    $scope.haveMore = false;
                }
                $scope.USList = [].concat($scope.USList, data);
                $scope.lock = false;
            });
            }
        });
     }else{
        console.log("end");
    }
}
};
})
.controller('UKListController', function(wechatShare, $scope){
    wechatShare("英国院校排名大全", 'http://www.liunar.com/img/englandflag.jpg', "英国大学权威排名榜");    
})
.controller('USOneController', function(wechatShare, $scope){
       $scope.initShare = function(t, i, d){
        wechatShare(t,i,d);
    };
})
.controller("schoolDetail", function($rootScope, $http, $scope, wechatShare, en_ch){

    $scope.initShare = function(t, i, d){
        wechatShare(t,i,d);
    };

    var isArray = function(arg) {
        return Object.prototype.toString.call(arg) === '[object Array]';
    };

    $scope.mapModules = [
    {
        text: "街道地图",
        value: "google.maps.MapTypeId.ROADMAP"
    },
    {
        text: "实景街图",
        value: "streetView"
    },
    {
        text: "鸟瞰地图",
        value: "google.maps.MapTypeId.SATELLITE"
    }
    ];
    
    $scope.selected = $scope.mapModules[0];
    $scope.mapType = $scope.mapModules[0].value;
    
    $scope.select = function(item){
        //console.log(item);
        $scope.selected = item;
        $scope.mapType = item.value;
    };

    function fetchData(data, callback){
        var rankData;

        if(data !== null){
            rankData = data.split(",");
            rankData.pop();
            //console.log(rankData.indexOf("n/a"))
            if(rankData.indexOf("n/a") !== -1){
                rankData.forEach(
                    function(a, i){
                        if(a == "n/a"){
                            rankData[i] = 2.5;
                        }
                    }
                    )
            }
        }else{
            rankData = [];
        }
        callback(rankData);
    };


    $scope.initChart = function(data){
        fetchData(data, function(rankData){
            console.log(rankData);
            $scope.labels = ["2011","2012","2013","2014","2015"];
            $scope.data = [
            rankData
            ];
            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
            $scope.options = {
                scaleOverride: true,
                scaleSteps: 4,
                scaleStepWidth: 2.5,
                scaleStartValue: 0
            };
        });
    };

    $scope.tableInit = function(data){
        var renderedTable = {};

        if(data !== null){
            var table = data.split(";");
            table.pop();
            table.forEach(
                function(a,i){
                    //console.log(a,i);
                    var b = a.split(":");
                    //console.log(b);
                    if(b[1].indexOf(",")!==-1){
                        var c = b[1].split(",");
                        c.pop()
                        renderedTable[b[0]] = c;
                    }else{
                        $scope.firstValue = b[1];
                        $scope.firstKey = en_ch.schoolTable[b[0]];//schoolTable[b[0]];
                    }
                }
                )
        }
        
        $scope.table = renderedTable;
    };

    $scope.translate = en_ch.schoolTable;

    function getSchoolComment(id, callback){
        $http({
            url:  $rootScope.serverUrlPrefix + "comments/2/" + id,
            method: "GET",
        }).then(
        function(res){
            if(isArray(res.data) == true && res.data.length){
                callback(res.data);
            }else{
                callback("");
            }
        }
        );
    };

    $scope.getSchoolComment = function(id){
        getSchoolComment(id, function(res){
            if(res !== ""){
                $scope.schoolComment = res;
            } 
        });
    };

})
