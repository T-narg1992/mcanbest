angular.module("pay",['liunarBackend','directives','factories'])
.controller("payController", function($scope, $rootScope,wechatShare,localStorage, $http, wechatPay){

  var productTitle = $('#productTitle').text();
  var coverSrc = $('#coverSrc').val();
  var description = $('#description').text();
  var shareLink = "http://www.liunar.com/testpay/paycallbackredirect?o_id="+$('#o_id').text();
  var page = $('#page').text();

  if(page == 'product'){
    wechatShare(productTitle, coverSrc, description);
  }else if(page == 'share'){
    wechatShare(productTitle, coverSrc, description);
  }else{
    wechatShare(productTitle, coverSrc, description, 1, shareLink);
  }

  $scope.saveName = function(){
   var phone = $('#other_addr_tel').val();
   var name = $('#other_addr_name').val();
   localStorage.setItem("name", name);
   // localStorage.setItem("phone", phone);
 }


 $scope.pay = function(t, nstr, pkg, stype, psign, curl){
  wechatPay(t, nstr, pkg, stype, psign, curl).then(
    function(res){

                /*$http({
                    url: $rootScope.serverUrlPrefix + "/pay/wxPayinfo",
                    method: "POST",
                    data: {
                        info: JSON.stringify(res.detail)
                    }
                  })*/
                  window.location.href=curl;
                },
                function(err){
                  alert(JSON.stringify(err.detail));
                }
                );
};

})