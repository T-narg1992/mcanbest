angular.module("home",['liunarBackend','directives','factories','slickCarousel'])
.controller("homeCtrl", function(seedUrl, $window, $scope, $http, $location, userAuth, wechatShare, $rootScope){
    $scope.slickConfig = {
        enabled: true,
        autoplay: true,
        draggable: true,
        autoplaySpeed: 3000,
        dots: true,
        arrows: false,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };

    $scope.tags = [
        {kw: "留学规划"},
        {kw: "留学生活"},
        {kw: "观点"},
        {kw: "中小学"},
        {kw: "常青藤"},
        {kw: "热门专业"}
    ];

    function searchApi(kw, callback){
        $http({
            url: "/search/search?kw=" + kw + "&start=0&limit=9",
            method: "GET"
        }).then(
            function(res){
                callback(res.data);
            }
        )
    };

    $scope.search = function(index, kw){
        $scope.selected = index;
        searchApi(kw, function(res){
            $scope.lists = res;
        })
    };

    $scope.search(0, $scope.tags[0].kw);

    wechatShare("留哪儿","http://www.liunar.com/img/icon.png","让孩子选择世界");
})