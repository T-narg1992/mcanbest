angular.module("product", ['liunarBackend', 'directives', 'factories', 'slickCarousel'])
.controller("productController", function ($window, $scope, $http, $location, userAuth, wechatShare, $rootScope) {

  var productTitle = $('#productTitle').text();
  var coverSrc = $('#coverSrc').val();
  var description = $('#description').text();
  wechatShare(productTitle, coverSrc, description);

  var loginStatus = new userAuth.checkLogin();
  console.log(loginStatus);
  if ($location.$$absUrl.indexOf('?referid=') === -1) {
      //没有 refer id：
      if (loginStatus.isLogin === false) {
        return;
      } else {
        window.location = $location.$$absUrl + "?referid=" + loginStatus.uid;
        window.referUId = loginStatus.uid;
      }
    } else {
      //have referer id already
      window.referUId = window.location.search.split('=')[1]
    }
  })
.controller('ListController', function ($scope, $http, $document) {
  var cur_map = {
    'CAD': '加币',
    'CNY': '人民币',
    'AUD': '澳币',
    'USD': '美元',
    'EUR': '欧元'
  }
  var product_map = {
    "冬夏令营": 0,
    "背景提升": 1,
    "中小学": 2,
    "特色学院": 3,
    "知名大学": 4,
    "团购产品":5
  }

  var loadFilter = function () {
    $.ajax({
      type: "get",
      url: 'http://www.liunar.com/api/productnew/filter/' + product_map[$('#getCategory').val()],
      success: function (data) {
        console.log(data);
        $scope.$apply(function () {
          $scope.filterhtml = data;
        });
        $('label').click(function () {
          $(this).prev().trigger('click');
        })
      }
    });
  };

  var getMoreProducts = function (post) {
    console.log(document._active, $scope.end, document._active != 'filter' && $scope.end == false)
      if (document._active != 'filter' && $scope.end == false) { //escapes scrolldown error if blank angular list
        $.ajax({
          type: "post",
          url: 'http://www.liunar.com/api/productnew/list',
          data: post,
          success: function (data) {
            console.log(data);
            if (data.err) {
              $('#errHeader').css('display', 'block');
            } else {
              $('#errHeader').css('display', 'none');
            }
            if (data.length < post.count) { // not 6
              $scope.end = true;
            }
            for (var i = 0; i < data.length; i++) {
              cur = data[i].currency;
              data[i].currency = cur_map[cur];
              tags = data[i].tags.split(',');
              tags.splice(-1, 1)
              if (tags.length > 3) {
                data[i].tags = tags.slice(0, 4);
              } else {
                data[i].tags = tags;
              }
              $scope.$apply(function () {
                $scope.lists.push(data[i]);
              });
            }
          }
        });
        console.log('got from index:', $scope.post.index, 'to', $scope.post.index + 6);
        $scope.post.index += $scope.post.count; //will go over total available if total is a multiple of post.count
      }
    };
    var resetScopeValues = function () {
      $scope.lists = [];
      $scope.end = false;
      $scope.post.index = 0;
    };
    $scope.end = false;
    $scope.post = {};
    $scope.lists = [];
    $scope.post.type = $('#getCategory').val();
    $scope.post.index = 6;
    $scope.post.count = 6;
    $scope.getTrigger = function () {

      $scope.post.json = $('#angularSender').val();
      console.log($scope.post.json);
      if ($scope.post.json == '[]') { // only master list; user made no selections OR user selected 1/more 不限
        location.reload();
      } else {
        resetScopeValues();
        $scope.end = true;
        $('#angular').fadeIn();
        $('#list').fadeOut();
        $('#filter').fadeOut();
        $scope.end = false;
        document._active = 'angular';
        getMoreProducts($scope.post);
      }
    }

    $scope.loadMore = function () {
      getMoreProducts($scope.post);
    };

    loadFilter()

    // $scope.ages = {
    //     min: 2,
    //     max: 16
    // };

    //     //data for min-max range
    //     $scope.agesRange = {
    //         min: 8,
    //         max: 24,
    //     };

    //     $scope.getValue = function(){
    //         console.log($scope.ages.min, $scope.ages.max);
    //     };
  })
.controller("desktopList", function ($rootScope, $scope, $http, tagsFactory, tagsCollection) {

  $scope.slickConfig = {
    enabled: true,
    autoplay: true,
    draggable: true,
    autoplaySpeed: 3000,
    dots: true,
    arrows: false,
    method: {},
    event: {
      beforeChange: function (event, slick, currentSlide, nextSlide) {},
      afterChange: function (event, slick, currentSlide, nextSlide) {}
    }
  };

  function getSchools(data, callback) {
    $http({
      url: $rootScope.serverUrlPrefix + "productnew/list",
      method: "POST",
      data: data
    }).then(
    function (res) {
      console.log(res);
      callback(res.data);
    }
    )
  };

  $scope.page = 6;

  $scope.products = [];

  $scope.filter = false;

  $scope.initSchool = function (type) {
    $scope.data = {
      index: $scope.page,
      type: type,
      count: 200
    }
    getSchools($scope.data, function (products) {
      if (products.length) {
        $scope.products = [].concat($scope.products, products);
      }
    });
  };

  $scope.tags = tagsFactory;

  $scope.initTag = function (type) {
    tagsCollection(type, function (res) {
      $scope.ajaxTags = res;
      $scope.indexs = Object.keys($scope.ajaxTags);
    });
  };



  $scope.selected = ["", "", ""];

  $scope.keys = function (key) {
    console.log($scope.selected);
  }

  $scope.select = function (type) {
    $scope.filter = true;
      /*var actual_array = $scope.selected.filter(function(e) { 
        return e.length;
      });*/
      var actual_array = $scope.selected.filter(
        function (e) {
          if (e.length) {
            return e.length && e != "";
          }
        }
        );
      console.log($scope.selected, actual_array);
      $scope.products = [];
      if (actual_array && actual_array.length) {
        $scope.filterData = {
          index: 0,
          type: type,
          count: 200,
          json: JSON.stringify(actual_array)
        }
      } else {
        $scope.filterData = {
          index: 0,
          type: type,
          count: 200
        }
      }
      getSchools($scope.filterData, function (products) {
        if (products.length && products[0]) {
          $scope.products = products;
        } else {
          $scope.products = [];
          $scope.noProduct = true;
        }

      })
    };
  })
.controller("desktopCamp", function ($scope, $rootScope, $http, userAuth, $location) {

  var loginStatus = new userAuth.checkLogin();
  console.log(loginStatus);
  if ($location.$$absUrl.indexOf('?referid=') === -1) {
      //没有 refer id：
      if (loginStatus.isLogin === false) {
        //return;
      } else {
        window.location = $location.$$absUrl + "?referid=" + loginStatus.uid;
        window.referUId = loginStatus.uid;
      }
    } else {
      //have referer id already
      window.referUId = window.location.search.split('=')[1];
    }
    $scope.order = {
      time: "0",
      index: "",
    };

    $scope.sIndex = 0;

    $scope.setIndex = function (i) {
      $scope.sIndex = i;
    };

    $scope.openForm = function () {
      console.log($scope.order.time)
      if ($scope.order.time !== "") {
        $('#orderForm').modal('show');
      } else {
        $('#selectTime').modal('show');
      }
    };

    $scope.openPayForm = function () {
      console.log($scope.order.time)
      if ($scope.order.time !== "") {
        $('#payForm').modal('show');
      } else {
        $('#selectTime').modal('show');
      }
    };

    $scope.payInfo = {
      pid:"",
      name:"",
      tel:"",
      paytype: "DP",
      referId: window.referUId,
    };

    $scope.nativePay = function(){
      if (!$scope.payInfo.name || $scope.payInfo.name.length < 2) {
        $scope.payNameFail = true;
        $scope.errName = "请输入您的名字且不少于2个字";
      } else {
        $scope.payNameFail = false;
      }

      if (/(\d){6,}/g.test($scope.payInfo.tel) == false) {
        $scope.payPhoneFail = true;
        $scope.errPhone = "请输入您的有效电话";
      } else {
        $scope.payPhoneFail = false;
      }
      if ($scope.payPhoneFail == false && $scope.payNameFail == false) {
        $('#qrModal').modal('show');
        $('#payForm').modal('hide');
        $http({
          url: $rootScope.serverUrlPrefix + "pay/createPayUrl",
          method: "POST",
          data: $scope.payInfo
        }).then(
        function(res){
          console.log(res);
          if(res.data.code_url){
            $scope.payUrlSuccess = true;
            $scope.qrString = res.data.code_url;
            $scope.qrSize = 150;
          }else{
            $('#qrModal').modal('hide');
            $('#payForm').modal('show');
          }
        },
        function(err){
          $('#qrModal').modal('hide');
          $('#payForm').modal('show');
        }
        )
      }
    };

    $scope.info = {
      ProductName: "",
      ProductId: "",
      wechat: "",
      mobile: "",
      chineseName: "",
      liunarUId: window.referUId
    };

    $scope.orderProduct = function () {
      console.log($scope.info, $scope.info.chineseName.length);

      if (!$scope.info.chineseName || $scope.info.chineseName.length < 2) {
        $scope.nameFail = true;
        $scope.errName = "请输入您的名字且不少于2个字";
      } else {
        $scope.nameFail = false;
      }

      if (!$scope.info.mobile || $scope.info.mobile == "") {
        $scope.phoneFail = true;
        $scope.errPhone = "请输入您的有效电话";
      } else {
        $scope.phoneFail = false;
      }

      if ($scope.phoneFail == false && $scope.nameFail == false) {
        $('#orderForm').modal('hide');
        $http({
          url: "http://yun.liunar.com/api/client",
          method: "POST",
          data: $scope.info,
          withCredentials: false
        }).then(
        function (res) {
          if (res.data.student) {
            $("#successMsg").modal("show");
          } else {
            $("#errMsg").modal("show");

          }
        },
        function (err) {
          $("#errMsg").modal("show");

        }
        );
      }
    };

    $scope.closeSuccess = function () {
      $("#successMsg").modal("hide");
    };

    $scope.closeErr = function () {
      $("#errMsg").modal("hide");
      $('#orderForm').modal('show');
    };

    $scope.closeQr = function(){
      $('#qrModal').modal('hide');
    }

  })
.controller("desktopOverseas", function ($scope, $rootScope, $http, userAuth, $location) {
  var loginStatus = new userAuth.checkLogin();
  console.log(loginStatus);
  if ($location.$$absUrl.indexOf('?referid=') === -1) {
      //没有 refer id：
      if (loginStatus.isLogin === false) {
        //return;
      } else {
        window.location = $location.$$absUrl + "?referid=" + loginStatus.uid;
        window.referUId = loginStatus.uid;
      }
    } else {
      //have referer id already
      window.referUId = window.location.search.split('=')[1]
    }
    $scope.openForm = function () {
      $('#orderForm').modal('show');
    };

    $scope.info = {
      ProductName: "",
      ProductId: "",
      wechat: "",
      mobile: "",
      chineseName: "",
      liunarUId: window.referUId
    }

    $scope.orderProduct = function () {
      console.log($scope.info, $scope.info.chineseName.length);

      if (!$scope.info.chineseName || $scope.info.chineseName.length < 2) {
        $scope.nameFail = true;
        $scope.errName = "请输入您的名字且不少于2个字";
      } else {
        $scope.nameFail = false;
      }

      if (!$scope.info.mobile || $scope.info.mobile == "") {
        $scope.phoneFail = true;
        $scope.errPhone = "请输入您的有效电话";
      } else {
        $scope.phoneFail = false;
      }

      if ($scope.phoneFail == false && $scope.nameFail == false) {
        $('#orderForm').modal('hide');
        $http({
          url: "http://yun.liunar.com/api/client",
          method: "POST",
          data: $scope.info,
          withCredentials: false
        }).then(
        function (res) {
          if (res.data.student) {
            $("#successMsg").modal("show");
          } else {
            $("#errMsg").modal("show");

          }
        },
        function (err) {
          $("#errMsg").modal("show");

        }
        );
      }
    };

    $scope.closeSuccess = function () {
      $("#successMsg").modal("hide");
    };

    $scope.closeErr = function () {
      $("#errMsg").modal("hide");
      $('#orderForm').modal('show');
    };

  })
.controller("gaopinController", function ($window, $scope, $http, $location, userAuth, wechatShare, $rootScope) {

  var productTitle = $('#productTitle').text();
  var coverSrc = $('#coverSrc').val();
  var description = $('#description').text();
  console.log(productTitle, coverSrc, description);
  wechatShare(productTitle, coverSrc, description);

  var loginStatus = new userAuth.checkLogin();
  console.log(loginStatus);
  if ($location.$$absUrl.indexOf('?referid=') === -1) {
      //没有 refer id：
      if (loginStatus.isLogin === false) {
        return;
      } else {
        window.location = $location.$$absUrl + "?referid=" + loginStatus.uid;
        window.referUId = loginStatus.uid;
      }
    } else {
      //have referer id already
      window.referUId = window.location.search.split('=')[1];
    }
  })