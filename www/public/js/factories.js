angular.module("factories",[])
.factory('getUrlParams', function($location){
    return function(){

    }
})
.factory('detectBrowser', function($window, $timeout){
    return {
        isWechat: function(){
            //OPTION ONE:
            var result = (/MicroMessenger/i).test($window.navigator.userAgent);
            return result;
        }
    }
})
.factory('localStorage', function ($window, $q) {
    return {
        setItem: function (key, value) {
            $window.localStorage.setItem(key, value);
        },
        getItem: function (key, defaultValue) {
            return $window.localStorage.getItem(key) || null;
        },
        setObject: function (key, obj) {
            $window.localStorage.setItem(key, JSON.stringify(obj));
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage.getItem(key) || 'null');
        },
        remove: function(key){
            return $window.localStorage.removeItem(key);  
        },
        clear: function () {
            $window.localStorage.clear();
        }
    }
})
.factory('userAuth', function(localStorage, $cookies){
    return {
        checkLogin: function(){
            //alert($cookies.getObject('user'));
            if(localStorage.getObject('canbestUserInfo') !== null && localStorage.getObject("canbestUserInfo").uid){
                return {
                    isLogin: true,
                    uid: localStorage.getObject("canbestUserInfo").uid
                };
            }else if($cookies.getObject('user') && $cookies.getObject('user').uid){
                return {
                    isLogin: true,
                    uid: $cookies.getObject('user').uid
                };
            }else{
                return {
                    isLogin: false,
                    uid: null
                };
            }
        }
    }
})
.factory("wechatShare", function($window, $location, $rootScope){
    var wx = window.wx;
    return function(title, img, desc, customLink = 0, linkURL = ""){
        //console.log(wx, $location.$$absUrl, title, img, desc);
        var link = "";
        if( customLink == 1 ){
            link = linkURL;
        }else{
            link =  $location.$$absUrl.indexOf('?referid=') === -1 ? $location.$$absUrl + $rootScope.shareState : ($location.$$absUrl.indexOf('?referid=') !== -1 && $location.$$absUrl.indexOf('?referid=null') !== -1) ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl;
        }
        wx.ready(function(){
            wx.onMenuShareTimeline({
                title: title, // 分享标题
                desc: desc,//分享预览文字
                link: link, // 分享链接
                imgUrl: img, // 分享图标
                success: function () {
                    // alert("success Post");
                },
                cancel: function () { 
                    //alert("cancel");
                },
                error: function(){
                    // alert("error");
                }
            });
            wx.onMenuShareAppMessage({
                title: title, // 分享标题
                link: link, // 分享链接
                desc: desc,//分享预览文字
                imgUrl: img, // 分享图标
                success: function () {
                    // alert(this.link + "," + $rootScope.shareState);
                },
                cancel: function () { 
                    //alert("cancel");
                },
                error: function(){
                    // alert("error");
                }
            });       
        });
    }
})
.factory("wechatPay", function($window, $location, $rootScope, $q){
    var wx = window.wx;

    var deferred = $q.defer();
    return function(t, nstr, pkg, stype, psign, curl){
        //console.log(wx, $location.$$absUrl, title, img, desc);
        wx.ready(function(){
            wx.chooseWXPay({
                timestamp: t, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
                nonceStr: nstr, // 支付签名随机串，不长于 32 位
                package: pkg, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
                signType: stype, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
                paySign: psign, // 支付签名
                success: function (res) {
                    deferred.resolve({err: "ok", msg: "付款成功", detail: res});
                },
                cancel: function(e){
                    deferred.reject({err: "fail", msg: "您取消了付款", detail: e});
                },
                fail: function(err){
                    deferred.reject({err: "fail", msg: "付款失败", detail: err});
                }
            });    
        });
        return deferred.promise;
    }
})
.factory("seedUrl", function($rootScope,$location){
    return {
        createUrl: function(type, id, time, taskId){
            if(time){
                return "http://www.liunar.com/img/pv.jpg?referid=" + $rootScope.referId + "&" + type + "id=" + id + "&sharetasktime=" + time + "&taskid=" + taskId;;
            }else{
                return "http://www.liunar.com/img/pv.jpg?referid=" + $rootScope.referId + "&" + type + "id=" + id;
            }
        }
    }
})
.factory("en_ch", function(){
    return {
        cities: {
            BC: {
                "Surrey": "素里",
                "Vancouver": "温哥华",
                "Victoria": "维多利亚",
                "Burnaby": "本那比",
                "Richmond": "列治文",
                "Abbotsford": "阿伯茨福德",
                "North Vancouver": "北温哥华",
                "Prince George": "乔治王子市",
                "Delta": "三角洲",
                "Kelowna": "基隆拿",
                "Langley": "兰里",
                "Kamloops": "甘露市",
                "Chilliwack": "奇利瓦克",
                "Coquitlam": "高贵林",
                "Nanaimo": "纳奈莫",
                "Maple Ridge": "枫树岭",
                "Port Coquitlam": "高贵林港",
                "West Vancouver": "西温哥华",
                "Vernon": "弗农",
                "Mission": "米逊",
            },
            AB: {
                "Edmonton": "埃德蒙顿",
                "Calgary": "卡尔加里",
                "Fort McMurray": "麦克默里堡",
                "St. Albert": "圣艾伯特",
                "Grande Prairie": "大草原城",
                "Medicine Hat": "梅迪辛哈特",
                "Sherwood Park": "舍伍德帕克",
                "Okotoks": "奥科托克斯",
                "Lethbridge": "莱斯布里奇",
                "Red Deer": "红鹿",
                "Leduc": "勒杜克",
                "Airdrie": "艾尔德里",
                "Stony Plain": "斯托尼普莱恩",
                "Cochrane": "科克伦",
                "Fort Saskatchewan": "萨斯喀彻温堡",
                "Lacombe": "拉科姆",
                "Drayton Valley": "德雷顿瓦利",
                "Spruce Grove": "斯普鲁斯格罗夫",
                "Strathmore": "斯特拉思莫尔",
                "Ardrossan": "阿德罗森",
            },
            ON: {
                "Toronto": "多伦多",
                "Mississauga": "密西沙加",
                "London": "伦敦",
                "Ottawa": "渥太华",
                "Hamilton": "哈密尔顿",
                "Brampton": "布兰普顿",
                "Windsor": "温莎",
                "Kitchener": "基奇纳",
                "Markham": "万锦",
                "Burlington": "伯林顿",
                "Oakville": "奥克维尔",
                "Barrie": "巴利",
                "Guelph": "贵湖",
                "St Catharines": "圣凯瑟琳",
                "Cambridge": "剑桥",
                "Brantford": "布兰特福德",
                "Richmond Hill": "列治文山",
                "Oshawa": "奥沙瓦",
                "Thunder Bay": "桑德贝",
                "Kingston": "金斯顿",
            }
        },
        schoolTable: {
            "gr12_enrollment": "12年级入学人数",
            "ave_exam_mark": "12年级毕业考平均分",
            "percentage_of_exams_failed": "12年级毕业考失败率",
            "school_vs_exam_mark_difference": "学校毕业课程成绩对比相关考试成绩",
            "english_gender_gap": "学校男女生英语10考试平均分差距",
            "math_gender_gap": "学校男女生数学10考试平均分差距",
            "graduation_rate": "高中毕业率",
            "delayed_advancement_rate": "推迟毕业率",
            
            "gr4_enrollment": "4年级入学人数",
            "gr4_avg_score_reading": "四年级学生FSA阅读平均分",
            "gr4_avg_score_writing": "四年级学生FSA写作平均分",
            "gr4_avg_score_numeracy": "四年级学生FSA算术平均分",
            "gr7_avg_score_reading": "七年级学生FSA阅读平均分",
            "gr7_ave_score_reading": "七年级学生FSA阅读平均分",
            "gr7_avg_score_writing": "七年级学生FSA写作平均分",
            "gr7_avg_score_numeracy": "七年级学生FSA算术平均分",
            "gr7_gender_gap_reading": "七年级男女生阅读成绩差距",
            "gr7_gender_gap_numeracy": "七年级男女生算术成绩差距",
            "below_expectations_percentage": "省考失败率",
            "tests_not_written_percentage": "考试缺席率",
            
            "OSSLT_count": "OSSLT学生参与率",
            "ave_gr9_math_acad": "九年级学术数学平均分",
            "ave_gr9_math_apld": "九年级应用数学平均分",
            "OSSLT_passed_percentage_FTE": "OSSLT通过率-FTE",
            "OSSLT_passed_percentage_PE": "OSSLT通过率-PE",
            "tests_below_standard_percentage": "考试未通过率",
            "gender_gap_level_math": "男女生数学成绩差距",
            "gender_gap_OSSLT": "男女生OSSLT成绩差距",
            "gr9_tests_not_written_percentage": "九年级考试缺考率",
            
            "gr6_enrollment": "6年级入学人数",
            "gr3_ave_level_reading": "三年级学生EQAO阅读平均分",
            "gr3_ave_level_writing": "三年级学生EQAO写作平均分",
            "gr3_ave_level_math": "三年级学生EQAO数学平均分",
            "gr6_ave_level_reading": "六年级学生EQAO阅读平均分",
            "gr6_ave_level_writing": "六年级学生EQAO写作平均分",
            "gr6_ave_level_math": "六年级学生EQAO数学平均分",
            "gender_gap_level_reading": "男女生阅读成绩差距",
            
            "g12_enrollment": "12年级入学人数",
            "courses_taken_per_student": "在校第三年所修毕业课程平均数",
            "diploma_completion_rate": "高中毕业率",
            
            "gr3_ave_test_mark_lang_arts": "三年级语言艺术平均分",
            "gr3_ave_test_mark_math": "三年级数学平均分",
            "gr6_ave_test_mark_lang_arts": "六年级语言艺术平均分",
            "gr6_ave_test_mark_math": "六年级数学平均分",
            "gr6_ave_test_mark_science": "六年级科学平均分",
            "gr6_ave_test_mark_social_studies": "六年级社会学平均分",
            "gr6_gender_gap_lang_arts": "六年级男女生语言艺术成绩差距",
            "gr6_gender_gap_math": "六年级男女生数学成绩差距",
            "percentage_of_tests_failed": "学校学科挂科率",
        }
    }
})
.factory("tagsFactory", function(){
    return {
        "国家": [
        {"加拿大": "2"},
        {"美国": "1"},
        {"澳大利亚":"3"},
        {"新西兰":"5"},
        {"英国":"4"},
        {"芬兰":"116"},
        {"德国":"117"},
        {"法国":"118"},
        {"意大利":"119"},
        ],
        "营地主题": [
        {"全真插班": "120"},
        {"名校游学": "121"},
        {"户外探索": "122"},
        {"艺术人文": "123"},
        {"科学技术": "124"},
        {"体育运动": "125"},
        {"童子军": "126"},
        {"语言提高": "127"}
        ],
        "年龄": [
        {"4-6岁": "142"},
        {"7-13岁": "143"},
        {"13-18岁": "144"}
        ],
        "类别": [
        {"慈善义工类": "128"},
        {"领导力类": "129"},
        {"科研学术类": "130"},
        {"专业技能比赛类": "131"},
        {"商业及实习类": "132"},
        {"艺术体育类": "133"},
        {"海外交流类": "134"},
        {"科考科普类": "135"},
        {"环保类": "136"},
        {"户外挑战类": "137"}
        ],
        "学历": [
        {"本科": "145"},
        {"硕士": "146"},
        {"博士": "147"}
        ],
        "专业": [
        {"理工科": "138"},
        {"商科": "139"},
        {"文科": "140"},
        {"医科": "141"}
        ],
        "学校类别": [
        {"私立小学": "15"},
        {"公立小学": "16"},
        {"私立中学": "17"},
        {"公立中学": "18"}
        ]
    }
})
.factory("tagsCollection", function($rootScope, $http){
    return function(type, callback){
        $http({
            url: $rootScope.serverUrlPrefix + "productnew/filter/" + type,
            method: "GET",
        }).then(
        function(res){
            callback(res.data);
        }
        );
    }
})