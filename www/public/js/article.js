angular.module("article",['liunarBackend','directives','factories'])
.controller("articleCtrl", function(seedUrl, $window, $scope, $http, $location, userAuth, wechatShare, $rootScope,$cookies,detectBrowser){

    //console.log($location);
    
    var articleTitle = angular.element(document.querySelector('#articleTitle')).text();
    
    var articleDesc = angular.element(document.querySelector('#articleDesc')).text();
    
    var articleThumb = angular.element(document.querySelector('#articleThumb')).text();
    
    var articleId = angular.element(document.querySelector('#articleId')).text();

    var taskid = angular.element(document.querySelector('#taskId')).text();

    wechatShare(articleTitle, articleThumb, articleDesc);

    var loginStatus = new userAuth.checkLogin();
    
    if($location.$$absUrl.indexOf('?referid=') === -1){//没有 refer id：
        if(loginStatus.isLogin === false){

        }else{
            window.location = $location.$$absUrl + "?referid=" + loginStatus.uid;
        }
    }else if($location.$$absUrl.indexOf('?referid=null') !== -1){//有 refer id可是为null:
        //先剔除 referid=null;
        var url = $location.$$absUrl.slice(0, $location.$$absUrl.indexOf('?referid=null'));
        if(loginStatus.isLogin === false){

        }else{
            window.location = url + "?referid=" + loginStatus.uid;
        }
    }

    if(detectBrowser.isWechat() == true){
        $scope.showBD = false;
    }else{
        $scope.showBD = true;
    }

    // var getArticleProperties = function(){
    //     $http({
    //         url: $rootScope.serverUrlPrefix + "articles/detail/" + articleId,
    //         method: "GET",
    //     }).then(function(res){
    //         //console.log(res);
    //         if(res.data.hasTopicTag == 1){
    //             $scope.topicTag = JSON.parse(res.data.topicTags)[0].tag;
    //             $scope.tagId = JSON.parse(res.data.topicTags)[0].tagId;
    //             $scope.showTagHeader = true;
    //         }else{
    //             $scope.showTagHeader = false;
    //         }
    //     })
    // };

    // getArticleProperties();

    var addView = function(){
        if(!$cookies.get("viewed" + articleId)){
            console.log("can add");
            $http({
                url: $rootScope.serverUrlPrefix + "articles/view/" + articleId,
                method: "GET",
            }).then(
            function(res){
                $cookies.put(
                    "viewed" + articleId, 
                    new Date().getTime().toString()
                    /*{
                        expires: new Date(new Date().getTime() + 1000 * 3600).toGMTString(),
                    }*/
                );
                //console.log(res);
            });
        }else{
            console.log("viewd");
        }   
    };
    
    var getComms = function(){
        $http({
            url: $rootScope.serverUrlPrefix + "comments/1/" + articleId,
            method: "GET"
        }).then(
        function(res){
            $scope.comments = res.data;
                //console.log($scope.comments);
            }
            );
    };

    var getRelative = function(){
        var tags = angular.element(document.querySelector('#tags')).text();
        if(tags !== "" && tags.indexOf(",") !== -1){
            var tags = tags.split(",").slice(0, 2).join(" ");
            $http({
                url: "/search/search?kw=" + tags + "&start=0&limit=3&type=1",
                //responseType: "json"
            }).then(function(res){
                console.log(res);
                if(res.data && res.data.length > 0 && articleId != '38226'){ 
                    $scope.showRelative = true;
                    $scope.lists = res.data;
                }else{
                    $scope.showRelative = false;
                }
            });
        }else{
            $scope.showRelative = false;
        }
    };

    addView();
    getComms();
    getRelative();
    
    //console.log(seedImg,lastEle);
    
    var shareTime = $location.absUrl().indexOf('&sharetime=');
    
    if(shareTime !== -1){
        console.log("havetime");
        shareTime += "&sharetime=".length;
        var stop = $location.absUrl().indexOf('&', shareTime);
        if(stop === -1){
            var urlTime = $location.absUrl().substring(shareTime);
        }else{
            var urlTime = $location.absUrl().substring(shareTime, stop);
        }
        $scope.pvImg = seedUrl.createUrl("article", articleId, urlTime, taskid);
    }else{
        $scope.pvImg = seedUrl.createUrl("article", articleId);    
    }
    
    
})
.controller("articleCommCtrl", function($location, $window, $scope, userAuth, $http, $rootScope){
    var user = new userAuth.checkLogin();

    if(user.isLogin !== true){
        alert("请登录后再评论");
        $window.location.href = "http://www.liunar.com/mobile/#/userLogin?lastview=" + $location.$$absUrl;
    }else{
    }


    $scope.comment = function(id, text, refercode){
        console.log(id, text);
        if (!text || text.length < 6) {
            alert("评论不能为空，而且必须多于6个字");
        } else {
            $http({
                url: $rootScope.serverUrlPrefix + "comments",
                method: "POST",
                data: {
                    row_id: id,
                    content: text,
                    module: 1,
                    uid: user.uid
                }
            }).then(
            function (res) {
                alert("感谢您的评论");
                $window.location.href = "/" + refercode + "/" + id;
            },
            function (res) {
                if(res.data.success){
                    alert(res.data.message);
                }else{
                    alert('对不起，出错了,请通知管理员！');
                }
            }
            );
        }
    }
})
.controller("articleListCtrl", function(wechatShare, $location, $window, $scope, userAuth, $http, $rootScope){

    wechatShare("留哪儿头条","http://www.liunar.com/img/lntt.png","掌握更多留学出国资讯");

    $scope.busy = false;

    $scope.articles = [];

    var getArticleList = function(page, callback){
        $http({
            url: $rootScope.serverUrlPrefix + "articles/list/15/" + page,
            method: "GET",
        }).then(
        function(res){
            callback(res.data);
        }
        );
    };

    $scope.page = 1;

    $scope.getMore = function(){
        $scope.page ++;
        $scope.busy = true;
        getArticleList($scope.page, function(res){
            $scope.articles = [].concat($scope.articles, res);
            $scope.busy = false;
            //console.log($scope.articles);
        })
    };

    getArticleList($scope.page, function(res){
        $scope.articles = [].concat($scope.articles, res);
    });
})