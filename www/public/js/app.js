angular.module("liunarBackend",['ngCookies','ja.qr'])
.run(function($http, $window, $location, $rootScope, $cookies, userAuth){

    $rootScope.serverUrlPrefix = "https://www.liunar.com/api/";
    
    var urlstring = $location.$$absUrl;
    
    var shareUidUrl = urlstring.indexOf("?referid=");

    if(shareUidUrl !== -1){
        if(urlstring.indexOf("?referid=null") === -1){
            shareUidUrl += "?referid=".length;
            var stop = urlstring.indexOf('&', shareUidUrl);
            if(stop === -1){
                var referId = urlstring.substring(shareUidUrl);
            }else{
                var referId = urlstring.substring(shareUidUrl, stop);
            }
            localStorage.setItem("referId", referId);
            $rootScope.referId = referId;
        }else{
            localStorage.setItem("referId", null);
            $rootScope.referId = "";
        }
    }else{
        $rootScope.referId = "";
    }
    
    var loginStatus = new userAuth.checkLogin();
    
    if(loginStatus.isLogin === false){
        $rootScope.shareState = $rootScope.referId == "" ? "" : "?referid="+ $rootScope.referId;
        $rootScope.anaImg = "/img/mi.jpg?referid=null"
    }else{
        $rootScope.shareState = "?referid=" + loginStatus.uid;
        $rootScope.anaImg = "/img/mi.jpg?referid=" + $rootScope.referId;
        $cookies.putObject("user", {
            uid: loginStatus.uid
        })
    }

    //console.log($cookies.get("jwtToken"));

    if($cookies.get("jwtToken")){
        localStorage.setItem("jwtToken", $cookies.get("jwtToken"));
    }else{
        if(localStorage.getItem("jwtToken")){
            localStorage.removeItem("jwtToken");
        }
    }
    
    $http({
        url: $rootScope.serverUrlPrefix + "wechatSign",
        method: "post",
        data:{
            url: $location.$$absUrl
        }
    }).then(
        function (res) {
            var cfg = {
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                    appId: res.data.appId, // 必填，公众号的唯一标识
                    timestamp: res.data.timestamp, // 必填，生成签名的时间戳
                    nonceStr: res.data.nonceStr, // 必填，生成签名的随机串
                    signature: res.data.signature,// 必填，签名，见附录1
                    jsApiList: ['checkJsApi','onMenuShareTimeline','onMenuShareAppMessage','chooseWXPay']
                };
            $window.wx.config(cfg);
        }
    );
})
.config(function($httpProvider, $windowProvider){
    $window = $windowProvider.$get();
    var token =  $window.localStorage.jwtToken;
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    if( token){
        $httpProvider.defaults.headers.common['token'] = token;
    }
    $httpProvider.defaults.transformRequest.unshift(function(obj) {
        var str = [];
        for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    }
    );  
})
.config(function($cookiesProvider){
    $cookiesProvider.defaults.domain = ".liunar.com";
    $cookiesProvider.defaults.path = "/";
})