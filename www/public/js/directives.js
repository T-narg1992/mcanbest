angular.module("directives", [])
    .directive("hideContent", function (userAuth, $timeout, $location, detectBrowser) {
        return {
            restrict: 'A',
            scope: {},
            link: function (scope, ele, attrs) {

                console.log(scope, ele, attrs);
                var loginStatus = new userAuth.checkLogin();

                var createButton = function (top) {
                    var btn = document.createElement("div");
                    btn.style.width = "100%";
                    btn.style.height = "120px";
                    btn.style.position = "absolute";
                    btn.style.bottom = "0px";
                    btn.style.left = "0px";
                    btn.style.color = "#52baba";
                    btn.style.textAlign = "center";
                    btn.style.lineHeight = "200px";
                    btn.style.background = "-webkit-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.style.background = "-o-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.style.background = " -moz-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.style.background = "linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.innerHTML = "<a href='#/userLogin?lastview=" + $location.$$path + "'>点击阅读全文...</a>"
                    ele[0].appendChild(btn);
                }

                $timeout(
                    function () {
                        scope.height = ele[0].offsetHeight;
                        if (loginStatus.isLogin === true) {

                        } else if (loginStatus.isLogin === false && detectBrowser.isWechat() == true) {
                            angular.element(ele).attr('style', "max-height: " + scope.height / 3 + "px; overflow: hidden; position: relative");
                            createButton(scope.height / 3 + 52);
                            console.log("not login");
                        }
                    }
                );
            }
        }
    })
    .directive('likeBtn', function ($http, userAuth, $location, $window, $rootScope,$cookies) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div><span></span><span></span></div>',
            scope: {
                module: '@',
                rowid: '@',
            },
            link: function (scope, el, attr) {
                //console.log(el, el[0].childNodes[0], $location.$$absUrl);
                //angular.element(el).attr();
                angular.element(el[0]).attr('style', 'display: inline-block');

                angular.element(el[0].childNodes[0]).attr('style', 'font-size: 18px; color: #3E606F');

                angular.element(el[0].childNodes[1]).attr('style', 'font-size: 16px; color: #8c8c8c; margin-left: 4px;');


                var shape = {
                    outline: function () {
                        angular.element(el[0].childNodes[0]).attr('class', 'fa fa-thumbs-o-up');
                    },
                    fill: function () {
                        angular.element(el[0].childNodes[0]).attr('class', 'fa fa-thumbs-up');
                    }
                };

                var statusInit = function () {
                    $http({
                        url: $rootScope.serverUrlPrefix + "likes/total/" + scope.module + "/" + scope.rowid,
                        method: "GET"
                    }).then(
                        function (res) {
                            var likeNum = res.data[0].likes;
                            angular.element(el[0].childNodes[1]).text(likeNum);
                        }
                    );
                    if (scope.like == true) {
                        shape.fill();
                    } else if (scope.like == false) {
                        shape.outline();
                    }
                };

                var like = function(uid){
                    $http({
                        url: $rootScope.serverUrlPrefix + "likes/like",
                        method: "POST",
                        data: {
                            uid: uid ? uid : login.uid,
                            module: scope.module,
                            row_id: scope.rowid
                        }
                    }).then(
                        function (res) {
                            console.log(res);
                            if(uid){
                                $cookies.put(
                                    "liked_" + scope.module + "_" + scope.rowid, 
                                    new Date().getTime().toString()
                                );
                            }
                            statusInit();
                        }
                    );
                };

                var unlike = function(){
                    $http({
                        url: $rootScope.serverUrlPrefix + "likes/unlike",
                        method: "POST",
                        data: {
                            uid: login.uid,
                            module: scope.module,
                            row_id: scope.rowid
                        }
                    }).then(
                        function (res) {
                            console.log(res);
                            statusInit();
                        }
                    );
                };

                var login = new userAuth.checkLogin();

                if (login.isLogin == true) {
                    $http({
                        url: $rootScope.serverUrlPrefix + "likes/" + login.uid,
                        method: "GET",
                    }).then(
                        function (res) {
                            //console.log(res.data);
                            var datas = res.data;
                            if (datas.length != 0) {
                                for (var i = 0; i < datas.length; i++) {
                                    scope.like = false;
                                    if (datas[i].module == scope.module && datas[i].row_id == scope.rowid) {
                                        scope.like = true;
                                        break;
                                    }
                                }
                            } else {
                                scope.like = false;
                            }
                            statusInit();
                        }
                    );
                } else if (login.isLogin == false) {
                    if(!$cookies.get("liked_" + scope.module + "_" + scope.rowid)){
                        scope.like = false;
                    }else{
                        scope.like = true;
                    }
                    statusInit();
                }

                el.bind('click', function (res) {
                    if (login.isLogin == true) {
                        if (scope.like == true) {
                            scope.like = false;
                            unlike();
                        } else if (scope.like == false) {
                            scope.like = true;
                            like();
                        }
                    } else {
                        if(scope.like == false){
                            scope.like = true;
                            like(Math.floor(new Date().getTime()/1000));
                        }
                    }
                });
            }
        }
    })
    .directive("saveTag", function ($http, userAuth, $location, $window, $rootScope) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div><button></button><p></p></div>',
            scope: {
                targetid: '@',
                //rowid: '@',
            },
            link: function ($scope, $ele, $attr) {
                console.log($ele[0]);

                var status = {
                    follow: function () {
                        angular.element($ele[0].childNodes[0]).text("关注");
                        angular.element($ele[0].childNodes[0]).attr(
                            'style', 'background: transparent; border: 1px solid #52baba; color: #52baba; top: 17px; border-radius: 4px;right: 17px; min-width: 82px; font-size: 14px; position: absolute; line-height: 32px;'
                        );
                    },
                    unfollow: function () {
                        angular.element($ele[0].childNodes[0]).text("已关注");
                        angular.element($ele[0].childNodes[0]).attr(
                            'style', 'background: #52baba; color: #fff; top: 17px; border-radius: 4px; right: 17px; min-width: 82px; font-size: 14px; position: absolute; line-height: 32px; border: none;'
                        );
                    }
                };

                var userStatus = new userAuth.checkLogin();

                var statusInit = function () {
                    $http({
                        url: $rootScope.serverUrlPrefix + "tags/count/" + $scope.targetid,
                        method: "GET"
                    }).then(
                        function (res) {
                            console.log(res);
                            if (res.data !== "") {
                                $scope.tagCount = res.data.userCount;
                                angular.element($ele[0].childNodes[1]).attr("style", "font-size: 14px; margin: 0px;");
                                angular.element($ele[0].childNodes[1]).html("关注: <span style='color: #52baba;'>" + $scope.tagCount + "</span>");
                            } else {
                                angular.element($ele[0].childNodes[1]).attr("style", "font-size: 14px; margin: 0px;");
                                angular.element($ele[0].childNodes[1]).html("关注: <span style='color: #52baba;'>0</span>");
                            }
                        }
                    );
                    if ($scope.follow == true) {
                        status.unfollow();
                    } else if ($scope.follow == false) {
                        status.follow();
                    }
                };

                if (userStatus.isLogin === true) {
                    $http({
                        url: $rootScope.serverUrlPrefix + "tags/mylist/" + userStatus.uid,
                        method: "GET"
                    }).then(
                        function (res) {
                            console.log(res);
                            var datas = res.data;
                            if (datas.length > 0) {
                                for (var i = 0; i < datas.length; i++) {
                                    $scope.follow = false;
                                    if (datas[i].tag_id == $scope.targetid && datas[i].uid == userStatus.uid) {
                                        console.log("followed");
                                        $scope.follow = true;
                                        break;
                                    }
                                }
                            } else {
                                $scope.follow = false;
                            }

                            statusInit();
                        }
                    );
                } else {
                    $scope.follow = false;
                    statusInit();
                }

                $ele.bind("click", function (res) {
                    if (userStatus.isLogin == true) {
                        if ($scope.follow == true) {
                            $scope.follow = false;
                            $http({
                                url: $rootScope.serverUrlPrefix + "tags/unfollow",
                                method: "POST",
                                data: {
                                    tagid: $scope.targetid,
                                    uid: userStatus.uid
                                }
                            }).then(
                                function (res) {
                                    console.log(res);
                                    statusInit();
                                }
                            );
                        } else if ($scope.follow == false) {
                            $scope.follow = true;
                            $http({
                                url: $rootScope.serverUrlPrefix + "tags/follow",
                                method: "POST",
                                data: {
                                    tagid: $scope.targetid,
                                    uid: userStatus.uid
                                }
                            }).then(
                                function (res) {
                                    console.log(res);
                                    statusInit();
                                }
                            );
                        }
                    } else {
                        alert('请微信登录之后再关注');
                        $window.location.href = "http://www.liunar.com/#/userLogin?lastview=" + $location.$$absUrl;
                    }
                });
            }
        }
    })
    .directive('atBottom', function () {
        return {
            restrict: 'A',
            scope: {
                method: "&method"
            },
            link: function (scope, ele, attrs) {
                //console.log(scope, ele, attrs);
                ele.bind('scroll', function () {
                    scope.position = ele[0].offsetHeight + ele[0].scrollTop;
                    scope.height = ele[0].scrollHeight;
                    //console.log(scope.position, scope.height);
                    if (scope.position >= scope.height) {
                        scope.method();
                    }
                });
            }
        };
    })
    .directive('windowBottom', function ($window) {
        return {
            restrict: "A",
            scope: {
                method: "&method"
            },
            link: function (scope, ele, attrs) {
                window.onscroll = function () {
                    var raw = angular.element($window.document).scrollTop();
                    scope.position = angular.element($window.document).scrollTop() + 1200; //670: footer height;
                    scope.height = angular.element($window.document).height();
                    console.log(raw, scope.height);
                    if (scope.position >= scope.height) {
                        scope.method();
                    }
                }
            }
        }
    })
    .directive("checkImage", function () {
        return {
            restrict: 'A',
            scope: {
                replacesrc: "@replacesrc"
            },
            link: function (scope, element, attrs) {
                //console.log(scope);
                element.bind('error', function (res) {
                    angular.element(element).attr('src', scope.replacesrc);
                })
            }
        }
    })
    .directive("checkboxGroup", function() {
        return {
            restrict: "A",
            scope: {
                onSelected: "&onSelected",
                group: "@group",
                indicator: "@indicator",
                container: "=container"
            },
            link: function(scope, elem, attrs) {
                // Determine initial checked boxes
                /*if (scope.array.indexOf(scope.item.id) !== -1) {
                    elem[0].checked = true;
                }*/

                // Update array on click
                elem.bind('click', function() {
                    var index = scope.container.indexOf(scope.indicator);
                    // Add if checked
                    if (elem[0].checked) {
                        if (index === -1) scope.container.push(scope.indicator);
                    }
                    // Remove if unchecked
                    else {
                        if (index !== -1) scope.container.splice(index, 1);
                    }
                    // Sort and update DOM display
                    scope.$apply(scope.container.sort(function(a, b) {
                        return a - b
                    }));

                    scope.onSelected();
                });
            }
        }
    })
    .directive('flipScale', function(){
        return {
            restrict: 'A',
            scope: {
            },
            link: function (scope, element, attrs) {
                //console.log(element);
                console.log(element[0].clientWidth - element[0].clientHeight);
                var diff = element[0].clientWidth - element[0].clientHeight;
                if(diff <= 0){
                    angular.element(element).attr('style', "height: 100%; width: auto");
                }else if(diff > 0){
                    angular.element(element).attr('style', "height: 100%; width: auto");
                }
            }
        }
    })
    .directive('map', function () {
        return {
            restrict: 'E',
            replace: true,
            template: '<div></div>',
            scope: {
                'address': "@",
                'mapType': "@"
            },
            link: function (scope, element, attrs) {
                //console.log(scope.address);
                var geoCoder = new google.maps.Geocoder();

                var mapOptions = {
                    zoom: 16,
                    mapTypeId: eval(scope.mapType),
                    draggable: true,
                    disableDefaultUI: true,
                    zoomControl: true,
                    fullscreenControl: true
                };

                var viewOptions = {
                    pov: { heading: 0, pitch: 0 },
                    zoom: 1,
                    streetViewControl: false
                };

                var mapInit = function () {
                    var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
                    var marker = new google.maps.Marker({
                        map: map,
                    });
                    /*google.maps.event.addListener(map,'center_changed',function() {
                        window.setTimeout(
                            function() {
                                map.panTo(marker.getPosition());
                            },
                        1);
                    });*/
                    geoCoder.geocode(
                        { 'address': scope.address },
                        function (result, status) {
                            //console.log(result, status);
                            if (status === "OK") {
                                map.setCenter(result[0].geometry.location);
                                marker.setPosition(result[0].geometry.location);
                                marker.addListener('click', function (res) {
                                    window.open('http://maps.google.com/maps?q=' + scope.address);
                                });
                            }
                        }
                    );
                };

                var streetViewInit = function () {
                    var panorama = new google.maps.StreetViewPanorama(document.getElementById(attrs.id), viewOptions);
                    geoCoder.geocode(
                        { 'address': scope.address },
                        function (result, status) {
                            //console.log(result, status);
                            if (status === "OK") {
                                panorama.setPosition(result[0].geometry.location);
                            }
                        }
                    );
                }

                mapInit();

                scope.$watch('mapType', function (a, b) {
                    if (a) {
                        if (a === "streetView") {
                            streetViewInit();
                        } else {
                            if (a === undefined || a === "") {
                                mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
                            } else {
                                mapOptions.mapTypeId = eval(a);
                            }
                            mapInit();
                            //console.log(a);   
                        }
                    }
                });
            }
        };
    })
    .directive('toutiaoUrl', function ($http, $rootScope) {
        return {
            restrict: 'A',
            scope: {
                uid: "@uid",
                site: "@site",
                aid: "@aid"
            },
            link: function(scope, ele, attrs){
                if(scope.site == 1){
                    $http({
                        method: "get",
                        url: $rootScope.serverUrlPrefix + "usercenter/user/getrefercode/" + scope.uid,
                    }).then(
                        function(res){
                            if(res.data.reference_code){
                                angular.element(ele).attr("href", "/" + res.data.reference_code + "/"+ scope.aid);
                            }else{
                                angular.element(ele).attr("href", "/0/" + scope.aid);
                            }
                        }
                    )
                }else{
                    angular.element(ele).attr("href", "/0/"+scope.aid);
                }
            }
        }
    })
    .filter('html', ['$sce', function ($sce) { 
        return function (text) {
            return $sce.trustAsHtml(text);
        };    
    }])