angular.module("user",['liunarBackend','directives','factories'])
.factory("introduceNewUser", function($window){
    var wx = window.wx;
    if(wx){
        return function(shareParams, username){

            wx.ready(function(){
                wx.onMenuShareAppMessage({
                    title: "最聪明的教育创业计划，零成本打造您的在线留学工作室", // 分享标题
                    link: "http://www.liunar.com/mobile/#/userShare" + shareParams + "&vip=1", // 分享链接
                    desc:  username + "邀你成为留学教育合伙达人，零投入，无需场地，可兼职，只需转发分享，即可赚取额外收入！",//分享预览文字
                    imgUrl: "http://www.liunar.com/img/icon.png?v=3", // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });
                wx.onMenuShareTimeline({
                    title: "最聪明的教育创业计划，零成本打造您的在线留学工作室", // 分享标题
                    link: "http://www.liunar.com/mobile/#/userShare" + shareParams + "&vip=1", // 分享链接
                    //desc:  user + "邀你成为留学教育合伙达人，零投入，无需场地，可兼职，只需转发分享，即可赚取额外收入！",//分享预览文字
                    imgUrl: "http://www.liunar.com/img/icon.png?v=3", // 分享图标
                    success: function () {
                    },
                    cancel: function () { 
                    },
                    error: function(){
                    }
                });
            });    
        }
    }else{
        return "wx is not init";
    }
})
.controller("userRecommend", function(introduceNewUser){
 angular.element(document).ready(function () {
    var shareParam = "?referid="+document.getElementById('uid').value;
    var fullname = document.getElementById('fullname').value;
    introduceNewUser(shareParam, fullname); 
});
})