window.onload = function(){    
    document.body.addEventListener('touchmove', function (ev) {
        var target = ev.target
        //console.log(ev.target, ev.target.classList, ev);
        //console.log(ev.target.classList);
        /*if(ev.target.classList.contains("headerBar") || ev.target.classList.contains("title") || ev.target.classList.contains("footerBar") || ev.target.classList.contains("input-inset") || ev.target.classList.contains("input-inset-wrapper") || ev.target.classList.contains("botButt") || ev.target.classList.contains("ng-empty") || ev.target.classList.contains("ng-not-empty") || ev.target.classList.contains("tab-item") || ev.target.classList.contains("fa") ||ev.target.classList.contains("icon")){
            //console.log("head or foot");    
            ev.preventDefault();
        }*/
        var parent = ev.srcElement.offsetParent.className;
        var header_or_footer = /(headerBar|footerBar|tabs-container)/g.test(parent);
        if(header_or_footer == true){
            ev.preventDefault();
        }
    }, false);
        
    var content = document.querySelector('.content-container');
        

    var startY;
        
    content.addEventListener('touchstart', function (e) {
        startY = e.touches[0].clientY;
    }, false);

    content.addEventListener('touchmove', function (e) {

        var status = '11';
        var ele = this;

        var currentY = e.touches[0].clientY;

        if (ele.scrollTop === 0) {
            status = ele.offsetHeight >= ele.scrollHeight ? '00' : '01';
        } else if (ele.scrollTop + ele.offsetHeight >= ele.scrollHeight) {
            status = '10';
        } 
        if (status != '11') {
            var direction = currentY - startY > 0 ? '10' : '01';
            if (!(parseInt(status, 2) & parseInt(direction, 2))) {
                e.preventDefault();
            }
        }
    }, false);
}