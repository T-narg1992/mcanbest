angular.module('canbest', ['ngCookies','slickCarousel', 'ngFileUpload', 'chart.js', 'ionic', 'angulartics', 'angulartics.baidu', 'canbestServer', 'homeController', 'serverConfig', 'en_ch', 'qandAController', 'event', 'qandADetailController', 'userController', 'agencyController', 'detailController', 'schoolController', 'schoolDetailController', 'articleListController', 'estimateController', 'liunarProduct', 'applyController', 'search', 'msg'])
.run(function ($http, $ionicPlatform, localStorage, $state, $timeout, $location, httpServices, pressureTest, $rootScope, $window, $ionicHistory, seedUrl, detectBrowser, $ionicPopup, $cookies) {

    var urlstring = $location.$$absUrl;

    var start = urlstring.indexOf("?code=");

    var shareUidUrl = urlstring.indexOf("?referid=");

    if (shareUidUrl !== -1) {
        if (urlstring.indexOf("?referid=null") === -1) {
            shareUidUrl += "?referid=".length;
            var stop = urlstring.indexOf('&', shareUidUrl);
            if (stop === -1) {
                var referId = urlstring.substring(shareUidUrl);
            } else {
                var referId = urlstring.substring(shareUidUrl, stop);
            }
            localStorage.setItem("referId", referId);
            $rootScope.referId = referId;
        } else {
            localStorage.setItem("referId", null);
            $rootScope.referId = "null";
        }
    } else {
        $rootScope.referId = "null";
    }

    console.log($rootScope.referId, $location.search());

    if (start !== -1) {
        start += "?code=".length;
        var end = urlstring.indexOf('&', start);

            //alert("微信之后的跳转");

            if (end === -1) {
                var code_value = urlstring.substring(start);
            } else {
                var code_value = urlstring.substring(start, end);
            }

            httpServices.wechatLogin(code_value, localStorage.getItem("referId")).then(
                function (res) {
                    // alert("授权成功");
                    // console.log(res);
                    if (!res.errcode) {
                        localStorage.setObject("canbestUserInfo", res);
                        localStorage.setItem('jwtToken', res.token);
                        $cookies.put('jwtToken', res.token);
                        $cookies.putObject("user",{
                            uid: res.uid
                        })
                        //console.log($location, $ionicHistory.viewHistory());
                        $window.location.href = localStorage.getItem("lastview") == null ? "http://www.liunar.com/mobile/#/app/userCenter" : (localStorage.getItem("lastview").indexOf("http://") == -1) ? "http://www.liunar.com/mobile/#" + localStorage.getItem("lastview") : localStorage.getItem("lastview");
                        localStorage.remove("lastview");
                    }
                }
                )

        };

        $rootScope.showCallBtn = true;

        $rootScope.call = function () {
            $ionicPopup.confirm({
                title: '留哪儿',
                template: '<p style="text-align: center">确认拨打咨询电话</p>',
                cancelText: '取消', // String (default: 'Cancel'). The text of the Cancel button.
                okText: '确认', // String (default: 'OK'). The text of the OK button.
                okType: 'calm',
            }).then(function (res) {
                if (res) {
                    //console.log(res);
                    window.location.href = 'tel:4008099039';
                } else {
                    console.log("cancle");
                }
            });
        };

        if (localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid) {
            $rootScope.shareState = "?referid=null";
        } else {
            $rootScope.shareState = "?referid=" + localStorage.getObject("canbestUserInfo").uid;
        }

        httpServices.jsSDK().then(
            function (res) {
                //console.log(res);
                var cfg = {
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                    appId: res.appId, // 必填，公众号的唯一标识
                    timestamp: res.timestamp, // 必填，生成签名的时间戳
                    nonceStr: res.nonceStr, // 必填，生成签名的随机串
                    signature: res.signature,// 必填，签名，见附录1
                    jsApiList: ['checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage']
                };
                $window.wx.config(cfg);
            }
            );

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
                //console.log(cordova.plugins.Keyboard);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.hideTabs = '';

        $rootScope.$on("$ionicView.beforeEnter",
            function (evt, view) {
                var seedImg = document.createElement("img");
                seedImg.setAttribute("src", "img/pv.jpg?from=" + $location.$$url);
                var lastEle = document.getElementsByTagName("*")[document.getElementsByTagName("*").length - 1];
                lastEle.parentNode.insertBefore(seedImg, lastEle);
                if (view.stateName == "app.mission" || view.stateName == "app.postDetail") {
                    $rootScope.hideTabs = 'tabs-item-hide';
                } else {
                    $rootScope.hideTabs = '';
                }
            }
            );
    })
.directive('lazyScroll',
    function ($rootScope) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                var origEvent = $scope.$onScroll;
                $scope.$onScroll = function () {
                    $rootScope.$broadcast('lazyScrollEvent');
                    if (typeof origEvent === 'function') {
                        origEvent();
                    }
                };
            }
        };
    })
.directive('imageLazySrc',
    function ($document, $timeout, $ionicScrollDelegate, $compile) {
        return {
            restrict: 'A',
            scope: {
                lazyScrollResize: "@lazyScrollResize",
                imageLazyBackgroundImage: "@imageLazyBackgroundImage",
                imageLazySrc: "@"
            },
            link: function ($scope, $element, $attributes) {
                if (!$attributes.imageLazyDistanceFromBottomToLoad) {
                    $attributes.imageLazyDistanceFromBottomToLoad = 0;
                }
                if (!$attributes.imageLazyDistanceFromRightToLoad) {
                    $attributes.imageLazyDistanceFromRightToLoad = 0;
                }

                var loader;
                if ($attributes.imageLazyLoader) {
                    loader = $compile('<div class="image-loader-container"><ion-spinner class="image-loader" icon="' + $attributes.imageLazyLoader + '"></ion-spinner></div>')($scope);
                    $element.after(loader);
                }

                $scope.$watch('imageLazySrc', function (oldV, newV) {
                    if (loader)
                        loader.remove();
                    if ($attributes.imageLazyLoader) {
                        loader = $compile('<div class="image-loader-container"><ion-spinner class="image-loader" icon="' + $attributes.imageLazyLoader + '"></ion-spinner></div>')($scope);
                        $element.after(loader);
                    }
                    var deregistration = $scope.$on('lazyScrollEvent', function () {
                        //    console.log('scroll');
                        if (isInView()) {
                            loadImage();
                            deregistration();
                        }
                    }
                    );
                    $timeout(function () {
                        if (isInView()) {
                            loadImage();
                            deregistration();
                        }
                    }, 500);
                });
                var deregistration = $scope.$on('lazyScrollEvent', function () {
                    // console.log('scroll');
                    if (isInView()) {
                        loadImage();
                        deregistration();
                    }
                }
                );

                function loadImage() {
                    //Bind "load" event
                    $element.bind("load", function (e) {
                        if ($attributes.imageLazyLoader) {
                            loader.remove();
                        }
                        if ($scope.lazyScrollResize == "true") {
                            //Call the resize to recalculate the size of the screen
                            $ionicScrollDelegate.resize();
                        }
                        $element.unbind("load");
                    });

                    if ($scope.imageLazyBackgroundImage == "true") {
                        var bgImg = new Image();
                        bgImg.onload = function () {
                            if ($attributes.imageLazyLoader) {
                                loader.remove();
                            }
                            $element[0].style.backgroundImage = 'url(' + $attributes.imageLazySrc + ')'; // set style attribute on element (it will load image)
                            if ($scope.lazyScrollResize == "true") {
                                //Call the resize to recalculate the size of the screen
                                $ionicScrollDelegate.resize();
                            }
                        };
                        bgImg.src = $attributes.imageLazySrc;
                    } else {
                        $element[0].src = $attributes.imageLazySrc; // set src attribute on element (it will load image)
                    }
                }

                function isInView() {
                    var clientHeight = $document[0].documentElement.clientHeight;
                    var clientWidth = $document[0].documentElement.clientWidth;
                    var imageRect = $element[0].getBoundingClientRect();
                    return (imageRect.top >= 0 && imageRect.top <= clientHeight + parseInt($attributes.imageLazyDistanceFromBottomToLoad))
                    && (imageRect.left >= 0 && imageRect.left <= clientWidth + parseInt($attributes.imageLazyDistanceFromRightToLoad));
                }

                // bind listener
                // listenerRemover = scrollAndResizeListener.bindListener(isInView);

                // unbind event listeners if element was destroyed
                // it happens when you change view, etc
                $element.on('$destroy', function () {
                    deregistration();
                });

                // explicitly call scroll listener (because, some images are in viewport already and we haven't scrolled yet)
                $timeout(function () {
                    if (isInView()) {
                        loadImage();
                        deregistration();
                    }
                }, 500);
            }
        };
    })
.directive('elScroll', function ($location, $ionicScrollDelegate) {
    return {
        restrict: 'A',
        scope: {
            target: "@elScroll",
            delegate: "@",
            animate: "@"
        },
        link: function (scope, element, attrs) {
        },
        controller: function ($scope, $element, $attrs) {
            var shouldAnimate = $scope.animate;
            $scope.scrollTo = function () {
                $location.hash($scope.target);
                var handle = $ionicScrollDelegate.$getByHandle($scope.delegate);
                handle.anchorScroll(shouldAnimate);
            };
            $element.bind("click", function (e) {
                e.preventDefault();
                $scope.scrollTo();
            });
        }
    }
})
.directive('map', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div></div>',
        scope: {
            'address': "@",
            'mapType': "@"
        },
        link: function (scope, element, attrs) {
                //console.log(scope.address);
                var geoCoder = new google.maps.Geocoder();

                var mapOptions = {
                    zoom: 16,
                    mapTypeId: eval(scope.mapType),
                    draggable: true,
                    disableDefaultUI: true,
                    zoomControl: true,
                    fullscreenControl: true
                };

                var viewOptions = {
                    pov: { heading: 0, pitch: 0 },
                    zoom: 1,
                    streetViewControl: false
                };

                var mapInit = function () {
                    var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
                    var marker = new google.maps.Marker({
                        map: map,
                    });
                    /*google.maps.event.addListener(map,'center_changed',function() {
                        window.setTimeout(
                            function() {
                                map.panTo(marker.getPosition());
                            },
                        1);
                    });*/
                    geoCoder.geocode(
                        { 'address': scope.address },
                        function (result, status) {
                            //console.log(result, status);
                            if (status === "OK") {
                                map.setCenter(result[0].geometry.location);
                                marker.setPosition(result[0].geometry.location);
                                marker.addListener('click', function (res) {
                                    window.open('http://maps.google.com/maps?q=' + scope.address);
                                });
                            }
                        }
                        );
                };

                var streetViewInit = function () {
                    var panorama = new google.maps.StreetViewPanorama(document.getElementById(attrs.id), viewOptions);
                    geoCoder.geocode(
                        { 'address': scope.address },
                        function (result, status) {
                            //console.log(result, status);
                            if (status === "OK") {
                                panorama.setPosition(result[0].geometry.location);
                            }
                        }
                        );
                }

                mapInit();

                scope.$watch('mapType', function (a, b) {
                    if (a) {
                        if (a === "streetView") {
                            streetViewInit();
                        } else {
                            if (a === undefined || a === "") {
                                mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
                            } else {
                                mapOptions.mapTypeId = eval(a);
                            }
                            mapInit();
                            //console.log(a);   
                        }
                    }
                });
            }
        };
    })
.directive('likeBtn', function ($ionicPopup, $state, $http, userAuth, $location, urlConfig) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div><i></i><span></span></div>',
        scope: {
            module: '@',
            rowid: '@',
        },
        link: function (scope, el, attr) {
            angular.element(el).attr('style', 'margin: 12px 0px;');
            angular.element(el[0].childNodes[0]).attr('style', 'font-size: 24px; color: rgba(246, 83, 74, 1); vertical-align: middle');

            angular.element(el[0].childNodes[1]).attr('style', 'font-size: 14px; color: steelblue; vertical-align: middle; margin-left: 8px;');


            var shape = {
                outline: function () {
                    angular.element(el[0].childNodes[0]).attr('class', 'ion-android-favorite-outline');
                },
                fill: function () {
                    angular.element(el[0].childNodes[0]).attr('class', 'ion-android-favorite');
                }
            }

            var statusInit = function () {
                    //console.log(scope.like);
                    $http({
                        url: urlConfig.url + "likes/total/" + scope.module + "/" + scope.rowid,
                        method: "GET"
                    }).then(
                    function (res) {
                        var likeNum = res.data[0].likes;
                        angular.element(el[0].childNodes[1]).text(likeNum);
                    }
                    );
                    if (scope.like == true) {
                        shape.fill();
                    } else if (scope.like == false) {
                        shape.outline();
                    }
                };

                var login = new userAuth.checkLogin();

                if (login.isLogin == true) {
                    $http({
                        url: urlConfig.url + "likes/" + login.uid,
                        method: "GET",
                    }).then(
                    function (res) {
                        var datas = res.data;
                        if (datas.length != 0) {
                            for (var i = 0; i < datas.length; i++) {
                                scope.like = false;
                                if (datas[i].module == scope.module && datas[i].row_id == scope.rowid) {
                                    scope.like = true;
                                    break;
                                }
                            }
                        } else {
                            scope.like = false;
                        }
                        statusInit();
                    }
                    );
                } else if (login.isLogin == false) {
                    scope.like = false;
                    statusInit();
                }

                el.bind('click', function (res) {
                    if (login.isLogin == true) {
                        if (scope.like == true) {
                            scope.like = false;
                            $http({
                                url: urlConfig.url + "likes/unlike",
                                method: "POST",
                                data: { uid: login.uid, module: scope.module, row_id: scope.rowid }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        } else if (scope.like == false) {
                            scope.like = true;
                            $http({
                                url: urlConfig.url + "likes/like",
                                method: "POST",
                                data: { uid: login.uid, module: scope.module, row_id: scope.rowid }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        }
                    } else {
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '请微信登录之后再点赞'
                        }).then(
                        function () {
                            $state.go("userLogin", { lastview: $location.$$path });
                        }
                        );
                    }
                });
            }
        }
    })
.directive("saveTag", function ($ionicPopup, $state, $http, userAuth, $location, $window, urlConfig) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div><button></button><p></p></div>',
        scope: {
            targetid: '@',
                //rowid: '@',
            },
            link: function ($scope, $ele, $attr) {

                var status = {
                    follow: function () {
                        angular.element($ele[0].childNodes[0]).text("关注");
                        angular.element($ele[0].childNodes[0]).attr(
                            'style', 'background: transparent; border: 1px solid #52baba; color: #52baba; top: 17px; right: 17px; min-width: 82px; font-size: 14px; position: absolute;'
                            );
                    },
                    unfollow: function () {
                        angular.element($ele[0].childNodes[0]).text("已关注");
                        angular.element($ele[0].childNodes[0]).attr(
                            'style', 'background: #52baba; color: #fff; top: 17px; right: 17px; min-width: 82px; font-size: 14px; position: absolute; border: none;'
                            );
                    }
                };

                var userStatus = new userAuth.checkLogin();

                var statusInit = function () {
                    $http({
                        url: urlConfig.url + "tags/count/" + $scope.targetid,
                        method: "GET"
                    }).then(
                    function (res) {
                        console.log(res);
                        if (res.data !== "") {
                            $scope.tagCount = res.data.userCount;
                            angular.element($ele[0].childNodes[1]).attr("style", "font-size: 14px; margin: 0px;");
                            angular.element($ele[0].childNodes[1]).html("关注: <span style='color: #52baba;'>" + $scope.tagCount + "</span>");
                        } else {
                            angular.element($ele[0].childNodes[1]).attr("style", "font-size: 14px; margin: 0px;");
                            angular.element($ele[0].childNodes[1]).html("关注: <span style='color: #52baba;'>0</span>");
                        }
                    }
                    );
                    if ($scope.follow == true) {
                        status.unfollow();
                    } else if ($scope.follow == false) {
                        status.follow();
                    }
                };

                if (userStatus.isLogin === true) {
                    $http({
                        url: urlConfig.url + "tags/mylist/" + userStatus.uid,
                        method: "GET"
                    }).then(
                    function (res) {
                        var datas = res.data;
                        if (datas.length > 0) {
                            for (var i = 0; i < datas.length; i++) {
                                $scope.follow = false;
                                if (datas[i].tag_id == $scope.targetid && datas[i].uid == userStatus.uid) {
                                    $scope.follow = true;
                                    break;
                                }
                            }
                        } else {
                            $scope.follow = false;
                        }

                        statusInit();
                    }
                    );
                } else {
                    $scope.follow = false;
                    statusInit();
                }

                $ele.bind("click", function (res) {
                    if (userStatus.isLogin == true) {
                        if ($scope.follow == true) {
                            $scope.follow = false;
                            $http({
                                url: urlConfig.url + "tags/unfollow",
                                method: "POST",
                                data: {
                                    tagid: $scope.targetid,
                                    uid: userStatus.uid
                                }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        } else if ($scope.follow == false) {
                            $scope.follow = true;
                            $http({
                                url: urlConfig.url + "tags/follow",
                                method: "POST",
                                data: {
                                    tagid: $scope.targetid,
                                    uid: userStatus.uid
                                }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        }
                    } else {
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '请微信登录之后再关注'
                        }).then(
                        function () {
                            $state.go("userLogin", { lastview: $location.$$path });
                        }
                        );
                    }
                });
            }
        }
    })
.directive("hideContent", function (userAuth, $timeout, $location, detectBrowser) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, ele, attrs) {

            var loginStatus = new userAuth.checkLogin();

            var createButton = function (top) {
                var btn = document.createElement("div");
                btn.style.width = "100%";
                btn.style.height = "120px";
                btn.style.position = "absolute";
                    //btn.style.top = top + "px";
                    btn.style.bottom = "0px";
                    btn.style.left = "0px";
                    btn.style.color = "#52baba";
                    btn.style.textAlign = "center";
                    btn.style.lineHeight = "200px";
                    //btn.style.background = "rgba(255,255,255,1)";
                    btn.style.background = "-webkit-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    //btn.style.background = "red";
                    btn.style.background = "-o-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.style.background = " -moz-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.style.background = "linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    //btn.textContent = "阅读全文...";
                    //btn.href = "#/app/userCenter";
                    btn.innerHTML = "<a href='#/userLogin?lastview=" + $location.$$path + "'>点击阅读全文...</a>"
                    ele[0].appendChild(btn);
                }

                $timeout(
                    function () {
                        scope.height = ele[0].offsetHeight;
                        if (loginStatus.isLogin === true) {

                        } else if ($location.$$absUrl.indexOf('referid=') !== -1 && loginStatus.isLogin === false && detectBrowser.isWechat() == true) {
                            angular.element(ele).attr('style', "max-height: " + scope.height / 3 + "px; overflow: hidden; position: relative");
                            createButton(scope.height / 3 + 52);
                            console.log("not login");
                        }
                    }
                    );
            }
        }
    })
.directive("checkImage", function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
                //console.log(scope);
                element.bind('error', function (res) {
                    angular.element(element).attr('class', 'hide');
                })
            }
        }
    })
.directive("checkSchoolImage", function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
                //console.log(scope);
                element.bind('error', function (res) {
                    angular.element(element).attr('src', 'img/school-default.png');
                })
            }
        }
    })
.directive('preventOverScroll', function ($window, $ionicScrollDelegate, $timeout) {
    return {
        restrict: 'A',
        link: function ($scope, $el) {
            var startY;
            $el.on('touchstart', function (e) {
                startY = e.touches[0].clientY;
                    //console.log("from: ", startY);
                });

            $el.on("touchmove", function (evt) {
                var status = '11';
                var ele = this;
                var currentY = evt.touches[0].clientY;
                    //console.log("to: ", currentY)
                    //console.log(ele.scrollTop);
                    if (ele.scrollTop === 0) {
                        status = ele.offsetHeight >= ele.scrollHeight ? '00' : '01';

                    } else if (ele.scrollTop + ele.offsetHeight >= ele.scrollHeight) {
                        status = '10';
                    }
                    if (status != '11') {
                        var direction = currentY - startY > 0 ? '10' : '01';

                        if (!(parseInt(status, 2) & parseInt(direction, 2))) {
                            evt.preventDefault();
                            //console.log("preventScroll");
                        }
                    }
                    //console.log(status);
                });
        }
    }
})
.directive('preventScroll', function ($window, $ionicScrollDelegate, $timeout) {
    return {
        restrict: 'A',
        link: function ($scope, $el) {

            $el.on("touchmove", function (evt) {
                    //console.log("no scroll", $el[0].children);
                    if ($el[0].nodeName == "ION-TABS") {
                        angular.element("div.tab-nav.tabs").on("touchmove", function (evt) {
                            evt.preventDefault();
                        })
                    } else {
                        evt.preventDefault();
                    }
                });
        }
    }
})
.directive('hideTabs', function ($rootScope) {
    return {
        restrict: 'A',
        link: function ($scope, $el) {
                //console.log($el);
                $rootScope.hideTabs = 'tabs-item-hide';
                $scope.$on('$destroy', function (evet, data) {
                    //console.log(evet, data);
                    $rootScope.hideTabs = '';
                });
            }
        };
    })
.directive('progressBar', function () {
    return {
        restrict: 'E',
        replace: 'true',
        template: '<div></div>',
        scope: {
            'max': '@',
            'value': '@',
        },
        link: function (scope, el, attr) {
                //console.log(scope, el);
                el[0].style.backgroundColor = "#e5e5e5";
                el[0].style.overflow = "hidden";
                el[0].style.position = "relative";
                el[0].style.height = "12px";
                el[0].style.borderRadius = "6px";
                var valueBlock = document.createElement('div');
                valueBlock.style.height = "12px";
                valueBlock.style.borderRadius = "6px";
                valueBlock.style.backgroundColor = "rgb(155, 207, 0)";
                valueBlock.style.position = "absolute";
                valueBlock.style.left = "0";
                valueBlock.style.top = "0";
                el[0].appendChild(valueBlock);
                scope.$watch('value', function (a, b) {
                    if (a) {
                        valueBlock.style.width = a + "%";
                    }
                })
            }
        }
    })
.config(function ($ionicConfigProvider) {
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.tabs.style('standard');
    $ionicConfigProvider.views.transition('ios');
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.navBar.positionPrimaryButtons('left');
    $ionicConfigProvider.navBar.positionSecondaryButtons('right');
    $ionicConfigProvider.templates.maxPrefetch(0);
    $ionicConfigProvider.scrolling.jsScrolling(false);
})
.config(function($cookiesProvider){
    $cookiesProvider.defaults.domain = ".liunar.com";
    $cookiesProvider.defaults.path = "/";
})
.config(function ($httpProvider, $windowProvider) {
    $window = $windowProvider.$get();
    var token =  $window.localStorage.jwtToken;
    if( token){
        $httpProvider.defaults.headers.common['token'] = token;
    }
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.transformRequest.unshift(function (obj) {
        var str = [];
        for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    });
})
.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/tabs.html',
        controller: 'tabsController'
    })
    .state('schoolSearch', {
        url: "/schoolsearch",
        templateUrl: 'templates/search.html',
        controller: "schoolSearchController"
    })
    .state('tagSearch', {
        url: "/tagsearch/:tag",
        templateUrl: "templates/searchTags.html",
        controller: "tagSearchController"
    })
    .state("tagSquare", {
        url: "/tagsquare",
        templateUrl: "templates/tagSquare.html",
        controller: "tagsController"
    })
    .state('promote', {
        url: '/promote',
        templateUrl: "templates/promote.html",
    })
    .state('app.estimate', {
        url: '/estimate',
        views: {
            'tab-home': {
                templateUrl: "templates/estimate.html",
                controller: "estimateController"
            }
        }
    })
    .state('app.estimateResult', {
        url: '/estimate/estimateResult',
        cache: false,
        views: {
            'tab-home': {
                templateUrl: "templates/estimateResult.html",
                controller: "estimateController",
            }
        }
    })
    .state('app.mainHome', {
        url: '/mainhome',
        views: {
            'tab-home': {
                templateUrl: 'templates/mainHome.html?v=4',
                controller: 'homeController'
            }
        }
    })
    .state('app.eventList', {
        url: '/events',
        views: {
            'tab-home': {
                templateUrl: 'templates/eventList.html',
                controller: "eventListController"
            }
        }
    })
    .state('app.liunarProduct', {
        url: "/mainhome/liunarProduct/:typeId/",
        views: {
            'tab-home': {
                templateUrl: "templates/liunarProduct.html",
                controller: "liunarProductController",
                resolve: {
                    productList: function ($stateParams, httpServices) {
                        return httpServices.getLiunarProductList($stateParams.typeId);
                    },
                    pastCases: function ($stateParams, httpServices) {
                        return httpServices.getLiunarPastCase($stateParams.typeId);
                    },
                    schoolList: function ($stateParams, httpServices) {
                        if ($stateParams.typeId == 3) {
                            return httpServices.getSchoolListByProvince($stateParams.typeId, 5, 0, "", "", "BC");
                        } else {
                            return httpServices.getSchoolRankList($stateParams.typeId, "", "", "");
                        }
                    }
                }
            }
        }
    })
    .state('app.pastCase', {
        url: "/mainhome/liunarProduct/pastCase/:typeId/:caseId",
        views: {
            'tab-home': {
                templateUrl: "templates/liunarProductPastCase.html",
                controller: "liunarPastCaseController",
                resolve: {
                    caseDetail: function ($stateParams, httpServices) {
                        return httpServices.getLiunarPastCase($stateParams.typeId);
                    },
                }
            }
        }

    })
    .state('app.summerCamps', {
        url: "/mainhome/summercamps",
        views: {
            'tab-home': {
                templateUrl: "templates/schoolCamp.html",
                controller: "campController"
            }
        }
    })
    .state('app.summerCampsDetail', {
        url: "/mainhome/summercamps/campdetail/:campId",
        views: {
            'tab-home': {
                templateUrl: "templates/schoolCampDetail.html",
                controller: "campDetailController",
                resolve: {
                    campDetail: function ($stateParams, httpServices) {
                        return httpServices.getCampProductDetail($stateParams.campId);
                    }
                }
            }
        }
    })
    .state('app.postDetail', {
        url: '/mainhome/postdetail/:postId/:taskId?',
        views: {
            'tab-home': {
                templateUrl: 'templates/postDetail.html',
                controller: 'detailController',
                resolve: {
                    postDetail: function ($stateParams, httpServices) {
                        return httpServices.getPostDetails($stateParams.postId);
                    }
                }
            }
        }
    })
    .state('app.gongLue', {
        url: "/mainhome/toutiao",
        views: {
            'tab-home': {
                templateUrl: "templates/gongLue.html",
                controller: "liunarTouTiaoListController",
            }
        }
    })
    .state("app.tagResultList", {
        url: "/mainhome/topicresultlist/:tag",
        views: {
            'tab-home': {
                templateUrl: "templates/tagResultList.html",
                controller: "tagResultsController"
            }
        }
    })
    .state('app.QandA', {
        url: '/mainhome/QandA',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandA.html',
                controller: "qandAController",
                resolve: {
                    newQuestion: function (httpServices) {
                        return httpServices.getQuestions(1, 1, "newest", 3);
                    },
                    hotQuestions: function (httpServices) {
                        return httpServices.getQuestions(1, 1, "most-answered", 3)
                    }
                }
            }
        }
    })
    .state('app.QandATypeList', {
        url: '/mainhome/QandAtype/:typeId',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandATypeList.html',
                controller: "qandATypeListController",
            }
        }
    })
    .state('app.QandAList', {
        url: '/mainhome/QandA/QandAList/:order',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandAList.html',
                controller: "qandAListController",
                resolve: {
                    initQuestions: function (httpServices, $stateParams) {
                        return httpServices.getQuestions(1, 1, $stateParams.order, 10)
                    }
                }
            }
        }
    })
    .state('app.QandAForm', {
        url: '/mainhome/QandA/QandAForm',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandAForm.html',
                controller: "qandAFormController",
                resolve: {
                    hotQuestions: function (httpServices) {
                        return httpServices.getQuestions(1, 1, "most-answered", 5)
                    }
                }
            }
        }
    })
    .state('app.QandADetail', {
        url: "/mainhome/QandA/QandAList/QandADetail/:questionId/:order",
        views: {
            'tab-home': {
                templateUrl: 'templates/QandADetail.html',
                controller: "qandADetailController",
                resolve: {
                    questionDetail: function (httpServices, $stateParams) {
                        return httpServices.getQuestionDetail($stateParams.questionId);
                    },
                    answerDetail: function (httpServices, $stateParams) {
                        return httpServices.getQuestionAnswer($stateParams.questionId);
                    }
                }
            }
        }
    })
    .state('app.school', {
        url: '/school',
        views: {
            'tab-school': {
                templateUrl: 'templates/school.html',
                controller: 'schoolMainController'
            }
        }
    })
    .state('app.schoolRank', {
        url: "/schoolRank/:cateID",
        views: {
            'tab-school': {
                templateUrl: 'templates/schoolRank.html',
                controller: "schoolController",
            }
        }
    })
    .state('app.schoolRankProvince', {
        url: "/schoolrankprovince/:cateID/:province/:city/:public",
        views: {
            'tab-school': {
                templateUrl: 'templates/new_school_rank.html',
                controller: "schoolProvinceController",
            }
        }
    })
    .state('app.schoolStandard', {
        url: "/schoolStandard/:schoolId",
        views: {
            'tab-school': {
                templateUrl: "templates/schoolStandard.html",
                controller: "schoolDetailController",
                resolve: {
                    schoolDetail: function ($stateParams, httpServices) {
                        return httpServices.getSchoolDetail($stateParams.schoolId);
                    },
                    schoolComment: function ($stateParams, httpServices) {
                        return httpServices.getSchoolComment($stateParams.schoolId);
                    }
                }
            }
        }
    })
    .state('app.schoolProvinceStandard', {
        url: "/schoolDetail/:schoolId",
        views: {
            'tab-school': {
                templateUrl: "templates/new_school_std.html",
                controller: "newSchoolDetailController",
                resolve: {
                    schoolDetail: function ($stateParams, httpServices) {
                        return httpServices.getSchoolProvinceDetail($stateParams.schoolId);
                    },
                    schoolComment: function ($stateParams, httpServices) {
                        return httpServices.getSchoolComment($stateParams.schoolId);
                    }
                }
            }
        }
    })
    .state('apply', {
                //url: "/apply/:pageTitle/:productId",
                url: "/apply/:schoolType",
                templateUrl: "templates/applyForm.html",
                controller: "applyController"
            })
    .state('app.shop', {
        url: "/shop",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopNew.html?v=2",
                controller: "shopPage"
            }
        }
    })
    .state('app.shopnew', {
        url: "/shopnew",
        views: {
            'tab-shop': {
                templateUrl: "templates/new_shop.html",
                controller: "shopPageNew"
            }
        }
    })
    .state('app.allProduct', {
        url: "/allproduct",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopAll.html",
                controller: "shopProductAll"
            }
        }
    })
    .state('app.countryProduct', {
        url: "/countryproduct/:country",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopCountry.html",
                controller: "shopProductCountry",
            }
        }
    })
    .state('app.shopDetail', {
        url: "/shopProductDetail/:productId/:taskId?",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopProductDetail.html?v=2",
                controller: "shopProductDetail",
            }
        }
    })
    .state('app.productdetail', {
        url: "/productdetail/:productId/:taskId?",
        views: {
            'tab-shop': {
                templateUrl: "templates/new_shop_detail.html",
                controller: "newShopDetail",
            }
        }
    })
    .state('app.productmoredetail', {
        url: "/productmoredetail/:prdID",
        views: {
            'tab-shop': {
               templateUrl: "templates/productMoreDetail.html",
               controller: "productMoreDetail"
           }
       }
   })
    .state('app.shopPurchase', {
        url: "shopPurchase/:productId",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopPurchase.html",
                controller: "shopPurchase",
                resolve: {
                    purchaseDetail: function ($stateParams, httpServices) {
                        return httpServices.getShopProductDetail($stateParams.productId);
                    }
                }
            }
        },
    })
    .state('app.productpurchase', {
        url: "productpurchase/:productId",
        views: {
            'tab-shop': {
                templateUrl: "templates/new_shop_purchase.html",
                controller: "shopPurchase",
                resolve: {
                    purchaseDetail: function ($stateParams, httpServices) {
                        return httpServices.getProductDetailNew($stateParams.productId);
                    }
                }
            }
        },
    })
    .state('app.mission', {
        url: "/mission",
        views: {
            'tab-home': {
                templateUrl: "templates/articleMission.html?v=1",
                controller: "missionDetailController"
            }
        }
    })
    .state('app.hots', {
        url: "/hots",
        views: {
            'tab-home': {
                templateUrl: "templates/hotItem.html?v=1",
                controller: "hotItemsController"
            }
        }
    })
    .state('userLogin', {
        url: '/userLogin?lastview',
        controller: "userLogin",
        templateUrl: "templates/userLogin.html?v=3",
    })
            /*.state('userSignup', {
              url: "/userSignup",
              templateUrl: "templates/userSignup.html",
              controller: "userController"
          })*/
          .state('app.userCenter', {
            url: "/userCenter",
            views: {
                'tab-user': {
                    templateUrl: "templates/userCenter.html",
                    controller: "userController"
                }
            }
        })
          .state('app.userOrder', {
            url: "/userOrder",
            views: {
                'tab-user': {
                    templateUrl: "templates/userOrder.html",
                    controller: "userOrder"
                }
            }
        })
          .state('app.userProductPrice', {
            url: "/productsPrices",
            views: {
                'tab-user': {
                    templateUrl: "templates/userProductPrice.html",
                    controller: "shopProductAll"
                }
            }
        })
          .state('userAgentTutorial', {
            url: '/viptutorial',
            templateUrl: "templates/userVipTutorial.html",
        })
          .state('app.userAgentPromote', {
            url: "/vippromote",
            views: {
                'tab-user': {
                    templateUrl: "templates/userVIPPromote.html",
                    controller: "promoteController"
                }
            }
        }).state('testUpload', {
           url: "/testUpload",
           templateUrl: "templates/testUpload.html",
           controller: "testUpload"
       })
        .state('app.userApplyAgent', {
            url: "/userapplyagent",
            views: {
                'tab-user': {
                    templateUrl: "templates/userApplyVIP.html",
                    controller: "userUpload"
                }
            }
        })
        .state('app.userApplyStatus', {
            url: "/userapplystatus",
            views: {
                'tab-user': {
                    templateUrl: "templates/userApplyVIPStatus.html",
                    controller: ""
                }
            }
        })
        .state('userRecommend', {
            url: "/userRecommend",
            templateUrl: "templates/userRecommend.html",
            controller: "userRecommend"
        })
        .state('userShare', {
            url: "/userShare",
            templateUrl: "templates/userShare.html",
            controller: "userShare"
        })
        .state('app.userPoints', {
            url: "/userPoints",
            views: {
                'tab-user': {
                    templateUrl: "templates/userPoints.html",
                    controller: "userController"
                }
            }
        })
        .state('app.userCash', {
            url: "/userCash",
            views: {
                'tab-user': {
                    templateUrl: "templates/userCashOut.html",
                    controller: "userController"
                }
            }
        })
        .state('app.userClientList', {
            url: "/clientList",
            views: {
                'tab-user': {
                    templateUrl: "templates/userClientLists.html",
                    controller: "userClient"
                }
            }
        })
        .state('app.userClientDetail', {
            url: "/clientList/clientDetail/:orderId",
            views: {
                'tab-user': {
                    templateUrl: "templates/userClientDetail.html",
                    controller: "userClientOrderDetailController"
                }
            }
        })
        .state('app.userComplete', {
            url: "/userDetail",
            views: {
                'tab-user': {
                    templateUrl: "templates/userCompleteInfo.html",
                        controller: "userUpload"//"userController"
                    }
                }
            })
        .state('app.msgCenter', {
            url: "/msgcenter",
            views: {
                'tab-msg': {
                    templateUrl: "templates/msgCenter.html",
                    controller: "msgCenterController"
                }
            }
        })
        .state('app.msgDetail', {
            url: "/msgdetail/:id",
            views: {
                'tab-msg': {
                    templateUrl: "templates/msgDetail.html",
                    controller: "msgDetailController"
                }
            }
        })
        .state('app.msgPost', {
            url: "/post",
            views: {
                'tab-msg': {
                    templateUrl: "templates/msgPost.html",
                    controller: "msgPostController"
                }
            }
        })
        .state('yun', {
            url: "/yun",
            templateUrl: "templates/yun.html",
            controller: "yunController"            
        })
// if none of the above states are matched, use this as the fallback
$urlRouterProvider.otherwise('/app/mainhome');
        //$locationProvider.html5Mode(true);
    });
