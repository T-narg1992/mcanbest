angular.module('canbest', ['ngCookies','slickCarousel', 'ngFileUpload', 'chart.js', 'ionic', 'angulartics', 'angulartics.baidu', 'canbestServer', 'homeController', 'serverConfig', 'en_ch', 'qandAController', 'event', 'qandADetailController', 'userController', 'agencyController', 'detailController', 'schoolController', 'schoolDetailController', 'articleListController', 'estimateController', 'liunarProduct', 'applyController', 'search', 'msg'])
.run(["$http", "$ionicPlatform", "localStorage", "$state", "$timeout", "$location", "httpServices", "pressureTest", "$rootScope", "$window", "$ionicHistory", "seedUrl", "detectBrowser", "$ionicPopup", "$cookies", function ($http, $ionicPlatform, localStorage, $state, $timeout, $location, httpServices, pressureTest, $rootScope, $window, $ionicHistory, seedUrl, detectBrowser, $ionicPopup, $cookies) {

    var urlstring = $location.$$absUrl;

    var start = urlstring.indexOf("?code=");

    var shareUidUrl = urlstring.indexOf("?referid=");

    if (shareUidUrl !== -1) {
        if (urlstring.indexOf("?referid=null") === -1) {
            shareUidUrl += "?referid=".length;
            var stop = urlstring.indexOf('&', shareUidUrl);
            if (stop === -1) {
                var referId = urlstring.substring(shareUidUrl);
            } else {
                var referId = urlstring.substring(shareUidUrl, stop);
            }
            localStorage.setItem("referId", referId);
            $rootScope.referId = referId;
        } else {
            localStorage.setItem("referId", null);
            $rootScope.referId = "null";
        }
    } else {
        $rootScope.referId = "null";
    }

    console.log($rootScope.referId, $location.search());

    if (start !== -1) {
        start += "?code=".length;
        var end = urlstring.indexOf('&', start);

            //alert("微信之后的跳转");

            if (end === -1) {
                var code_value = urlstring.substring(start);
            } else {
                var code_value = urlstring.substring(start, end);
            }

            httpServices.wechatLogin(code_value, localStorage.getItem("referId")).then(
                function (res) {
                    // alert("授权成功");
                    // console.log(res);
                    if (!res.errcode) {
                        localStorage.setObject("canbestUserInfo", res);
                        localStorage.setItem('jwtToken', res.token);
                        $cookies.put('jwtToken', res.token);
                        $cookies.putObject("user",{
                            uid: res.uid
                        })
                        //console.log($location, $ionicHistory.viewHistory());
                        $window.location.href = localStorage.getItem("lastview") == null ? "http://www.liunar.com/mobile/#/app/userCenter" : (localStorage.getItem("lastview").indexOf("http://") == -1) ? "http://www.liunar.com/mobile/#" + localStorage.getItem("lastview") : localStorage.getItem("lastview");
                        localStorage.remove("lastview");
                    }
                }
                )

        };

        $rootScope.showCallBtn = true;

        $rootScope.call = function () {
            $ionicPopup.confirm({
                title: '留哪儿',
                template: '<p style="text-align: center">确认拨打咨询电话</p>',
                cancelText: '取消', // String (default: 'Cancel'). The text of the Cancel button.
                okText: '确认', // String (default: 'OK'). The text of the OK button.
                okType: 'calm',
            }).then(function (res) {
                if (res) {
                    //console.log(res);
                    window.location.href = 'tel:4008099039';
                } else {
                    console.log("cancle");
                }
            });
        };

        if (localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid) {
            $rootScope.shareState = "?referid=null";
        } else {
            $rootScope.shareState = "?referid=" + localStorage.getObject("canbestUserInfo").uid;
        }

        httpServices.jsSDK().then(
            function (res) {
                //console.log(res);
                var cfg = {
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                    appId: res.appId, // 必填，公众号的唯一标识
                    timestamp: res.timestamp, // 必填，生成签名的时间戳
                    nonceStr: res.nonceStr, // 必填，生成签名的随机串
                    signature: res.signature,// 必填，签名，见附录1
                    jsApiList: ['checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage']
                };
                $window.wx.config(cfg);
            }
            );

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
                //console.log(cordova.plugins.Keyboard);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.hideTabs = '';

        $rootScope.$on("$ionicView.beforeEnter",
            function (evt, view) {
                var seedImg = document.createElement("img");
                seedImg.setAttribute("src", "img/pv.jpg?from=" + $location.$$url);
                var lastEle = document.getElementsByTagName("*")[document.getElementsByTagName("*").length - 1];
                lastEle.parentNode.insertBefore(seedImg, lastEle);
                if (view.stateName == "app.mission" || view.stateName == "app.postDetail") {
                    $rootScope.hideTabs = 'tabs-item-hide';
                } else {
                    $rootScope.hideTabs = '';
                }
            }
            );
    }])
.directive('lazyScroll',
    ["$rootScope", function ($rootScope) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                var origEvent = $scope.$onScroll;
                $scope.$onScroll = function () {
                    $rootScope.$broadcast('lazyScrollEvent');
                    if (typeof origEvent === 'function') {
                        origEvent();
                    }
                };
            }
        };
    }])
.directive('imageLazySrc',
    ["$document", "$timeout", "$ionicScrollDelegate", "$compile", function ($document, $timeout, $ionicScrollDelegate, $compile) {
        return {
            restrict: 'A',
            scope: {
                lazyScrollResize: "@lazyScrollResize",
                imageLazyBackgroundImage: "@imageLazyBackgroundImage",
                imageLazySrc: "@"
            },
            link: function ($scope, $element, $attributes) {
                if (!$attributes.imageLazyDistanceFromBottomToLoad) {
                    $attributes.imageLazyDistanceFromBottomToLoad = 0;
                }
                if (!$attributes.imageLazyDistanceFromRightToLoad) {
                    $attributes.imageLazyDistanceFromRightToLoad = 0;
                }

                var loader;
                if ($attributes.imageLazyLoader) {
                    loader = $compile('<div class="image-loader-container"><ion-spinner class="image-loader" icon="' + $attributes.imageLazyLoader + '"></ion-spinner></div>')($scope);
                    $element.after(loader);
                }

                $scope.$watch('imageLazySrc', function (oldV, newV) {
                    if (loader)
                        loader.remove();
                    if ($attributes.imageLazyLoader) {
                        loader = $compile('<div class="image-loader-container"><ion-spinner class="image-loader" icon="' + $attributes.imageLazyLoader + '"></ion-spinner></div>')($scope);
                        $element.after(loader);
                    }
                    var deregistration = $scope.$on('lazyScrollEvent', function () {
                        //    console.log('scroll');
                        if (isInView()) {
                            loadImage();
                            deregistration();
                        }
                    }
                    );
                    $timeout(function () {
                        if (isInView()) {
                            loadImage();
                            deregistration();
                        }
                    }, 500);
                });
                var deregistration = $scope.$on('lazyScrollEvent', function () {
                    // console.log('scroll');
                    if (isInView()) {
                        loadImage();
                        deregistration();
                    }
                }
                );

                function loadImage() {
                    //Bind "load" event
                    $element.bind("load", function (e) {
                        if ($attributes.imageLazyLoader) {
                            loader.remove();
                        }
                        if ($scope.lazyScrollResize == "true") {
                            //Call the resize to recalculate the size of the screen
                            $ionicScrollDelegate.resize();
                        }
                        $element.unbind("load");
                    });

                    if ($scope.imageLazyBackgroundImage == "true") {
                        var bgImg = new Image();
                        bgImg.onload = function () {
                            if ($attributes.imageLazyLoader) {
                                loader.remove();
                            }
                            $element[0].style.backgroundImage = 'url(' + $attributes.imageLazySrc + ')'; // set style attribute on element (it will load image)
                            if ($scope.lazyScrollResize == "true") {
                                //Call the resize to recalculate the size of the screen
                                $ionicScrollDelegate.resize();
                            }
                        };
                        bgImg.src = $attributes.imageLazySrc;
                    } else {
                        $element[0].src = $attributes.imageLazySrc; // set src attribute on element (it will load image)
                    }
                }

                function isInView() {
                    var clientHeight = $document[0].documentElement.clientHeight;
                    var clientWidth = $document[0].documentElement.clientWidth;
                    var imageRect = $element[0].getBoundingClientRect();
                    return (imageRect.top >= 0 && imageRect.top <= clientHeight + parseInt($attributes.imageLazyDistanceFromBottomToLoad))
                    && (imageRect.left >= 0 && imageRect.left <= clientWidth + parseInt($attributes.imageLazyDistanceFromRightToLoad));
                }

                // bind listener
                // listenerRemover = scrollAndResizeListener.bindListener(isInView);

                // unbind event listeners if element was destroyed
                // it happens when you change view, etc
                $element.on('$destroy', function () {
                    deregistration();
                });

                // explicitly call scroll listener (because, some images are in viewport already and we haven't scrolled yet)
                $timeout(function () {
                    if (isInView()) {
                        loadImage();
                        deregistration();
                    }
                }, 500);
            }
        };
    }])
.directive('elScroll', ["$location", "$ionicScrollDelegate", function ($location, $ionicScrollDelegate) {
    return {
        restrict: 'A',
        scope: {
            target: "@elScroll",
            delegate: "@",
            animate: "@"
        },
        link: function (scope, element, attrs) {
        },
        controller: ["$scope", "$element", "$attrs", function ($scope, $element, $attrs) {
            var shouldAnimate = $scope.animate;
            $scope.scrollTo = function () {
                $location.hash($scope.target);
                var handle = $ionicScrollDelegate.$getByHandle($scope.delegate);
                handle.anchorScroll(shouldAnimate);
            };
            $element.bind("click", function (e) {
                e.preventDefault();
                $scope.scrollTo();
            });
        }]
    }
}])
.directive('map', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div></div>',
        scope: {
            'address': "@",
            'mapType': "@"
        },
        link: function (scope, element, attrs) {
                //console.log(scope.address);
                var geoCoder = new google.maps.Geocoder();

                var mapOptions = {
                    zoom: 16,
                    mapTypeId: eval(scope.mapType),
                    draggable: true,
                    disableDefaultUI: true,
                    zoomControl: true,
                    fullscreenControl: true
                };

                var viewOptions = {
                    pov: { heading: 0, pitch: 0 },
                    zoom: 1,
                    streetViewControl: false
                };

                var mapInit = function () {
                    var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
                    var marker = new google.maps.Marker({
                        map: map,
                    });
                    /*google.maps.event.addListener(map,'center_changed',function() {
                        window.setTimeout(
                            function() {
                                map.panTo(marker.getPosition());
                            },
                        1);
                    });*/
                    geoCoder.geocode(
                        { 'address': scope.address },
                        function (result, status) {
                            //console.log(result, status);
                            if (status === "OK") {
                                map.setCenter(result[0].geometry.location);
                                marker.setPosition(result[0].geometry.location);
                                marker.addListener('click', function (res) {
                                    window.open('http://maps.google.com/maps?q=' + scope.address);
                                });
                            }
                        }
                        );
                };

                var streetViewInit = function () {
                    var panorama = new google.maps.StreetViewPanorama(document.getElementById(attrs.id), viewOptions);
                    geoCoder.geocode(
                        { 'address': scope.address },
                        function (result, status) {
                            //console.log(result, status);
                            if (status === "OK") {
                                panorama.setPosition(result[0].geometry.location);
                            }
                        }
                        );
                }

                mapInit();

                scope.$watch('mapType', function (a, b) {
                    if (a) {
                        if (a === "streetView") {
                            streetViewInit();
                        } else {
                            if (a === undefined || a === "") {
                                mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
                            } else {
                                mapOptions.mapTypeId = eval(a);
                            }
                            mapInit();
                            //console.log(a);   
                        }
                    }
                });
            }
        };
    })
.directive('likeBtn', ["$ionicPopup", "$state", "$http", "userAuth", "$location", "urlConfig", function ($ionicPopup, $state, $http, userAuth, $location, urlConfig) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div><i></i><span></span></div>',
        scope: {
            module: '@',
            rowid: '@',
        },
        link: function (scope, el, attr) {
            angular.element(el).attr('style', 'margin: 12px 0px;');
            angular.element(el[0].childNodes[0]).attr('style', 'font-size: 24px; color: rgba(246, 83, 74, 1); vertical-align: middle');

            angular.element(el[0].childNodes[1]).attr('style', 'font-size: 14px; color: steelblue; vertical-align: middle; margin-left: 8px;');


            var shape = {
                outline: function () {
                    angular.element(el[0].childNodes[0]).attr('class', 'ion-android-favorite-outline');
                },
                fill: function () {
                    angular.element(el[0].childNodes[0]).attr('class', 'ion-android-favorite');
                }
            }

            var statusInit = function () {
                    //console.log(scope.like);
                    $http({
                        url: urlConfig.url + "likes/total/" + scope.module + "/" + scope.rowid,
                        method: "GET"
                    }).then(
                    function (res) {
                        var likeNum = res.data[0].likes;
                        angular.element(el[0].childNodes[1]).text(likeNum);
                    }
                    );
                    if (scope.like == true) {
                        shape.fill();
                    } else if (scope.like == false) {
                        shape.outline();
                    }
                };

                var login = new userAuth.checkLogin();

                if (login.isLogin == true) {
                    $http({
                        url: urlConfig.url + "likes/" + login.uid,
                        method: "GET",
                    }).then(
                    function (res) {
                        var datas = res.data;
                        if (datas.length != 0) {
                            for (var i = 0; i < datas.length; i++) {
                                scope.like = false;
                                if (datas[i].module == scope.module && datas[i].row_id == scope.rowid) {
                                    scope.like = true;
                                    break;
                                }
                            }
                        } else {
                            scope.like = false;
                        }
                        statusInit();
                    }
                    );
                } else if (login.isLogin == false) {
                    scope.like = false;
                    statusInit();
                }

                el.bind('click', function (res) {
                    if (login.isLogin == true) {
                        if (scope.like == true) {
                            scope.like = false;
                            $http({
                                url: urlConfig.url + "likes/unlike",
                                method: "POST",
                                data: { uid: login.uid, module: scope.module, row_id: scope.rowid }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        } else if (scope.like == false) {
                            scope.like = true;
                            $http({
                                url: urlConfig.url + "likes/like",
                                method: "POST",
                                data: { uid: login.uid, module: scope.module, row_id: scope.rowid }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        }
                    } else {
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '请微信登录之后再点赞'
                        }).then(
                        function () {
                            $state.go("userLogin", { lastview: $location.$$path });
                        }
                        );
                    }
                });
            }
        }
    }])
.directive("saveTag", ["$ionicPopup", "$state", "$http", "userAuth", "$location", "$window", "urlConfig", function ($ionicPopup, $state, $http, userAuth, $location, $window, urlConfig) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div><button></button><p></p></div>',
        scope: {
            targetid: '@',
                //rowid: '@',
            },
            link: function ($scope, $ele, $attr) {

                var status = {
                    follow: function () {
                        angular.element($ele[0].childNodes[0]).text("关注");
                        angular.element($ele[0].childNodes[0]).attr(
                            'style', 'background: transparent; border: 1px solid #52baba; color: #52baba; top: 17px; right: 17px; min-width: 82px; font-size: 14px; position: absolute;'
                            );
                    },
                    unfollow: function () {
                        angular.element($ele[0].childNodes[0]).text("已关注");
                        angular.element($ele[0].childNodes[0]).attr(
                            'style', 'background: #52baba; color: #fff; top: 17px; right: 17px; min-width: 82px; font-size: 14px; position: absolute; border: none;'
                            );
                    }
                };

                var userStatus = new userAuth.checkLogin();

                var statusInit = function () {
                    $http({
                        url: urlConfig.url + "tags/count/" + $scope.targetid,
                        method: "GET"
                    }).then(
                    function (res) {
                        console.log(res);
                        if (res.data !== "") {
                            $scope.tagCount = res.data.userCount;
                            angular.element($ele[0].childNodes[1]).attr("style", "font-size: 14px; margin: 0px;");
                            angular.element($ele[0].childNodes[1]).html("关注: <span style='color: #52baba;'>" + $scope.tagCount + "</span>");
                        } else {
                            angular.element($ele[0].childNodes[1]).attr("style", "font-size: 14px; margin: 0px;");
                            angular.element($ele[0].childNodes[1]).html("关注: <span style='color: #52baba;'>0</span>");
                        }
                    }
                    );
                    if ($scope.follow == true) {
                        status.unfollow();
                    } else if ($scope.follow == false) {
                        status.follow();
                    }
                };

                if (userStatus.isLogin === true) {
                    $http({
                        url: urlConfig.url + "tags/mylist/" + userStatus.uid,
                        method: "GET"
                    }).then(
                    function (res) {
                        var datas = res.data;
                        if (datas.length > 0) {
                            for (var i = 0; i < datas.length; i++) {
                                $scope.follow = false;
                                if (datas[i].tag_id == $scope.targetid && datas[i].uid == userStatus.uid) {
                                    $scope.follow = true;
                                    break;
                                }
                            }
                        } else {
                            $scope.follow = false;
                        }

                        statusInit();
                    }
                    );
                } else {
                    $scope.follow = false;
                    statusInit();
                }

                $ele.bind("click", function (res) {
                    if (userStatus.isLogin == true) {
                        if ($scope.follow == true) {
                            $scope.follow = false;
                            $http({
                                url: urlConfig.url + "tags/unfollow",
                                method: "POST",
                                data: {
                                    tagid: $scope.targetid,
                                    uid: userStatus.uid
                                }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        } else if ($scope.follow == false) {
                            $scope.follow = true;
                            $http({
                                url: urlConfig.url + "tags/follow",
                                method: "POST",
                                data: {
                                    tagid: $scope.targetid,
                                    uid: userStatus.uid
                                }
                            }).then(
                            function (res) {
                                statusInit();
                            }
                            );
                        }
                    } else {
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '请微信登录之后再关注'
                        }).then(
                        function () {
                            $state.go("userLogin", { lastview: $location.$$path });
                        }
                        );
                    }
                });
            }
        }
    }])
.directive("hideContent", ["userAuth", "$timeout", "$location", "detectBrowser", function (userAuth, $timeout, $location, detectBrowser) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, ele, attrs) {

            var loginStatus = new userAuth.checkLogin();

            var createButton = function (top) {
                var btn = document.createElement("div");
                btn.style.width = "100%";
                btn.style.height = "120px";
                btn.style.position = "absolute";
                    //btn.style.top = top + "px";
                    btn.style.bottom = "0px";
                    btn.style.left = "0px";
                    btn.style.color = "#52baba";
                    btn.style.textAlign = "center";
                    btn.style.lineHeight = "200px";
                    //btn.style.background = "rgba(255,255,255,1)";
                    btn.style.background = "-webkit-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    //btn.style.background = "red";
                    btn.style.background = "-o-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.style.background = " -moz-linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    btn.style.background = "linear-gradient(top, rgba(254,254,254,0) 0%, rgba(255,255,255,0.48) 30%, rgba(255,255,255,1) 63%)";
                    //btn.textContent = "阅读全文...";
                    //btn.href = "#/app/userCenter";
                    btn.innerHTML = "<a href='#/userLogin?lastview=" + $location.$$path + "'>点击阅读全文...</a>"
                    ele[0].appendChild(btn);
                }

                $timeout(
                    function () {
                        scope.height = ele[0].offsetHeight;
                        if (loginStatus.isLogin === true) {

                        } else if ($location.$$absUrl.indexOf('referid=') !== -1 && loginStatus.isLogin === false && detectBrowser.isWechat() == true) {
                            angular.element(ele).attr('style', "max-height: " + scope.height / 3 + "px; overflow: hidden; position: relative");
                            createButton(scope.height / 3 + 52);
                            console.log("not login");
                        }
                    }
                    );
            }
        }
    }])
.directive("checkImage", function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
                //console.log(scope);
                element.bind('error', function (res) {
                    angular.element(element).attr('class', 'hide');
                })
            }
        }
    })
.directive("checkSchoolImage", function () {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
                //console.log(scope);
                element.bind('error', function (res) {
                    angular.element(element).attr('src', 'img/school-default.png');
                })
            }
        }
    })
.directive('preventOverScroll', ["$window", "$ionicScrollDelegate", "$timeout", function ($window, $ionicScrollDelegate, $timeout) {
    return {
        restrict: 'A',
        link: function ($scope, $el) {
            var startY;
            $el.on('touchstart', function (e) {
                startY = e.touches[0].clientY;
                    //console.log("from: ", startY);
                });

            $el.on("touchmove", function (evt) {
                var status = '11';
                var ele = this;
                var currentY = evt.touches[0].clientY;
                    //console.log("to: ", currentY)
                    //console.log(ele.scrollTop);
                    if (ele.scrollTop === 0) {
                        status = ele.offsetHeight >= ele.scrollHeight ? '00' : '01';

                    } else if (ele.scrollTop + ele.offsetHeight >= ele.scrollHeight) {
                        status = '10';
                    }
                    if (status != '11') {
                        var direction = currentY - startY > 0 ? '10' : '01';

                        if (!(parseInt(status, 2) & parseInt(direction, 2))) {
                            evt.preventDefault();
                            //console.log("preventScroll");
                        }
                    }
                    //console.log(status);
                });
        }
    }
}])
.directive('preventScroll', ["$window", "$ionicScrollDelegate", "$timeout", function ($window, $ionicScrollDelegate, $timeout) {
    return {
        restrict: 'A',
        link: function ($scope, $el) {

            $el.on("touchmove", function (evt) {
                    //console.log("no scroll", $el[0].children);
                    if ($el[0].nodeName == "ION-TABS") {
                        angular.element("div.tab-nav.tabs").on("touchmove", function (evt) {
                            evt.preventDefault();
                        })
                    } else {
                        evt.preventDefault();
                    }
                });
        }
    }
}])
.directive('hideTabs', ["$rootScope", function ($rootScope) {
    return {
        restrict: 'A',
        link: function ($scope, $el) {
                //console.log($el);
                $rootScope.hideTabs = 'tabs-item-hide';
                $scope.$on('$destroy', function (evet, data) {
                    //console.log(evet, data);
                    $rootScope.hideTabs = '';
                });
            }
        };
    }])
.directive('progressBar', function () {
    return {
        restrict: 'E',
        replace: 'true',
        template: '<div></div>',
        scope: {
            'max': '@',
            'value': '@',
        },
        link: function (scope, el, attr) {
                //console.log(scope, el);
                el[0].style.backgroundColor = "#e5e5e5";
                el[0].style.overflow = "hidden";
                el[0].style.position = "relative";
                el[0].style.height = "12px";
                el[0].style.borderRadius = "6px";
                var valueBlock = document.createElement('div');
                valueBlock.style.height = "12px";
                valueBlock.style.borderRadius = "6px";
                valueBlock.style.backgroundColor = "rgb(155, 207, 0)";
                valueBlock.style.position = "absolute";
                valueBlock.style.left = "0";
                valueBlock.style.top = "0";
                el[0].appendChild(valueBlock);
                scope.$watch('value', function (a, b) {
                    if (a) {
                        valueBlock.style.width = a + "%";
                    }
                })
            }
        }
    })
.config(["$ionicConfigProvider", function ($ionicConfigProvider) {
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.tabs.style('standard');
    $ionicConfigProvider.views.transition('ios');
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.navBar.positionPrimaryButtons('left');
    $ionicConfigProvider.navBar.positionSecondaryButtons('right');
    $ionicConfigProvider.templates.maxPrefetch(0);
    $ionicConfigProvider.scrolling.jsScrolling(false);
}])
.config(["$cookiesProvider", function($cookiesProvider){
    $cookiesProvider.defaults.domain = ".liunar.com";
    $cookiesProvider.defaults.path = "/";
}])
.config(["$httpProvider", "$windowProvider", function ($httpProvider, $windowProvider) {
    $window = $windowProvider.$get();
    var token =  $window.localStorage.jwtToken;
    if( token){
        $httpProvider.defaults.headers.common['token'] = token;
    }
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.transformRequest.unshift(function (obj) {
        var str = [];
        for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    });
}])
.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/tabs.html',
        controller: 'tabsController'
    })
    .state('schoolSearch', {
        url: "/schoolsearch",
        templateUrl: 'templates/search.html',
        controller: "schoolSearchController"
    })
    .state('tagSearch', {
        url: "/tagsearch/:tag",
        templateUrl: "templates/searchTags.html",
        controller: "tagSearchController"
    })
    .state("tagSquare", {
        url: "/tagsquare",
        templateUrl: "templates/tagSquare.html",
        controller: "tagsController"
    })
    .state('promote', {
        url: '/promote',
        templateUrl: "templates/promote.html",
    })
    .state('app.estimate', {
        url: '/estimate',
        views: {
            'tab-home': {
                templateUrl: "templates/estimate.html",
                controller: "estimateController"
            }
        }
    })
    .state('app.estimateResult', {
        url: '/estimate/estimateResult',
        cache: false,
        views: {
            'tab-home': {
                templateUrl: "templates/estimateResult.html",
                controller: "estimateController",
            }
        }
    })
    .state('app.mainHome', {
        url: '/mainhome',
        views: {
            'tab-home': {
                templateUrl: 'templates/mainHome.html?v=4',
                controller: 'homeController'
            }
        }
    })
    .state('app.eventList', {
        url: '/events',
        views: {
            'tab-home': {
                templateUrl: 'templates/eventList.html',
                controller: "eventListController"
            }
        }
    })
    .state('app.liunarProduct', {
        url: "/mainhome/liunarProduct/:typeId/",
        views: {
            'tab-home': {
                templateUrl: "templates/liunarProduct.html",
                controller: "liunarProductController",
                resolve: {
                    productList: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getLiunarProductList($stateParams.typeId);
                    }],
                    pastCases: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getLiunarPastCase($stateParams.typeId);
                    }],
                    schoolList: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        if ($stateParams.typeId == 3) {
                            return httpServices.getSchoolListByProvince($stateParams.typeId, 5, 0, "", "", "BC");
                        } else {
                            return httpServices.getSchoolRankList($stateParams.typeId, "", "", "");
                        }
                    }]
                }
            }
        }
    })
    .state('app.pastCase', {
        url: "/mainhome/liunarProduct/pastCase/:typeId/:caseId",
        views: {
            'tab-home': {
                templateUrl: "templates/liunarProductPastCase.html",
                controller: "liunarPastCaseController",
                resolve: {
                    caseDetail: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getLiunarPastCase($stateParams.typeId);
                    }],
                }
            }
        }

    })
    .state('app.summerCamps', {
        url: "/mainhome/summercamps",
        views: {
            'tab-home': {
                templateUrl: "templates/schoolCamp.html",
                controller: "campController"
            }
        }
    })
    .state('app.summerCampsDetail', {
        url: "/mainhome/summercamps/campdetail/:campId",
        views: {
            'tab-home': {
                templateUrl: "templates/schoolCampDetail.html",
                controller: "campDetailController",
                resolve: {
                    campDetail: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getCampProductDetail($stateParams.campId);
                    }]
                }
            }
        }
    })
    .state('app.postDetail', {
        url: '/mainhome/postdetail/:postId/:taskId?',
        views: {
            'tab-home': {
                templateUrl: 'templates/postDetail.html',
                controller: 'detailController',
                resolve: {
                    postDetail: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getPostDetails($stateParams.postId);
                    }]
                }
            }
        }
    })
    .state('app.gongLue', {
        url: "/mainhome/toutiao",
        views: {
            'tab-home': {
                templateUrl: "templates/gongLue.html",
                controller: "liunarTouTiaoListController",
            }
        }
    })
    .state("app.tagResultList", {
        url: "/mainhome/topicresultlist/:tag",
        views: {
            'tab-home': {
                templateUrl: "templates/tagResultList.html",
                controller: "tagResultsController"
            }
        }
    })
    .state('app.QandA', {
        url: '/mainhome/QandA',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandA.html',
                controller: "qandAController",
                resolve: {
                    newQuestion: ["httpServices", function (httpServices) {
                        return httpServices.getQuestions(1, 1, "newest", 3);
                    }],
                    hotQuestions: ["httpServices", function (httpServices) {
                        return httpServices.getQuestions(1, 1, "most-answered", 3)
                    }]
                }
            }
        }
    })
    .state('app.QandATypeList', {
        url: '/mainhome/QandAtype/:typeId',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandATypeList.html',
                controller: "qandATypeListController",
            }
        }
    })
    .state('app.QandAList', {
        url: '/mainhome/QandA/QandAList/:order',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandAList.html',
                controller: "qandAListController",
                resolve: {
                    initQuestions: ["httpServices", "$stateParams", function (httpServices, $stateParams) {
                        return httpServices.getQuestions(1, 1, $stateParams.order, 10)
                    }]
                }
            }
        }
    })
    .state('app.QandAForm', {
        url: '/mainhome/QandA/QandAForm',
        views: {
            'tab-home': {
                templateUrl: 'templates/QandAForm.html',
                controller: "qandAFormController",
                resolve: {
                    hotQuestions: ["httpServices", function (httpServices) {
                        return httpServices.getQuestions(1, 1, "most-answered", 5)
                    }]
                }
            }
        }
    })
    .state('app.QandADetail', {
        url: "/mainhome/QandA/QandAList/QandADetail/:questionId/:order",
        views: {
            'tab-home': {
                templateUrl: 'templates/QandADetail.html',
                controller: "qandADetailController",
                resolve: {
                    questionDetail: ["httpServices", "$stateParams", function (httpServices, $stateParams) {
                        return httpServices.getQuestionDetail($stateParams.questionId);
                    }],
                    answerDetail: ["httpServices", "$stateParams", function (httpServices, $stateParams) {
                        return httpServices.getQuestionAnswer($stateParams.questionId);
                    }]
                }
            }
        }
    })
    .state('app.school', {
        url: '/school',
        views: {
            'tab-school': {
                templateUrl: 'templates/school.html',
                controller: 'schoolMainController'
            }
        }
    })
    .state('app.schoolRank', {
        url: "/schoolRank/:cateID",
        views: {
            'tab-school': {
                templateUrl: 'templates/schoolRank.html',
                controller: "schoolController",
            }
        }
    })
    .state('app.schoolRankProvince', {
        url: "/schoolrankprovince/:cateID/:province/:city/:public",
        views: {
            'tab-school': {
                templateUrl: 'templates/new_school_rank.html',
                controller: "schoolProvinceController",
            }
        }
    })
    .state('app.schoolStandard', {
        url: "/schoolStandard/:schoolId",
        views: {
            'tab-school': {
                templateUrl: "templates/schoolStandard.html",
                controller: "schoolDetailController",
                resolve: {
                    schoolDetail: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getSchoolDetail($stateParams.schoolId);
                    }],
                    schoolComment: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getSchoolComment($stateParams.schoolId);
                    }]
                }
            }
        }
    })
    .state('app.schoolProvinceStandard', {
        url: "/schoolDetail/:schoolId",
        views: {
            'tab-school': {
                templateUrl: "templates/new_school_std.html",
                controller: "newSchoolDetailController",
                resolve: {
                    schoolDetail: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getSchoolProvinceDetail($stateParams.schoolId);
                    }],
                    schoolComment: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getSchoolComment($stateParams.schoolId);
                    }]
                }
            }
        }
    })
    .state('apply', {
                //url: "/apply/:pageTitle/:productId",
                url: "/apply/:schoolType",
                templateUrl: "templates/applyForm.html",
                controller: "applyController"
            })
    .state('app.shop', {
        url: "/shop",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopNew.html?v=2",
                controller: "shopPage"
            }
        }
    })
    .state('app.shopnew', {
        url: "/shopnew",
        views: {
            'tab-shop': {
                templateUrl: "templates/new_shop.html",
                controller: "shopPageNew"
            }
        }
    })
    .state('app.allProduct', {
        url: "/allproduct",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopAll.html",
                controller: "shopProductAll"
            }
        }
    })
    .state('app.countryProduct', {
        url: "/countryproduct/:country",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopCountry.html",
                controller: "shopProductCountry",
            }
        }
    })
    .state('app.shopDetail', {
        url: "/shopProductDetail/:productId/:taskId?",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopProductDetail.html?v=2",
                controller: "shopProductDetail",
            }
        }
    })
    .state('app.productdetail', {
        url: "/productdetail/:productId/:taskId?",
        views: {
            'tab-shop': {
                templateUrl: "templates/new_shop_detail.html",
                controller: "newShopDetail",
            }
        }
    })
    .state('app.productmoredetail', {
        url: "/productmoredetail/:prdID",
        views: {
            'tab-shop': {
               templateUrl: "templates/productMoreDetail.html",
               controller: "productMoreDetail"
           }
       }
   })
    .state('app.shopPurchase', {
        url: "shopPurchase/:productId",
        views: {
            'tab-shop': {
                templateUrl: "templates/shopPurchase.html",
                controller: "shopPurchase",
                resolve: {
                    purchaseDetail: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getShopProductDetail($stateParams.productId);
                    }]
                }
            }
        },
    })
    .state('app.productpurchase', {
        url: "productpurchase/:productId",
        views: {
            'tab-shop': {
                templateUrl: "templates/new_shop_purchase.html",
                controller: "shopPurchase",
                resolve: {
                    purchaseDetail: ["$stateParams", "httpServices", function ($stateParams, httpServices) {
                        return httpServices.getProductDetailNew($stateParams.productId);
                    }]
                }
            }
        },
    })
    .state('app.mission', {
        url: "/mission",
        views: {
            'tab-home': {
                templateUrl: "templates/articleMission.html?v=1",
                controller: "missionDetailController"
            }
        }
    })
    .state('app.hots', {
        url: "/hots",
        views: {
            'tab-home': {
                templateUrl: "templates/hotItem.html?v=1",
                controller: "hotItemsController"
            }
        }
    })
    .state('userLogin', {
        url: '/userLogin?lastview',
        controller: "userLogin",
        templateUrl: "templates/userLogin.html?v=3",
    })
            /*.state('userSignup', {
              url: "/userSignup",
              templateUrl: "templates/userSignup.html",
              controller: "userController"
          })*/
          .state('app.userCenter', {
            url: "/userCenter",
            views: {
                'tab-user': {
                    templateUrl: "templates/userCenter.html",
                    controller: "userController"
                }
            }
        })
          .state('app.userOrder', {
            url: "/userOrder",
            views: {
                'tab-user': {
                    templateUrl: "templates/userOrder.html",
                    controller: "userOrder"
                }
            }
        })
          .state('app.userProductPrice', {
            url: "/productsPrices",
            views: {
                'tab-user': {
                    templateUrl: "templates/userProductPrice.html",
                    controller: "shopProductAll"
                }
            }
        })
          .state('userAgentTutorial', {
            url: '/viptutorial',
            templateUrl: "templates/userVipTutorial.html",
        })
          .state('app.userAgentPromote', {
            url: "/vippromote",
            views: {
                'tab-user': {
                    templateUrl: "templates/userVIPPromote.html",
                    controller: "promoteController"
                }
            }
        }).state('testUpload', {
           url: "/testUpload",
           templateUrl: "templates/testUpload.html",
           controller: "testUpload"
       })
        .state('app.userApplyAgent', {
            url: "/userapplyagent",
            views: {
                'tab-user': {
                    templateUrl: "templates/userApplyVIP.html",
                    controller: "userUpload"
                }
            }
        })
        .state('app.userApplyStatus', {
            url: "/userapplystatus",
            views: {
                'tab-user': {
                    templateUrl: "templates/userApplyVIPStatus.html",
                    controller: ""
                }
            }
        })
        .state('userRecommend', {
            url: "/userRecommend",
            templateUrl: "templates/userRecommend.html",
            controller: "userRecommend"
        })
        .state('userShare', {
            url: "/userShare",
            templateUrl: "templates/userShare.html",
            controller: "userShare"
        })
        .state('app.userPoints', {
            url: "/userPoints",
            views: {
                'tab-user': {
                    templateUrl: "templates/userPoints.html",
                    controller: "userController"
                }
            }
        })
        .state('app.userCash', {
            url: "/userCash",
            views: {
                'tab-user': {
                    templateUrl: "templates/userCashOut.html",
                    controller: "userController"
                }
            }
        })
        .state('app.userClientList', {
            url: "/clientList",
            views: {
                'tab-user': {
                    templateUrl: "templates/userClientLists.html",
                    controller: "userClient"
                }
            }
        })
        .state('app.userClientDetail', {
            url: "/clientList/clientDetail/:orderId",
            views: {
                'tab-user': {
                    templateUrl: "templates/userClientDetail.html",
                    controller: "userClientOrderDetailController"
                }
            }
        })
        .state('app.userComplete', {
            url: "/userDetail",
            views: {
                'tab-user': {
                    templateUrl: "templates/userCompleteInfo.html",
                        controller: "userUpload"//"userController"
                    }
                }
            })
        .state('app.msgCenter', {
            url: "/msgcenter",
            views: {
                'tab-msg': {
                    templateUrl: "templates/msgCenter.html",
                    controller: "msgCenterController"
                }
            }
        })
        .state('app.msgDetail', {
            url: "/msgdetail/:id",
            views: {
                'tab-msg': {
                    templateUrl: "templates/msgDetail.html",
                    controller: "msgDetailController"
                }
            }
        })
        .state('app.msgPost', {
            url: "/post",
            views: {
                'tab-msg': {
                    templateUrl: "templates/msgPost.html",
                    controller: "msgPostController"
                }
            }
        })
        .state('yun', {
            url: "/yun",
            templateUrl: "templates/yun.html",
            controller: "yunController"            
        })
// if none of the above states are matched, use this as the fallback
$urlRouterProvider.otherwise('/app/mainhome');
        //$locationProvider.html5Mode(true);
    }]);

angular.module("en_ch",[])
.factory("schoolTable", ["$window", function($window){
    return {
        "gr12_enrollment": "12年级入学人数",
        "ave_exam_mark": "12年级毕业考平均分",
        "percentage_of_exams_failed": "12年级毕业考失败率",
        "school_vs_exam_mark_difference": "学校毕业课程成绩对比相关考试成绩",
        "english_gender_gap": "学校男女生英语10考试平均分差距",
        "math_gender_gap": "学校男女生数学10考试平均分差距",
        "graduation_rate": "高中毕业率",
        "delayed_advancement_rate": "推迟毕业率",
        
        "gr4_enrollment": "4年级入学人数",
        "gr4_avg_score_reading": "四年级学生FSA阅读平均分",
        "gr4_avg_score_writing": "四年级学生FSA写作平均分",
        "gr4_avg_score_numeracy": "四年级学生FSA算术平均分",
        "gr7_avg_score_reading": "七年级学生FSA阅读平均分",
        "gr7_avg_score_writing": "七年级学生FSA写作平均分",
        "gr7_avg_score_numeracy": "七年级学生FSA算术平均分",
        "gr7_gender_gap_reading": "七年级男女生阅读成绩差距",
        "gr7_gender_gap_numeracy": "七年级男女生算术成绩差距",
        "below_expectations_percentage": "省考失败率",
        "tests_not_written_percentage": "考试缺席率",
        
        "OSSLT_count": "OSSLT学生参与率",
        "ave_gr9_math_acad": "九年级学术数学平均分",
        "ave_gr9_math_apld": "九年级应用数学平均分",
        "OSSLT_passed_percentage_FTE": "OSSLT通过率-FTE",
        "OSSLT_passed_percentage_PE": "OSSLT通过率-PE",
        "tests_below_standard_percentage": "考试未通过率",
        "gender_gap_level_math": "男女生数学成绩差距",
        "gender_gap_OSSLT": "男女生OSSLT成绩差距",
        "gr9_tests_not_written_percentage": "九年级考试缺考率",
        
        "gr6_enrollment": "6年级入学人数",
        "gr3_ave_level_reading": "三年级学生EQAO阅读平均分",
        "gr3_ave_level_writing": "三年级学生EQAO写作平均分",
        "gr3_ave_level_math": "三年级学生EQAO数学平均分",
        "gr6_ave_level_reading": "六年级学生EQAO阅读平均分",
        "gr6_ave_level_writing": "六年级学生EQAO写作平均分",
        "gr6_ave_level_math": "六年级学生EQAO数学平均分",
        "gender_gap_level_reading": "男女生阅读成绩差距",
        
        "g12_enrollment": "12年级入学人数",
        "courses_taken_per_student": "在校第三年所修毕业课程平均数",
        "diploma_completion_rate": "高中毕业率",
        
        "gr3_ave_test_mark_lang_arts": "三年级语言艺术平均分",
        "gr3_ave_test_mark_math": "三年级数学平均分",
        "gr6_ave_test_mark_lang_arts": "六年级语言艺术平均分",
        "gr6_ave_test_mark_math": "六年级数学平均分",
        "gr6_ave_test_mark_science": "六年级科学平均分",
        "gr6_ave_test_mark_social_studies": "六年级社会学平均分",
        "gr6_gender_gap_lang_arts": "六年级男女生语言艺术成绩差距",
        "gr6_gender_gap_math": "六年级男女生数学成绩差距",
        "percentage_of_tests_failed": "学校学科挂科率",
    }
}]);
angular.module("canbestServer", [])
.factory('detectBrowser', ["$window", "$timeout", function($window, $timeout){
    return {
        isWechat: function(){
            //OPTION ONE:
            var result = (/MicroMessenger/i).test($window.navigator.userAgent);
            return result;
        }
    }
}])
.factory('dataStore', function(){
    var setData = {};
    return {
        set: function(data){
            setData = data;
        },
        get: function(){
            return setData;
        }
    }
})
.factory('localStorage', ["$window", "$ionicHistory", "$q", function ($window, $ionicHistory, $q) {
    return {
        setItem: function (key, value) {
            $window.localStorage.setItem(key, value);
        },
        getItem: function (key, defaultValue) {
            return $window.localStorage.getItem(key) || null;
        },
        setObject: function (key, obj) {
            $window.localStorage.setItem(key, JSON.stringify(obj));
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage.getItem(key) || 'null');
        },
        remove: function(key){
            return $window.localStorage.removeItem(key);  
        },
        clear: function () {
            $window.localStorage.clear();
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
        }
    }
}])
.factory('userAuth', ["localStorage", "$cookies", function(localStorage, $cookies){
    return {
        checkUcLogin: function(){
            if(localStorage.getObject('canbestUserInfo') !== null && localStorage.getObject("canbestUserInfo").uid){
                return {
                    isLogin: true,
                    uid: localStorage.getObject("canbestUserInfo").uid
                };
            }else{
                return {
                    isLogin: false,
                    uid: null
                };
            }
        },
        checkLogin: function(){
            //var haha = $cookies.getObject('user');
            //console.log(haha);
            //alert($cookies.getObject('user'));
            if(localStorage.getObject('canbestUserInfo') !== null && localStorage.getObject("canbestUserInfo").uid){
                return {
                    isLogin: true,
                    uid: localStorage.getObject("canbestUserInfo").uid
                };
            }else if($cookies.getObject('user') && $cookies.getObject('user').uid){
                return {
                    isLogin: true,
                    uid: $cookies.getObject('user').uid
                };
            }else{
                return {
                    isLogin: false,
                    uid: null
                };
            }   
        }
    }
}])
.factory('httpServices', ["$location", "$window", "$http", "$q", "urlConfig", "Upload", "userAuth", function ($location, $window, $http, $q, urlConfig, Upload, userAuth) {
    return {

        urlFour: urlConfig.url, 
        
        urlTwo: "http://www.canbest.org/",

        wechatLogin: function (code, reId) {

            var deferred = $q.defer();

            var url = this.urlFour + "usercenter/wx/" + code + "/" + reId;

            $http({
                url: url,
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });

            return deferred.promise;

        },
        
        extractUserInfo: function(id){
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "usercenter/user/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });

            return deferred.promise;
        },
        
        extractUserPoints: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/points/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        extractUserClient: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/clients/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;    
        },
        
        extractUserClientMemo: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/memo/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;    
        },
        
        extractUserOrder: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/orders/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise; 
        },
        
        extractUserReferer: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/referees/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;  
        },
        
        updateUserInfo: function(id, phone, fullname, slogan, wechat, uid, email){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/profile/" + id.toString(),
                method: "POST",
                data: {
                    introducer: uid, 
                    phone: phone, 
                    fullname: fullname, 
                    agentType: '2', 
                    slogan: slogan, 
                    wechat_Id: wechat == undefined ? "" : wechat,
                    email: email == undefined ? "" : email
                }
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;     
        },
        
        uploadUserFiles: function(file, type, userId){
            var deferred = $q.defer();
            Upload.upload({
                url: this.urlFour + "usercenter/user/upload/" + type + "/" + userId,
                file: file,
            }).then(
            function (res) {
               deferred.resolve(res);
           }, function (resp) {
            deferred.reject(resp);
        }, function(evt) {
            var progress = Math.round((evt.loaded/evt.total) * 100);
            deferred.notify(progress);
        });
            return deferred.promise
        },
        
        userOrder: function(uid, pid, phone, fullname, aid, rCode){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/neworder/" + uid.toString() + "/" + pid.toString(),
                method: "POST",
                data: {phone: phone, fullname: fullname, agentId: aid, ref_code: rCode}
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise; 
        },
        
        getUserMission: function(){
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: this.urlFour + "articles/placeholders/detail/34020",
            }).then(
            function(res){
                deferred.resolve(res.data[0]);   
            }
            ); 
            return deferred.promise;
        },
		
		getHotItems: function(){
			var deferred = $q.defer();
            $http({
                method: "GET",
                url: this.urlFour + "articles/placeholders/detail/34680",
            }).then(
            function(res){
                deferred.resolve(res.data[0]);   
            }
            ); 
            return deferred.promise;
		},
        
        getMissionLists: function(){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "statistics/get/dailyTasks",
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getRefCodeById: function(id){
           var deferred = $q.defer();
           $http({
            url: this.urlFour + "usercenter/user/getrefercode/" + id.toString(),
            method: "GET"
        }).then(
        function (res) {
            deferred.resolve(res.data);
        });
        return deferred.promise;  
    },

    getIdByRefCode: function(code){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "usercenter/user/getByReferCode/" + code.toString(),
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res.data);
        });
        return deferred.promise;
    },

    apply: function (obj) {
        var deferred = $q.defer();

        $http({
            url: this.urlTwo + "index.php?s=/home/index/post.html",
            method: "POST",
            data: obj,
        }).then(
        function (res) {
            deferred.resolve(res.data);
        }
        );
        return deferred.promise;
    },

    getPostDetails: function(id){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "articles/detail/"+ id.toString(),
                    //url: this.urlOne + "index.php?s=News/index/apiGetNewsDetail/id/" + id.toString() + ".html",
                    method: "GET",
                }).then(
                function(res){
                    deferred.resolve(res.data);
                });
                return deferred.promise;
            },

            getPostDetailsComment: function(id){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "comments/1/" + id.toString(),
                    method: "GET",
                }).then(
                function(res){
                    deferred.resolve(res.data);
                });
                return deferred.promise;
            },

            addView: function(id){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "articles/view/" + id,
                    method: "GET",
                }).then(
                function(res){
                    deferred.resolve(res);
                });
                return deferred.promise;    
            },

            postComment: function(obj){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "comments",
                    method: "POST",
                    data: obj
                }).then(
                function(res){
                    deferred.resolve(res);
                });
                return deferred.promise;
            },

            postDelete: function(body){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "square/delete",
                    method: "POST",
                    data: body
                }).then(
                function(body){
                    deferred.resolve(body);
                });
                return deferred.promise;
            },


            getLiunarProductList: function(typeId){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "products/list/" + typeId.toString(),
                    method: "GET"
                }).then(
                function(res){
                    deferred.resolve(res.data);
                }
                );
                return deferred.promise;
            },

            getProductPromo: function(){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "articles/placeholders/1",
                    method: "GET"
                }).then(
                function(res){
                    deferred.resolve(res.data);
                }
                );
                return deferred.promise;
            },

            getProductList: function(){
             var deferred = $q.defer();
             $http({
                url: this.urlFour + "products/list/promo/all",
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getProductListNew: function(){
         var deferred = $q.defer();
         $http({
            url: this.urlFour + "productnew/list/promo/all",
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res.data);
        }
        );
        return deferred.promise;
    },

    getAllProducts: function(limit){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "products/list/all/" + limit.toString(),
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getAllProductsNew: function(limit){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "productnew/list/all/" + limit.toString(),
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getCountryProducts: function(country){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "products/list/country/" + country,
            method: "GET",
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getCountryProductsNew: function(country){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "productnew/list/country/" + country,
            method: "GET",
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getShopProductDetail: function(productId){
        var deferred = $q.defer();
        $http({
                //url: this.urlOne + "index.php?s=/issue/index/apiGetRankingList&issue_id=" + schoolCateId.toString(),
                url: this.urlFour + "products/" + productId.toString(),
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        getProductDetailNew: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "productnew/detail/" + id.toString(),
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        getProductAccessAuthorization: function(uid,refcode){
            var deferred = $q.defer();
            var url;
            if(refcode){
                url = this.urlFour + "productnew/authorizeClient/" + uid + "/" + refcode;
            }else{
                url = this.urlFour + "productnew/authorizeClient/" + uid;
            }
            console.log(url);
            $http({
                url: url,
                method:"GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            },function(err){
                deferred.reject(err);
            }
            );
            return deferred.promise;
        },
        getAuthorizedProductData: function(prodid, uid,refcode){
            var deferred = $q.defer();
            var url;
            if(refcode){
                url = this.urlFour + "productnew/get/memberOnlyDetails/" + prodid +'/'+ uid + "/" + refcode;
            }else{
                url = this.urlFour + "productnew/get/memberOnlyDetails/" + prodid + '/' + uid;
            }
            console.log(url);
            $http({
                url: url,
                method:"GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            },function(err){
                deferred.reject(err);
            }
            );
            return deferred.promise;
        },
        
        getHomeEvents: function(){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "articles/events",
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);    
            }
            );
            return deferred.promise;
        },
        
        getEvents: function(count, index){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "articles/events/all/" + count + "/" + index,
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);    
            }
            );
            return deferred.promise;
        },

        getHomePosts: function () {
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/promo",
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        getHomePlaceholder: function(){
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/placeholders/0",
                method: "GET",
            }).then(
            function (res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
            
        },

        getGongLueList: function (count, index) {
            var that = this;
            var deferred = $q.defer();

            $http ({
                url: this.urlFour + "articles/list/" + count + "/" + index + "/",
                method: "GET",
                timeout: "2000"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            },
            function(err, status){
                that.getGongLueList(count,index).then(
                    function(res){
                        deferred.resolve(res);
                    }
                    )
            }
            );
            return deferred.promise;
        },

        getLiunarProductList: function (typeId) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "products/list/" + typeId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        getLiunarPastCase: function (typeId) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "pastcases/list/" + typeId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getCampProducts: function (schoolCateId) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/issue/index/apiGetSchools&issue_id=" + schoolCateId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getCampProductDetail: function (id) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/issue/index/apiGetProductDetail&id=" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getCities:function(){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "school/cities",
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;  
        },
        
        getSchoolRankList: function (schoolCateId, public, city, limit) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "school/" + schoolCateId.toString() + "/1000/0",
                method: "GET",
                cache: true
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getSchoolListByProvince: function(cateId, num, page, city, public, province){
            var deferred = $q.defer();
            //isPublic=1&province=bc&city=burnaby
            $http({
                url: this.urlFour + "school/" + cateId.toString() + "/" + num + "/" + page + "?city=" + city + "&isPublic=" + public + "&province=" + province,
                method: "GET",
                //cache: true,
            }).then(
            function (res) {
                console.log(res);
                deferred.resolve(res.data);
            },
            function(err){
                deferred.reject(err);
            }
            );
            return deferred.promise;
        },

        getSchoolDetail: function (id) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "schools/" + id.toString(),
                //url: this.urlFour + "school/school_standard/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data[0]);
            }
            );
            return deferred.promise;
        },
        
        getSchoolProvinceDetail: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "school/school_standard/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data[0]);
            }
            );
            return deferred.promise;
        },
        
        getSchoolComment: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "comments/2/" + id.toString(),
                //url: this.urlOne + "index.php?s=/news/index/apiGetComments&app=news&id=" + id.toString(),
                method: "GET",
            }).then(
            function(res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        getQuestionsByType: function (typeId, limit){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "questions/promo/" + typeId + "/" +limit,
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;    
        },
        
        getQuestions: function (column, data, order, limit) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "questions/list/" + order + "/" +limit,
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        postQuestion: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "questions",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },

        getQuestionDetail: function (id) {
            var deferred = $q.defer()
            $http({
                url: this.urlFour + "questions/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getQuestionAnswer: function(id){
            var deferred = $q.defer()
            $http({
                url: this.urlFour + "questions/answers/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;    
        },

        postAnswer: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/question/index/apiPostAnswer",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },

        postSurveyResult: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "survey/submit/",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },
        
        jsSDK: function(){
            var deferred = $q.defer();
            //console.log($location);
            $http({
                url: this.urlFour + "wechatSign",
                method: "post",
                data:{
                    url: $location.$$absUrl
                }
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getMsgCenterList: function(type, index, id){
           var deferred = $q.defer();
           var url;
           var that = this;
           if(id){
            url = this.urlFour + "square/get/" + type + "/" + id + "/4/" + index;
        }else{
            url = this.urlFour + "square/get/" + type + "/4/" + index ;
        }
        $http({
            url: url,
            method: "get",
            timeout: "10000"
        }).then(
        function (res) {
         deferred.resolve(res.data);
     }, 
     function(err, status){
         console.log(err);
         that.getMsgCenterList(type, index, id).then(
          function(res){
           deferred.resolve(res);
       }
       )
     }
     );
        return deferred.promise;
    },
    getMsgDetail: function(id){
       var deferred = $q.defer();
            //console.log($location);
            $http({
                url: this.urlFour + "square/get/" + id,
                method: "get",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;	
        },

        postMsg: function(obj){
           var deferred = $q.defer();
           $http({
            url: this.urlFour + "square/post",
            method: "post",
            data: obj
        }).then(
        function(res){
            deferred.resolve(res.data);
        },
        function(err){
            console.log(err);
            deferred.reject(err.data);
        }
     );
        return deferred.promise;
    },

		uploadMsgQR: function(file, userId){
			var deferred = $q.defer();
			Upload.upload({
				url: this.urlFour + "square/post/qr/"+userId,
				data: {file: file, 'uid': userId}
			}).then(
			function (res) {
			   deferred.resolve(res);
			}, function (resp) {
				deferred.reject(resp);
			});
			return deferred.promise;
		},
		
		createMsgPoster: function(obj){
			var deferred = $q.defer();
			$http({
				url: this.urlFour + "createpng/create",
				method: "POST",
				data: obj
			}).then(
				function(res){
					deferred.resolve(res.data);
				},
				function(err){
					deferred.reject(err);
				}
			);
			return deferred.promise;
		},
		
		requestQRCode: function(url){
			var deferred = $q.defer();
			$http({
				url: "http://188.166.210.103:1234/qr/generate",
				method: "POST",
				data: {url: url}
			}).then(
				function(res){
					deferred.resolve(res.data);
				},
				function(err){
					deferred.reject(err);
				}
			);
			return deferred.promise;
		}
	}
}])
.factory("seedUrl", ["$rootScope", "$location", function($rootScope,$location){
    return {
        createUrl: function(type, id, time, taskId){
            if (time){
                return "img/pv.jpg?referid=" + $rootScope.referId + "&" + type + "id=" + id + "&sharetasktime=" + time + "&taskid=" + taskId;     
            }else{
                return "img/pv.jpg?referid=" + $rootScope.referId + "&" + type + "id=" + id;
            }
        }    
    }
}])
.factory("wechatShare", ["$window", "$location", "$rootScope", function($window, $location, $rootScope){
    var wx = window.wx;
    console.log(wx);
    if(wx){
        return function(title, img, desc, url){
            //console.log(wx, $location.$$absUrl, title, img, desc);
            var link;

            if(url){
                link = url + $rootScope.shareState;
            }else{
                link = $location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState;
            }

            wx.ready(function(){
                wx.onMenuShareTimeline({
                    title: title, // 分享标题
                    link: link,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });
                wx.onMenuShareAppMessage({
                    title: title, // 分享标题
                    link: link,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    desc: desc,//分享预览文字
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });       
            });
        }
    }else{
        return function(){
            console.log("wx is not init");
        };
    }
}])
.factory("missionShare", ["$window", "$location", "$rootScope", function($window, $location, $rootScope){
    var wx = window.wx;
    console.log(wx);
    if(wx){
        return function(title, img, desc, url){
            //console.log(wx, $location.$$absUrl, title, img, desc);
            var link;

            var now = Math.floor((new Date().getTime())/1000);

            if(url){
                link = url + $rootScope.shareState;
            }else{
                link = $location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState;
            }

            wx.ready(function(){
                wx.onMenuShareTimeline({
                    title: title, // 分享标题
                    link: link + "&sharetime=" + now,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });
                wx.onMenuShareAppMessage({
                    title: title, // 分享标题
                    link: link + "&sharetime=" + now,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    desc: desc,//分享预览文字
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });       
            });
        }
    }else{
        return "wx is not init!";
    }
}])
.factory("introduceNewUser", ["$window", function($window){
    var wx = window.wx;
    if(wx){
        return function(recNum, user){
            wx.ready(function(){
                wx.onMenuShareAppMessage({
                    title: "最聪明的教育创业计划，零成本打造您的在线留学工作室", // 分享标题
                    link: "http://www.liunar.com/mobile/#/userShare" + recNum + "&vip=1", // 分享链接
                    desc:  user + "邀你成为留学教育合伙达人，零投入，无需场地，可兼职，只需转发分享，即可赚取额外收入！",//分享预览文字
                    imgUrl: "http://www.liunar.com/img/icon.png?v=3", // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });

                wx.onMenuShareTimeline({
                    title: "最聪明的教育创业计划，零成本打造您的在线留学工作室", // 分享标题
                    link: "http://www.liunar.com/mobile/#/userShare" + recNum + "&vip=1", // 分享链接
                    //desc:  user + "邀你成为留学教育合伙达人，零投入，无需场地，可兼职，只需转发分享，即可赚取额外收入！",//分享预览文字
                    imgUrl: "http://www.liunar.com/img/icon.png?v=3", // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });
            });    
        }
    }else{
        return "wx is not init";
    }
}])
.factory("pressureTest", ["$http", "urlConfig", function ($http, urlConfig) {
    return {
        post: function () {
            $http({
                url: urlConfig.url + "articles/user",
                method: "POST",
            }).then(
            function (res) {
                //console.log(res);
            }, function (res) {
                //console.log(res);
            }
            );
        },

        put: function () {
            $http({
                url: urlConfig.url + "articles/user",
                method: "PUT",
            }).then(
            function (res) {
                //console.log(res);
            }, function (res) {
                //console.log(res);
            }
            );
        },

        get: function () {
            $http({
                url: urlConfig.url + "articles/fulllist",
                method: "GET",
            }).then(
            function (res) {
                //console.log(res);
            }, function (res) {
                //console.log(res);
            }
            );
        }
    }
}])
.factory("$exceptionHandler", function(){
    return function(exception, cause){
        console.error(exception,cause);
    }
});

angular.module('agencyController',[])
.controller('agencyController', ["$rootScope", "$scope", "$ionicPopup", "localStorage", "httpServices", function($rootScope, $scope, $ionicPopup, localStorage, httpServices){
    
    if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){ 
        $scope.showCustomedCard = false;
    }else{
        httpServices.extractUserInfo(localStorage.getObject("canbestUserInfo").uid).then(
            function(res){
                if(res.agentType === 1){
                    //console.log(res);
                    $scope.agentInfo = res;
                    $scope.QRPopUp = function(){
                        $ionicPopup.alert({
                            title: "长按二维码识别",
                            template:"<div style='padding: 24px;'><img ng-src='http://api.liunar.com/uploads/" + res.wechat_qrcode + "' width='100%' /></div>"
                        });
                    };
                    $scope.showCustomedCard = true;
                }else{
                    $scope.showCustomedCard = false;
                }
            }
        );
    }
}])
.controller('agencyBottomController', ["$rootScope", "$scope", "$ionicPopup", "localStorage", "httpServices", function($rootScope, $scope, $ionicPopup, localStorage, httpServices){
        
    if($rootScope.referId !== 'null'){
        httpServices.extractUserInfo($rootScope.referId).then(
            function(res){
                //console.log(res);
                if(res.agentType === 1){
                    $scope.agentShowInfo = res;
                    $scope.showAgentCard = true;
                }else{
                    $scope.showAgentCard = false;
                }
                
            }
        );    
    }else{
        $scope.showAgentCard = false;
    }
}]);
angular.module('applyController',[])
.controller('applyController', ["$scope", "$state", "$stateParams", "$ionicHistory", "$ionicPopup", "httpServices", function($scope, $state, $stateParams, $ionicHistory, $ionicPopup, httpServices){
    
    console.log($stateParams);
    
    $scope.applyObj = {
        name: "",
        number: "",
        description: "from mobile"
    };
    
    var productNames = {
        1: "加拿大知名大学申请定制服务",
        2: "加拿大特色院校申请定制服务",
        3: "加拿大中学申请定制服务",
        4: "加拿大小学申请定制服务"
    };
    
    $scope.productName = productNames[$stateParams.schoolType];
    
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.from = $ionicHistory.viewHistory().backView;//.backView.url;
        console.log($scope.from, $stateParams);
            $scope.apply = function(){
                if($scope.applyObj.name === "" || $scope.applyObj.name.length === 0){
                    $ionicPopup.alert({
                        title: '留哪儿',
                        template: '名字不能为空'
                    }).then(
                    );
                }else if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test($scope.applyObj.number) !== true){
                    $ionicPopup.alert({
                        title: '留哪儿',
                        template: '请输入有效电话号码'
                    }).then(
                        function(){
                            $scope.applyObj.number = null;
                        }
                    );
                }else{
                    //$scope.applyObj.referrer = $stateParams.pageTitle + "/" + $stateParams.productId + $scope.from.url;
                    $scope.applyObj.referrer = $scope.productName;
                    //console.log($scope.applyObj.referrer);
                    httpServices.apply($scope.applyObj).then(
                        function(res){
                            if(res !== 0 ){
                               $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '感谢您的申请，我们将会在24小时内联系您！'
                                }).then(
                                   function(){
                                       $state.go('app.mainHome');
                                       $scope.applyObj = null;
                                   }
                                ); 
                            }
                        }
                    );
                }
            }
    });
}]);

angular.module('articleListController',[])
.controller('liunarTouTiaoListController', ["$scope", "httpServices", "$ionicLoading", function($scope, httpServices, $ionicLoading){
    
    
    //$scope.articles = initList;
    
    var num = 0;
    
    $scope.$on("$ionicView.beforeEnter", function(){
        console.log(num);
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
            noBackDrop: true
        });
    });
    
    $scope.initList = function(){
        httpServices.getGongLueList(10, num).then(
            function(res){
                console.log(res);
                $scope.articles = res;
                $ionicLoading.hide();
            }
        );
    };
    
    $scope.$on("$ionicView.afterEnter", function(){
        $ionicLoading.hide();
    });
        
    $scope.getMoreArticles = function(){
        num ++;
        console.log(num);
        /*setTimeout(
            function(){
                num --;
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 7000
        );*/
        httpServices.getGongLueList(10, num).then(
            function(res){
                console.log(res);
                $scope.articles = [].concat($scope.articles, res);
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }
        );
    };
    
    $scope.tags = {0:"其它", 1:"海外规划", 2:"留学申请", 3:"校园内外", 4:"本地生活", 5:"实习就业", 6:"职业移民"};
    
}])
.controller("tagResultsController", ["$ionicHistory", "$scope", "$stateParams", "$http", "urlConfig", function ($ionicHistory, $scope, $stateParams, $http, urlConfig) {
    //console.log($scope, $stateParams, $http);
    //console.log(JSON.parse($stateParams.tag));
    if($ionicHistory.viewHistory().backView !== null){
        $scope.from = $ionicHistory.viewHistory().backView.url;
    }else{
        $scope.from = "/tagsquare";
    }

    $scope.$on("$ionicView.beforeEnter", function(evt, view){
        console.log(evt, view)
        $http({
            url: urlConfig.url + "tags/content/" + $stateParams.tag,
            method: "GET",
        }).then(
            function(res){
                console.log(res);
                //var res2 = unescape(res.data);
                var res2 = res.data.replace(/\'/g, '"').replace(/\ u/g, "").replace(/None/g, '"None"');
                console.log(res.data, res2[62], res2[63]);
                $scope.lists = JSON.parse(res2);
                console.log($scope.lists);
                $scope.pageTag = JSON.parse(res2)[0].tagname;
                view.title = $scope.pageTag;
            }
        );
    });

}]);

angular.module('detailController',[])
.controller('detailController', ["$ionicPopup", "$scope", "$http", "httpServices", "$stateParams", "$sce", "$state", "$ionicHistory", "postDetail", "seedUrl", "wechatShare", "userAuth", "$location", "$rootScope", "missionShare", function($ionicPopup, $scope, $http, httpServices, $stateParams, $sce, $state, $ionicHistory, postDetail, seedUrl, wechatShare, userAuth, $location, $rootScope, missionShare){
    
    
    $scope.$on("$ionicView.beforeEnter", function(){
        
        var thumbnail;
        
        if(postDetail.have_thumbnail === 1){
            thumbnail = "http://www.liunar.com" + postDetail.thumbnail;     
        }else if(postDetail.have_thumbnail === 2){
            thumbnail = "http://cms.liunar.com/uploadimage/articles/" + postDetail.promo_thumb;
        }        
        
        console.log(postDetail);
        
        if($ionicHistory.backView() && $ionicHistory.backView().stateName == "app.mission"){
            missionShare(postDetail.title, thumbnail, postDetail.description, "http://www.liunar.com/web/article/" + postDetail.id + "/" + $stateParams.taskId);
        }else{
            wechatShare(postDetail.title, thumbnail, postDetail.description, "http://www.liunar.com/web/article/" + postDetail.id);
        }
        
        var shareTime = $location.absUrl().indexOf('&sharetime=');
    
        if(shareTime !== -1){
            shareTime += "&sharetime=".length;
            var stop = $location.absUrl().indexOf('&', shareTime);
            if(stop === -1){
                var urlTime = $location.absUrl().substring(shareTime);
            }else{
                var urlTime = $location.absUrl().substring(shareTime, stop);
            }
            $scope.shareUrl = seedUrl.createUrl("article", postDetail.id, urlTime, $stateParams.taskId);
        }else{
            $scope.shareUrl = seedUrl.createUrl("article", postDetail.id);    
        }
        
        if($ionicHistory.viewHistory().backView !== null){
            $scope.from = $ionicHistory.viewHistory().backView.url;    
        }else{
            $scope.from = "/app/mainHome";
            $scope.fromShare = true;
            if(postDetail.id == 34677){
                $scope.fromShare = false;    
            }
        }
        
        httpServices.addView(postDetail.id);
        
    });
    

    
        
    $scope.postDetail = postDetail;
    if(postDetail.hasTopicTag == 1){
        $scope.topicTag = JSON.parse(postDetail.topicTags)[0].tag;
        $scope.topicTagId = JSON.parse(postDetail.topicTags)[0].tagId;
    }
    //$scope.description = $sce.trustAsHtml(postDetail.description);
    $scope.postDetailContent = $sce.trustAsHtml(postDetail.content_html);

    $scope.getRelative = function(){
        if(postDetail.tags !== null && postDetail.tags != ""){
            var tags = postDetail.tags.split(",").slice(0, 2).join(" ");
            console.log(tags);
            $http({
                url: "/search/search?kw=" + tags + "&start=0&limit=3&type=1",
                method:'GET'
            }).then(
                function(res){
                    if(res.data.length > 0){
                        $scope.showRelative = true;
                        $scope.lists = res.data;
                        console.log(res);
                    }else{
                        $scope.showRelative = false;
                    }
                }
            );
        }else{
            $scope.showRelative = false;
        }
    };
    
    $scope.getRelative();
       
    $scope.getPostComments = function(){
       httpServices.getPostDetailsComment($stateParams.postId).then(
            function(res){
                if(res[0]==="error"){
                }else{
                    $scope.comments = res;
                }
            }
        );
    };
    
    $scope.commentObj = {
        row_id : $stateParams.postId,
        content: "",
        module: 1   
    };
    
    var loginStatus = new userAuth.checkLogin();
    
    $scope.postNewsComment = function(){
        if(loginStatus.isLogin == false){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $state.go("userLogin", {lastview: $location.$$path});   
                }
            );
        }else{
            $scope.commentObj.uid = loginStatus.uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                $scope.disable = true;
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $scope.disble = false;
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.commentObj.content = null;
                                $scope.getPostComments();
                            }
                        )
                    }
                );
            }
        }
    };
}])
.controller("missionDetailController", ["wechatShare", "httpServices", "$scope", "$sce", "$rootScope", "dataStore", function(wechatShare, httpServices, $scope, $sce, $rootScope, dataStore){
    $scope.from = "/app/mainHome";
    $scope.$on("$ionicView.beforeEnter", function(){
        $rootScope.showCallBtn = false;
        $rootScope.hideTabs = 'tabs-item-hide';
        console.log($rootScope.hideTabs);
        
        httpServices.getUserMission().then(
            function(res){
                $scope.missionDetail = res;
                httpServices.getMissionLists().then(
                    function(res2){
                        if(!res2.err && res2 !== "No tasks today!"){
                            $scope.articles = [];
                            $scope.products = [];
                            $scope.events = [];
                            
                            res2.forEach(function(v,i){
                                if(v.type == "article"){
                                    $scope.articles.push(v);
                                }else if(v.type == "product"){
                                    $scope.products.push(v);
                                }else if(v.type == "event"){
                                    $scope.events.push(v);
                                }
                            });

                            var articleHtml = "";

                            var eventHtml = "";

                            var productHtml = "";

                            $scope.articles.forEach(
                                function(v,i){                                   
                                    articleHtml += "<a style='color: rgb(0, 112, 192); text-decoration: underline' href='#/app/mainhome/postdetail/" + v.id + "/"+ v.dailyTaskId +"'>" + (i+1) + "、 " + v.title + "</a><br><br>";
                                }
                            );

                            $scope.products.forEach(
                                function(v,i){
                                    productHtml += "<a style='color: rgb(0, 112, 192); text-decoration: underline' href='#/app/shopProductDetail/" + v.id + "/"+ v.dailyTaskId +"'>" + (i+1) + "、 " + v.title + "</a><br><br>";
                                }
                            );

                            $scope.events.forEach(
                                function(v,i){
                                    eventHtml += "<a style='color: rgb(0, 112, 192); text-decoration: underline' href='#/app/mainhome/postdetail/" + v.id + "/"+ v.dailyTaskId +"'>" + (i+1) + "、 " + v.title + "</a><br><br>";
                                }
                            );

                            var newHtml = res.content_html.replace(/{{articles}}/, articleHtml).replace(/{{products}}/, productHtml).replace(/{{events}}/, eventHtml);

                            $scope.missionDetailContent = $sce.trustAsHtml(newHtml);
                            
                        }else {

                            var emptyHtml = "<div style='text-align: center; margin-top: 45%; color: steelblue'><p style='font-size: 18px'>今天的任务还没有发布，<br>请耐心等待...</p></div>";
                            $scope.missionDetailContent = $sce.trustAsHtml(emptyHtml);

                        }
                    }
                )
            }
        );    
    });
}])
.controller("hotItemsController", ["wechatShare", "httpServices", "$scope", "$sce", "$rootScope", "dataStore", function(wechatShare, httpServices, $scope, $sce, $rootScope, dataStore){
    $scope.from = "/app/mainHome";
    $scope.$on("$ionicView.beforeEnter", function(){
        $rootScope.showCallBtn = false;
        $rootScope.hideTabs = 'tabs-item-hide';
		httpServices.getHotItems().then(
			function(res){
				$scope.hotItem = res;
				if(res.content_html){
					$scope.hotItemContent = $sce.trustAsHtml(res.content_html);
				}
			}
		)
	});
}])

angular.module('estimateController',[])
.controller('estimateController',["$ionicPopup", "$scope", "$state", "httpServices", "$rootScope", function($ionicPopup, $scope, $state, httpServices, $rootScope){
    
    $scope.answer = [];
    
    $scope.select = function(key, id, value){
        //console.log(key);
        $scope.selected = key;
        $scope.value = value;
        
    };
    
    $scope.selectValue = {
        value: 0,
    }
    
    $scope.selected = 'A';
    $scope.value = 0;
    
    var singleQuestions = [
        {
            id: 1,
            question: "我想要申请",
            answers: {
                A: {text: "中小学", value: 0}, 
                B: {text: "高中", value: 1}, 
                C: {text: "名校本科", value: 2}, 
                D: {text: "专业学院", value: 3},
                //E: {text: "海外移民置业", value: 4}
            }
        },
        {
            id: 2,
            question: "我喜欢的学校类型",
            answers: {
                A: {text: "公校", value: 0}, 
                B: {text: "私校", value: 1}
            }
        },
        {
            id: 3,
            question: "我的英语水平",
            answers: {
                A: {text: "有考试成绩", value: 0}, 
                B: {text: "无考试成绩", value: 1}
            }
        },
        {
            id: 4,
            question: "SSAT",
            answers: {
                A: {text: "2100－2400", value: 0}, 
                B: {text: "1800－2100", value: 1}, 
                C: {text: "1320－1800", value: 2},
                D: {text: "无", value: -1}
            }
        },
        {
            id: 5,
            question: "IELTS",
            answers: {
                A: {text: "6－7以上", value: 0}, 
                B: {text: "5－5.5", value: 1}, 
                C: {text: "4－4.5", value: 2},
                D: {text: "无", value: -1}
            }
        },
        {
            id: 6,
            question: "TOFEL",
            answers: {
                A: {text: "100以上", value: 0},
                B: {text: "80－99", value: 1},
                C: {text: "80以下", value: 2},
                D: {text: "无", value: -1}
            }
        },
        {
            id: 7,
            question: "无考试成绩",
            answers: {
                A: {text: "优良", value: 0},
                B: {text: "一般", value: 1},
                C: {text: "有限", value: 2},
                D: {text: "不知道", value: 3}
            }
        },
        {
            id: 8,
            question: "我的学校成绩",
            answers: {
                A: {text: "90以上", value: 0},
                B: {text: "80－89", value: 1},
                C: {text: "70－79", value: 2},
                D: {text: "70以下", value: 3}
            }
        },
        {
            id: 9,
            question: "我的留学预算",
            answers: {
                A: {text: "30万以上/年", value: 0},
                B: {text: "21－30万/年", value: 1},
                C: {text: "10－20万/年", value: 2}
            }
        },
        {
            id: 101,
            question: "IELT",
            answers: {
                A: {text: "6－7以上", value: 0}, 
                B: {text: "5－5.5", value: 1}, 
                C: {text: "4－4.5", value: 2},
                D: {text: "4以下", value: 3},
                E: {text: "无", value: -1}
            }        
        },
        {
            id: 102,
            question: "TOFEL",
            answers: {
                A: {text: "100以上", value: 0},
                B: {text: "80－99", value: 1},
                C: {text: "80以下", value: 2},
                D: {text: "无", value: -1}
            }       
        },
        {
            id: 103,
            question: "我的学校成绩",
            answers: {
                A: {text: "90以上", value: 0},
                B: {text: "80－89", value: 1},
                C: {text: "70－79", value: 2},
                D: {text: "70以下", value: 3}
            }
        },
        {
            id: 104,
            question: "我非常关注目标学校的",
            answers: {
                A: {text: "学校知名度", value: 0},
                B: {text: "学费和生活费", value: 1},
                C: {text: "毕业的难度", value: 2},
                D: {text: "专业有助于移民", value: 3}
            }
        },
        {
            id: 105,
            question: "我想学的专业",
            answers: {
                A: {text: "文科", value: 0},
                B: {text: "工科", value: 1},
                C: {text: "理科", value: 2},
                D: {text: "商科", value: 3}
            }
        },
        {
            id: 201,
            question: "我想要申请的移民类型",
            answers: {
                A: {text: "投资移民", value: 0}, 
                B: {text: "技术移民", value: 1}
            }
        },
        {
            id: 202,
            question: "我的英语水平",
            answers: {
                A: {text: "有考试成绩", value: 0}, 
                B: {text: "无考试成绩", value: 1}
            }
        },
        {
            id: 203,
            question: "IELT",
            answers: {
                A: {text: "6－7以上", value: 0}, 
                B: {text: "5－5.5", value: 1}, 
                C: {text: "4－4.5", value: 2},
                D: {text: "4以下", value: 3}
            }        
        },
        {
            id: 204,
            question: "CELPIP",
            answers: {
                A: {text: "8以上", value: 0}, 
                B: {text: "6－7", value: 1}, 
                C: {text: "5以下", value: 2}
            }        
        },
        {
            id: 205,
            question: "暂无考试成绩",
            answers: {
                A: {text: "熟练", value: 0}, 
                B: {text: "一般", value: 1}, 
                C: {text: "有限", value: 2}
            }        
        },
        {
            id: 206,
            question: "我的资产（包括存款，夫妻共同财产，投资，股票以及不动产等）",
            answers: {
                A: {text: "500万以上", value: 0}, 
                B: {text: "200万－500万", value: 1}, 
                C: {text: "50－200万", value: 2},
                D: {text: "50万以下", value: 3}      
            }        
        },
        {
            id: 207,
            question: "我能接受的投资金额",
            answers: {
                A: {text: "150万以上", value: 0}, 
                B: {text: "80万－150万", value: 1}, 
                C: {text: "30万－80万", value: 2},
                D: {text: "15万－30万", value: 3}    
            }        
        },
        {
            id: 208,
            question: "我期望的海外居住时间",
            answers: {
                A: {text: "时间灵活，可接受任何居住时间的要求", value: 0}, 
                B: {text: "时间有限，能达到一定的居住要求", value: 1}, 
                C: {text: "基本不方便海外居住", value: 2} 
            }        
        }   
    ];
    
    var qNum = 0;
    
    $scope.question = singleQuestions[0];
    
    $scope.nextButton = false;
    
    $scope.next = function(id, value){
        //console.log(id,value, $scope.answer, $scope.answer[0]);
        //$scope.selected = null;
        $scope.answer.push([id, value]);
        if(id == 1){
            
            $scope.nextButton = true;
            
            if($scope.answer[0][1] === 0 || $scope.answer[0][1] === 1){
                
                $scope.question = singleQuestions[1];
                
            }else if($scope.answer[0][1] === 2 || $scope.answer[0][1] === 3){
                
                $scope.question = singleQuestions[9];
                
            }else if($scope.answer[0][1] === 4){
                
                $scope.question = singleQuestions[14];
                
            }
        }else if(id == 2){
        
            $scope.question = singleQuestions[2];
            
        }else if(id == 3){
            if($scope.answer[2][1] === 0){
                $scope.question = singleQuestions[3]
            }else if($scope.answer[2][1] === 1){
                $scope.question = singleQuestions[6];
            }
        }else if(id == 4){
            
            $scope.question = singleQuestions[4];
            
        }else if(id == 5){

            $scope.question = singleQuestions[5];
            
        }else if(id == 6){
            
            $scope.question = singleQuestions[7];
            
        }else if(id == 7){
            
            $scope.question = singleQuestions[7];
            
        }else if(id == 8){
            
            $scope.question = singleQuestions[8];
            
        }else if(id == 9){
            
            $scope.answerObj = {"uid": 1, "answers": $scope.answer};
            httpServices.postSurveyResult($scope.answerObj);
            $state.go("app.estimateResult");
            $rootScope.answer = $scope.answer;
            
        }else if(id == 101){
            
            $scope.question = singleQuestions[10];
            
        }else if(id == 102){
            
            $scope.question = singleQuestions[11];
            
        }else if(id == 103){
           
            $scope.question = singleQuestions[12];
            
        }else if(id == 104){
            
            $scope.question = singleQuestions[13];
            
        }else if(id == 105){
            
            $scope.question = singleQuestions[8];
            
        }else if(id == 201){
           
            $scope.question = singleQuestions[15];
            
        }else if(id == 202){
            
            if($scope.answer[2][1] === 0){
                $scope.question = singleQuestions[16]
            }else{
                $scope.question = singleQuestions[18];
            }
        }else if(id == 203){
            
            $scope.question = singleQuestions[17];
            
        }else if(id == 204){
            
            $scope.question = singleQuestions[19];
            
        }else if(id == 205){
           
            $scope.question = singleQuestions[19];
            
        }else if(id == 206){
            
            $scope.question = singleQuestions[20];
            
        }else if(id == 207){
            
            $scope.question = singleQuestions[21];
            
        }else if(id == 208){
            
            $scope.answerObj = {"uid": 1, "answers": $scope.answer}
            httpServices.postSurveyResult($scope.answerObj);
            $state.go("app.estimateResult");
            
        }
        
    };
    
    $scope.back = function(id){
        //console.log(id);
        //console.log($scope.answer);
        $scope.selected = null;
        $scope.answer.pop();
        if(id == 1){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 2){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 3){
            
            $scope.question = singleQuestions[1];
            
        }else if(id == 4){
            
            $scope.question = singleQuestions[2];
            
        }else if(id == 5){

            $scope.question = singleQuestions[3];
            
        }else if(id == 6){
            
            $scope.question = singleQuestions[4];
            
        }else if(id == 7){
            
            $scope.question = singleQuestions[2];
            
        }else if(id == 8){
            
            if($scope.answer[2][1] === 0){
            $scope.question = singleQuestions[5];
            }else{
            $scope.question = singleQuestions[6];   
            }
            
        }else if(id == 9){
            
            if($scope.answer[0][1] === 0 || $scope.answer[0][1] === 1){
            $scope.question = singleQuestions[7];
            }else if($scope.answer[0][1] === 2 || $scope.answer[0][1] === 3){
            $scope.question = singleQuestions[13];   
            }//need get value!
            
        }else if(id == 101){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 102){
            
            $scope.question = singleQuestions[9];
            
        }else if(id == 103){
           
            $scope.question = singleQuestions[10];
            
        }else if(id == 104){
            
            $scope.question = singleQuestions[11];
            
        }else if(id == 105){
            
            $scope.question = singleQuestions[12];
            
        }else if(id == 201){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 202){
            
            $scope.question = singleQuestions[14];
            
        }else if(id == 203){
            
            $scope.question = singleQuestions[15];
            
        }else if(id == 204){
            
            $scope.question = singleQuestions[16];
            
        }else if(id == 205){
           
            $scope.question = singleQuestions[15];
            
        }else if(id == 206){
            if($scope.answer[2][1] === 0){
            $scope.question = singleQuestions[17];
            }else{
            $scope.question = singleQuestions[18];   
            }
        }else if(id == 207){
            
            $scope.question = singleQuestions[19];
            
        }else if(id == 208){
            
            $scope.question = singleQuestions[20];
            
        }
    };
    
    
    $scope.$on("$ionicView.afterLeave", function(){
        $scope.question = singleQuestions[0];
        $rootScope.answer = "";
    });
    
    $scope.calculation = function(){
        //console.log($rootScope.answer);
        var result = [];
        
        if($rootScope.answer[0][1] == 0 || $rootScope.answer[0][1] == 1){
            $rootScope.answer.splice(0,2);
            $rootScope.typeId = 2;
        }else if($rootScope.answer[0][1] == 2 || $rootScope.answer[0][1] == 3){
            if($rootScope.answer[0][1] == 2){
                $rootScope.typeId = 4;   
            }else if($rootScope.answer[0][1] == 3){
                $rootScope.typeId = 3;  
            }
            $rootScope.answer.splice(0,1);
            $rootScope.answer.splice($rootScope.answer.length - 3,2);
        }
        
        
        for (var i = 0; i < $rootScope.answer.length; i++){
            result.push($rootScope.answer[i][1]);
        }

        var count = {}; 
        result.forEach(function(i) { count[i] = (count[i]||0)+1;  });
        
        if(count[-1]){
            delete count[-1];
        }
        
        if(!count[3] && !count[2] && !count[1]){
            $scope.score = 5;
        }else if(!count[3] && !count[2] && count[1] <= 3){
            $scope.score = 4;
        }else if(!count[3] && count[2] <= 1){
            $scope.score = 3;
        }else if(count[2] <= 2){
            $scope.score = 2;
        }else if(count[2] <= 3){
            $scope.score = 1;
        }else{
            $scope.score = 3;
        }
        $scope.inverseScore = 5 - $scope.score;
        
        //console.log($scope.score, $scope.inverseScore);
        $scope.getNumber = function(num) {
            return new Array(num);   
        }
    }
    
    $scope.apply = function(){
            //console.log($rootScope.answer);
        //$state.go('app.liunarProduct',{typeId: $rootScope.typeId});
        window.location.href = "http://shop.liunar.com/list/" + $rootScope.typeId;
    };
}]);
angular.module('event',[])
.controller('eventListController', ["$scope", "httpServices", function($scope, httpServices){
    httpServices.getEvents(30,0).then(
        function(res){
            $scope.events = res;
        }
    );
    $scope.now = (new Date().getTime())/1000;
    //console.log($scope.now);
}]);
angular.module('homeController',[])
.controller('homeController', ["$ionicPopup", "$scope", "$ionicModal", "$ionicScrollDelegate", "httpServices", "$ionicSlideBoxDelegate", "$rootScope", "$ionicPosition", "$location", "$stateParams", "localStorage", "$sce", "$state", "$location", "$window", "wechatShare", "$ionicPopup", "$timeout", function($ionicPopup, $scope, $ionicModal, $ionicScrollDelegate, httpServices, $ionicSlideBoxDelegate,$rootScope, $ionicPosition, $location, $stateParams, localStorage, $sce, $state, $location, $window, wechatShare, $ionicPopup, $timeout) {
  
    wechatShare("留哪儿","http://liunar.com/img/icon.png","更聪明的留学与移民");
    
    $scope.slick = false;
    
    //hide header bar scroll top:
    $scope.shouldHide = true;
    
    $scope.goTo = function(where){
        return $state.go('app.shopnew').then(
            function(){
                $location.hash(where);
            }
        );    
    };
    
    $scope.headerBarControll = function(){
        if($ionicScrollDelegate.getScrollPosition().top > 100){
            $scope.shouldHide = false;
        }else{
            $scope.shouldHide = true;
        }
    }; 
    
    $scope.slideChanged = function(index){
        $scope.slideIndex = index;
    };
    
    httpServices.getHomePosts().then(
        function(res){
            $scope.posts = res;
        }
    );
    
    httpServices.getHomePlaceholder().then(
        function(res){
            console.log(res);
            $scope.placeholderOne = $sce.trustAsHtml(res[1].content_html);
            $scope.questions = $sce.trustAsHtml(res[4].content_html);
            $scope.placeholderTwo = $sce.trustAsHtml(res[2].content_html); 
            $scope.topSlider = $sce.trustAsHtml(res[0].content_html);
            $scope.slickConfig.enabled = true;
            $timeout(
                function(){
                    $scope.slick = true
                }
            );
            console.log($scope.questions);
        }
    );
    
    httpServices.getHomeEvents().then(
        function(res){
            $scope.events = res;
            $scope.now = (new Date().getTime())/1000;
        }
    );
    
    $scope.slickConfig = {
        enabled: false,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 3000,
        dots: true,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };
    
    $scope.grades = {
        "一年级": 1,
        "二年级": 2,
        "三年级": 3,
        "四年级": 4,
        "五年级": 5,
        "六年级": 6,
        "初一": 7,
        "初二": 8,
        "初三": 9,
        "高一": 10,
        "高二": 11,
        "高三": 12,
    };
    
    $scope.countries = {
        "美国": "America",
        "加拿大": "Canada",
        "英国": "England",
        "澳大利亚": "Australia",
        "新西兰": "New Zealand",
        "其他": "Other",
    };
    
    $scope.collectObj = {
        country: "Canada",
        grade: "10",
    }
    
    $scope.collectForm = function(){
        var myPopUp = $ionicPopup.show(
            {
                cssClass: "collectForm",
                scope: $scope,
                title: "中外双总部 8位跨国导师",
                subTitle: "1600名学员成功入读北美 Top100",
                templateUrl: 'templates/collectForm.html',
                buttons: [
                    { 
                        text: '免费获取定制方案',
                        onTap: function(e) {
                            if(!$scope.collectObj.name || !$scope.collectObj.phone){
                                $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '名字与电话不能为空'
                                }).then(
                                    function(res){
                                        $scope.collectObj.name = null;
                                        $scope.collectObj.phone = null;
                                    }
                                );
                                e.preventDefault();
                            }else{
                                $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '我们将会在24小时内联系您，为您定制方案。'
                                }).then(
                                    function(res){
                                        $scope.collectObj.name = null;
                                        $scope.collectObj.phone = null;
                                    }
                                );      
                            }    
                        }
                    },
                ]
            }
        );
        
        myPopUp.then(function(res){
            console.log(res);    
        });
        
        $scope.closePop = function(){
            myPopUp.close();
        };
    };
}])
.controller("tabsController", ["$scope", "$state", function($scope, $state){
    function getRegionValue(){
        if(document.cookie.indexOf('region')>-1){
     match = document.cookie.match(new RegExp('region' + '=([^;]+)'));
     if(match && match[1] != null){
          return match[1];
      if (location.href.indexOf(regionValue)== -1 ) {
       //current region and cookie region differs
      }
     }
        }
    }
    $scope.goToHome = function(){
        window.location.href="http://www.liunar.com/"+getRegionValue();
        //$state.go("app.mainHome");
    };
    
    $scope.goToSchool = function(){
        window.location.href="http://www.liunar.com/schools/";
        //$state.go("app.school");
    };
    
    $scope.goToShop = function(){
        window.location.href="http://shop.liunar.com/"+getRegionValue();
        //$state.go("app.shopnew");
    };
    
}]);

angular.module("liunarProduct",[])
.controller("liunarProductController", ["$ionicHistory", "$scope", "$stateParams", "productList", "pastCases", "schoolList", "wechatShare", "seedUrl", function($ionicHistory, $scope, $stateParams, productList, pastCases, schoolList, wechatShare, seedUrl){
    //console.log($stateParams, productList, pastCases, schoolList);
    
    $scope.type = $stateParams.typeId;
    
    console.log(schoolList);
    
    if($stateParams.typeId == 1){
        $scope.cate = "知名大学";
        $scope.wechatShareImage = "http://www.liunar.com/img/zmdx.png";
    }else if($stateParams.typeId == 2){
        $scope.cate = "特色院校";
        $scope.wechatShareImage = "http://www.liunar.com/img/tsyx.png";
    }else if($stateParams.typeId == 3){
        $scope.cate = "低龄留学";
        $scope.wechatShareImage = "http://www.liunar.com/img/dllx.png";
    }
    
    $scope.shareUrl = seedUrl.createUrl("bulletboard", $stateParams.typeId);
    
    wechatShare($scope.cate, $scope.wechatShareImage, $scope.cate);
    
    $scope.pastCases = pastCases;
    
    $scope.productLists = productList;
    
    $scope.schools = schoolList.slice(0,4);
    
    $scope.anchor = $stateParams.typeId;
    
    if($ionicHistory.viewHistory().backView !== null){
        $scope.from = $ionicHistory.viewHistory().backView.url;
    }else{
        $scope.from = "/app/mainHome";
        $scope.fromShare = true;
    }
    
}])
.controller("liunarPastCaseController", ["$scope", "$stateParams", "caseDetail", "$ionicHistory", "wechatShare", "seedUrl", function($scope, $stateParams, caseDetail, $ionicHistory, wechatShare, seedUrl){
    for(var i = 0; i < caseDetail.length; i++){
        if(caseDetail[i].id == $stateParams.caseId){
            $scope.case = caseDetail[i];
            break;
        }
    }
    
    //console.log($scope.case);
    
    $scope.shareUrl = seedUrl.createUrl("pastcase", $scope.case.id);
    
    wechatShare("祝贺"  +  $scope.case.name + "，申请成功！", "http://www.liunar.com/schools/" + $scope.case.school_id +"/1.png", $scope.case.extra_info);
    
    if($ionicHistory.viewHistory().backView !== null){
        $scope.from = $ionicHistory.viewHistory().backView.url;
    }else{
        $scope.from = "/app/mainHome";
        $scope.fromShare = true;
    }
}])
.controller("shopProductAll", ["$scope", "httpServices", function($scope, httpServices){
    httpServices.getAllProductsNew(100).then(
        function(res){
            console.log(res);
            $scope.products = res.data;
        }
        );
    
    $scope.imgUrl = function(string){
        var match = String(string).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);
        var url = (match && match.length && match[1]) ? match[1] : ''
        return url;
    };
}])
.controller("shopProductCountry", ["$scope", "httpServices", "$stateParams", function($scope, httpServices, $stateParams){
    httpServices.getCountryProductsNew($stateParams.country).then(
        function(res){
            $scope.products = res.data;
        }
        );
}])
.controller("shopProductDetail", ["userAuth", "$rootScope", "$sce", "$scope", "$state", "$stateParams", "seedUrl", "wechatShare", "httpServices", "localStorage", "$ionicLoading", "$ionicPopup", "$ionicModal", "$ionicHistory", "$location", "missionShare", function(userAuth, $rootScope, $sce, $scope, $state, $stateParams, seedUrl, wechatShare, httpServices, localStorage, $ionicLoading, $ionicPopup, $ionicModal, $ionicHistory, $location, missionShare){

    //Product Detail:
    $scope.$on("$ionicView.beforeEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
            noBackDrop: true
        });
        httpServices.getShopProductDetail($stateParams.productId).then(
            function(res){
                $scope.productId = res[0].id;

                $scope.productDetail = $sce.trustAsHtml(res[0].details);

                $scope.productTitle = res[0].title; 

                $scope.productPrice = res[0].price;

                var matches = String(res[0].details).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);

                var result = (matches && matches.length && matches[1]) ? matches[1] : '';

                if($ionicHistory.backView() && $ionicHistory.backView().stateName == "app.mission"){
                    missionShare($scope.productTitle, result, res[0].description)
                }else{
                    wechatShare($scope.productTitle, result, res[0].description);
                }
                
                var shareTime = $location.absUrl().indexOf('&sharetime=');

                if(shareTime !== -1){
                    console.log("havetime");
                    shareTime += "&sharetime=".length;
                    var stop = $location.absUrl().indexOf('&', shareTime);
                    if(stop === -1){
                        var urlTime = $location.absUrl().substring(shareTime);
                    }else{
                        var urlTime = $location.absUrl().substring(shareTime, stop);
                    }
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id, urlTime, $stateParams.taskId);
                }else{
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id);    
                }
            }
            );
    });
    
    $scope.$on("$ionicView.afterEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.hide();
    });
    
    var user = new userAuth.checkLogin();
    
    $scope.purchase = function(){
        if(user.isLogin !== true){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再购买'
            }).then(
            function(){
                $state.go("userLogin");    
            }
            );
        }else{
            $state.go("app.shopPurchase",{ productId: $scope.productId});
        }
    };
    
    //show referId logic 
    if($rootScope.referId == "null"){
        if(user.isLogin == true){
            httpServices.getRefCodeById(user.uid).then(
                function(res){
                    if(!res.err){
                        $scope.refCode = res.reference_code;
                    }else{
                        $scope.showCode = false;
                    }
                }
                );    
        }else{
            $scope.showCode = false;    
        }   
    }else{
        httpServices.getRefCodeById($rootScope.referId).then(
            function(res){
                if(!res.err){
                    $scope.refCode = res.reference_code;
                }else{
                    $scope.showCode = false;
                }
            }
            );    
    }
}])
.controller("newShopDetail", ["userAuth", "$rootScope", "$sce", "$scope", "$state", "$stateParams", "seedUrl", "wechatShare", "httpServices", "localStorage", "$ionicLoading", "$ionicPopup", "$ionicModal", "$ionicHistory", "$location", "missionShare", "$ionicModal", function(userAuth, $rootScope, $sce, $scope, $state, $stateParams, seedUrl, wechatShare, httpServices, localStorage, $ionicLoading, $ionicPopup, $ionicModal, $ionicHistory, $location, missionShare, $ionicModal){

    //Product Detail:
    $scope.$on("$ionicView.beforeEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
            noBackDrop: true
        });http://liunar.com/#/app/productdetail/30/
        httpServices.getProductDetailNew($stateParams.productId).then(
            function(res){
                console.log(res);
                $scope.productId = res[0].id;

                $scope.productDetail = $sce.trustAsHtml(res[0].details);

                $scope.productTitle = res[0].title; 

                $scope.productPrice = res[0].msrp + " " + res[0].currency;

                var matches = String(res[0].details).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);

                var result = (matches && matches.length && matches[1]) ? matches[1] : '';

                if($ionicHistory.backView() && $ionicHistory.backView().stateName == "app.mission"){
                    missionShare($scope.productTitle, result, res[0].description)
                }else{
                    wechatShare($scope.productTitle, result, res[0].description);
                }
                
                var shareTime = $location.absUrl().indexOf('&sharetime=');

                if(shareTime !== -1){
                    console.log("havetime");
                    shareTime += "&sharetime=".length;
                    var stop = $location.absUrl().indexOf('&', shareTime);
                    if(stop === -1){
                        var urlTime = $location.absUrl().substring(shareTime);
                    }else{
                        var urlTime = $location.absUrl().substring(shareTime, stop);
                    }
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id, urlTime, $stateParams.taskId);
                }else{
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id);    
                }
				
				var imgs = String(res[0].details).match(/<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/g);
				
            }
            );
    });
    
    $scope.$on("$ionicView.afterEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.hide();
    });
    
    var user = new userAuth.checkLogin();

    if(user.isLogin == true){
        var refCode = user.refCode;
		//uid,refcode
        if(!refCode){
            httpServices.getProductAccessAuthorization(user.uid).then(
                function(res){
                    console.log('response:'+res,res.code == 'true');
                    if(res.code == 'true'){
                        $scope.showButton = true;
                    }
                    return;
                }, 
                function(err){
                    console.log(err);
                });
        }else{
            httpServices.getProductAccessAuthorization(user.uid,refCode).then(function(res){
                if(res.code == 'true'){
                    $scope.showButton = true;
                }
            },function(err){
                console.log(err);
            });
        }
    }

    $scope.purchase = function(){
        if(user.isLogin !== true){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再购买'
            }).then(
            function(){
                $state.go("userLogin");    
            }
            );
        }else{
            $state.go("app.productpurchase",{ productId: $scope.productId});
        }
    };

	$ionicModal.fromTemplateUrl('templates/imgEnlarge.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});
	
	$scope.openModal = function(src) {
		$scope.modal.show();
		$scope.imgUrl = src;
  	};

    //show referId logic 
    if($rootScope.referId == "null"){
        if(user.isLogin == true){
            httpServices.getRefCodeById(user.uid).then(
                function(res){
                    if(!res.err){
                        $scope.refCode = res.reference_code;
                    }else{
                        $scope.showCode = false;
                    }
                }
                );    
        }else{
            $scope.showCode = false;    
        }   
    }else{
        httpServices.getRefCodeById($rootScope.referId).then(
            function(res){
                if(!res.err){
                    $scope.refCode = res.reference_code;
                }else{
                    $scope.showCode = false;
                }
            }
            );    
    }
}]).controller("productMoreDetail", ["$timeout", "$window", "$sce", "$state", "localStorage", "$stateParams", "$scope", "httpServices", "$location", "$ionicScrollDelegate", "userAuth", function($timeout, $window, $sce, $state, localStorage, $stateParams, $scope, httpServices, $location, $ionicScrollDelegate, userAuth){
    var user = new userAuth.checkLogin();
    $scope.prdID = $stateParams.prdID;
    if(user.isLogin == true){
        var refCode = user.refCode;
        if(!refCode){ //no refcode in localstorage
            httpServices.getAuthorizedProductData($stateParams.prdID,user.uid).then(
                function(res){
                    var prod = res[0][0];
                    var files = res[1];
                    if(prod){
                        $scope.title = prod.title;
                        $scope.commission = prod.commission;
                        $scope.referral_fee_process_time = prod.referral_fee_process_time;
                        $scope.sale_instruction = $sce.trustAsHtml(prod.sale_instruction);
                        $scope.market_instruction = $sce.trustAsHtml(prod.market_instruction);
                        $scope.files = files;
                    }else{
                        $state.go("app.productdetail", {productId: $stateParams.prdID});
                    }
                }, 
                function(err){
                    console.log(err);
                });
        }else{ //have refcode localstorage
			//prodid, uid,refcode
            httpServices.getAuthorizedProductData($stateParams.prdID,user.uid,refCode).then(function(res){
                $state.go("productMoreDetail");
                return;
            },function(err){
                console.log(err);
            });
        }
    }else{
        $scope.productTitle = '請登錄！';
    }
}])
.controller("shopPage", ["$timeout", "$window", "$sce", "$state", "localStorage", "$stateParams", "$scope", "httpServices", "$location", "$ionicScrollDelegate", function($timeout, $window, $sce, $state, localStorage, $stateParams, $scope, httpServices, $location, $ionicScrollDelegate){

    $scope.slick = false;
    
    $scope.slickConfig = {
        enabled: false,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 3000,
        dots: true,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };
    
    httpServices.getProductPromo().then(
        function(res){
            console.log(res);
            $scope.topCarousel = $sce.trustAsHtml(res[0].content_html);
            $scope.secondSection = $sce.trustAsHtml(res[1].content_html);
            $scope.midBanner = $sce.trustAsHtml(res[2].content_html);
            $scope.slickConfig.enabled = true;
            $timeout(
                function(){
                    $scope.slick = true;
                }
                );
        }
        );
    
    console.log($location);
    //0:低龄留学 1:短期留学 2:特色院校 3:知名大学 4:移民项目

    httpServices.getProductList().then(
        function(res){
            console.log(res);
            $scope.a = [];
            $scope.b = []; 
            $scope.c = [];
            $scope.d = [];
            $scope.e = [];
            res.forEach(function(v,i){
                if(v.promo_group == 0){
                    $scope.a.push(v);
                }else if(v.promo_group == 1){
                    $scope.b.push(v);
                }else if(v.promo_group == 2){
                    $scope.c.push(v);
                }else if(v.promo_group == 3){
                    $scope.d.push(v);
                }else if(v.promo_group == 4){
                    $scope.e.push(v);
                } 
            });
            $ionicScrollDelegate.resize();
            if($location.$$hash !== ""){
                console.log($ionicScrollDelegate.resize());
                $ionicScrollDelegate.anchorScroll(true);
                console.log("shouldScroll");
            }
        }
        );
    
}])
.controller("shopPageNew", ["$timeout", "$window", "$sce", "$state", "localStorage", "$stateParams", "$scope", "httpServices", "$location", "$ionicScrollDelegate", "wechatShare", function($timeout, $window, $sce, $state, localStorage, $stateParams, $scope, httpServices, $location, $ionicScrollDelegate, wechatShare){

    $scope.slick = false;
    
    $scope.slickConfig = {
        enabled: false,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 3000,
        dots: true,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };

    wechatShare("留哪儿精选——精选全球最优教育资源", "http://www.liunar.com/img/icon.png", "留哪儿精选是Canbest集团旗下的海外教育类垂直电商平台，留哪儿精选将全球最优质的留学、游学资源，一站式提供给中国学子，让他们能更直接、更高效地选择世界。")
    
    httpServices.getProductPromo().then(
        function(res){
            console.log(res);
            $scope.topCarousel = $sce.trustAsHtml(res[0].content_html);
            //$scope.firstSection = $sce.trustAsHtml(res[1].content_html);
            $scope.secondSection = $sce.trustAsHtml(res[1].content_html);
            $scope.midBanner = $sce.trustAsHtml(res[2].content_html);
            $scope.slickConfig.enabled = true;
            $timeout(
                function(){
                    $scope.slick = true;
                }
                );
        }
        );
    
    console.log($location);
    //0:低龄留学 1:短期留学 2:特色院校 3:知名大学 4:移民项目

    httpServices.getProductListNew().then(
        function(res){
            console.log(res);
            $scope.a = [];
            $scope.b = []; 
            $scope.c = [];
            $scope.d = [];
            $scope.e = [];
            res.forEach(function(v,i){
                if(v.promo_group == 0){
                    $scope.a.push(v);
                }else if(v.promo_group == 1){
                    $scope.b.push(v);
                }else if(v.promo_group == 2){
                    $scope.c.push(v);
                }else if(v.promo_group == 3){
                    $scope.d.push(v);
                }else if(v.promo_group == 4){
                    $scope.e.push(v);
                } 
            });
            $ionicScrollDelegate.resize();
            if($location.$$hash !== ""){
                console.log($ionicScrollDelegate.resize());
                $ionicScrollDelegate.anchorScroll(true);
                console.log("shouldScroll");
            }
        }
        );
    
}])
.controller("shopPurchase", ["userAuth", "$state", "localStorage", "$stateParams", "purchaseDetail", "$scope", "httpServices", "$ionicPopup", "$rootScope", function(userAuth, $state, localStorage, $stateParams, purchaseDetail, $scope, httpServices, $ionicPopup, $rootScope){

    var user = new userAuth.checkLogin();
    
    if(user.isLogin !== true){

        $state.go("userLogin");    

    }else{

        $scope.purchaseDetail = purchaseDetail[0];

        $scope.clicked = false

        var matches = String(purchaseDetail[0].details).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);

        $scope.imgUrl = (matches && matches.length && matches[1]) ? matches[1] : '';
        
        
        
        $scope.orderObj = {
            refCode: ""
        }
        
        httpServices.extractUserInfo(user.uid).then(
            function(res){
                //console.log(res);
                $scope.orderObj.name = res.fullname;
                $scope.orderObj.phone = res.phone;
            }
            );
        
        if($rootScope.referId == "null"){
            if(user.isLogin == true){
                httpServices.getRefCodeById(user.uid).then(
                    function(res){
                        if(!res.err){
                            $scope.orderObj.refCode = res.reference_code;
                        }else{
                            $scope.orderObj.refCode = null;
                        }
                    }
                    );    
            }else{
                $scope.orderObj.refCode = null;    
            }   
        }else{
            httpServices.getRefCodeById($rootScope.referId).then(
                function(res){
                    if(!res.err){
                        $scope.orderObj.refCode = res.reference_code;
                    }else{
                        $scope.orderObj.refCode = null;
                    }
                }
                );    
        }

        $scope.orderProduct = function(){
            //console.log($scope.orderObj);
            if($scope.orderObj.name && $scope.orderObj.phone){
                if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test($scope.orderObj.phone) == true){
                    var aid;
                    //console.log(localStorage.getItem("referId"));
                    if(localStorage.getItem("referId") && localStorage.getItem("referId") != 'null'){
                        aid = localStorage.getItem("referId");
                    }else{
                        aid = 0;
                    }
                    //console.log(aid);
                    httpServices.userOrder(localStorage.getObject("canbestUserInfo").uid, purchaseDetail[0].id, $scope.orderObj.phone, $scope.orderObj.name, aid, $scope.orderObj.refCode).then(
                        function(res){
                            //console.log(res);
                            if(!res.err){
                                $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '恭喜您预定成功，我们会尽快联系您'
                                }).then(
                                function(){
                                    $state.go("app.shop");
                                }
                                );        
                            }
                        }
                        );
                }else{
                    alert('电话号码只能由 +,-,0-9 组成');
                }
            }else{
                alert("电话和姓名不能为空");
            }
        }
    }
}]);

angular.module("msg", [])
  .controller("msgCenterController", ["$state", "$location", "$scope", "$ionicScrollDelegate", "httpServices", "userAuth", "$ionicPopup", "wechatShare", function ($state, $location, $scope, $ionicScrollDelegate, httpServices, userAuth, $ionicPopup, wechatShare) {
    $scope.initList = function () {
      httpServices.getMsgCenterList($scope.type, $scope.index, $scope.id).then(
        function (res) {
          console.log(res);
          $scope.lists = res;
        }
      );
    };

    $scope.index = 0;
    $scope.type = "list";
    $scope.id = null;
    $scope.haveMore = true;
    $scope.getMore = function () {
      $scope.index++;
      httpServices.getMsgCenterList($scope.type, $scope.index, $scope.id).then(
        function (res) {
          console.log(res);
          $scope.lists = [].concat($scope.lists, res);
          $scope.$broadcast('scroll.infiniteScrollComplete');
          if (res.length) {
            $scope.haveMore = true;
          } else {
            $scope.haveMore = false;
          }
        }
      );
    };

    $scope.tabSelected = 1;
    $scope.tabSelect = function (tab) {
      $scope.tabSelected = tab;
      $scope.id = null;
      $scope.index = 0;
      $scope.haveMore = true;
      console.log(tab);
      if (tab == 1) {
        $scope.type = "list";
        $scope.initList();
      } else if (tab == 2) {
        $scope.type = "expiring";
        $scope.initList();
      } else if (tab == 3) {
        $scope.type = "expired";
        $scope.initList();
      } else if (tab == 4) {
        var user = new userAuth.checkLogin();
        if (user.isLogin !== true) {

          $ionicPopup.alert({
            title: '留哪儿',
            template: '微信登陆后才能看到你发布的信息'
          }).then(
            function () {
              $state.go("userLogin", {
                lastview: $location.$$path
              });
            }
          );
        } else {
          $scope.id = user.uid;
          $scope.type = "my";
          $scope.initList(user.uid);
        }
      }
    };
    $scope.initList();
  }])
  .controller("msgDetailController", ["$ionicLoading", "$state", "$location", "$scope", "$stateParams", "httpServices", "userAuth", "$ionicPopup", "wechatShare", "$ionicModal", function ($ionicLoading, $state, $location, $scope, $stateParams, httpServices, userAuth, $ionicPopup, wechatShare, $ionicModal) {
    var loginStatus = new userAuth.checkLogin();
    var id;
    var contentInit = function (callback) {
      httpServices.getMsgDetail($stateParams.id).then(
        function (res) {
          $scope.msg = res[0][0];
          id = $scope.msg.id;
          $scope.comments = JSON.parse(res[1]);
          console.log(res, $scope.msg, $scope.comments);
          callback($scope.msg);
        }
      );
    };
    $scope.$on("$ionicView.beforeEnter", function () {
      contentInit(function (res) {
        wechatShare(res.title, "http://www.liunar.com/img/lntt.png", res.title);
        if (loginStatus.uid == res.uid) {
          $scope.showDelete = true;
          $scope.showCreate = true;
        }
      });
      $scope.show = false;
      //title, img, desc, url
    });
    $scope.showConfirmation = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: '留哪儿',
        template: '确定要删除?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          postDelete();
        }
      });
    };

    console.log($location);

    $scope.createPoster = function (author, avatar, type, title, body, expire) {
      $ionicLoading.show({
        template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner><br/>正在生成海报...',
        noBackDrop: true
      });
      httpServices.requestQRCode($location.$$absUrl).then(
        function (qrcode) {
          console.log(qrcode);
          if (qrcode.img) {
            httpServices.createMsgPoster({
              author: author,
              avatar: avatar,
              title: title,
              type: type,
              body: body,
              qrcode: qrcode.img,
              expire_time: expire
            }).then(
              function (res) {
                if (res.img) {
                  $ionicLoading.hide();
                  popUpModal(res.img);
                } else {
                  $ionicPopup.alert({
                    title: '留哪儿',
                    template: res.err
                  })
                }
              },
              function (err) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: '留哪儿',
                  template: err.toString()
                })
              }
            );
          } else {
            $ionicLoading.hide();
            $ionicPopup.alert({
              title: '留哪儿',
              template: qrcode.err
            })
          }
        },
        function (err) {
          $ionicLoading.hide();
          $ionicPopup.alert({
            title: '留哪儿',
            template: err.toString()
          })
        }
      );
    };

    var popUpModal = function (img) {
      $ionicModal.fromTemplateUrl('templates/imgEnlarge.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
        $scope.imgPoster = img;
        $scope.modal.show();
      });
    };

    $scope.closeModal = function () {
      $scope.modal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.modal.remove();
    });

    var postDelete = function () {
      httpServices.postDelete({
        id: id
      }).then(function (result) {
        if (result.data.code == 1) {
          $ionicPopup.alert({
            title: '留哪儿',
            template: '删除成功'
          }).then(function (res) {
            $state.go("app.msgCenter");
          });
        } else {
          $ionicPopup.alert({
            title: '删除没有成功！',
            template: '没有查到此post,请截屏然后发给技术人员'
          });
        }
      });
    };
    $scope.showContact = function () {
      if ($scope.show == false) {
        $scope.show = true;
      } else {
        $scope.show = false;
      }
    }

    $scope.msgComment = {
      row_id: $stateParams.id,
      content: "",
      module: 5
    };


    $scope.postMsgComment = function () {
      if (loginStatus.isLogin == false) {
        $ionicPopup.alert({
          title: '留哪儿',
          template: '请微信登录之后再评论'
        }).then(
          function () {
            $state.go("userLogin", {
              lastview: $location.$$path
            });
          }
        );
      } else {
        $scope.msgComment.uid = loginStatus.uid;
        if ($scope.msgComment.content == "" || $scope.msgComment.content.length < 6) {
          $ionicPopup.alert({
            title: '留哪儿',
            template: '评论不能为空，而且必须多于6个字'
          });
        } else {
          $scope.disable = true;
          httpServices.postComment($scope.msgComment).then(
            function (res) {
              $scope.disble = false;
              $ionicPopup.alert({
                title: '留哪儿',
                template: '感谢您的评论'
              }).then(
                function (res) {
                  $scope.msgComment.content = null;
                  contentInit();
                }
              )
            }
          );
        }
      }
    };

  }])
  .controller("msgPostController", ["$scope", "userAuth", "httpServices", "$state", "$ionicPopup", "$ionicLoading", function ($scope, userAuth, httpServices, $state, $ionicPopup, $ionicLoading) {
    var user = userAuth.checkLogin();
    console.log(user);
    if (user.isLogin !== true) {
      $ionicPopup.alert({
        title: '留哪儿',
        template: '请微信授权之后再发布消息'
      }).then(
        function (res) {
          $state.go("userLogin", {
            lastview: "/app/post"
          });
        }
      );
    } else {

      $scope.msgObj = {
        phone: "",
        wechat: "",
        title: null,
        content: null,
        deadline: "",
        location: "",
        uid: user.uid,
        type: 1
      };

      $scope.time = new Date(new Date().setDate(new Date().getDate() + 365));
      console.log($scope.time);

      $scope.postMsg = function () {
        console.log($scope.msgObj.type, $scope.msgObj.title.length);
        if (($scope.msgObj.title == null && $scope.msgObj.content == null) || $scope.msgObj.title.length > 12) {
          $ionicPopup.alert({
            title: '留哪儿',
            template: '消息标题和消息内容不能为空，且标题不能多于12个字'
          }).then();
        } else {
          if ($scope.msgObj.phone.length && /^\d+$/.test($scope.msgObj.phone) !== true) {
            $ionicPopup.alert({
              title: '留哪儿',
              template: '电话号码只能由数字组成'
            }).then();
          } else {
            $scope.msgObj.deadline = Math.round($scope.time.getTime() / 1000);
            console.log($scope.msgObj.deadline);
            httpServices.postMsg($scope.msgObj).then(
              function (res) {
                if (res.code == "success") {
                  $scope.msgObj = null;
                  $ionicPopup.alert({
                    title: '留哪儿',
                    template: '发布成功'
                  }).then(
                    function () {
                      $state.go("app.msgCenter");
                    }
                  );
                } else {
                  $ionicPopup.alert({
                    title: '留哪儿',
                    template: '发布不成功，请重试'
                  }).then(
                    function () {
                      $scope.msgObj = null;
                    }
                  );
                }
              },
              function (err){
                $ionicPopup.alert({
                  title: '留哪儿',
                  template: err
                }).then();
              }
            );
          }
        }
      };

      $scope.qrSrc = "http://api.liunar.com/uploads/qr/qr-" + user.uid + ".jpg";

      $scope.uploadQrCode = function (file, err) {
        $ionicLoading.show({
          template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
          noBackDrop: true
        });
        httpServices.uploadMsgQR(file, user.uid).then(
          function (res) {
            if (res.statusText == "OK") {
              $ionicLoading.hide();
              $ionicPopup.alert({
                title: '留哪儿',
                template: 'QR Code 上传成功'
              }).then(function () {
                $scope.qrSrc = "http://api.liunar.com" + res.data.path + "?stamp=" + new Date().getTime();
              });
            } else {
              $ionicLoading.hide();
              $ionicPopup.alert({
                title: '留哪儿',
                template: 'QR Code 上传不成功，请重试'
              }).then(function () {

              });
            }
          }
        )
      };
    }
  }])
  .controller("yunController", ["$scope", "userAuth", "httpServices", "$state", "$ionicPopup", function ($scope, userAuth, httpServices, $state, $ionicPopup) {
    var user = userAuth.checkLogin();
    if (user.isLogin !== true) {
      $ionicPopup.alert({
        title: '留哪儿',
        template: '请进入微信授权'
      }).then(
        function (res) {
          $state.go("userLogin", {
            lastview: "/yun"
          });
        }
      );
    } else {
      window.location.href = "http://yun.liunar.com/verifiedAgent/index/?u=" + user.uid;
    };
  }])
angular.module('qandAController',[])
.controller('qandAController', ["localStorage", "$scope", "httpServices", "$stateParams", "$state", "$ionicHistory", "$ionicPopup", "newQuestion", "hotQuestions", function(localStorage, $scope,httpServices,$stateParams,$state,$ionicHistory, $ionicPopup, newQuestion, hotQuestions){
    $scope.hotQuestions = hotQuestions;
    
    $scope.newQuestion = newQuestion;
    
}])
.controller('qandAListController', ["localStorage", "$scope", "httpServices", "$stateParams", "$state", "$ionicHistory", "$ionicPopup", "initQuestions", function(localStorage, $scope,httpServices,$stateParams,$state,$ionicHistory, $ionicPopup, initQuestions){
    
    //console.log($stateParams, initQuestions);
    
    $scope.order = $stateParams.order;
    
    if($stateParams.order === "answer_num"){
        $scope.listTitle = "最热问题";
    }else if($stateParams.order === "create_time"){
        $scope.listTitle = "最新问题";
    }
    
    var questionNum = 10;
    
    $scope.questions = initQuestions;

    var ids =[];

    var pushId = initQuestions.forEach(
        function(v,i){
            ids.push(v.id);
        }
    );

    
    $scope.loadMoreQuestions = function(){
        
        questionNum = questionNum + 5;
        //console.log(questionNum);
        httpServices.getQuestions(1,1,$stateParams.order,questionNum).then(
            function(res){
                $scope.questions = res;
                ids = [];
                res.forEach(function(v,i){
                    ids.push(v.id);
                });
                //console.log(res);
                //console.log(ids);
                $scope.$broadcast('scroll.infiniteScrollComplete');
                
            }
        );
    };
    
}])
.controller('qandATypeListController', ["$scope", "$stateParams", "httpServices", function($scope, $stateParams, httpServices){
    //console.log($stateParams);
    var questionNum = 10;
    $scope.haveMore = true;
    httpServices.getQuestionsByType($stateParams.typeId, questionNum).then(
        function(res){
            //console.log(res);
            $scope.questions = res;
        }
    );
    
    $scope.loadMoreQuestions = function(){
        
        questionNum = questionNum + 5;
        //console.log(questionNum);
        httpServices.getQuestionsByType($stateParams.typeId, questionNum).then(
            function(res){
                $scope.questions = res;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                if(questionNum > res.length){
                    $scope.haveMore = false;
                };
            }
        );
        
        //console.log($scope.haveMore);
    };
    
}])
.controller('qandAFormController', ["$scope", "localStorage", "$ionicPopup", "httpServices", "$state", "hotQuestions", function($scope, localStorage, $ionicPopup, httpServices, $state, hotQuestions){
    
    $scope.$on("$ionicView.beforeEnter", function(){
        //console.log("yay");
        if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再提问'
            }).then(
                function(res){
                    $state.go("userLogin",{lastview: "/app/mainhome/QandA/QandAForm"});
                }
            );
        }    
    });
    //ask questions form:
    /*$scope.tags = [];
    
    $scope.addTags = function(tag){
        if($scope.tags.indexOf(tag) === -1){
        $scope.tags.push(tag);
        }else{
            //console.log("exist");
        }
    };
    
    $scope.removeTags = function(tag){
        if($scope.tags.indexOf(tag) !== -1){
        //console.log(tag);
        $scope.tags.splice($scope.tags.indexOf(tag),1);
        }else{
        }
    };*/
    
    $scope.hotQuestions = hotQuestions;
    
    $scope.questionObj = {
        title: "",
        description: "",
        category: "1"
        //tags: $scope.tags
    };
    
    $scope.submitQ = function(){
            if($scope.questionObj.title == "" || $scope.questionObj.title.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '问题不能为空，而且必须多于6个字'
                });  
            }else{
                $scope.questionObj.uid = localStorage.getObject("canbestUserInfo").uid;
                httpServices.postQuestion($scope.questionObj).then(
                    function(res){
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的提问，我们将会在24小时内答复。'
                        }).then(
                            function(){
                                $state.go("app.QandA");
                            }
                        )
                    }
                );
            }        
    };
}]);
angular.module('qandADetailController',[])
.controller('qandADetailController',["localStorage", "$scope", "httpServices", "$stateParams", "$state", "questionDetail", "answerDetail", "seedUrl", "wechatShare", "httpServices", function(localStorage, $scope, httpServices, $stateParams, $state, questionDetail, answerDetail, seedUrl, wechatShare, httpServices){
    
    
    ///////getQandA Detail//////
    
    
    $scope.order = $stateParams.order;
    
    if($scope.order.indexOf('type') !== -1){
        var typeId = $scope.order.substring($scope.order.indexOf('type') + "type".length);
        $scope.backUrl = "app.QandATypeList({typeId: " + typeId + "})";
    }else if($scope.order.indexOf('search') !== -1){
        $scope.backUrl = "tagSearch({tag: ''})";
    }
    else{
        $scope.backUrl = "app.QandAList({order: '" + $scope.order + "'})";
    }
    
    
    $scope.qDetail = questionDetail[0];
    $scope.qAnswer = answerDetail;
    //console.log(questionDetail, answerDetail);
    
    $scope.$on("$ionicView.beforeEnter", function(){
        if(questionDetail[0] !== undefined){
            $scope.shareUrl = seedUrl.createUrl("question",questionDetail[0].id);
            wechatShare(questionDetail[0].title,"http://www.liunar.com/img/zjdy.png", questionDetail[0].description !== "" ? questionDetail[0].description : questionDetail[0].title);
        }
    });
    
    $scope.refresh = function(){
        httpServices.getQuestionAnswer($stateParams.questionId).then(
        function(res){
           $scope.qAnswer = res;
           $scope.$broadcast('scroll.refreshComplete');
        });
    };
    
    ////////Post Answer//////
    /*$scope.answerObj = {
        question_id: $stateParams.questionId,
        content: "",
        //uid: localStorage.getObject('canbestUserInfo').uid,
        //commentPerson: null,
    };*/
    
    //$scope.autoFocus = false;
    
    /*$scope.addPerson = function(p){
        if($scope.answerObj.commentPerson === null){
            $scope.answerObj.commentPerson = p;
            $scope.showPerson = true;
        }else if($scope.answerObj.commentPerson !== null){
            if($scope.answerObj.commentPerson === p){
                $scope.answerObj.commentPerson = null;
                $scope.showPerson = false;
            }else{
                $scope.answerObj.commentPerson = p;
                $scope.showPerson = true;
            }
        }
        //console.log($scope.answerObj, p);
    };*/
    
    /*$scope.postAnswer = function(){
        //console.log($scope.answerObj);
        
        if(localStorage.getObject("canbestUserInfo") === null){
            $scope.anwserObj.uid = 0;
        }else{
            $scope.answerObj.uid = localStorage.getObject("canbestUserInfo").uid;
        }
        
        httpServices.postAnswer($scope.answerObj).then(
            function(res){
                //console.log(res);
                $scope.getQuestionDetail();
            }
        );
        $scope.answerObj.content = null;
    };*/
}]);

angular.module('schoolController',[])
.controller('schoolController', ["$rootScope", "$scope", "httpServices", "$stateParams", "seedUrl", "wechatShare", function($rootScope, $scope, httpServices, $stateParams, seedUrl, wechatShare){
    //functions => load Content:
    if($stateParams.cateID == 1){
        
        $scope.title = "加拿大大学排名";
        $scope.wechatShareImage = "http://www.liunar.com/img/daXueIcon.png";
        $scope.showFliter = false;
    
    }else if($stateParams.cateID == 2){
        
        $scope.title = "加拿大特色院校";
        $scope.wechatShareImage = "http://www.liunar.com/img/teSeXueYuanIcon.png";
        $scope.showFliter = false;
    
    }else if($stateParams.cateID == 3){
        
        $scope.title = "BC省中学排名";
        $scope.wechatShareImage = "http://www.liunar.com/img/zhongXueIcon.png";
        $scope.showFliter = true;
        
    }else if($stateParams.cateID == 4){
        
        $scope.title = "BC省小学排名";
        $scope.wechatShareImage = "http://www.liunar.com/img/xiaoXueIcon.png";
        $scope.showFliter = true;
    };
    
    //$scope.schoolListsRank = schoolList;
    
    var getCities = function(){
        httpServices.getCities().then(
            function(res){
                $scope.cities = res;
            }
        );
    };
    
    $scope.searchObj = {
        public: "",
        city: ""
    };
    
    $scope.getSchoolsRank = function(){
        httpServices.getSchoolRankList($stateParams.cateID,$scope.searchObj.public, $scope.searchObj.city,"").then(
            function(res){
                //console.log(res);
                $scope.schoolListsRank = res;
                $scope.shareUrl = seedUrl.createUrl("schoolRankList", $stateParams.cateID);
                wechatShare($scope.title, 'http://www.liunar.com/img/schoolShareV2.jpeg', $scope.title);
            }
        );
    };
    
    $scope.$on("$ionicView.beforeEnter", function(){
        getCities();
        $scope.getSchoolsRank();
    });
    
    $scope.$on("$ionicView.beforLeave", function(){
        $scope.haveMore = true;
    });
    
}])
.controller('schoolProvinceController', ["$state", "$ionicScrollDelegate", "$rootScope", "$scope", "httpServices", "$stateParams", "seedUrl", "wechatShare", "$ionicHistory", function($state, $ionicScrollDelegate, $rootScope, $scope, httpServices, $stateParams, seedUrl, wechatShare, $ionicHistory){
    
    $scope.scrollTop = function() {
        $ionicScrollDelegate.scrollTop(true);
    };
    
    $ionicHistory.nextViewOptions({
        disableAnimate: true
    });
    
    var cities = {
        BC: {
            "Surrey": "素里",
            "Vancouver": "温哥华",
            "Victoria": "维多利亚",
            "Burnaby": "本那比",
            "Richmond": "列治文",
            "Abbotsford": "阿伯茨福德",
            "North Vancouver": "北温哥华",
            "Prince George": "乔治王子市",
            "Delta": "三角洲",
            "Kelowna": "基隆拿",
            "Langley": "兰里",
            "Kamloops": "甘露市",
            "Chilliwack": "奇利瓦克",
            "Coquitlam": "高贵林",
            "Nanaimo": "纳奈莫",
            "Maple Ridge": "枫树岭",
            "Port Coquitlam": "高贵林港",
            "West Vancouver": "西温哥华",
            "Vernon": "弗农",
            "Mission": "米逊",
        },
        AB: {
            "Edmonton": "埃德蒙顿",
            "Calgary": "卡尔加里",
            "Fort McMurray": "麦克默里堡",
            "St. Albert": "圣艾伯特",
            "Grande Prairie": "大草原城",
            "Medicine Hat": "梅迪辛哈特",
            "Sherwood Park": "舍伍德帕克",
            "Okotoks": "奥科托克斯",
            "Lethbridge": "莱斯布里奇",
            "Red Deer": "红鹿",
            "Leduc": "勒杜克",
            "Airdrie": "艾尔德里",
            "Stony Plain": "斯托尼普莱恩",
            "Cochrane": "科克伦",
            "Fort Saskatchewan": "萨斯喀彻温堡",
            "Lacombe": "拉科姆",
            "Drayton Valley": "德雷顿瓦利",
            "Spruce Grove": "斯普鲁斯格罗夫",
            "Strathmore": "斯特拉思莫尔",
            "Ardrossan": "阿德罗森",
        },
        ON: {
            "Toronto": "多伦多",
            "Mississauga": "密西沙加",
            "London": "伦敦",
            "Ottawa": "渥太华",
            "Hamilton": "哈密尔顿",
            "Brampton": "布兰普顿",
            "Windsor": "温莎",
            "Kitchener": "基奇纳",
            "Markham": "万锦",
            "Burlington": "伯林顿",
            "Oakville": "奥克维尔",
            "Barrie": "巴利",
            "Guelph": "贵湖",
            "St Catharines": "圣凯瑟琳",
            "Cambridge": "剑桥",
            "Brantford": "布兰特福德",
            "Richmond Hill": "列治文山",
            "Oshawa": "奥沙瓦",
            "Thunder Bay": "桑德贝",
            "Kingston": "金斯顿",
        }
    };
    
    $scope.cities = cities[$stateParams.province];
    
    $scope.city = $stateParams.city;
    
    $scope.public = $stateParams.public;    
        
    if($stateParams.cateID == 3 && $stateParams.province == 'ON'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "安大略省" + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "安大略省中学排名";    
        }
        
    }else if($stateParams.cateID == 4 && $stateParams.province == 'ON'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "安大略省" + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "安大略省小学排名";    
        }
        
    }else if($stateParams.cateID == 3 && $stateParams.province == 'AB'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "阿尔伯特省" + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "阿尔伯特省中学排名";    
        }
        
    }else if($stateParams.cateID == 4 && $stateParams.province == 'AB'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "阿尔伯特省" + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "阿尔伯特省小学排名";    
        }
        
    }else if($stateParams.cateID == 3 && $stateParams.province == 'BC'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "卑诗省" + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "卑诗省中学排名";    
        }
        
    }else if($stateParams.cateID == 4 && $stateParams.province == 'BC'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "卑诗省" + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "卑诗省小学排名";    
        }
        
    };
    
    wechatShare($scope.title, 'http://www.liunar.com/img/schoolShare.jpg', $scope.title);
    
    $scope.showFliter = true;
    
    $scope.changeCity = function(city){
        $state.go("app.schoolRankProvince", {cateID: $stateParams.cateID, city: city, public: $stateParams.public})
    };
    
    $scope.changePublic = function(public){
        $state.go("app.schoolRankProvince", {cateID: $stateParams.cateID, city: $stateParams.city, public: public})
    }
    
    $scope.loadMore = true;
    
    var num = 0;
    
    httpServices.getSchoolListByProvince($stateParams.cateID, 6, num, $scope.city == "all" ? '':$scope.city, $scope.public == "all" ? '' : $scope.public, $stateParams.province).then(
        function(res){
            if(res == "page requested is not available"){
                $scope.loadMore = false;
            }else{
                $scope.loadMore = true;
                $scope.schoolListsRank = res;
            }
        }
    );
    
    $scope.getMoreSchool = function(){
        //console.log(num);
        num ++;
        httpServices.getSchoolListByProvince($stateParams.cateID, 6, num, $scope.city == "all" ? '':$scope.city, $scope.public == "all" ? '' : $scope.public, $stateParams.province).then(
            function(res){
                //console.log(res);
                if(res == "page requested is not available"){
                    $scope.loadMore = false;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }else{
                    $scope.loadMore = true;
                    $scope.schoolListsRank = [].concat($scope.schoolListsRank, res);
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
                
            }
        );
        //console.log(num);
    };
    
}])
.controller('campController', ["$scope", "$http", "httpServices", "$ionicPosition", "$stateParams", function($scope, $http, httpServices, $ionicPosition, $stateParams){
    
    $scope.getCampList = function(){
        
        httpServices.getCampProducts(15).then(
            function(res){
                $scope.camplist = res.all;
            }
        );
    };
}])
.controller('schoolMainController', ["wechatShare", "$scope", function(wechatShare, $scope){
    $scope.$on("$ionicView.beforeEnter", function(){
        wechatShare("加拿大院校排名大全", 'http://www.liunar.com/img/schoolShareV2.jpeg', "大学、学院、中学、小学各种排名一网打尽");    
    });
}])
angular.module('schoolDetailController',[])
.controller('schoolDetailController', ["$scope", "$stateParams", "$sce", "$ionicPopup", "httpServices", "wechatShare", "seedUrl", "schoolDetail", "$ionicModal", "schoolComment", "$state", "schoolTable", "$location", "userAuth", function($scope, $stateParams, $sce, $ionicPopup, httpServices, wechatShare, seedUrl, schoolDetail, $ionicModal, schoolComment, $state, schoolTable, $location, userAuth){
    
    $scope.school = schoolDetail;
    $scope.schoolComment = schoolComment;
    console.log(schoolDetail, schoolComment);
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.shareUrl = seedUrl.createUrl("schoolDetail", schoolDetail.id);
        wechatShare(schoolDetail.en_name + " (" + schoolDetail.ch_name + ")", "http://www.liunar.com/schools/" + schoolDetail.id + "/1.png", schoolDetail.detail_info);
    });
    
    $scope.mapModules = [
        {
            text: "街道地图",
            value: "google.maps.MapTypeId.ROADMAP"
        },
        {
            text: "实景街图",
            value: "streetView"
        },
        {
            text: "鸟瞰地图",
            value: "google.maps.MapTypeId.SATELLITE"
        }
    ];
    
    $scope.selected = $scope.mapModules[0];
    $scope.mapType = $scope.mapModules[0].value;
    
    $scope.select = function(item){
        //console.log(item);
        $scope.selected = item;
        $scope.mapType = item.value;
    };
    
    /////////点评功能以后或许会开放////////
    $ionicModal.fromTemplateUrl('templates/schoolCommentModal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    
    $scope.commentObj = {
        row_id : $stateParams.schoolId,
        content: "",
        module: 2   
    };
    
    var getComm = function(){
      httpServices.getSchoolComment($stateParams.schoolId).then(
          function(res){
             $scope.schoolComment = res;  
          }
      )  
    };
    
    $scope.address = schoolDetail.contact.slice(schoolDetail.contact.indexOf('地址') + 1 +'地址'.length);
    
    var loginStatus = new userAuth.checkLogin();
    
    $scope.leaveComment = function(){
        if(loginStatus.isLogin == false){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $scope.modal.hide();
                    $state.go("userLogin",{lastview: $location.$$path});    
                }
            );
        }else{
            $scope.commentObj.uid = loginStatus.uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                $scope.disable = true;
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $scope.disable = false;
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.modal.hide();
                                $scope.commentObj.content = null;
                                getComm();
                            }
                        ); 
                    }
                );
            }
        }
    };
    
}])
.controller('newSchoolDetailController', ["$ionicHistory", "$scope", "$stateParams", "$sce", "$ionicPopup", "httpServices", "wechatShare", "seedUrl", "schoolDetail", "$ionicModal", "schoolComment", "$state", "schoolTable", "$location", "userAuth", function($ionicHistory, $scope, $stateParams, $sce, $ionicPopup, httpServices, wechatShare, seedUrl, schoolDetail, $ionicModal, schoolComment, $state, schoolTable, $location, userAuth){
    
    $scope.school = schoolDetail;
    $scope.schoolComment = schoolComment;
    
    if($ionicHistory.backView() != null){
        $scope.backUrl = "#" + $ionicHistory.backView().url;
    }else{
        $scope.backUrl = "#" + "/app/schoolrankprovince/" + schoolDetail.type + "/" + schoolDetail.province + "/all/all";
    }
    
    //console.log(schoolDetail);
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.shareUrl = seedUrl.createUrl("schoolDetail", schoolDetail.id);
        wechatShare(schoolDetail.en_name + " (" + schoolDetail.ch_name + ")", "http://www.liunar.com/schools/" + schoolDetail.id + "/1.png", schoolDetail.detail_info);
    });
    
    $scope.mapModules = [
        {
            text: "街道地图",
            value: "google.maps.MapTypeId.ROADMAP"
        },
        {
            text: "实景街图",
            value: "streetView"
        },
        {
            text: "鸟瞰地图",
            value: "google.maps.MapTypeId.SATELLITE"
        }
    ];
    
    $scope.selected = $scope.mapModules[0];
    $scope.mapType = $scope.mapModules[0].value;
    
    $scope.select = function(item){
        //console.log(item);
        $scope.selected = item;
        $scope.mapType = item.value;
    };
    
    var rankData;

    if(schoolDetail.overall_rating !== null){
        rankData = schoolDetail.overall_rating.split(",");
        rankData.pop();
        //console.log(rankData.indexOf("n/a"))
        if(rankData.indexOf("n/a") !== -1){
            rankData.forEach(
                function(a, i){
                    if(a == "n/a"){
                        rankData[i] = 2.5;
                    }
                }
            )
        }
    }else{
        rankData = [];
    }
    
    $scope.labels = ["2011","2012","2013","2014","2015"];
    $scope.data = [
        rankData
    ];
    $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
    $scope.options = {
        scaleOverride: true,
        scaleSteps: 4,
        scaleStepWidth: 2.5,
        scaleStartValue: 0
    };
    
    $scope.tableInit = function(){
        var renderedTable = {};

        if(schoolDetail.other !== null){
            var table = schoolDetail.other.split(";");
            table.pop();
            table.forEach(
                function(a,i){
                    //console.log(a,i);
                    var b = a.split(":");
                    //console.log(b);
                    if(b[1].indexOf(",")!==-1){
                        var c = b[1].split(",");
                        c.pop()
                        renderedTable[b[0]] = c;
                    }else{
                        $scope.firstValue = b[1];
                        $scope.firstKey = schoolTable[b[0]];
                    }
                }
            )
        }
        
        $scope.table = renderedTable;
    };
    
    $scope.translate = schoolTable;
    
    /////////点评功能以后或许会开放////////
    $ionicModal.fromTemplateUrl('templates/schoolCommentModal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    
    $scope.commentObj = {
        row_id : $stateParams.schoolId,
        content: "",
        module: 2   
    };
    
    var getComm = function(){
      httpServices.getSchoolComment($stateParams.schoolId).then(
          function(res){
             $scope.schoolComment = res;  
          }
      )  
    };
    
    var loginStatus = new userAuth.checkLogin();
    
    $scope.leaveComment = function(){
        if(loginStatus.isLogin == false){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $scope.modal.hide();
                    $state.go("userLogin",{lastview: $location.$$path});    
                }
            );
        }else{
            $scope.commentObj.uid = loginStatus.uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                $scope.disable = true;
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $scope.disable = false;
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.modal.hide();
                                $scope.commentObj.content = null;
                                getComm();
                                //$scope.getPostDetail();
                            }
                        ); 
                    }
                );
            }
        }
    };
    
}])
.controller('campDetailController', ["$scope", "$stateParams", "campDetail", "$sce", function($scope, $stateParams, campDetail, $sce){
    //console.log(campDetail, $sce);
    $scope.school = campDetail;
    $scope.daily = $sce.trustAsHtml(campDetail["daily_schedule"]);
    $scope.detail = $sce.trustAsHtml(campDetail["personalize"])
    //console.log(Object.keys(campDetail), campDetail["daily_schedule"]);
}]);
angular.module("search",[])
.controller("tagsController", ["$scope", function($scope){
    $scope.tags = [
        {
            cate: "国家",
            icon: "fa fa-globe",
            tags: [
                "加拿大", "美国", "澳大利亚", "英国", "新西兰", "北欧", 
            ]
        },
        {
            cate: "阶段",
            icon: "fa fa-line-chart",
            tags: [
                "低龄", "本科", "研究生"
            ]
        },
        {
            cate: "留学周期",
            icon: "fa fa-clock-o",
            tags: [
                "留学前", "留学中", "留学后"
            ]
        },
        {
            cate: "专业",
            icon: "fa fa-graduation-cap",
            tags: [
                "文科专业", "理工科专业", "医科专业", "商科专业", "职业类专业", "艺术类专业", "热门专业"
            ]  
        },
        {
            cate: "考试",
            icon: "fa fa-pencil-square-o",
            tags: [
                "SAT/SSAT", "GMAT", "GRE", "托福", "雅思", "IB", "AP"
            ]  
        },
        {
            cate: "签证",
            icon: "fa fa-plane",
            tags: [
                "学习签证", "工作签证", "旅游签证", "陪读签证"
            ]  
        },
        {
            cate: "经验分享",
            icon: "fa fa-bullhorn",
            tags: [
                "留学申请经验", "留学生活经验", "留学就业经验", "移民经验", "留学规划"
            ]  
        },
        {
            cate: "常识",
            icon: "fa fa-question-circle",
            tags: [
                "留学生活", "衣食住行", "留学误区", "留学安全", "国家文化", "娱乐"
            ]  
        },
        {
            cate: "留学成本",
            icon: "fa fa-usd",
            tags: [
                "低成本留学", "中端预算留学", "高端留学", 
            ]  
        },
        {
            cate: "移民",
            icon: "fa fa-home",
            tags: [
                "移民政策", "移民申请", "移民生活", "移民资讯"
            ]  
        },
        {
            cate: "就业",
            icon: "fa fa-briefcase",
            tags: [
                "打工", "实习", "留学生就业", "留学就业薪资", "工作面试", "留学职业规划"
            ]  
        },
        {
            cate: "出国目的",
            icon: "fa fa-bullseye",
            tags: [
                "留学", "移民", "游学", "旅游", "工作", "陪读"
            ]  
        },
        {
            cate: "专题",
            icon: "fa fa-hashtag",
            tags: [
                "美国中学", "加拿大中学", "海外游学", "美国本科留学", "加拿大本科留学", "美国研究生留学", "加拿大研究生留学", "加拿大移民", "美国移民", "亲子活动", "讲座"
            ]  
        },
    ];
    
    $scope.clicked = $scope.tags[0];
    $scope.select = function(item){
        console.log(item);
        $scope.clicked = item;
    };
    
    console.log($scope.clicked);
    
}])
.controller("schoolSearchController", ["$state", "$scope", "$ionicHistory", "$http", "$q", function($state, $scope, $ionicHistory, $http, $q){
    $scope.back = function() {
        if($ionicHistory.backView() == null){
            $state.go("app.school");   
        }else{
            $ionicHistory.goBack();    
        }
        //console.log($ionicHistory, $ionicHistory.backView());
    };
    
    $scope.search = function(kw){
        if(kw.length > 0){
            $http({
                url: "/search/school?kw=" + kw,
                method: "GET",
            }).then(
                function(res){
                    //console.log(res);
                    $scope.results = res.data.slice(0,9);
                },
                function(err){
                    //console.log(err);
                }
            );
        }else{
            $scope.results = null;
            console.log("empty");
        }
    };
    
}])
.controller("tagSearchController", ["$stateParams", "$state", "$scope", "$ionicHistory", "$http", "$q", function($stateParams, $state, $scope, $ionicHistory, $http, $q){
    $scope.back = function() {
		if($stateParams.tag == "" || $ionicHistory.backView() == null){
			window.location.href="http://www.liunar.com"
		}else{
        	$state.go("tagSquare");
		}
        //console.log($ionicHistory, $ionicHistory.backView());
    };
    
    $scope.more = true;
    
    $scope.tag = $stateParams.tag;
    
    var start;
    
    var end;
    
    var tagSearch = function(kw,index, limit){
        var deferred = $q.defer();
        
        console.log(index);
        if(kw.length > 0){
            $http({
                url: "/search/search?kw=" + kw + "&start=" + index  + "&limit=" + limit, //+ "&type=" + type ,
                method: "GET",
            }).then(
                function(res){
                    deferred.resolve(res.data);
                },
                function(err){
                    deferred.resolve([]);
                    
                }
            );
        }else{
            console.log("empty");
            deferred.resolve([]);
        }
        return deferred.promise;
    };
    
    $scope.resultInit = function(){
        start = 0;
        end = 7;
        console.log("start from: " + start + ", " + end);
        tagSearch($scope.tag, start, end).then(
            function(res){
                $scope.results = res;
                if(res.length < 1){
                    $scope.more = false;
                }else{
                    $scope.more = true;
                }
                console.log("begin: ", $scope.results);
            }
        )
    };
    
    $scope.inputSearch = function(tag){
        start = 0;
        end = 7;
        console.log(tag);
        console.log("input=>start from: " + start + ", " + end);
        tagSearch(tag, start, end).then(
            function(res){
                if(res.length < 1){
                    $scope.results = null;
                    $scope.more = false;
                }else{
                    $scope.results = res;
                    $scope.more = true;
                }
                console.log($scope.results);
            }
        );
    };
    
    $scope.searchMore = function(){
        start = start + 8;
        end = end + 8;
        console.log("continue: " + start + ", " + end);
        tagSearch($scope.tag, start, end).then(
            function(res){
                if(res.length < 1){
                    $scope.more = false;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }else{
                    $scope.more = true;
                    $scope.results = [].concat($scope.results, res);
                    console.log("add: " + res.length, "after add: " + $scope.results.length);
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
            }
        );
    };
    
    //console.log($scope.results);
    
}]);
angular.module('userController',[])
.controller("userLogin", ["$ionicLoading", "wechatShare", "$state", "userAuth", "$window", "$stateParams", "$rootScope", "$scope", "localStorage", "detectBrowser", function($ionicLoading, wechatShare, $state, userAuth, $window, $stateParams, $rootScope, $scope, localStorage, detectBrowser){
	$scope.$on("$ionicView.beforeEnter", function(){
		//alert(detectBrowser.isWechat());
		if(detectBrowser.isWechat() === false){
			$scope.showBroMsg = true;
		}
	});
	
    $scope.$on("$ionicView.afterEnter", function(){
        if(userAuth.checkUcLogin().isLogin !== true){
			if($stateParams.lastview && $stateParams.lastview !== ""){
				localStorage.setItem("lastview", $stateParams.lastview);
			}
        }else{
            $state.go("app.userCenter");
        }
    });
    $scope.wechatRedirect = function(){
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner><br/>正在登录中...',
            noBackDrop: true
        });
        $window.location.assign("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx0933bfa899eedcaf&redirect_uri=http%3A%2F%2Fwww.liunar.com%2Fmobile%2F%23%2FuserLogin&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect");
        setTimeout(
            function(){
                $window.location.reload();
                $ionicLoading.hide();
            }, 8000
        );
    };
    wechatShare("登录留哪儿","http://liunar.com/img/icon.png","登录留哪儿，开启知识分享之旅");
    
}])
.controller('userController', ["httpServices", "$scope", "localStorage", "$state", "$ionicPopup", "$ionicHistory", "$rootScope", "$location", "$stateParams", "urlConfig", "userAuth", "$window", "$cookies", function(httpServices, $scope,localStorage,$state,$ionicPopup,$ionicHistory,$rootScope,$location, $stateParams, urlConfig, userAuth, $window, $cookies){
    
    console.log($cookies);
    $rootScope.hideTabs = '';
    
    /*var getVipRight = function(){
        if(localStorage.getItem("referId") && localStorage.getItem("referId") !== null){
            httpServices.getRefCodeById(localStorage.getItem("referId")).then(
                function(res){
                    if(res.err){
                        $scope.vipRight = false;
                        $scope.ref_code = null;
                    }else if(res.reference_code){
                        $scope.vipRight = true;
                        $scope.ref_code = res.reference_code;
                    }
                }
            )
        }else{
            $scope.vipRight = false;
            $scope.ref_code = null;    
        }    
    };*/
    
    $scope.agentTypes = ['普通达人','认证达人','正在审核中','审核未通过','达人身份被取消'];
    
    var user = new userAuth.checkUcLogin();
    
    var userInit = function(){
        if(user.isLogin !== true){ 
            $state.go("userLogin");
        }else{
            httpServices.extractUserInfo(user.uid).then(
                function(res){
                    //console.log(res);
                    $scope.totalPoints = res.loginPoints + res.userIntroPoints + res.salePoints + res.sharePoints;
                    if(res.agentType == 'null'){
                        $scope.agentType = 0;    
                    }else{
                        $scope.agentType = res.agentType;    
                    }
                    $scope.reference_code = res.reference_code;
                    $scope.phone = res.phone;
                    $scope.avatar = res.avatar;
                    $scope.nickname = res.nickname;
                    $scope.fullname = res.fullname;
                    //console.log($scope.agentType);
                }
            );
            
            httpServices.extractUserPoints(user.uid).then(
                function(res){
                    //console.log(res);
                    $scope.pointHistory = res;
                    $scope.pointTypes = ['首次微信授权','引进新用户','产品销售','内容分享'];
                }
            );
        }
    };
    
    $scope.$on("$ionicView.beforeEnter", function(event){
        //console.log(event);
        userInit();
        //getVipRight();
    });
    
    $scope.logout = function(){
        localStorage.clear();
        /*var cookies = $cookies.getAll();
        angular.forEach(cookies, function (v, k) {
            $cookies.remove(k);
        });*/
        $cookies.putObject("user",{"":""});
        //window.location.href="http://www.liunar.com";
        window.location.reload();
    };
    
    $scope.selectCode = function(){
        var sel = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(document.getElementById("refCode"));
        sel.removeAllRanges();
        sel.addRange(range);
        
        console.log(sel, range, window.clipboardData);
        alert(sel);
    };
    
}])
.controller("userOrder", ["$scope", "httpServices", "userAuth", function($scope, httpServices, userAuth){
    var user = new userAuth.checkUcLogin();
    var orderInit = function(){
        if(user.isLogin !== true){ 
            $state.go("userLogin");
        }else{
            httpServices.extractUserOrder(user.uid)
            .then(function(res){
                console.log(res);
                $scope.orderList = res;
                $scope.status = ["新客户","咨询","放弃","已购买","未付款"];
            });
        }  
    };
    
    $scope.$on("$ionicView.beforeEnter", function(event){
    	orderInit();
    });
    
}])
.controller("userClient", ["httpServices", "$scope", "userAuth", "$state", function(httpServices, $scope, userAuth, $state){
    var user = new userAuth.checkUcLogin();
    var clientInit = function(){
        if(user.isLogin !== true){ 
            $state.go("userLogin");
        }else{
            httpServices.extractUserClient(user.uid)
            .then(function(res){
                //console.log(res);
                $scope.clientList = res;
                $scope.status = ["新客户","咨询","放弃","已购买","未付款"];
            });
            httpServices.extractUserReferer(user.uid)
            .then(function(res){
                console.log(res);
                $scope.referList = res;
            });
        }
    };
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.selected = 2;
        clientInit();
    });
}])
.controller("userClientOrderDetailController", ["$scope", "$stateParams", "httpServices", function($scope, $stateParams, httpServices){
    httpServices.extractUserClientMemo($stateParams.orderId).then(
        function(res){
            if(!res.err){
                $scope.memos = res.data;
            }else{
            }
        }
    );
}])
.controller("userRecommend", ["$state", "$rootScope", "userAuth", "introduceNewUser", "httpServices", "$scope", function($state, $rootScope, userAuth, introduceNewUser, httpServices, $scope){
    
    var user = new userAuth.checkLogin();
    
    if(user.isLogin !== true){ 
        $state.go("userLogin");
    }else{
        httpServices.extractUserInfo(user.uid).then(
            function(res){
                //console.log(res);
                $scope.reference_code = res.reference_code;
                introduceNewUser($rootScope.shareState, res.fullname);
            }
        );
    }
}])
.controller("userShare", ["introduceNewUser", "userAuth", "$rootScope", "$scope", "localStorage", "$state", "httpServices", "detectBrowser", function(introduceNewUser, userAuth, $rootScope, $scope, localStorage, $state, httpServices, detectBrowser){
    var user = new userAuth.checkUcLogin();
    function creatWxUrl(id){
        if(detectBrowser.isWechat() == true){
            return "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1eabd0cb2070a271&redirect_uri=http%3A%2F%2Fyun.liunar.com/account/wxauth/" + id +"&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect";
        }else{
            return "https://open.weixin.qq.com/connect/qrconnect?appid=wxa9717b409b6a35a2&redirect_uri=http%3A%2F%2Fyun.liunar.com%2Faccount%2Fwx%2F" + id + "%2F&response_type=code&scope=snsapi_login#wechat_redirect";
        }
    };

    $scope.options = {
        direction: "vertical",
        paginationType: "fraction",
        grabCursor: true
    }
    
    $scope.url = creatWxUrl(localStorage.getItem('referId'));
    if(user.isLogin !== true){
        $scope.showShareMethod = false;
        httpServices.extractUserInfo(localStorage.getItem('referId')).then(
            function(res){
                //console.log(res);
                introduceNewUser("?referid=" + localStorage.getItem('referId'), res.fullname);
                $scope.refCode = res.reference_code;

                //$scope.url = creatWxUrl(localStorage.getItem('referId'));
                
            }
        );
    }else{
        httpServices.extractUserInfo(user.uid).then(
            function(res){
                //console.log(res, $rootScope.shareState);
                if(res.agentType == 1){
                    introduceNewUser($rootScope.shareState, res.fullname);  
                    $scope.refCode = res.reference_code;
                    $scope.showShareMethod = true;
                    //$scope.url = creatWxUrl(localStorage.getItem('referId'));;
                }else{
                    $scope.showShareMethod = false;
                    httpServices.extractUserInfo(localStorage.getItem('referId')).then(
                        function(res){
                            //console.log(res);
                            introduceNewUser("?referid=" + localStorage.getItem('referId'), res.fullname);
                            $scope.refCode = res.reference_code;
                            //$scope.url = creatWxUrl(localStorage.getItem('referId'));
                        }
                    );   
                }
            }
        );
    }
    
}])
.controller("userUpload", ["localStorage", "$rootScope", "userAuth", "$state", "$scope", "urlConfig", "httpServices", "$ionicPopup", function(localStorage, $rootScope, userAuth, $state, $scope, urlConfig, httpServices, $ionicPopup){

        var user = new userAuth.checkUcLogin();
    
        $rootScope.hideTabs = '';
    
        if(user.isLogin !== true){
            $ionicPopup.alert({
                title: "留哪儿",
                template: "请微信授权后再申请认证达人"
            }).then(
                function(){
                    $state.go("userLogin",{lastview: '/app/userapplyagent'});     
                }
            ); 
        }else{
            
            httpServices.extractUserInfo(user.uid).then(
                function(res){
                    if(res.agentType === 1){
                        $ionicPopup.alert({
                            title: "留哪儿",
                            template: "您已经是留哪儿的认证达人了"
                        }).then(
                            function(){
                                $state.go("app.userCenter");     
                            }
                        );     
                    }
                }
            );
            
            $scope.new = {
                phone: ""
            }

            /*$scope.update = function(){
                if($scope.new.phone !== ""){
                    httpServices.updateUserInfo(user.uid, $scope.new.phone).then(
                        function(res){
                            //console.log(res);
                        }
                    );
                } 
            };*/

            //uploadStatus: a => uploading; b => success; c => fail; 
            
            $scope.uploadID = function(file,err){
                //console.log(file,err);
                if(file !== null){
                    $scope.idUploadProgress = 0;
                    httpServices.uploadUserFiles(file, 2, user.uid).then(
                        function(res){
                            //console.log(res);
                            if(res.data.file){
                                $scope.idUploadStatus = 'b';
                                $scope.statusText = "上传成功！";
                                $scope.statusColor = "#9BCF00";
                            }else if(res.data.err){
                                $scope.idUploadStatus = 'c';
                                $scope.statusText = "上传失败！请重试";
                                $scope.statusColor = "#942200";
                            };
                            //console.log($scope.idUploadStatus);
                        },
                        function(res){
                            //console.log(res);
                            $scope.idUploadStatus = 'c';
                            $scope.statusText = "上传失败！请重试";
                            $scope.statusColor = "#942200";
                        },
                        function(res){
                            $scope.idUploadStatus = 'a';
                            $scope.idUploadProgress = res;
                            //console.log(res, $scope.idUploadStatus);
                        }
                    );
                }else{
                    $scope.idUploadStatus = 'c';
                    $scope.statusText = "请选择有效的文件类型－PDF或者图片文件";
                    $scope.statusColor = "#942200";    
                }
            };

            $scope.errText = "";
            
            $scope.slogan = "更聪明的留学与移民";
            
            if(localStorage.getItem("referId") && localStorage.getItem("referId") != null){
                httpServices.getRefCodeById(localStorage.getItem("referId")).then(
                    function(res){
                        console.log(res);
                        if(!res.err){
                            $scope.refCode = res.reference_code;
                        }
                    }
                )
            }else{
                $scope.refCode = null;
            }

            $scope.applyAgent = function(refCode, name, phone, slogan, wechat, email){
                //console.log(name, phone, slogan);
                if($scope.idUploadStatus == 'b'){
                    if(refCode !== null && refCode !== undefined){
                        httpServices.getIdByRefCode(refCode).then(
                            function(res){
                                if(res.err){
                                    $scope.errText = "您输入的并不是有效推荐码";    
                                }else{
                                    if(name !== undefined && phone !== undefined){
                                        if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test(phone) == true){
                                            httpServices.updateUserInfo(user.uid, phone, name, slogan, wechat, res.id, email).then(
                                                function(res2){
                                                    //console.log(res2);
                                                    $scope.errText = "";
                                                    if(!res2.err){
                                                        $ionicPopup.alert({
                                                            title: '留哪儿',
                                                            template: '恭喜您提交申请成功，我们会尽快审核'
                                                        }).then(
                                                            function(){
                                                                $state.go("app.userApplyStatus");
                                                            }
                                                        );    
                                                    }
                                                }
                                            );
                                        }else{
                                            $scope.errText = "电话号码只能由 +,-,0-9组成";    
                                        }
                                    }else{
                                        $scope.errText = "姓名与电话号码不能为空";
                                    }    
                                }
                            }
                        );
                    }else{
                        $scope.errText = "推荐码不能为空";    
                    }
                }else{
                    $scope.errText = "请成功上传身份信息文件后再提交";
                }
            };
        }
}])
.controller('promoteController', ["$rootScope", "$scope", "wechatShare", function($rootScope, $scope, wechatShare){
    $scope.$on("$ionicView.beforeEnter", function(){
        
        $rootScope.hideTabs = 'tabs-item-hide';
        
        wechatShare('招募留学教育合伙人', "http://www.liunar.com/img/vip1.jpg", '零投入，无需场地，共享经济新模式');
        
    });
}])
.controller("testUpload", ["$rootScope", "localStorage", "$state", "$scope", "urlConfig", "httpServices", "$ionicPopup", function($rootScope, localStorage, $state, $scope, urlConfig, httpServices, $ionicPopup){
            $scope.new = {
                phone: ""
            }
            
            httpServices.getRefCodeById(localStorage.getItem("referId") || 0).then(
                function(res){
                    //console.log(res);
                    $scope.referenceCode = res.reference_code;
                }
            )

            $scope.update = function(){
                if($scope.new.phone !== ""){
                    httpServices.updateUserInfo(62, $scope.new.phone).then(
                        function(res){
                            //console.log(res);
                        }
                    );
                } 
            };
            
            //uploadStatus: a => uploading; b => success; c => fail; 
            
            $scope.uploadID = function(file,err){
                //console.log(file,err);
                if(file !== null){
                    $scope.idUploadProgress = 0;
                    httpServices.uploadUserFiles(file, 2, 62).then(
                        function(res){
                            //console.log(res);
                            if(res.data.file){
                                $scope.idUploadStatus = 'b';
                                $scope.statusText = "上传成功！";
                                $scope.statusColor = "#9BCF00";
                            }else if(res.data.err){
                                $scope.idUploadStatus = 'c';
                                $scope.statusText = "上传失败！请重试";
                                $scope.statusColor = "#942200";
                            };
                            //console.log($scope.idUploadStatus);
                        },
                        function(res){
                            //console.log(res);
                            $scope.idUploadStatus = 'c';
                            $scope.statusText = "上传失败！请重试";
                            $scope.statusColor = "#942200";
                        },
                        function(res){
                            $scope.idUploadStatus = 'a';
                            $scope.idUploadProgress = res;
                            //console.log(res, $scope.idUploadStatus);
                        }
                    );
                }else{
                    $scope.idUploadStatus = 'c';
                    $scope.statusText = "请选择有效的文件类型－PDF或者图片文件";
                    $scope.statusColor = "#942200";    
                }
            };

            /*$scope.uploadQRcode = function(file, err){
                //console.log(file);
                if(file !== null){
                    $scope.qrUploadProgress = 0;
                    httpServices.uploadUserFiles(file, 2, 62).then(
                        function(res){
                            //console.log(res);
                            if(res.data.file){
                                $scope.qrUploadStatus = 'b';
                                $scope.statusText = "上传成功！";
                                $scope.statusColor = "#9BCF00";
                            }else if(res.data.err){
                                $scope.qrUploadStatus = 'c';
                                $scope.statusText = "上传失败！请重试";
                                $scope.statusColor = "#942200";
                            };
                            //console.log($scope.qrUploadStatus);
                        },
                        function(res){
                            //console.log(res);
                            $scope.qrUploadStatus = 'c';
                            $scope.statusText = "上传失败！请重试";
                            $scope.statusColor = "#942200";
                        },
                        function(res){
                            $scope.qrUploadStatus = 'a';
                            $scope.qrUploadProgress = res;
                            //console.log(res, $scope.qrUploadStatus);
                        }
                    );
                }else{
                    $scope.qrUploadStatus = 'c';
                    $scope.statusText = "请选择有效的文件类型－图片文件";
                    $scope.statusColor = "#942200";    
                }
            };*/

            $scope.errText = "";

            $scope.applyAgent = function(refCode, name, phone, slogan, wechat, email){
                //console.log(name, phone, slogan);
                if($scope.idUploadStatus == 'b'){
                    if(refCode !== undefined){
                        httpServices.getIdByRefCode(refCode).then(
                            function(res){
                                //console.log(res);
                                if(res.err){
                                    $scope.errText = "您输入的并不是有效推荐码";    
                                }else{
                                    if(name !== undefined && phone !== undefined){
                                        if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test(phone) == true){
                                            httpServices.updateUserInfo(62, phone, name, slogan, wechat, res.id, email).then(
                                                function(res2){
                                                    //console.log(res2);
                                                    $scope.errText = "";
                                                    if(!res2.err){
                                                        $ionicPopup.alert({
                                                            title: '留哪儿',
                                                            template: '恭喜您提交申请成功，我们会尽快审核'
                                                        }).then(
                                                            function(){
                                                                $state.go("app.userApplyStatus");
                                                            }
                                                        );    
                                                    }
                                                }
                                            );
                                        }else{
                                            $scope.errText = "电话号码只能由 +,-,0-9 组成";    
                                        }
                                    }else{
                                        $scope.errText = "姓名与电话号码不能为空";
                                    }    
                                }
                            }
                        );
                    }else{
                        $scope.errText = "推荐码不能为空";    
                    }
                }else{
                    $scope.errText = "请成功上传身份信息文件后再提交";
                }
            };
}]);
