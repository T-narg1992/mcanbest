angular.module("canbestServer", [])
.factory('detectBrowser', function($window, $timeout){
    return {
        isWechat: function(){
            //OPTION ONE:
            var result = (/MicroMessenger/i).test($window.navigator.userAgent);
            return result;
        }
    }
})
.factory('dataStore', function(){
    var setData = {};
    return {
        set: function(data){
            setData = data;
        },
        get: function(){
            return setData;
        }
    }
})
.factory('localStorage', function ($window, $ionicHistory, $q) {
    return {
        setItem: function (key, value) {
            $window.localStorage.setItem(key, value);
        },
        getItem: function (key, defaultValue) {
            return $window.localStorage.getItem(key) || null;
        },
        setObject: function (key, obj) {
            $window.localStorage.setItem(key, JSON.stringify(obj));
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage.getItem(key) || 'null');
        },
        remove: function(key){
            return $window.localStorage.removeItem(key);  
        },
        clear: function () {
            $window.localStorage.clear();
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
        }
    }
})
.factory('userAuth', function(localStorage, $cookies){
    return {
        checkUcLogin: function(){
            if(localStorage.getObject('canbestUserInfo') !== null && localStorage.getObject("canbestUserInfo").uid){
                return {
                    isLogin: true,
                    uid: localStorage.getObject("canbestUserInfo").uid
                };
            }else{
                return {
                    isLogin: false,
                    uid: null
                };
            }
        },
        checkLogin: function(){
            //var haha = $cookies.getObject('user');
            //console.log(haha);
            //alert($cookies.getObject('user'));
            if(localStorage.getObject('canbestUserInfo') !== null && localStorage.getObject("canbestUserInfo").uid){
                return {
                    isLogin: true,
                    uid: localStorage.getObject("canbestUserInfo").uid
                };
            }else if($cookies.getObject('user') && $cookies.getObject('user').uid){
                return {
                    isLogin: true,
                    uid: $cookies.getObject('user').uid
                };
            }else{
                return {
                    isLogin: false,
                    uid: null
                };
            }   
        }
    }
})
.factory('httpServices', function ($location, $window, $http, $q, urlConfig, Upload, userAuth) {
    return {

        urlFour: urlConfig.url, 
        
        urlTwo: "http://www.canbest.org/",

        wechatLogin: function (code, reId) {

            var deferred = $q.defer();

            var url = this.urlFour + "usercenter/wx/" + code + "/" + reId;

            $http({
                url: url,
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });

            return deferred.promise;

        },
        
        extractUserInfo: function(id){
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "usercenter/user/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });

            return deferred.promise;
        },
        
        extractUserPoints: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/points/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        extractUserClient: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/clients/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;    
        },
        
        extractUserClientMemo: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/memo/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;    
        },
        
        extractUserOrder: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/orders/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise; 
        },
        
        extractUserReferer: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/referees/" + id.toString(),
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;  
        },
        
        updateUserInfo: function(id, phone, fullname, slogan, wechat, uid, email){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/profile/" + id.toString(),
                method: "POST",
                data: {
                    introducer: uid, 
                    phone: phone, 
                    fullname: fullname, 
                    agentType: '2', 
                    slogan: slogan, 
                    wechat_Id: wechat == undefined ? "" : wechat,
                    email: email == undefined ? "" : email
                }
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;     
        },
        
        uploadUserFiles: function(file, type, userId){
            var deferred = $q.defer();
            Upload.upload({
                url: this.urlFour + "usercenter/user/upload/" + type + "/" + userId,
                file: file,
            }).then(
            function (res) {
               deferred.resolve(res);
           }, function (resp) {
            deferred.reject(resp);
        }, function(evt) {
            var progress = Math.round((evt.loaded/evt.total) * 100);
            deferred.notify(progress);
        });
            return deferred.promise
        },
        
        userOrder: function(uid, pid, phone, fullname, aid, rCode){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "usercenter/user/neworder/" + uid.toString() + "/" + pid.toString(),
                method: "POST",
                data: {phone: phone, fullname: fullname, agentId: aid, ref_code: rCode}
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise; 
        },
        
        getUserMission: function(){
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: this.urlFour + "articles/placeholders/detail/34020",
            }).then(
            function(res){
                deferred.resolve(res.data[0]);   
            }
            ); 
            return deferred.promise;
        },
		
		getHotItems: function(){
			var deferred = $q.defer();
            $http({
                method: "GET",
                url: this.urlFour + "articles/placeholders/detail/34680",
            }).then(
            function(res){
                deferred.resolve(res.data[0]);   
            }
            ); 
            return deferred.promise;
		},
        
        getMissionLists: function(){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "statistics/get/dailyTasks",
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getRefCodeById: function(id){
           var deferred = $q.defer();
           $http({
            url: this.urlFour + "usercenter/user/getrefercode/" + id.toString(),
            method: "GET"
        }).then(
        function (res) {
            deferred.resolve(res.data);
        });
        return deferred.promise;  
    },

    getIdByRefCode: function(code){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "usercenter/user/getByReferCode/" + code.toString(),
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res.data);
        });
        return deferred.promise;
    },

    apply: function (obj) {
        var deferred = $q.defer();

        $http({
            url: this.urlTwo + "index.php?s=/home/index/post.html",
            method: "POST",
            data: obj,
        }).then(
        function (res) {
            deferred.resolve(res.data);
        }
        );
        return deferred.promise;
    },

    getPostDetails: function(id){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "articles/detail/"+ id.toString(),
                    //url: this.urlOne + "index.php?s=News/index/apiGetNewsDetail/id/" + id.toString() + ".html",
                    method: "GET",
                }).then(
                function(res){
                    deferred.resolve(res.data);
                });
                return deferred.promise;
            },

            getPostDetailsComment: function(id){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "comments/1/" + id.toString(),
                    method: "GET",
                }).then(
                function(res){
                    deferred.resolve(res.data);
                });
                return deferred.promise;
            },

            addView: function(id){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "articles/view/" + id,
                    method: "GET",
                }).then(
                function(res){
                    deferred.resolve(res);
                });
                return deferred.promise;    
            },

            postComment: function(obj){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "comments",
                    method: "POST",
                    data: obj
                }).then(
                function(res){
                    deferred.resolve(res);
                });
                return deferred.promise;
            },

            postDelete: function(body){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "square/delete",
                    method: "POST",
                    data: body
                }).then(
                function(body){
                    deferred.resolve(body);
                });
                return deferred.promise;
            },


            getLiunarProductList: function(typeId){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "products/list/" + typeId.toString(),
                    method: "GET"
                }).then(
                function(res){
                    deferred.resolve(res.data);
                }
                );
                return deferred.promise;
            },

            getProductPromo: function(){
                var deferred = $q.defer();
                $http({
                    url: this.urlFour + "articles/placeholders/1",
                    method: "GET"
                }).then(
                function(res){
                    deferred.resolve(res.data);
                }
                );
                return deferred.promise;
            },

            getProductList: function(){
             var deferred = $q.defer();
             $http({
                url: this.urlFour + "products/list/promo/all",
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getProductListNew: function(){
         var deferred = $q.defer();
         $http({
            url: this.urlFour + "productnew/list/promo/all",
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res.data);
        }
        );
        return deferred.promise;
    },

    getAllProducts: function(limit){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "products/list/all/" + limit.toString(),
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getAllProductsNew: function(limit){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "productnew/list/all/" + limit.toString(),
            method: "GET"
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getCountryProducts: function(country){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "products/list/country/" + country,
            method: "GET",
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getCountryProductsNew: function(country){
        var deferred = $q.defer();
        $http({
            url: this.urlFour + "productnew/list/country/" + country,
            method: "GET",
        }).then(
        function(res){
            deferred.resolve(res);
        }
        );
        return deferred.promise;
    },

    getShopProductDetail: function(productId){
        var deferred = $q.defer();
        $http({
                //url: this.urlOne + "index.php?s=/issue/index/apiGetRankingList&issue_id=" + schoolCateId.toString(),
                url: this.urlFour + "products/" + productId.toString(),
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        getProductDetailNew: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "productnew/detail/" + id.toString(),
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        getProductAccessAuthorization: function(uid,refcode){
            var deferred = $q.defer();
            var url;
            if(refcode){
                url = this.urlFour + "productnew/authorizeClient/" + uid + "/" + refcode;
            }else{
                url = this.urlFour + "productnew/authorizeClient/" + uid;
            }
            console.log(url);
            $http({
                url: url,
                method:"GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            },function(err){
                deferred.reject(err);
            }
            );
            return deferred.promise;
        },
        getAuthorizedProductData: function(prodid, uid,refcode){
            var deferred = $q.defer();
            var url;
            if(refcode){
                url = this.urlFour + "productnew/get/memberOnlyDetails/" + prodid +'/'+ uid + "/" + refcode;
            }else{
                url = this.urlFour + "productnew/get/memberOnlyDetails/" + prodid + '/' + uid;
            }
            console.log(url);
            $http({
                url: url,
                method:"GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            },function(err){
                deferred.reject(err);
            }
            );
            return deferred.promise;
        },
        
        getHomeEvents: function(){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "articles/events",
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);    
            }
            );
            return deferred.promise;
        },
        
        getEvents: function(count, index){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "articles/events/all/" + count + "/" + index,
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);    
            }
            );
            return deferred.promise;
        },

        getHomePosts: function () {
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/promo",
                method: "GET",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        getHomePlaceholder: function(){
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/placeholders/0",
                method: "GET",
            }).then(
            function (res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
            
        },

        getGongLueList: function (count, index) {
            var that = this;
            var deferred = $q.defer();

            $http ({
                url: this.urlFour + "articles/list/" + count + "/" + index + "/",
                method: "GET",
                timeout: "2000"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            },
            function(err, status){
                that.getGongLueList(count,index).then(
                    function(res){
                        deferred.resolve(res);
                    }
                    )
            }
            );
            return deferred.promise;
        },

        getLiunarProductList: function (typeId) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "products/list/" + typeId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        getLiunarPastCase: function (typeId) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "pastcases/list/" + typeId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getCampProducts: function (schoolCateId) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/issue/index/apiGetSchools&issue_id=" + schoolCateId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getCampProductDetail: function (id) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/issue/index/apiGetProductDetail&id=" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getCities:function(){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "school/cities",
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;  
        },
        
        getSchoolRankList: function (schoolCateId, public, city, limit) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "school/" + schoolCateId.toString() + "/1000/0",
                method: "GET",
                cache: true
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getSchoolListByProvince: function(cateId, num, page, city, public, province){
            var deferred = $q.defer();
            //isPublic=1&province=bc&city=burnaby
            $http({
                url: this.urlFour + "school/" + cateId.toString() + "/" + num + "/" + page + "?city=" + city + "&isPublic=" + public + "&province=" + province,
                method: "GET",
                //cache: true,
            }).then(
            function (res) {
                console.log(res);
                deferred.resolve(res.data);
            },
            function(err){
                deferred.reject(err);
            }
            );
            return deferred.promise;
        },

        getSchoolDetail: function (id) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "schools/" + id.toString(),
                //url: this.urlFour + "school/school_standard/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data[0]);
            }
            );
            return deferred.promise;
        },
        
        getSchoolProvinceDetail: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "school/school_standard/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data[0]);
            }
            );
            return deferred.promise;
        },
        
        getSchoolComment: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "comments/2/" + id.toString(),
                //url: this.urlOne + "index.php?s=/news/index/apiGetComments&app=news&id=" + id.toString(),
                method: "GET",
            }).then(
            function(res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        
        getQuestionsByType: function (typeId, limit){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "questions/promo/" + typeId + "/" +limit,
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;    
        },
        
        getQuestions: function (column, data, order, limit) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "questions/list/" + order + "/" +limit,
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        postQuestion: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "questions",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },

        getQuestionDetail: function (id) {
            var deferred = $q.defer()
            $http({
                url: this.urlFour + "questions/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getQuestionAnswer: function(id){
            var deferred = $q.defer()
            $http({
                url: this.urlFour + "questions/answers/" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;    
        },

        postAnswer: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/question/index/apiPostAnswer",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },

        postSurveyResult: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "survey/submit/",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },
        
        jsSDK: function(){
            var deferred = $q.defer();
            //console.log($location);
            $http({
                url: this.urlFour + "wechatSign",
                method: "post",
                data:{
                    url: $location.$$absUrl
                }
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getMsgCenterList: function(type, index, id){
           var deferred = $q.defer();
           var url;
           var that = this;
           if(id){
            url = this.urlFour + "square/get/" + type + "/" + id + "/4/" + index;
        }else{
            url = this.urlFour + "square/get/" + type + "/4/" + index ;
        }
        $http({
            url: url,
            method: "get",
            timeout: "10000"
        }).then(
        function (res) {
         deferred.resolve(res.data);
     }, 
     function(err, status){
         console.log(err);
         that.getMsgCenterList(type, index, id).then(
          function(res){
           deferred.resolve(res);
       }
       )
     }
     );
        return deferred.promise;
    },
    getMsgDetail: function(id){
       var deferred = $q.defer();
            //console.log($location);
            $http({
                url: this.urlFour + "square/get/" + id,
                method: "get",
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;	
        },

        postMsg: function(obj){
           var deferred = $q.defer();
           $http({
            url: this.urlFour + "square/post",
            method: "post",
            data: obj
        }).then(
        function(res){
            deferred.resolve(res.data);
        },
        function(err){
            console.log(err);
            deferred.reject(err.data);
        }
     );
        return deferred.promise;
    },

		uploadMsgQR: function(file, userId){
			var deferred = $q.defer();
			Upload.upload({
				url: this.urlFour + "square/post/qr/"+userId,
				data: {file: file, 'uid': userId}
			}).then(
			function (res) {
			   deferred.resolve(res);
			}, function (resp) {
				deferred.reject(resp);
			});
			return deferred.promise;
		},
		
		createMsgPoster: function(obj){
			var deferred = $q.defer();
			$http({
				url: this.urlFour + "createpng/create",
				method: "POST",
				data: obj
			}).then(
				function(res){
					deferred.resolve(res.data);
				},
				function(err){
					deferred.reject(err);
				}
			);
			return deferred.promise;
		},
		
		requestQRCode: function(url){
			var deferred = $q.defer();
			$http({
				url: "http://188.166.210.103:1234/qr/generate",
				method: "POST",
				data: {url: url}
			}).then(
				function(res){
					deferred.resolve(res.data);
				},
				function(err){
					deferred.reject(err);
				}
			);
			return deferred.promise;
		}
	}
})
.factory("seedUrl", function($rootScope,$location){
    return {
        createUrl: function(type, id, time, taskId){
            if (time){
                return "img/pv.jpg?referid=" + $rootScope.referId + "&" + type + "id=" + id + "&sharetasktime=" + time + "&taskid=" + taskId;     
            }else{
                return "img/pv.jpg?referid=" + $rootScope.referId + "&" + type + "id=" + id;
            }
        }    
    }
})
.factory("wechatShare", function($window, $location, $rootScope){
    var wx = window.wx;
    console.log(wx);
    if(wx){
        return function(title, img, desc, url){
            //console.log(wx, $location.$$absUrl, title, img, desc);
            var link;

            if(url){
                link = url + $rootScope.shareState;
            }else{
                link = $location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState;
            }

            wx.ready(function(){
                wx.onMenuShareTimeline({
                    title: title, // 分享标题
                    link: link,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });
                wx.onMenuShareAppMessage({
                    title: title, // 分享标题
                    link: link,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    desc: desc,//分享预览文字
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });       
            });
        }
    }else{
        return function(){
            console.log("wx is not init");
        };
    }
})
.factory("missionShare", function($window, $location, $rootScope){
    var wx = window.wx;
    console.log(wx);
    if(wx){
        return function(title, img, desc, url){
            //console.log(wx, $location.$$absUrl, title, img, desc);
            var link;

            var now = Math.floor((new Date().getTime())/1000);

            if(url){
                link = url + $rootScope.shareState;
            }else{
                link = $location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState;
            }

            wx.ready(function(){
                wx.onMenuShareTimeline({
                    title: title, // 分享标题
                    link: link + "&sharetime=" + now,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });
                wx.onMenuShareAppMessage({
                    title: title, // 分享标题
                    link: link + "&sharetime=" + now,//$location.$$absUrl.indexOf('?referid=') !== -1 ? $location.$$absUrl.slice(0,$location.$$absUrl.indexOf('?referid=')) + $rootScope.shareState : $location.$$absUrl + $rootScope.shareState, // 分享链接
                    desc: desc,//分享预览文字
                    imgUrl: img, // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });       
            });
        }
    }else{
        return "wx is not init!";
    }
})
.factory("introduceNewUser", function($window){
    var wx = window.wx;
    if(wx){
        return function(recNum, user){
            wx.ready(function(){
                wx.onMenuShareAppMessage({
                    title: "最聪明的教育创业计划，零成本打造您的在线留学工作室", // 分享标题
                    link: "http://www.liunar.com/mobile/#/userShare" + recNum + "&vip=1", // 分享链接
                    desc:  user + "邀你成为留学教育合伙达人，零投入，无需场地，可兼职，只需转发分享，即可赚取额外收入！",//分享预览文字
                    imgUrl: "http://www.liunar.com/img/icon.png?v=3", // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });

                wx.onMenuShareTimeline({
                    title: "最聪明的教育创业计划，零成本打造您的在线留学工作室", // 分享标题
                    link: "http://www.liunar.com/mobile/#/userShare" + recNum + "&vip=1", // 分享链接
                    //desc:  user + "邀你成为留学教育合伙达人，零投入，无需场地，可兼职，只需转发分享，即可赚取额外收入！",//分享预览文字
                    imgUrl: "http://www.liunar.com/img/icon.png?v=3", // 分享图标
                    success: function () {
                        //alert("success Post");
                    },
                    cancel: function () { 
                        //alert("cancel");
                    },
                    error: function(){
                        //alert("error");
                    }
                });
            });    
        }
    }else{
        return "wx is not init";
    }
})
.factory("pressureTest", function ($http, urlConfig) {
    return {
        post: function () {
            $http({
                url: urlConfig.url + "articles/user",
                method: "POST",
            }).then(
            function (res) {
                //console.log(res);
            }, function (res) {
                //console.log(res);
            }
            );
        },

        put: function () {
            $http({
                url: urlConfig.url + "articles/user",
                method: "PUT",
            }).then(
            function (res) {
                //console.log(res);
            }, function (res) {
                //console.log(res);
            }
            );
        },

        get: function () {
            $http({
                url: urlConfig.url + "articles/fulllist",
                method: "GET",
            }).then(
            function (res) {
                //console.log(res);
            }, function (res) {
                //console.log(res);
            }
            );
        }
    }
})
.factory("$exceptionHandler", function(){
    return function(exception, cause){
        console.error(exception,cause);
    }
});
