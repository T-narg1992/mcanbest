angular.module("liunarProduct",[])
.controller("liunarProductController", function($ionicHistory, $scope, $stateParams, productList, pastCases, schoolList, wechatShare, seedUrl){
    //console.log($stateParams, productList, pastCases, schoolList);
    
    $scope.type = $stateParams.typeId;
    
    console.log(schoolList);
    
    if($stateParams.typeId == 1){
        $scope.cate = "知名大学";
        $scope.wechatShareImage = "http://www.liunar.com/img/zmdx.png";
    }else if($stateParams.typeId == 2){
        $scope.cate = "特色院校";
        $scope.wechatShareImage = "http://www.liunar.com/img/tsyx.png";
    }else if($stateParams.typeId == 3){
        $scope.cate = "低龄留学";
        $scope.wechatShareImage = "http://www.liunar.com/img/dllx.png";
    }
    
    $scope.shareUrl = seedUrl.createUrl("bulletboard", $stateParams.typeId);
    
    wechatShare($scope.cate, $scope.wechatShareImage, $scope.cate);
    
    $scope.pastCases = pastCases;
    
    $scope.productLists = productList;
    
    $scope.schools = schoolList.slice(0,4);
    
    $scope.anchor = $stateParams.typeId;
    
    if($ionicHistory.viewHistory().backView !== null){
        $scope.from = $ionicHistory.viewHistory().backView.url;
    }else{
        $scope.from = "/app/mainHome";
        $scope.fromShare = true;
    }
    
})
.controller("liunarPastCaseController", function($scope, $stateParams, caseDetail, $ionicHistory, wechatShare, seedUrl){
    for(var i = 0; i < caseDetail.length; i++){
        if(caseDetail[i].id == $stateParams.caseId){
            $scope.case = caseDetail[i];
            break;
        }
    }
    
    //console.log($scope.case);
    
    $scope.shareUrl = seedUrl.createUrl("pastcase", $scope.case.id);
    
    wechatShare("祝贺"  +  $scope.case.name + "，申请成功！", "http://www.liunar.com/schools/" + $scope.case.school_id +"/1.png", $scope.case.extra_info);
    
    if($ionicHistory.viewHistory().backView !== null){
        $scope.from = $ionicHistory.viewHistory().backView.url;
    }else{
        $scope.from = "/app/mainHome";
        $scope.fromShare = true;
    }
})
.controller("shopProductAll", function($scope, httpServices){
    httpServices.getAllProductsNew(100).then(
        function(res){
            console.log(res);
            $scope.products = res.data;
        }
        );
    
    $scope.imgUrl = function(string){
        var match = String(string).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);
        var url = (match && match.length && match[1]) ? match[1] : ''
        return url;
    };
})
.controller("shopProductCountry", function($scope, httpServices, $stateParams){
    httpServices.getCountryProductsNew($stateParams.country).then(
        function(res){
            $scope.products = res.data;
        }
        );
})
.controller("shopProductDetail", function(userAuth, $rootScope, $sce, $scope, $state, $stateParams, seedUrl, wechatShare, httpServices, localStorage, $ionicLoading, $ionicPopup, $ionicModal, $ionicHistory, $location, missionShare){

    //Product Detail:
    $scope.$on("$ionicView.beforeEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
            noBackDrop: true
        });
        httpServices.getShopProductDetail($stateParams.productId).then(
            function(res){
                $scope.productId = res[0].id;

                $scope.productDetail = $sce.trustAsHtml(res[0].details);

                $scope.productTitle = res[0].title; 

                $scope.productPrice = res[0].price;

                var matches = String(res[0].details).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);

                var result = (matches && matches.length && matches[1]) ? matches[1] : '';

                if($ionicHistory.backView() && $ionicHistory.backView().stateName == "app.mission"){
                    missionShare($scope.productTitle, result, res[0].description)
                }else{
                    wechatShare($scope.productTitle, result, res[0].description);
                }
                
                var shareTime = $location.absUrl().indexOf('&sharetime=');

                if(shareTime !== -1){
                    console.log("havetime");
                    shareTime += "&sharetime=".length;
                    var stop = $location.absUrl().indexOf('&', shareTime);
                    if(stop === -1){
                        var urlTime = $location.absUrl().substring(shareTime);
                    }else{
                        var urlTime = $location.absUrl().substring(shareTime, stop);
                    }
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id, urlTime, $stateParams.taskId);
                }else{
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id);    
                }
            }
            );
    });
    
    $scope.$on("$ionicView.afterEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.hide();
    });
    
    var user = new userAuth.checkLogin();
    
    $scope.purchase = function(){
        if(user.isLogin !== true){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再购买'
            }).then(
            function(){
                $state.go("userLogin");    
            }
            );
        }else{
            $state.go("app.shopPurchase",{ productId: $scope.productId});
        }
    };
    
    //show referId logic 
    if($rootScope.referId == "null"){
        if(user.isLogin == true){
            httpServices.getRefCodeById(user.uid).then(
                function(res){
                    if(!res.err){
                        $scope.refCode = res.reference_code;
                    }else{
                        $scope.showCode = false;
                    }
                }
                );    
        }else{
            $scope.showCode = false;    
        }   
    }else{
        httpServices.getRefCodeById($rootScope.referId).then(
            function(res){
                if(!res.err){
                    $scope.refCode = res.reference_code;
                }else{
                    $scope.showCode = false;
                }
            }
            );    
    }
})
.controller("newShopDetail", function(userAuth, $rootScope, $sce, $scope, $state, $stateParams, seedUrl, wechatShare, httpServices, localStorage, $ionicLoading, $ionicPopup, $ionicModal, $ionicHistory, $location, missionShare, $ionicModal){

    //Product Detail:
    $scope.$on("$ionicView.beforeEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
            noBackDrop: true
        });http://liunar.com/#/app/productdetail/30/
        httpServices.getProductDetailNew($stateParams.productId).then(
            function(res){
                console.log(res);
                $scope.productId = res[0].id;

                $scope.productDetail = $sce.trustAsHtml(res[0].details);

                $scope.productTitle = res[0].title; 

                $scope.productPrice = res[0].msrp + " " + res[0].currency;

                var matches = String(res[0].details).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);

                var result = (matches && matches.length && matches[1]) ? matches[1] : '';

                if($ionicHistory.backView() && $ionicHistory.backView().stateName == "app.mission"){
                    missionShare($scope.productTitle, result, res[0].description)
                }else{
                    wechatShare($scope.productTitle, result, res[0].description);
                }
                
                var shareTime = $location.absUrl().indexOf('&sharetime=');

                if(shareTime !== -1){
                    console.log("havetime");
                    shareTime += "&sharetime=".length;
                    var stop = $location.absUrl().indexOf('&', shareTime);
                    if(stop === -1){
                        var urlTime = $location.absUrl().substring(shareTime);
                    }else{
                        var urlTime = $location.absUrl().substring(shareTime, stop);
                    }
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id, urlTime, $stateParams.taskId);
                }else{
                    $scope.shareUrl = seedUrl.createUrl("productdetail", res[0].id);    
                }
				
				var imgs = String(res[0].details).match(/<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/g);
				
            }
            );
    });
    
    $scope.$on("$ionicView.afterEnter", function(event, toState, toParams, fromState, fromParams){
        $ionicLoading.hide();
    });
    
    var user = new userAuth.checkLogin();

    if(user.isLogin == true){
        var refCode = user.refCode;
		//uid,refcode
        if(!refCode){
            httpServices.getProductAccessAuthorization(user.uid).then(
                function(res){
                    console.log('response:'+res,res.code == 'true');
                    if(res.code == 'true'){
                        $scope.showButton = true;
                    }
                    return;
                }, 
                function(err){
                    console.log(err);
                });
        }else{
            httpServices.getProductAccessAuthorization(user.uid,refCode).then(function(res){
                if(res.code == 'true'){
                    $scope.showButton = true;
                }
            },function(err){
                console.log(err);
            });
        }
    }

    $scope.purchase = function(){
        if(user.isLogin !== true){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再购买'
            }).then(
            function(){
                $state.go("userLogin");    
            }
            );
        }else{
            $state.go("app.productpurchase",{ productId: $scope.productId});
        }
    };

	$ionicModal.fromTemplateUrl('templates/imgEnlarge.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});
	
	$scope.openModal = function(src) {
		$scope.modal.show();
		$scope.imgUrl = src;
  	};

    //show referId logic 
    if($rootScope.referId == "null"){
        if(user.isLogin == true){
            httpServices.getRefCodeById(user.uid).then(
                function(res){
                    if(!res.err){
                        $scope.refCode = res.reference_code;
                    }else{
                        $scope.showCode = false;
                    }
                }
                );    
        }else{
            $scope.showCode = false;    
        }   
    }else{
        httpServices.getRefCodeById($rootScope.referId).then(
            function(res){
                if(!res.err){
                    $scope.refCode = res.reference_code;
                }else{
                    $scope.showCode = false;
                }
            }
            );    
    }
}).controller("productMoreDetail", function($timeout, $window, $sce, $state, localStorage, $stateParams, $scope, httpServices, $location, $ionicScrollDelegate, userAuth){
    var user = new userAuth.checkLogin();
    $scope.prdID = $stateParams.prdID;
    if(user.isLogin == true){
        var refCode = user.refCode;
        if(!refCode){ //no refcode in localstorage
            httpServices.getAuthorizedProductData($stateParams.prdID,user.uid).then(
                function(res){
                    var prod = res[0][0];
                    var files = res[1];
                    if(prod){
                        $scope.title = prod.title;
                        $scope.commission = prod.commission;
                        $scope.referral_fee_process_time = prod.referral_fee_process_time;
                        $scope.sale_instruction = $sce.trustAsHtml(prod.sale_instruction);
                        $scope.market_instruction = $sce.trustAsHtml(prod.market_instruction);
                        $scope.files = files;
                    }else{
                        $state.go("app.productdetail", {productId: $stateParams.prdID});
                    }
                }, 
                function(err){
                    console.log(err);
                });
        }else{ //have refcode localstorage
			//prodid, uid,refcode
            httpServices.getAuthorizedProductData($stateParams.prdID,user.uid,refCode).then(function(res){
                $state.go("productMoreDetail");
                return;
            },function(err){
                console.log(err);
            });
        }
    }else{
        $scope.productTitle = '請登錄！';
    }
})
.controller("shopPage", function($timeout, $window, $sce, $state, localStorage, $stateParams, $scope, httpServices, $location, $ionicScrollDelegate){

    $scope.slick = false;
    
    $scope.slickConfig = {
        enabled: false,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 3000,
        dots: true,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };
    
    httpServices.getProductPromo().then(
        function(res){
            console.log(res);
            $scope.topCarousel = $sce.trustAsHtml(res[0].content_html);
            $scope.secondSection = $sce.trustAsHtml(res[1].content_html);
            $scope.midBanner = $sce.trustAsHtml(res[2].content_html);
            $scope.slickConfig.enabled = true;
            $timeout(
                function(){
                    $scope.slick = true;
                }
                );
        }
        );
    
    console.log($location);
    //0:低龄留学 1:短期留学 2:特色院校 3:知名大学 4:移民项目

    httpServices.getProductList().then(
        function(res){
            console.log(res);
            $scope.a = [];
            $scope.b = []; 
            $scope.c = [];
            $scope.d = [];
            $scope.e = [];
            res.forEach(function(v,i){
                if(v.promo_group == 0){
                    $scope.a.push(v);
                }else if(v.promo_group == 1){
                    $scope.b.push(v);
                }else if(v.promo_group == 2){
                    $scope.c.push(v);
                }else if(v.promo_group == 3){
                    $scope.d.push(v);
                }else if(v.promo_group == 4){
                    $scope.e.push(v);
                } 
            });
            $ionicScrollDelegate.resize();
            if($location.$$hash !== ""){
                console.log($ionicScrollDelegate.resize());
                $ionicScrollDelegate.anchorScroll(true);
                console.log("shouldScroll");
            }
        }
        );
    
})
.controller("shopPageNew", function($timeout, $window, $sce, $state, localStorage, $stateParams, $scope, httpServices, $location, $ionicScrollDelegate, wechatShare){

    $scope.slick = false;
    
    $scope.slickConfig = {
        enabled: false,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 3000,
        dots: true,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };

    wechatShare("留哪儿精选——精选全球最优教育资源", "http://www.liunar.com/img/icon.png", "留哪儿精选是Canbest集团旗下的海外教育类垂直电商平台，留哪儿精选将全球最优质的留学、游学资源，一站式提供给中国学子，让他们能更直接、更高效地选择世界。")
    
    httpServices.getProductPromo().then(
        function(res){
            console.log(res);
            $scope.topCarousel = $sce.trustAsHtml(res[0].content_html);
            //$scope.firstSection = $sce.trustAsHtml(res[1].content_html);
            $scope.secondSection = $sce.trustAsHtml(res[1].content_html);
            $scope.midBanner = $sce.trustAsHtml(res[2].content_html);
            $scope.slickConfig.enabled = true;
            $timeout(
                function(){
                    $scope.slick = true;
                }
                );
        }
        );
    
    console.log($location);
    //0:低龄留学 1:短期留学 2:特色院校 3:知名大学 4:移民项目

    httpServices.getProductListNew().then(
        function(res){
            console.log(res);
            $scope.a = [];
            $scope.b = []; 
            $scope.c = [];
            $scope.d = [];
            $scope.e = [];
            res.forEach(function(v,i){
                if(v.promo_group == 0){
                    $scope.a.push(v);
                }else if(v.promo_group == 1){
                    $scope.b.push(v);
                }else if(v.promo_group == 2){
                    $scope.c.push(v);
                }else if(v.promo_group == 3){
                    $scope.d.push(v);
                }else if(v.promo_group == 4){
                    $scope.e.push(v);
                } 
            });
            $ionicScrollDelegate.resize();
            if($location.$$hash !== ""){
                console.log($ionicScrollDelegate.resize());
                $ionicScrollDelegate.anchorScroll(true);
                console.log("shouldScroll");
            }
        }
        );
    
})
.controller("shopPurchase", function(userAuth, $state, localStorage, $stateParams, purchaseDetail, $scope, httpServices, $ionicPopup, $rootScope){

    var user = new userAuth.checkLogin();
    
    if(user.isLogin !== true){

        $state.go("userLogin");    

    }else{

        $scope.purchaseDetail = purchaseDetail[0];

        $scope.clicked = false

        var matches = String(purchaseDetail[0].details).match(/<img.*?src="([^"]*)"[^>]*>(?:<\/img>)?/m);

        $scope.imgUrl = (matches && matches.length && matches[1]) ? matches[1] : '';
        
        
        
        $scope.orderObj = {
            refCode: ""
        }
        
        httpServices.extractUserInfo(user.uid).then(
            function(res){
                //console.log(res);
                $scope.orderObj.name = res.fullname;
                $scope.orderObj.phone = res.phone;
            }
            );
        
        if($rootScope.referId == "null"){
            if(user.isLogin == true){
                httpServices.getRefCodeById(user.uid).then(
                    function(res){
                        if(!res.err){
                            $scope.orderObj.refCode = res.reference_code;
                        }else{
                            $scope.orderObj.refCode = null;
                        }
                    }
                    );    
            }else{
                $scope.orderObj.refCode = null;    
            }   
        }else{
            httpServices.getRefCodeById($rootScope.referId).then(
                function(res){
                    if(!res.err){
                        $scope.orderObj.refCode = res.reference_code;
                    }else{
                        $scope.orderObj.refCode = null;
                    }
                }
                );    
        }

        $scope.orderProduct = function(){
            //console.log($scope.orderObj);
            if($scope.orderObj.name && $scope.orderObj.phone){
                if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test($scope.orderObj.phone) == true){
                    var aid;
                    //console.log(localStorage.getItem("referId"));
                    if(localStorage.getItem("referId") && localStorage.getItem("referId") != 'null'){
                        aid = localStorage.getItem("referId");
                    }else{
                        aid = 0;
                    }
                    //console.log(aid);
                    httpServices.userOrder(localStorage.getObject("canbestUserInfo").uid, purchaseDetail[0].id, $scope.orderObj.phone, $scope.orderObj.name, aid, $scope.orderObj.refCode).then(
                        function(res){
                            //console.log(res);
                            if(!res.err){
                                $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '恭喜您预定成功，我们会尽快联系您'
                                }).then(
                                function(){
                                    $state.go("app.shop");
                                }
                                );        
                            }
                        }
                        );
                }else{
                    alert('电话号码只能由 +,-,0-9 组成');
                }
            }else{
                alert("电话和姓名不能为空");
            }
        }
    }
});
