angular.module("msg", [])
  .controller("msgCenterController", function ($state, $location, $scope, $ionicScrollDelegate, httpServices, userAuth, $ionicPopup, wechatShare) {
    $scope.initList = function () {
      httpServices.getMsgCenterList($scope.type, $scope.index, $scope.id).then(
        function (res) {
          console.log(res);
          $scope.lists = res;
        }
      );
    };

    $scope.index = 0;
    $scope.type = "list";
    $scope.id = null;
    $scope.haveMore = true;
    $scope.getMore = function () {
      $scope.index++;
      httpServices.getMsgCenterList($scope.type, $scope.index, $scope.id).then(
        function (res) {
          console.log(res);
          $scope.lists = [].concat($scope.lists, res);
          $scope.$broadcast('scroll.infiniteScrollComplete');
          if (res.length) {
            $scope.haveMore = true;
          } else {
            $scope.haveMore = false;
          }
        }
      );
    };

    $scope.tabSelected = 1;
    $scope.tabSelect = function (tab) {
      $scope.tabSelected = tab;
      $scope.id = null;
      $scope.index = 0;
      $scope.haveMore = true;
      console.log(tab);
      if (tab == 1) {
        $scope.type = "list";
        $scope.initList();
      } else if (tab == 2) {
        $scope.type = "expiring";
        $scope.initList();
      } else if (tab == 3) {
        $scope.type = "expired";
        $scope.initList();
      } else if (tab == 4) {
        var user = new userAuth.checkLogin();
        if (user.isLogin !== true) {

          $ionicPopup.alert({
            title: '留哪儿',
            template: '微信登陆后才能看到你发布的信息'
          }).then(
            function () {
              $state.go("userLogin", {
                lastview: $location.$$path
              });
            }
          );
        } else {
          $scope.id = user.uid;
          $scope.type = "my";
          $scope.initList(user.uid);
        }
      }
    };
    $scope.initList();
  })
  .controller("msgDetailController", function ($ionicLoading, $state, $location, $scope, $stateParams, httpServices, userAuth, $ionicPopup, wechatShare, $ionicModal) {
    var loginStatus = new userAuth.checkLogin();
    var id;
    var contentInit = function (callback) {
      httpServices.getMsgDetail($stateParams.id).then(
        function (res) {
          $scope.msg = res[0][0];
          id = $scope.msg.id;
          $scope.comments = JSON.parse(res[1]);
          console.log(res, $scope.msg, $scope.comments);
          callback($scope.msg);
        }
      );
    };
    $scope.$on("$ionicView.beforeEnter", function () {
      contentInit(function (res) {
        wechatShare(res.title, "http://www.liunar.com/img/lntt.png", res.title);
        if (loginStatus.uid == res.uid) {
          $scope.showDelete = true;
          $scope.showCreate = true;
        }
      });
      $scope.show = false;
      //title, img, desc, url
    });
    $scope.showConfirmation = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: '留哪儿',
        template: '确定要删除?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          postDelete();
        }
      });
    };

    console.log($location);

    $scope.createPoster = function (author, avatar, type, title, body, expire) {
      $ionicLoading.show({
        template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner><br/>正在生成海报...',
        noBackDrop: true
      });
      httpServices.requestQRCode($location.$$absUrl).then(
        function (qrcode) {
          console.log(qrcode);
          if (qrcode.img) {
            httpServices.createMsgPoster({
              author: author,
              avatar: avatar,
              title: title,
              type: type,
              body: body,
              qrcode: qrcode.img,
              expire_time: expire
            }).then(
              function (res) {
                if (res.img) {
                  $ionicLoading.hide();
                  popUpModal(res.img);
                } else {
                  $ionicPopup.alert({
                    title: '留哪儿',
                    template: res.err
                  })
                }
              },
              function (err) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: '留哪儿',
                  template: err.toString()
                })
              }
            );
          } else {
            $ionicLoading.hide();
            $ionicPopup.alert({
              title: '留哪儿',
              template: qrcode.err
            })
          }
        },
        function (err) {
          $ionicLoading.hide();
          $ionicPopup.alert({
            title: '留哪儿',
            template: err.toString()
          })
        }
      );
    };

    var popUpModal = function (img) {
      $ionicModal.fromTemplateUrl('templates/imgEnlarge.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
        $scope.imgPoster = img;
        $scope.modal.show();
      });
    };

    $scope.closeModal = function () {
      $scope.modal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.modal.remove();
    });

    var postDelete = function () {
      httpServices.postDelete({
        id: id
      }).then(function (result) {
        if (result.data.code == 1) {
          $ionicPopup.alert({
            title: '留哪儿',
            template: '删除成功'
          }).then(function (res) {
            $state.go("app.msgCenter");
          });
        } else {
          $ionicPopup.alert({
            title: '删除没有成功！',
            template: '没有查到此post,请截屏然后发给技术人员'
          });
        }
      });
    };
    $scope.showContact = function () {
      if ($scope.show == false) {
        $scope.show = true;
      } else {
        $scope.show = false;
      }
    }

    $scope.msgComment = {
      row_id: $stateParams.id,
      content: "",
      module: 5
    };


    $scope.postMsgComment = function () {
      if (loginStatus.isLogin == false) {
        $ionicPopup.alert({
          title: '留哪儿',
          template: '请微信登录之后再评论'
        }).then(
          function () {
            $state.go("userLogin", {
              lastview: $location.$$path
            });
          }
        );
      } else {
        $scope.msgComment.uid = loginStatus.uid;
        if ($scope.msgComment.content == "" || $scope.msgComment.content.length < 6) {
          $ionicPopup.alert({
            title: '留哪儿',
            template: '评论不能为空，而且必须多于6个字'
          });
        } else {
          $scope.disable = true;
          httpServices.postComment($scope.msgComment).then(
            function (res) {
              $scope.disble = false;
              $ionicPopup.alert({
                title: '留哪儿',
                template: '感谢您的评论'
              }).then(
                function (res) {
                  $scope.msgComment.content = null;
                  contentInit();
                }
              )
            }
          );
        }
      }
    };

  })
  .controller("msgPostController", function ($scope, userAuth, httpServices, $state, $ionicPopup, $ionicLoading) {
    var user = userAuth.checkLogin();
    console.log(user);
    if (user.isLogin !== true) {
      $ionicPopup.alert({
        title: '留哪儿',
        template: '请微信授权之后再发布消息'
      }).then(
        function (res) {
          $state.go("userLogin", {
            lastview: "/app/post"
          });
        }
      );
    } else {

      $scope.msgObj = {
        phone: "",
        wechat: "",
        title: null,
        content: null,
        deadline: "",
        location: "",
        uid: user.uid,
        type: 1
      };

      $scope.time = new Date(new Date().setDate(new Date().getDate() + 365));
      console.log($scope.time);

      $scope.postMsg = function () {
        console.log($scope.msgObj.type, $scope.msgObj.title.length);
        if (($scope.msgObj.title == null && $scope.msgObj.content == null) || $scope.msgObj.title.length > 12) {
          $ionicPopup.alert({
            title: '留哪儿',
            template: '消息标题和消息内容不能为空，且标题不能多于12个字'
          }).then();
        } else {
          if ($scope.msgObj.phone.length && /^\d+$/.test($scope.msgObj.phone) !== true) {
            $ionicPopup.alert({
              title: '留哪儿',
              template: '电话号码只能由数字组成'
            }).then();
          } else {
            $scope.msgObj.deadline = Math.round($scope.time.getTime() / 1000);
            console.log($scope.msgObj.deadline);
            httpServices.postMsg($scope.msgObj).then(
              function (res) {
                if (res.code == "success") {
                  $scope.msgObj = null;
                  $ionicPopup.alert({
                    title: '留哪儿',
                    template: '发布成功'
                  }).then(
                    function () {
                      $state.go("app.msgCenter");
                    }
                  );
                } else {
                  $ionicPopup.alert({
                    title: '留哪儿',
                    template: '发布不成功，请重试'
                  }).then(
                    function () {
                      $scope.msgObj = null;
                    }
                  );
                }
              },
              function (err){
                $ionicPopup.alert({
                  title: '留哪儿',
                  template: err
                }).then();
              }
            );
          }
        }
      };

      $scope.qrSrc = "http://api.liunar.com/uploads/qr/qr-" + user.uid + ".jpg";

      $scope.uploadQrCode = function (file, err) {
        $ionicLoading.show({
          template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
          noBackDrop: true
        });
        httpServices.uploadMsgQR(file, user.uid).then(
          function (res) {
            if (res.statusText == "OK") {
              $ionicLoading.hide();
              $ionicPopup.alert({
                title: '留哪儿',
                template: 'QR Code 上传成功'
              }).then(function () {
                $scope.qrSrc = "http://api.liunar.com" + res.data.path + "?stamp=" + new Date().getTime();
              });
            } else {
              $ionicLoading.hide();
              $ionicPopup.alert({
                title: '留哪儿',
                template: 'QR Code 上传不成功，请重试'
              }).then(function () {

              });
            }
          }
        )
      };
    }
  })
  .controller("yunController", function ($scope, userAuth, httpServices, $state, $ionicPopup) {
    var user = userAuth.checkLogin();
    if (user.isLogin !== true) {
      $ionicPopup.alert({
        title: '留哪儿',
        template: '请进入微信授权'
      }).then(
        function (res) {
          $state.go("userLogin", {
            lastview: "/yun"
          });
        }
      );
    } else {
      window.location.href = "http://yun.liunar.com/verifiedAgent/index/?u=" + user.uid;
    };
  })