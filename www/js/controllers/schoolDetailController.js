angular.module('schoolDetailController',[])
.controller('schoolDetailController', function($scope, $stateParams, $sce, $ionicPopup, httpServices, wechatShare, seedUrl, schoolDetail, $ionicModal, schoolComment, $state, schoolTable, $location, userAuth){
    
    $scope.school = schoolDetail;
    $scope.schoolComment = schoolComment;
    console.log(schoolDetail, schoolComment);
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.shareUrl = seedUrl.createUrl("schoolDetail", schoolDetail.id);
        wechatShare(schoolDetail.en_name + " (" + schoolDetail.ch_name + ")", "http://www.liunar.com/schools/" + schoolDetail.id + "/1.png", schoolDetail.detail_info);
    });
    
    $scope.mapModules = [
        {
            text: "街道地图",
            value: "google.maps.MapTypeId.ROADMAP"
        },
        {
            text: "实景街图",
            value: "streetView"
        },
        {
            text: "鸟瞰地图",
            value: "google.maps.MapTypeId.SATELLITE"
        }
    ];
    
    $scope.selected = $scope.mapModules[0];
    $scope.mapType = $scope.mapModules[0].value;
    
    $scope.select = function(item){
        //console.log(item);
        $scope.selected = item;
        $scope.mapType = item.value;
    };
    
    /////////点评功能以后或许会开放////////
    $ionicModal.fromTemplateUrl('templates/schoolCommentModal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    
    $scope.commentObj = {
        row_id : $stateParams.schoolId,
        content: "",
        module: 2   
    };
    
    var getComm = function(){
      httpServices.getSchoolComment($stateParams.schoolId).then(
          function(res){
             $scope.schoolComment = res;  
          }
      )  
    };
    
    $scope.address = schoolDetail.contact.slice(schoolDetail.contact.indexOf('地址') + 1 +'地址'.length);
    
    var loginStatus = new userAuth.checkLogin();
    
    $scope.leaveComment = function(){
        if(loginStatus.isLogin == false){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $scope.modal.hide();
                    $state.go("userLogin",{lastview: $location.$$path});    
                }
            );
        }else{
            $scope.commentObj.uid = loginStatus.uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                $scope.disable = true;
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $scope.disable = false;
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.modal.hide();
                                $scope.commentObj.content = null;
                                getComm();
                            }
                        ); 
                    }
                );
            }
        }
    };
    
})
.controller('newSchoolDetailController', function($ionicHistory, $scope, $stateParams, $sce, $ionicPopup, httpServices, wechatShare, seedUrl, schoolDetail, $ionicModal, schoolComment, $state, schoolTable, $location, userAuth){
    
    $scope.school = schoolDetail;
    $scope.schoolComment = schoolComment;
    
    if($ionicHistory.backView() != null){
        $scope.backUrl = "#" + $ionicHistory.backView().url;
    }else{
        $scope.backUrl = "#" + "/app/schoolrankprovince/" + schoolDetail.type + "/" + schoolDetail.province + "/all/all";
    }
    
    //console.log(schoolDetail);
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.shareUrl = seedUrl.createUrl("schoolDetail", schoolDetail.id);
        wechatShare(schoolDetail.en_name + " (" + schoolDetail.ch_name + ")", "http://www.liunar.com/schools/" + schoolDetail.id + "/1.png", schoolDetail.detail_info);
    });
    
    $scope.mapModules = [
        {
            text: "街道地图",
            value: "google.maps.MapTypeId.ROADMAP"
        },
        {
            text: "实景街图",
            value: "streetView"
        },
        {
            text: "鸟瞰地图",
            value: "google.maps.MapTypeId.SATELLITE"
        }
    ];
    
    $scope.selected = $scope.mapModules[0];
    $scope.mapType = $scope.mapModules[0].value;
    
    $scope.select = function(item){
        //console.log(item);
        $scope.selected = item;
        $scope.mapType = item.value;
    };
    
    var rankData;

    if(schoolDetail.overall_rating !== null){
        rankData = schoolDetail.overall_rating.split(",");
        rankData.pop();
        //console.log(rankData.indexOf("n/a"))
        if(rankData.indexOf("n/a") !== -1){
            rankData.forEach(
                function(a, i){
                    if(a == "n/a"){
                        rankData[i] = 2.5;
                    }
                }
            )
        }
    }else{
        rankData = [];
    }
    
    $scope.labels = ["2011","2012","2013","2014","2015"];
    $scope.data = [
        rankData
    ];
    $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
    $scope.options = {
        scaleOverride: true,
        scaleSteps: 4,
        scaleStepWidth: 2.5,
        scaleStartValue: 0
    };
    
    $scope.tableInit = function(){
        var renderedTable = {};

        if(schoolDetail.other !== null){
            var table = schoolDetail.other.split(";");
            table.pop();
            table.forEach(
                function(a,i){
                    //console.log(a,i);
                    var b = a.split(":");
                    //console.log(b);
                    if(b[1].indexOf(",")!==-1){
                        var c = b[1].split(",");
                        c.pop()
                        renderedTable[b[0]] = c;
                    }else{
                        $scope.firstValue = b[1];
                        $scope.firstKey = schoolTable[b[0]];
                    }
                }
            )
        }
        
        $scope.table = renderedTable;
    };
    
    $scope.translate = schoolTable;
    
    /////////点评功能以后或许会开放////////
    $ionicModal.fromTemplateUrl('templates/schoolCommentModal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    
    $scope.commentObj = {
        row_id : $stateParams.schoolId,
        content: "",
        module: 2   
    };
    
    var getComm = function(){
      httpServices.getSchoolComment($stateParams.schoolId).then(
          function(res){
             $scope.schoolComment = res;  
          }
      )  
    };
    
    var loginStatus = new userAuth.checkLogin();
    
    $scope.leaveComment = function(){
        if(loginStatus.isLogin == false){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $scope.modal.hide();
                    $state.go("userLogin",{lastview: $location.$$path});    
                }
            );
        }else{
            $scope.commentObj.uid = loginStatus.uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                $scope.disable = true;
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $scope.disable = false;
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.modal.hide();
                                $scope.commentObj.content = null;
                                getComm();
                                //$scope.getPostDetail();
                            }
                        ); 
                    }
                );
            }
        }
    };
    
})
.controller('campDetailController', function($scope, $stateParams, campDetail, $sce){
    //console.log(campDetail, $sce);
    $scope.school = campDetail;
    $scope.daily = $sce.trustAsHtml(campDetail["daily_schedule"]);
    $scope.detail = $sce.trustAsHtml(campDetail["personalize"])
    //console.log(Object.keys(campDetail), campDetail["daily_schedule"]);
});