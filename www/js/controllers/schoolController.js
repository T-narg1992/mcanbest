angular.module('schoolController',[])
.controller('schoolController', function($rootScope, $scope, httpServices, $stateParams, seedUrl, wechatShare){
    //functions => load Content:
    if($stateParams.cateID == 1){
        
        $scope.title = "加拿大大学排名";
        $scope.wechatShareImage = "http://www.liunar.com/img/daXueIcon.png";
        $scope.showFliter = false;
    
    }else if($stateParams.cateID == 2){
        
        $scope.title = "加拿大特色院校";
        $scope.wechatShareImage = "http://www.liunar.com/img/teSeXueYuanIcon.png";
        $scope.showFliter = false;
    
    }else if($stateParams.cateID == 3){
        
        $scope.title = "BC省中学排名";
        $scope.wechatShareImage = "http://www.liunar.com/img/zhongXueIcon.png";
        $scope.showFliter = true;
        
    }else if($stateParams.cateID == 4){
        
        $scope.title = "BC省小学排名";
        $scope.wechatShareImage = "http://www.liunar.com/img/xiaoXueIcon.png";
        $scope.showFliter = true;
    };
    
    //$scope.schoolListsRank = schoolList;
    
    var getCities = function(){
        httpServices.getCities().then(
            function(res){
                $scope.cities = res;
            }
        );
    };
    
    $scope.searchObj = {
        public: "",
        city: ""
    };
    
    $scope.getSchoolsRank = function(){
        httpServices.getSchoolRankList($stateParams.cateID,$scope.searchObj.public, $scope.searchObj.city,"").then(
            function(res){
                //console.log(res);
                $scope.schoolListsRank = res;
                $scope.shareUrl = seedUrl.createUrl("schoolRankList", $stateParams.cateID);
                wechatShare($scope.title, 'http://www.liunar.com/img/schoolShareV2.jpeg', $scope.title);
            }
        );
    };
    
    $scope.$on("$ionicView.beforeEnter", function(){
        getCities();
        $scope.getSchoolsRank();
    });
    
    $scope.$on("$ionicView.beforLeave", function(){
        $scope.haveMore = true;
    });
    
})
.controller('schoolProvinceController', function($state, $ionicScrollDelegate, $rootScope, $scope, httpServices, $stateParams, seedUrl, wechatShare, $ionicHistory){
    
    $scope.scrollTop = function() {
        $ionicScrollDelegate.scrollTop(true);
    };
    
    $ionicHistory.nextViewOptions({
        disableAnimate: true
    });
    
    var cities = {
        BC: {
            "Surrey": "素里",
            "Vancouver": "温哥华",
            "Victoria": "维多利亚",
            "Burnaby": "本那比",
            "Richmond": "列治文",
            "Abbotsford": "阿伯茨福德",
            "North Vancouver": "北温哥华",
            "Prince George": "乔治王子市",
            "Delta": "三角洲",
            "Kelowna": "基隆拿",
            "Langley": "兰里",
            "Kamloops": "甘露市",
            "Chilliwack": "奇利瓦克",
            "Coquitlam": "高贵林",
            "Nanaimo": "纳奈莫",
            "Maple Ridge": "枫树岭",
            "Port Coquitlam": "高贵林港",
            "West Vancouver": "西温哥华",
            "Vernon": "弗农",
            "Mission": "米逊",
        },
        AB: {
            "Edmonton": "埃德蒙顿",
            "Calgary": "卡尔加里",
            "Fort McMurray": "麦克默里堡",
            "St. Albert": "圣艾伯特",
            "Grande Prairie": "大草原城",
            "Medicine Hat": "梅迪辛哈特",
            "Sherwood Park": "舍伍德帕克",
            "Okotoks": "奥科托克斯",
            "Lethbridge": "莱斯布里奇",
            "Red Deer": "红鹿",
            "Leduc": "勒杜克",
            "Airdrie": "艾尔德里",
            "Stony Plain": "斯托尼普莱恩",
            "Cochrane": "科克伦",
            "Fort Saskatchewan": "萨斯喀彻温堡",
            "Lacombe": "拉科姆",
            "Drayton Valley": "德雷顿瓦利",
            "Spruce Grove": "斯普鲁斯格罗夫",
            "Strathmore": "斯特拉思莫尔",
            "Ardrossan": "阿德罗森",
        },
        ON: {
            "Toronto": "多伦多",
            "Mississauga": "密西沙加",
            "London": "伦敦",
            "Ottawa": "渥太华",
            "Hamilton": "哈密尔顿",
            "Brampton": "布兰普顿",
            "Windsor": "温莎",
            "Kitchener": "基奇纳",
            "Markham": "万锦",
            "Burlington": "伯林顿",
            "Oakville": "奥克维尔",
            "Barrie": "巴利",
            "Guelph": "贵湖",
            "St Catharines": "圣凯瑟琳",
            "Cambridge": "剑桥",
            "Brantford": "布兰特福德",
            "Richmond Hill": "列治文山",
            "Oshawa": "奥沙瓦",
            "Thunder Bay": "桑德贝",
            "Kingston": "金斯顿",
        }
    };
    
    $scope.cities = cities[$stateParams.province];
    
    $scope.city = $stateParams.city;
    
    $scope.public = $stateParams.public;    
        
    if($stateParams.cateID == 3 && $stateParams.province == 'ON'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "安大略省" + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "安大略省中学排名";    
        }
        
    }else if($stateParams.cateID == 4 && $stateParams.province == 'ON'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "安大略省" + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "安大略省小学排名";    
        }
        
    }else if($stateParams.cateID == 3 && $stateParams.province == 'AB'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "阿尔伯特省" + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "阿尔伯特省中学排名";    
        }
        
    }else if($stateParams.cateID == 4 && $stateParams.province == 'AB'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "阿尔伯特省" + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "阿尔伯特省小学排名";    
        }
        
    }else if($stateParams.cateID == 3 && $stateParams.province == 'BC'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "卑诗省" + ($stateParams.public == 1 ? "公校" : "私校") + "中学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "卑诗省中学排名";    
        }
        
    }else if($stateParams.cateID == 4 && $stateParams.province == 'BC'){
        
        if($stateParams.city !== "all" && $stateParams.public !== "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city !== "all" && $stateParams.public == "all"){
            $scope.title = cities[$stateParams.province][$stateParams.city] + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public !== "all"){
            $scope.title = "卑诗省" + ($stateParams.public == 1 ? "公校" : "私校") + "小学排名";
        }else if($stateParams.city == "all" && $stateParams.public == "all"){
            $scope.title = "卑诗省小学排名";    
        }
        
    };
    
    wechatShare($scope.title, 'http://www.liunar.com/img/schoolShare.jpg', $scope.title);
    
    $scope.showFliter = true;
    
    $scope.changeCity = function(city){
        $state.go("app.schoolRankProvince", {cateID: $stateParams.cateID, city: city, public: $stateParams.public})
    };
    
    $scope.changePublic = function(public){
        $state.go("app.schoolRankProvince", {cateID: $stateParams.cateID, city: $stateParams.city, public: public})
    }
    
    $scope.loadMore = true;
    
    var num = 0;
    
    httpServices.getSchoolListByProvince($stateParams.cateID, 6, num, $scope.city == "all" ? '':$scope.city, $scope.public == "all" ? '' : $scope.public, $stateParams.province).then(
        function(res){
            if(res == "page requested is not available"){
                $scope.loadMore = false;
            }else{
                $scope.loadMore = true;
                $scope.schoolListsRank = res;
            }
        }
    );
    
    $scope.getMoreSchool = function(){
        //console.log(num);
        num ++;
        httpServices.getSchoolListByProvince($stateParams.cateID, 6, num, $scope.city == "all" ? '':$scope.city, $scope.public == "all" ? '' : $scope.public, $stateParams.province).then(
            function(res){
                //console.log(res);
                if(res == "page requested is not available"){
                    $scope.loadMore = false;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }else{
                    $scope.loadMore = true;
                    $scope.schoolListsRank = [].concat($scope.schoolListsRank, res);
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
                
            }
        );
        //console.log(num);
    };
    
})
.controller('campController', function($scope, $http, httpServices, $ionicPosition, $stateParams){
    
    $scope.getCampList = function(){
        
        httpServices.getCampProducts(15).then(
            function(res){
                $scope.camplist = res.all;
            }
        );
    };
})
.controller('schoolMainController', function(wechatShare, $scope){
    $scope.$on("$ionicView.beforeEnter", function(){
        wechatShare("加拿大院校排名大全", 'http://www.liunar.com/img/schoolShareV2.jpeg', "大学、学院、中学、小学各种排名一网打尽");    
    });
})