angular.module('estimateController',[])
.controller('estimateController',function($ionicPopup, $scope, $state, httpServices, $rootScope){
    
    $scope.answer = [];
    
    $scope.select = function(key, id, value){
        //console.log(key);
        $scope.selected = key;
        $scope.value = value;
        
    };
    
    $scope.selectValue = {
        value: 0,
    }
    
    $scope.selected = 'A';
    $scope.value = 0;
    
    var singleQuestions = [
        {
            id: 1,
            question: "我想要申请",
            answers: {
                A: {text: "中小学", value: 0}, 
                B: {text: "高中", value: 1}, 
                C: {text: "名校本科", value: 2}, 
                D: {text: "专业学院", value: 3},
                //E: {text: "海外移民置业", value: 4}
            }
        },
        {
            id: 2,
            question: "我喜欢的学校类型",
            answers: {
                A: {text: "公校", value: 0}, 
                B: {text: "私校", value: 1}
            }
        },
        {
            id: 3,
            question: "我的英语水平",
            answers: {
                A: {text: "有考试成绩", value: 0}, 
                B: {text: "无考试成绩", value: 1}
            }
        },
        {
            id: 4,
            question: "SSAT",
            answers: {
                A: {text: "2100－2400", value: 0}, 
                B: {text: "1800－2100", value: 1}, 
                C: {text: "1320－1800", value: 2},
                D: {text: "无", value: -1}
            }
        },
        {
            id: 5,
            question: "IELTS",
            answers: {
                A: {text: "6－7以上", value: 0}, 
                B: {text: "5－5.5", value: 1}, 
                C: {text: "4－4.5", value: 2},
                D: {text: "无", value: -1}
            }
        },
        {
            id: 6,
            question: "TOFEL",
            answers: {
                A: {text: "100以上", value: 0},
                B: {text: "80－99", value: 1},
                C: {text: "80以下", value: 2},
                D: {text: "无", value: -1}
            }
        },
        {
            id: 7,
            question: "无考试成绩",
            answers: {
                A: {text: "优良", value: 0},
                B: {text: "一般", value: 1},
                C: {text: "有限", value: 2},
                D: {text: "不知道", value: 3}
            }
        },
        {
            id: 8,
            question: "我的学校成绩",
            answers: {
                A: {text: "90以上", value: 0},
                B: {text: "80－89", value: 1},
                C: {text: "70－79", value: 2},
                D: {text: "70以下", value: 3}
            }
        },
        {
            id: 9,
            question: "我的留学预算",
            answers: {
                A: {text: "30万以上/年", value: 0},
                B: {text: "21－30万/年", value: 1},
                C: {text: "10－20万/年", value: 2}
            }
        },
        {
            id: 101,
            question: "IELT",
            answers: {
                A: {text: "6－7以上", value: 0}, 
                B: {text: "5－5.5", value: 1}, 
                C: {text: "4－4.5", value: 2},
                D: {text: "4以下", value: 3},
                E: {text: "无", value: -1}
            }        
        },
        {
            id: 102,
            question: "TOFEL",
            answers: {
                A: {text: "100以上", value: 0},
                B: {text: "80－99", value: 1},
                C: {text: "80以下", value: 2},
                D: {text: "无", value: -1}
            }       
        },
        {
            id: 103,
            question: "我的学校成绩",
            answers: {
                A: {text: "90以上", value: 0},
                B: {text: "80－89", value: 1},
                C: {text: "70－79", value: 2},
                D: {text: "70以下", value: 3}
            }
        },
        {
            id: 104,
            question: "我非常关注目标学校的",
            answers: {
                A: {text: "学校知名度", value: 0},
                B: {text: "学费和生活费", value: 1},
                C: {text: "毕业的难度", value: 2},
                D: {text: "专业有助于移民", value: 3}
            }
        },
        {
            id: 105,
            question: "我想学的专业",
            answers: {
                A: {text: "文科", value: 0},
                B: {text: "工科", value: 1},
                C: {text: "理科", value: 2},
                D: {text: "商科", value: 3}
            }
        },
        {
            id: 201,
            question: "我想要申请的移民类型",
            answers: {
                A: {text: "投资移民", value: 0}, 
                B: {text: "技术移民", value: 1}
            }
        },
        {
            id: 202,
            question: "我的英语水平",
            answers: {
                A: {text: "有考试成绩", value: 0}, 
                B: {text: "无考试成绩", value: 1}
            }
        },
        {
            id: 203,
            question: "IELT",
            answers: {
                A: {text: "6－7以上", value: 0}, 
                B: {text: "5－5.5", value: 1}, 
                C: {text: "4－4.5", value: 2},
                D: {text: "4以下", value: 3}
            }        
        },
        {
            id: 204,
            question: "CELPIP",
            answers: {
                A: {text: "8以上", value: 0}, 
                B: {text: "6－7", value: 1}, 
                C: {text: "5以下", value: 2}
            }        
        },
        {
            id: 205,
            question: "暂无考试成绩",
            answers: {
                A: {text: "熟练", value: 0}, 
                B: {text: "一般", value: 1}, 
                C: {text: "有限", value: 2}
            }        
        },
        {
            id: 206,
            question: "我的资产（包括存款，夫妻共同财产，投资，股票以及不动产等）",
            answers: {
                A: {text: "500万以上", value: 0}, 
                B: {text: "200万－500万", value: 1}, 
                C: {text: "50－200万", value: 2},
                D: {text: "50万以下", value: 3}      
            }        
        },
        {
            id: 207,
            question: "我能接受的投资金额",
            answers: {
                A: {text: "150万以上", value: 0}, 
                B: {text: "80万－150万", value: 1}, 
                C: {text: "30万－80万", value: 2},
                D: {text: "15万－30万", value: 3}    
            }        
        },
        {
            id: 208,
            question: "我期望的海外居住时间",
            answers: {
                A: {text: "时间灵活，可接受任何居住时间的要求", value: 0}, 
                B: {text: "时间有限，能达到一定的居住要求", value: 1}, 
                C: {text: "基本不方便海外居住", value: 2} 
            }        
        }   
    ];
    
    var qNum = 0;
    
    $scope.question = singleQuestions[0];
    
    $scope.nextButton = false;
    
    $scope.next = function(id, value){
        //console.log(id,value, $scope.answer, $scope.answer[0]);
        //$scope.selected = null;
        $scope.answer.push([id, value]);
        if(id == 1){
            
            $scope.nextButton = true;
            
            if($scope.answer[0][1] === 0 || $scope.answer[0][1] === 1){
                
                $scope.question = singleQuestions[1];
                
            }else if($scope.answer[0][1] === 2 || $scope.answer[0][1] === 3){
                
                $scope.question = singleQuestions[9];
                
            }else if($scope.answer[0][1] === 4){
                
                $scope.question = singleQuestions[14];
                
            }
        }else if(id == 2){
        
            $scope.question = singleQuestions[2];
            
        }else if(id == 3){
            if($scope.answer[2][1] === 0){
                $scope.question = singleQuestions[3]
            }else if($scope.answer[2][1] === 1){
                $scope.question = singleQuestions[6];
            }
        }else if(id == 4){
            
            $scope.question = singleQuestions[4];
            
        }else if(id == 5){

            $scope.question = singleQuestions[5];
            
        }else if(id == 6){
            
            $scope.question = singleQuestions[7];
            
        }else if(id == 7){
            
            $scope.question = singleQuestions[7];
            
        }else if(id == 8){
            
            $scope.question = singleQuestions[8];
            
        }else if(id == 9){
            
            $scope.answerObj = {"uid": 1, "answers": $scope.answer};
            httpServices.postSurveyResult($scope.answerObj);
            $state.go("app.estimateResult");
            $rootScope.answer = $scope.answer;
            
        }else if(id == 101){
            
            $scope.question = singleQuestions[10];
            
        }else if(id == 102){
            
            $scope.question = singleQuestions[11];
            
        }else if(id == 103){
           
            $scope.question = singleQuestions[12];
            
        }else if(id == 104){
            
            $scope.question = singleQuestions[13];
            
        }else if(id == 105){
            
            $scope.question = singleQuestions[8];
            
        }else if(id == 201){
           
            $scope.question = singleQuestions[15];
            
        }else if(id == 202){
            
            if($scope.answer[2][1] === 0){
                $scope.question = singleQuestions[16]
            }else{
                $scope.question = singleQuestions[18];
            }
        }else if(id == 203){
            
            $scope.question = singleQuestions[17];
            
        }else if(id == 204){
            
            $scope.question = singleQuestions[19];
            
        }else if(id == 205){
           
            $scope.question = singleQuestions[19];
            
        }else if(id == 206){
            
            $scope.question = singleQuestions[20];
            
        }else if(id == 207){
            
            $scope.question = singleQuestions[21];
            
        }else if(id == 208){
            
            $scope.answerObj = {"uid": 1, "answers": $scope.answer}
            httpServices.postSurveyResult($scope.answerObj);
            $state.go("app.estimateResult");
            
        }
        
    };
    
    $scope.back = function(id){
        //console.log(id);
        //console.log($scope.answer);
        $scope.selected = null;
        $scope.answer.pop();
        if(id == 1){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 2){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 3){
            
            $scope.question = singleQuestions[1];
            
        }else if(id == 4){
            
            $scope.question = singleQuestions[2];
            
        }else if(id == 5){

            $scope.question = singleQuestions[3];
            
        }else if(id == 6){
            
            $scope.question = singleQuestions[4];
            
        }else if(id == 7){
            
            $scope.question = singleQuestions[2];
            
        }else if(id == 8){
            
            if($scope.answer[2][1] === 0){
            $scope.question = singleQuestions[5];
            }else{
            $scope.question = singleQuestions[6];   
            }
            
        }else if(id == 9){
            
            if($scope.answer[0][1] === 0 || $scope.answer[0][1] === 1){
            $scope.question = singleQuestions[7];
            }else if($scope.answer[0][1] === 2 || $scope.answer[0][1] === 3){
            $scope.question = singleQuestions[13];   
            }//need get value!
            
        }else if(id == 101){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 102){
            
            $scope.question = singleQuestions[9];
            
        }else if(id == 103){
           
            $scope.question = singleQuestions[10];
            
        }else if(id == 104){
            
            $scope.question = singleQuestions[11];
            
        }else if(id == 105){
            
            $scope.question = singleQuestions[12];
            
        }else if(id == 201){
            $scope.nextButton = false;
            $scope.question = singleQuestions[0];
            
        }else if(id == 202){
            
            $scope.question = singleQuestions[14];
            
        }else if(id == 203){
            
            $scope.question = singleQuestions[15];
            
        }else if(id == 204){
            
            $scope.question = singleQuestions[16];
            
        }else if(id == 205){
           
            $scope.question = singleQuestions[15];
            
        }else if(id == 206){
            if($scope.answer[2][1] === 0){
            $scope.question = singleQuestions[17];
            }else{
            $scope.question = singleQuestions[18];   
            }
        }else if(id == 207){
            
            $scope.question = singleQuestions[19];
            
        }else if(id == 208){
            
            $scope.question = singleQuestions[20];
            
        }
    };
    
    
    $scope.$on("$ionicView.afterLeave", function(){
        $scope.question = singleQuestions[0];
        $rootScope.answer = "";
    });
    
    $scope.calculation = function(){
        //console.log($rootScope.answer);
        var result = [];
        
        if($rootScope.answer[0][1] == 0 || $rootScope.answer[0][1] == 1){
            $rootScope.answer.splice(0,2);
            $rootScope.typeId = 2;
        }else if($rootScope.answer[0][1] == 2 || $rootScope.answer[0][1] == 3){
            if($rootScope.answer[0][1] == 2){
                $rootScope.typeId = 4;   
            }else if($rootScope.answer[0][1] == 3){
                $rootScope.typeId = 3;  
            }
            $rootScope.answer.splice(0,1);
            $rootScope.answer.splice($rootScope.answer.length - 3,2);
        }
        
        
        for (var i = 0; i < $rootScope.answer.length; i++){
            result.push($rootScope.answer[i][1]);
        }

        var count = {}; 
        result.forEach(function(i) { count[i] = (count[i]||0)+1;  });
        
        if(count[-1]){
            delete count[-1];
        }
        
        if(!count[3] && !count[2] && !count[1]){
            $scope.score = 5;
        }else if(!count[3] && !count[2] && count[1] <= 3){
            $scope.score = 4;
        }else if(!count[3] && count[2] <= 1){
            $scope.score = 3;
        }else if(count[2] <= 2){
            $scope.score = 2;
        }else if(count[2] <= 3){
            $scope.score = 1;
        }else{
            $scope.score = 3;
        }
        $scope.inverseScore = 5 - $scope.score;
        
        //console.log($scope.score, $scope.inverseScore);
        $scope.getNumber = function(num) {
            return new Array(num);   
        }
    }
    
    $scope.apply = function(){
            //console.log($rootScope.answer);
        //$state.go('app.liunarProduct',{typeId: $rootScope.typeId});
        window.location.href = "http://shop.liunar.com/list/" + $rootScope.typeId;
    };
});