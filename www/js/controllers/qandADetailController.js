angular.module('qandADetailController',[])
.controller('qandADetailController',function(localStorage, $scope, httpServices, $stateParams, $state, questionDetail, answerDetail, seedUrl, wechatShare, httpServices){
    
    
    ///////getQandA Detail//////
    
    
    $scope.order = $stateParams.order;
    
    if($scope.order.indexOf('type') !== -1){
        var typeId = $scope.order.substring($scope.order.indexOf('type') + "type".length);
        $scope.backUrl = "app.QandATypeList({typeId: " + typeId + "})";
    }else if($scope.order.indexOf('search') !== -1){
        $scope.backUrl = "tagSearch({tag: ''})";
    }
    else{
        $scope.backUrl = "app.QandAList({order: '" + $scope.order + "'})";
    }
    
    
    $scope.qDetail = questionDetail[0];
    $scope.qAnswer = answerDetail;
    //console.log(questionDetail, answerDetail);
    
    $scope.$on("$ionicView.beforeEnter", function(){
        if(questionDetail[0] !== undefined){
            $scope.shareUrl = seedUrl.createUrl("question",questionDetail[0].id);
            wechatShare(questionDetail[0].title,"http://www.liunar.com/img/zjdy.png", questionDetail[0].description !== "" ? questionDetail[0].description : questionDetail[0].title);
        }
    });
    
    $scope.refresh = function(){
        httpServices.getQuestionAnswer($stateParams.questionId).then(
        function(res){
           $scope.qAnswer = res;
           $scope.$broadcast('scroll.refreshComplete');
        });
    };
    
    ////////Post Answer//////
    /*$scope.answerObj = {
        question_id: $stateParams.questionId,
        content: "",
        //uid: localStorage.getObject('canbestUserInfo').uid,
        //commentPerson: null,
    };*/
    
    //$scope.autoFocus = false;
    
    /*$scope.addPerson = function(p){
        if($scope.answerObj.commentPerson === null){
            $scope.answerObj.commentPerson = p;
            $scope.showPerson = true;
        }else if($scope.answerObj.commentPerson !== null){
            if($scope.answerObj.commentPerson === p){
                $scope.answerObj.commentPerson = null;
                $scope.showPerson = false;
            }else{
                $scope.answerObj.commentPerson = p;
                $scope.showPerson = true;
            }
        }
        //console.log($scope.answerObj, p);
    };*/
    
    /*$scope.postAnswer = function(){
        //console.log($scope.answerObj);
        
        if(localStorage.getObject("canbestUserInfo") === null){
            $scope.anwserObj.uid = 0;
        }else{
            $scope.answerObj.uid = localStorage.getObject("canbestUserInfo").uid;
        }
        
        httpServices.postAnswer($scope.answerObj).then(
            function(res){
                //console.log(res);
                $scope.getQuestionDetail();
            }
        );
        $scope.answerObj.content = null;
    };*/
});
