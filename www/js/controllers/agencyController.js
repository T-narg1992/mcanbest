angular.module('agencyController',[])
.controller('agencyController', function($rootScope, $scope, $ionicPopup, localStorage, httpServices){
    
    if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){ 
        $scope.showCustomedCard = false;
    }else{
        httpServices.extractUserInfo(localStorage.getObject("canbestUserInfo").uid).then(
            function(res){
                if(res.agentType === 1){
                    //console.log(res);
                    $scope.agentInfo = res;
                    $scope.QRPopUp = function(){
                        $ionicPopup.alert({
                            title: "长按二维码识别",
                            template:"<div style='padding: 24px;'><img ng-src='http://api.liunar.com/uploads/" + res.wechat_qrcode + "' width='100%' /></div>"
                        });
                    };
                    $scope.showCustomedCard = true;
                }else{
                    $scope.showCustomedCard = false;
                }
            }
        );
    }
})
.controller('agencyBottomController', function($rootScope, $scope, $ionicPopup, localStorage, httpServices){
        
    if($rootScope.referId !== 'null'){
        httpServices.extractUserInfo($rootScope.referId).then(
            function(res){
                //console.log(res);
                if(res.agentType === 1){
                    $scope.agentShowInfo = res;
                    $scope.showAgentCard = true;
                }else{
                    $scope.showAgentCard = false;
                }
                
            }
        );    
    }else{
        $scope.showAgentCard = false;
    }
});