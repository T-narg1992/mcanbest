angular.module("search",[])
.controller("tagsController", function($scope){
    $scope.tags = [
        {
            cate: "国家",
            icon: "fa fa-globe",
            tags: [
                "加拿大", "美国", "澳大利亚", "英国", "新西兰", "北欧", 
            ]
        },
        {
            cate: "阶段",
            icon: "fa fa-line-chart",
            tags: [
                "低龄", "本科", "研究生"
            ]
        },
        {
            cate: "留学周期",
            icon: "fa fa-clock-o",
            tags: [
                "留学前", "留学中", "留学后"
            ]
        },
        {
            cate: "专业",
            icon: "fa fa-graduation-cap",
            tags: [
                "文科专业", "理工科专业", "医科专业", "商科专业", "职业类专业", "艺术类专业", "热门专业"
            ]  
        },
        {
            cate: "考试",
            icon: "fa fa-pencil-square-o",
            tags: [
                "SAT/SSAT", "GMAT", "GRE", "托福", "雅思", "IB", "AP"
            ]  
        },
        {
            cate: "签证",
            icon: "fa fa-plane",
            tags: [
                "学习签证", "工作签证", "旅游签证", "陪读签证"
            ]  
        },
        {
            cate: "经验分享",
            icon: "fa fa-bullhorn",
            tags: [
                "留学申请经验", "留学生活经验", "留学就业经验", "移民经验", "留学规划"
            ]  
        },
        {
            cate: "常识",
            icon: "fa fa-question-circle",
            tags: [
                "留学生活", "衣食住行", "留学误区", "留学安全", "国家文化", "娱乐"
            ]  
        },
        {
            cate: "留学成本",
            icon: "fa fa-usd",
            tags: [
                "低成本留学", "中端预算留学", "高端留学", 
            ]  
        },
        {
            cate: "移民",
            icon: "fa fa-home",
            tags: [
                "移民政策", "移民申请", "移民生活", "移民资讯"
            ]  
        },
        {
            cate: "就业",
            icon: "fa fa-briefcase",
            tags: [
                "打工", "实习", "留学生就业", "留学就业薪资", "工作面试", "留学职业规划"
            ]  
        },
        {
            cate: "出国目的",
            icon: "fa fa-bullseye",
            tags: [
                "留学", "移民", "游学", "旅游", "工作", "陪读"
            ]  
        },
        {
            cate: "专题",
            icon: "fa fa-hashtag",
            tags: [
                "美国中学", "加拿大中学", "海外游学", "美国本科留学", "加拿大本科留学", "美国研究生留学", "加拿大研究生留学", "加拿大移民", "美国移民", "亲子活动", "讲座"
            ]  
        },
    ];
    
    $scope.clicked = $scope.tags[0];
    $scope.select = function(item){
        console.log(item);
        $scope.clicked = item;
    };
    
    console.log($scope.clicked);
    
})
.controller("schoolSearchController", function($state, $scope, $ionicHistory, $http, $q){
    $scope.back = function() {
        if($ionicHistory.backView() == null){
            $state.go("app.school");   
        }else{
            $ionicHistory.goBack();    
        }
        //console.log($ionicHistory, $ionicHistory.backView());
    };
    
    $scope.search = function(kw){
        if(kw.length > 0){
            $http({
                url: "/search/school?kw=" + kw,
                method: "GET",
            }).then(
                function(res){
                    //console.log(res);
                    $scope.results = res.data.slice(0,9);
                },
                function(err){
                    //console.log(err);
                }
            );
        }else{
            $scope.results = null;
            console.log("empty");
        }
    };
    
})
.controller("tagSearchController", function($stateParams, $state, $scope, $ionicHistory, $http, $q){
    $scope.back = function() {
		if($stateParams.tag == "" || $ionicHistory.backView() == null){
			window.location.href="http://www.liunar.com"
		}else{
        	$state.go("tagSquare");
		}
        //console.log($ionicHistory, $ionicHistory.backView());
    };
    
    $scope.more = true;
    
    $scope.tag = $stateParams.tag;
    
    var start;
    
    var end;
    
    var tagSearch = function(kw,index, limit){
        var deferred = $q.defer();
        
        console.log(index);
        if(kw.length > 0){
            $http({
                url: "/search/search?kw=" + kw + "&start=" + index  + "&limit=" + limit, //+ "&type=" + type ,
                method: "GET",
            }).then(
                function(res){
                    deferred.resolve(res.data);
                },
                function(err){
                    deferred.resolve([]);
                    
                }
            );
        }else{
            console.log("empty");
            deferred.resolve([]);
        }
        return deferred.promise;
    };
    
    $scope.resultInit = function(){
        start = 0;
        end = 7;
        console.log("start from: " + start + ", " + end);
        tagSearch($scope.tag, start, end).then(
            function(res){
                $scope.results = res;
                if(res.length < 1){
                    $scope.more = false;
                }else{
                    $scope.more = true;
                }
                console.log("begin: ", $scope.results);
            }
        )
    };
    
    $scope.inputSearch = function(tag){
        start = 0;
        end = 7;
        console.log(tag);
        console.log("input=>start from: " + start + ", " + end);
        tagSearch(tag, start, end).then(
            function(res){
                if(res.length < 1){
                    $scope.results = null;
                    $scope.more = false;
                }else{
                    $scope.results = res;
                    $scope.more = true;
                }
                console.log($scope.results);
            }
        );
    };
    
    $scope.searchMore = function(){
        start = start + 8;
        end = end + 8;
        console.log("continue: " + start + ", " + end);
        tagSearch($scope.tag, start, end).then(
            function(res){
                if(res.length < 1){
                    $scope.more = false;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }else{
                    $scope.more = true;
                    $scope.results = [].concat($scope.results, res);
                    console.log("add: " + res.length, "after add: " + $scope.results.length);
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
            }
        );
    };
    
    //console.log($scope.results);
    
});