angular.module('event',[])
.controller('eventListController', function($scope, httpServices){
    httpServices.getEvents(30,0).then(
        function(res){
            $scope.events = res;
        }
    );
    $scope.now = (new Date().getTime())/1000;
    //console.log($scope.now);
});