angular.module('applyController',[])
.controller('applyController', function($scope, $state, $stateParams, $ionicHistory, $ionicPopup, httpServices){
    
    console.log($stateParams);
    
    $scope.applyObj = {
        name: "",
        number: "",
        description: "from mobile"
    };
    
    var productNames = {
        1: "加拿大知名大学申请定制服务",
        2: "加拿大特色院校申请定制服务",
        3: "加拿大中学申请定制服务",
        4: "加拿大小学申请定制服务"
    };
    
    $scope.productName = productNames[$stateParams.schoolType];
    
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.from = $ionicHistory.viewHistory().backView;//.backView.url;
        console.log($scope.from, $stateParams);
            $scope.apply = function(){
                if($scope.applyObj.name === "" || $scope.applyObj.name.length === 0){
                    $ionicPopup.alert({
                        title: '留哪儿',
                        template: '名字不能为空'
                    }).then(
                    );
                }else if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test($scope.applyObj.number) !== true){
                    $ionicPopup.alert({
                        title: '留哪儿',
                        template: '请输入有效电话号码'
                    }).then(
                        function(){
                            $scope.applyObj.number = null;
                        }
                    );
                }else{
                    //$scope.applyObj.referrer = $stateParams.pageTitle + "/" + $stateParams.productId + $scope.from.url;
                    $scope.applyObj.referrer = $scope.productName;
                    //console.log($scope.applyObj.referrer);
                    httpServices.apply($scope.applyObj).then(
                        function(res){
                            if(res !== 0 ){
                               $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '感谢您的申请，我们将会在24小时内联系您！'
                                }).then(
                                   function(){
                                       $state.go('app.mainHome');
                                       $scope.applyObj = null;
                                   }
                                ); 
                            }
                        }
                    );
                }
            }
    });
});
