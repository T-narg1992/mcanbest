angular.module('detailController',[])
.controller('detailController', function($ionicPopup, $scope, $http, httpServices, $stateParams, $sce, $state, $ionicHistory, postDetail, seedUrl, wechatShare, userAuth, $location, $rootScope, missionShare){
    
    
    $scope.$on("$ionicView.beforeEnter", function(){
        
        var thumbnail;
        
        if(postDetail.have_thumbnail === 1){
            thumbnail = "http://www.liunar.com" + postDetail.thumbnail;     
        }else if(postDetail.have_thumbnail === 2){
            thumbnail = "http://cms.liunar.com/uploadimage/articles/" + postDetail.promo_thumb;
        }        
        
        console.log(postDetail);
        
        if($ionicHistory.backView() && $ionicHistory.backView().stateName == "app.mission"){
            missionShare(postDetail.title, thumbnail, postDetail.description, "http://www.liunar.com/web/article/" + postDetail.id + "/" + $stateParams.taskId);
        }else{
            wechatShare(postDetail.title, thumbnail, postDetail.description, "http://www.liunar.com/web/article/" + postDetail.id);
        }
        
        var shareTime = $location.absUrl().indexOf('&sharetime=');
    
        if(shareTime !== -1){
            shareTime += "&sharetime=".length;
            var stop = $location.absUrl().indexOf('&', shareTime);
            if(stop === -1){
                var urlTime = $location.absUrl().substring(shareTime);
            }else{
                var urlTime = $location.absUrl().substring(shareTime, stop);
            }
            $scope.shareUrl = seedUrl.createUrl("article", postDetail.id, urlTime, $stateParams.taskId);
        }else{
            $scope.shareUrl = seedUrl.createUrl("article", postDetail.id);    
        }
        
        if($ionicHistory.viewHistory().backView !== null){
            $scope.from = $ionicHistory.viewHistory().backView.url;    
        }else{
            $scope.from = "/app/mainHome";
            $scope.fromShare = true;
            if(postDetail.id == 34677){
                $scope.fromShare = false;    
            }
        }
        
        httpServices.addView(postDetail.id);
        
    });
    

    
        
    $scope.postDetail = postDetail;
    if(postDetail.hasTopicTag == 1){
        $scope.topicTag = JSON.parse(postDetail.topicTags)[0].tag;
        $scope.topicTagId = JSON.parse(postDetail.topicTags)[0].tagId;
    }
    //$scope.description = $sce.trustAsHtml(postDetail.description);
    $scope.postDetailContent = $sce.trustAsHtml(postDetail.content_html);

    $scope.getRelative = function(){
        if(postDetail.tags !== null && postDetail.tags != ""){
            var tags = postDetail.tags.split(",").slice(0, 2).join(" ");
            console.log(tags);
            $http({
                url: "/search/search?kw=" + tags + "&start=0&limit=3&type=1",
                method:'GET'
            }).then(
                function(res){
                    if(res.data.length > 0){
                        $scope.showRelative = true;
                        $scope.lists = res.data;
                        console.log(res);
                    }else{
                        $scope.showRelative = false;
                    }
                }
            );
        }else{
            $scope.showRelative = false;
        }
    };
    
    $scope.getRelative();
       
    $scope.getPostComments = function(){
       httpServices.getPostDetailsComment($stateParams.postId).then(
            function(res){
                if(res[0]==="error"){
                }else{
                    $scope.comments = res;
                }
            }
        );
    };
    
    $scope.commentObj = {
        row_id : $stateParams.postId,
        content: "",
        module: 1   
    };
    
    var loginStatus = new userAuth.checkLogin();
    
    $scope.postNewsComment = function(){
        if(loginStatus.isLogin == false){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $state.go("userLogin", {lastview: $location.$$path});   
                }
            );
        }else{
            $scope.commentObj.uid = loginStatus.uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                $scope.disable = true;
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $scope.disble = false;
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.commentObj.content = null;
                                $scope.getPostComments();
                            }
                        )
                    }
                );
            }
        }
    };
})
.controller("missionDetailController", function(wechatShare, httpServices, $scope, $sce, $rootScope, dataStore){
    $scope.from = "/app/mainHome";
    $scope.$on("$ionicView.beforeEnter", function(){
        $rootScope.showCallBtn = false;
        $rootScope.hideTabs = 'tabs-item-hide';
        console.log($rootScope.hideTabs);
        
        httpServices.getUserMission().then(
            function(res){
                $scope.missionDetail = res;
                httpServices.getMissionLists().then(
                    function(res2){
                        if(!res2.err && res2 !== "No tasks today!"){
                            $scope.articles = [];
                            $scope.products = [];
                            $scope.events = [];
                            
                            res2.forEach(function(v,i){
                                if(v.type == "article"){
                                    $scope.articles.push(v);
                                }else if(v.type == "product"){
                                    $scope.products.push(v);
                                }else if(v.type == "event"){
                                    $scope.events.push(v);
                                }
                            });

                            var articleHtml = "";

                            var eventHtml = "";

                            var productHtml = "";

                            $scope.articles.forEach(
                                function(v,i){                                   
                                    articleHtml += "<a style='color: rgb(0, 112, 192); text-decoration: underline' href='#/app/mainhome/postdetail/" + v.id + "/"+ v.dailyTaskId +"'>" + (i+1) + "、 " + v.title + "</a><br><br>";
                                }
                            );

                            $scope.products.forEach(
                                function(v,i){
                                    productHtml += "<a style='color: rgb(0, 112, 192); text-decoration: underline' href='#/app/shopProductDetail/" + v.id + "/"+ v.dailyTaskId +"'>" + (i+1) + "、 " + v.title + "</a><br><br>";
                                }
                            );

                            $scope.events.forEach(
                                function(v,i){
                                    eventHtml += "<a style='color: rgb(0, 112, 192); text-decoration: underline' href='#/app/mainhome/postdetail/" + v.id + "/"+ v.dailyTaskId +"'>" + (i+1) + "、 " + v.title + "</a><br><br>";
                                }
                            );

                            var newHtml = res.content_html.replace(/{{articles}}/, articleHtml).replace(/{{products}}/, productHtml).replace(/{{events}}/, eventHtml);

                            $scope.missionDetailContent = $sce.trustAsHtml(newHtml);
                            
                        }else {

                            var emptyHtml = "<div style='text-align: center; margin-top: 45%; color: steelblue'><p style='font-size: 18px'>今天的任务还没有发布，<br>请耐心等待...</p></div>";
                            $scope.missionDetailContent = $sce.trustAsHtml(emptyHtml);

                        }
                    }
                )
            }
        );    
    });
})
.controller("hotItemsController", function(wechatShare, httpServices, $scope, $sce, $rootScope, dataStore){
    $scope.from = "/app/mainHome";
    $scope.$on("$ionicView.beforeEnter", function(){
        $rootScope.showCallBtn = false;
        $rootScope.hideTabs = 'tabs-item-hide';
		httpServices.getHotItems().then(
			function(res){
				$scope.hotItem = res;
				if(res.content_html){
					$scope.hotItemContent = $sce.trustAsHtml(res.content_html);
				}
			}
		)
	});
})
