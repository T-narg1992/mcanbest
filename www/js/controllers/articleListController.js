angular.module('articleListController',[])
.controller('liunarTouTiaoListController', function($scope, httpServices, $ionicLoading){
    
    
    //$scope.articles = initList;
    
    var num = 0;
    
    $scope.$on("$ionicView.beforeEnter", function(){
        console.log(num);
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner>',
            noBackDrop: true
        });
    });
    
    $scope.initList = function(){
        httpServices.getGongLueList(10, num).then(
            function(res){
                console.log(res);
                $scope.articles = res;
                $ionicLoading.hide();
            }
        );
    };
    
    $scope.$on("$ionicView.afterEnter", function(){
        $ionicLoading.hide();
    });
        
    $scope.getMoreArticles = function(){
        num ++;
        console.log(num);
        /*setTimeout(
            function(){
                num --;
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 7000
        );*/
        httpServices.getGongLueList(10, num).then(
            function(res){
                console.log(res);
                $scope.articles = [].concat($scope.articles, res);
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }
        );
    };
    
    $scope.tags = {0:"其它", 1:"海外规划", 2:"留学申请", 3:"校园内外", 4:"本地生活", 5:"实习就业", 6:"职业移民"};
    
})
.controller("tagResultsController", function ($ionicHistory, $scope, $stateParams, $http, urlConfig) {
    //console.log($scope, $stateParams, $http);
    //console.log(JSON.parse($stateParams.tag));
    if($ionicHistory.viewHistory().backView !== null){
        $scope.from = $ionicHistory.viewHistory().backView.url;
    }else{
        $scope.from = "/tagsquare";
    }

    $scope.$on("$ionicView.beforeEnter", function(evt, view){
        console.log(evt, view)
        $http({
            url: urlConfig.url + "tags/content/" + $stateParams.tag,
            method: "GET",
        }).then(
            function(res){
                console.log(res);
                //var res2 = unescape(res.data);
                var res2 = res.data.replace(/\'/g, '"').replace(/\ u/g, "").replace(/None/g, '"None"');
                console.log(res.data, res2[62], res2[63]);
                $scope.lists = JSON.parse(res2);
                console.log($scope.lists);
                $scope.pageTag = JSON.parse(res2)[0].tagname;
                view.title = $scope.pageTag;
            }
        );
    });

});
