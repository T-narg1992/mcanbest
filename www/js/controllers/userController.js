angular.module('userController',[])
.controller("userLogin", function($ionicLoading, wechatShare, $state, userAuth, $window, $stateParams, $rootScope, $scope, localStorage, detectBrowser){
	$scope.$on("$ionicView.beforeEnter", function(){
		//alert(detectBrowser.isWechat());
		if(detectBrowser.isWechat() === false){
			$scope.showBroMsg = true;
		}
	});
	
    $scope.$on("$ionicView.afterEnter", function(){
        if(userAuth.checkUcLogin().isLogin !== true){
			if($stateParams.lastview && $stateParams.lastview !== ""){
				localStorage.setItem("lastview", $stateParams.lastview);
			}
        }else{
            $state.go("app.userCenter");
        }
    });
    $scope.wechatRedirect = function(){
        $ionicLoading.show({
            template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner><br/>正在登录中...',
            noBackDrop: true
        });
        $window.location.assign("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx0933bfa899eedcaf&redirect_uri=http%3A%2F%2Fwww.liunar.com%2Fmobile%2F%23%2FuserLogin&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect");
        setTimeout(
            function(){
                $window.location.reload();
                $ionicLoading.hide();
            }, 8000
        );
    };
    wechatShare("登录留哪儿","http://liunar.com/img/icon.png","登录留哪儿，开启知识分享之旅");
    
})
.controller('userController', function(httpServices, $scope,localStorage,$state,$ionicPopup,$ionicHistory,$rootScope,$location, $stateParams, urlConfig, userAuth, $window, $cookies){
    
    console.log($cookies);
    $rootScope.hideTabs = '';
    
    /*var getVipRight = function(){
        if(localStorage.getItem("referId") && localStorage.getItem("referId") !== null){
            httpServices.getRefCodeById(localStorage.getItem("referId")).then(
                function(res){
                    if(res.err){
                        $scope.vipRight = false;
                        $scope.ref_code = null;
                    }else if(res.reference_code){
                        $scope.vipRight = true;
                        $scope.ref_code = res.reference_code;
                    }
                }
            )
        }else{
            $scope.vipRight = false;
            $scope.ref_code = null;    
        }    
    };*/
    
    $scope.agentTypes = ['普通达人','认证达人','正在审核中','审核未通过','达人身份被取消'];
    
    var user = new userAuth.checkUcLogin();
    
    var userInit = function(){
        if(user.isLogin !== true){ 
            $state.go("userLogin");
        }else{
            httpServices.extractUserInfo(user.uid).then(
                function(res){
                    //console.log(res);
                    $scope.totalPoints = res.loginPoints + res.userIntroPoints + res.salePoints + res.sharePoints;
                    if(res.agentType == 'null'){
                        $scope.agentType = 0;    
                    }else{
                        $scope.agentType = res.agentType;    
                    }
                    $scope.reference_code = res.reference_code;
                    $scope.phone = res.phone;
                    $scope.avatar = res.avatar;
                    $scope.nickname = res.nickname;
                    $scope.fullname = res.fullname;
                    //console.log($scope.agentType);
                }
            );
            
            httpServices.extractUserPoints(user.uid).then(
                function(res){
                    //console.log(res);
                    $scope.pointHistory = res;
                    $scope.pointTypes = ['首次微信授权','引进新用户','产品销售','内容分享'];
                }
            );
        }
    };
    
    $scope.$on("$ionicView.beforeEnter", function(event){
        //console.log(event);
        userInit();
        //getVipRight();
    });
    
    $scope.logout = function(){
        localStorage.clear();
        /*var cookies = $cookies.getAll();
        angular.forEach(cookies, function (v, k) {
            $cookies.remove(k);
        });*/
        $cookies.putObject("user",{"":""});
        //window.location.href="http://www.liunar.com";
        window.location.reload();
    };
    
    $scope.selectCode = function(){
        var sel = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(document.getElementById("refCode"));
        sel.removeAllRanges();
        sel.addRange(range);
        
        console.log(sel, range, window.clipboardData);
        alert(sel);
    };
    
})
.controller("userOrder", function($scope, httpServices, userAuth){
    var user = new userAuth.checkUcLogin();
    var orderInit = function(){
        if(user.isLogin !== true){ 
            $state.go("userLogin");
        }else{
            httpServices.extractUserOrder(user.uid)
            .then(function(res){
                console.log(res);
                $scope.orderList = res;
                $scope.status = ["新客户","咨询","放弃","已购买","未付款"];
            });
        }  
    };
    
    $scope.$on("$ionicView.beforeEnter", function(event){
    	orderInit();
    });
    
})
.controller("userClient", function(httpServices, $scope, userAuth, $state){
    var user = new userAuth.checkUcLogin();
    var clientInit = function(){
        if(user.isLogin !== true){ 
            $state.go("userLogin");
        }else{
            httpServices.extractUserClient(user.uid)
            .then(function(res){
                //console.log(res);
                $scope.clientList = res;
                $scope.status = ["新客户","咨询","放弃","已购买","未付款"];
            });
            httpServices.extractUserReferer(user.uid)
            .then(function(res){
                console.log(res);
                $scope.referList = res;
            });
        }
    };
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.selected = 2;
        clientInit();
    });
})
.controller("userClientOrderDetailController", function($scope, $stateParams, httpServices){
    httpServices.extractUserClientMemo($stateParams.orderId).then(
        function(res){
            if(!res.err){
                $scope.memos = res.data;
            }else{
            }
        }
    );
})
.controller("userRecommend", function($state, $rootScope, userAuth, introduceNewUser, httpServices, $scope){
    
    var user = new userAuth.checkLogin();
    
    if(user.isLogin !== true){ 
        $state.go("userLogin");
    }else{
        httpServices.extractUserInfo(user.uid).then(
            function(res){
                //console.log(res);
                $scope.reference_code = res.reference_code;
                introduceNewUser($rootScope.shareState, res.fullname);
            }
        );
    }
})
.controller("userShare", function(introduceNewUser, userAuth, $rootScope, $scope, localStorage, $state, httpServices, detectBrowser){
    var user = new userAuth.checkUcLogin();
    function creatWxUrl(id){
        if(detectBrowser.isWechat() == true){
            return "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1eabd0cb2070a271&redirect_uri=http%3A%2F%2Fyun.liunar.com/account/wxauth/" + id +"&response_type=code&scope=snsapi_userinfo&state=wxlogin#wechat_redirect";
        }else{
            return "https://open.weixin.qq.com/connect/qrconnect?appid=wxa9717b409b6a35a2&redirect_uri=http%3A%2F%2Fyun.liunar.com%2Faccount%2Fwx%2F" + id + "%2F&response_type=code&scope=snsapi_login#wechat_redirect";
        }
    };

    $scope.options = {
        direction: "vertical",
        paginationType: "fraction",
        grabCursor: true
    }
    
    $scope.url = creatWxUrl(localStorage.getItem('referId'));
    if(user.isLogin !== true){
        $scope.showShareMethod = false;
        httpServices.extractUserInfo(localStorage.getItem('referId')).then(
            function(res){
                //console.log(res);
                introduceNewUser("?referid=" + localStorage.getItem('referId'), res.fullname);
                $scope.refCode = res.reference_code;

                //$scope.url = creatWxUrl(localStorage.getItem('referId'));
                
            }
        );
    }else{
        httpServices.extractUserInfo(user.uid).then(
            function(res){
                //console.log(res, $rootScope.shareState);
                if(res.agentType == 1){
                    introduceNewUser($rootScope.shareState, res.fullname);  
                    $scope.refCode = res.reference_code;
                    $scope.showShareMethod = true;
                    //$scope.url = creatWxUrl(localStorage.getItem('referId'));;
                }else{
                    $scope.showShareMethod = false;
                    httpServices.extractUserInfo(localStorage.getItem('referId')).then(
                        function(res){
                            //console.log(res);
                            introduceNewUser("?referid=" + localStorage.getItem('referId'), res.fullname);
                            $scope.refCode = res.reference_code;
                            //$scope.url = creatWxUrl(localStorage.getItem('referId'));
                        }
                    );   
                }
            }
        );
    }
    
})
.controller("userUpload", function(localStorage, $rootScope, userAuth, $state, $scope, urlConfig, httpServices, $ionicPopup){

        var user = new userAuth.checkUcLogin();
    
        $rootScope.hideTabs = '';
    
        if(user.isLogin !== true){
            $ionicPopup.alert({
                title: "留哪儿",
                template: "请微信授权后再申请认证达人"
            }).then(
                function(){
                    $state.go("userLogin",{lastview: '/app/userapplyagent'});     
                }
            ); 
        }else{
            
            httpServices.extractUserInfo(user.uid).then(
                function(res){
                    if(res.agentType === 1){
                        $ionicPopup.alert({
                            title: "留哪儿",
                            template: "您已经是留哪儿的认证达人了"
                        }).then(
                            function(){
                                $state.go("app.userCenter");     
                            }
                        );     
                    }
                }
            );
            
            $scope.new = {
                phone: ""
            }

            /*$scope.update = function(){
                if($scope.new.phone !== ""){
                    httpServices.updateUserInfo(user.uid, $scope.new.phone).then(
                        function(res){
                            //console.log(res);
                        }
                    );
                } 
            };*/

            //uploadStatus: a => uploading; b => success; c => fail; 
            
            $scope.uploadID = function(file,err){
                //console.log(file,err);
                if(file !== null){
                    $scope.idUploadProgress = 0;
                    httpServices.uploadUserFiles(file, 2, user.uid).then(
                        function(res){
                            //console.log(res);
                            if(res.data.file){
                                $scope.idUploadStatus = 'b';
                                $scope.statusText = "上传成功！";
                                $scope.statusColor = "#9BCF00";
                            }else if(res.data.err){
                                $scope.idUploadStatus = 'c';
                                $scope.statusText = "上传失败！请重试";
                                $scope.statusColor = "#942200";
                            };
                            //console.log($scope.idUploadStatus);
                        },
                        function(res){
                            //console.log(res);
                            $scope.idUploadStatus = 'c';
                            $scope.statusText = "上传失败！请重试";
                            $scope.statusColor = "#942200";
                        },
                        function(res){
                            $scope.idUploadStatus = 'a';
                            $scope.idUploadProgress = res;
                            //console.log(res, $scope.idUploadStatus);
                        }
                    );
                }else{
                    $scope.idUploadStatus = 'c';
                    $scope.statusText = "请选择有效的文件类型－PDF或者图片文件";
                    $scope.statusColor = "#942200";    
                }
            };

            $scope.errText = "";
            
            $scope.slogan = "更聪明的留学与移民";
            
            if(localStorage.getItem("referId") && localStorage.getItem("referId") != null){
                httpServices.getRefCodeById(localStorage.getItem("referId")).then(
                    function(res){
                        console.log(res);
                        if(!res.err){
                            $scope.refCode = res.reference_code;
                        }
                    }
                )
            }else{
                $scope.refCode = null;
            }

            $scope.applyAgent = function(refCode, name, phone, slogan, wechat, email){
                //console.log(name, phone, slogan);
                if($scope.idUploadStatus == 'b'){
                    if(refCode !== null && refCode !== undefined){
                        httpServices.getIdByRefCode(refCode).then(
                            function(res){
                                if(res.err){
                                    $scope.errText = "您输入的并不是有效推荐码";    
                                }else{
                                    if(name !== undefined && phone !== undefined){
                                        if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test(phone) == true){
                                            httpServices.updateUserInfo(user.uid, phone, name, slogan, wechat, res.id, email).then(
                                                function(res2){
                                                    //console.log(res2);
                                                    $scope.errText = "";
                                                    if(!res2.err){
                                                        $ionicPopup.alert({
                                                            title: '留哪儿',
                                                            template: '恭喜您提交申请成功，我们会尽快审核'
                                                        }).then(
                                                            function(){
                                                                $state.go("app.userApplyStatus");
                                                            }
                                                        );    
                                                    }
                                                }
                                            );
                                        }else{
                                            $scope.errText = "电话号码只能由 +,-,0-9组成";    
                                        }
                                    }else{
                                        $scope.errText = "姓名与电话号码不能为空";
                                    }    
                                }
                            }
                        );
                    }else{
                        $scope.errText = "推荐码不能为空";    
                    }
                }else{
                    $scope.errText = "请成功上传身份信息文件后再提交";
                }
            };
        }
})
.controller('promoteController', function($rootScope, $scope, wechatShare){
    $scope.$on("$ionicView.beforeEnter", function(){
        
        $rootScope.hideTabs = 'tabs-item-hide';
        
        wechatShare('招募留学教育合伙人', "http://www.liunar.com/img/vip1.jpg", '零投入，无需场地，共享经济新模式');
        
    });
})
.controller("testUpload", function($rootScope, localStorage, $state, $scope, urlConfig, httpServices, $ionicPopup){
            $scope.new = {
                phone: ""
            }
            
            httpServices.getRefCodeById(localStorage.getItem("referId") || 0).then(
                function(res){
                    //console.log(res);
                    $scope.referenceCode = res.reference_code;
                }
            )

            $scope.update = function(){
                if($scope.new.phone !== ""){
                    httpServices.updateUserInfo(62, $scope.new.phone).then(
                        function(res){
                            //console.log(res);
                        }
                    );
                } 
            };
            
            //uploadStatus: a => uploading; b => success; c => fail; 
            
            $scope.uploadID = function(file,err){
                //console.log(file,err);
                if(file !== null){
                    $scope.idUploadProgress = 0;
                    httpServices.uploadUserFiles(file, 2, 62).then(
                        function(res){
                            //console.log(res);
                            if(res.data.file){
                                $scope.idUploadStatus = 'b';
                                $scope.statusText = "上传成功！";
                                $scope.statusColor = "#9BCF00";
                            }else if(res.data.err){
                                $scope.idUploadStatus = 'c';
                                $scope.statusText = "上传失败！请重试";
                                $scope.statusColor = "#942200";
                            };
                            //console.log($scope.idUploadStatus);
                        },
                        function(res){
                            //console.log(res);
                            $scope.idUploadStatus = 'c';
                            $scope.statusText = "上传失败！请重试";
                            $scope.statusColor = "#942200";
                        },
                        function(res){
                            $scope.idUploadStatus = 'a';
                            $scope.idUploadProgress = res;
                            //console.log(res, $scope.idUploadStatus);
                        }
                    );
                }else{
                    $scope.idUploadStatus = 'c';
                    $scope.statusText = "请选择有效的文件类型－PDF或者图片文件";
                    $scope.statusColor = "#942200";    
                }
            };

            /*$scope.uploadQRcode = function(file, err){
                //console.log(file);
                if(file !== null){
                    $scope.qrUploadProgress = 0;
                    httpServices.uploadUserFiles(file, 2, 62).then(
                        function(res){
                            //console.log(res);
                            if(res.data.file){
                                $scope.qrUploadStatus = 'b';
                                $scope.statusText = "上传成功！";
                                $scope.statusColor = "#9BCF00";
                            }else if(res.data.err){
                                $scope.qrUploadStatus = 'c';
                                $scope.statusText = "上传失败！请重试";
                                $scope.statusColor = "#942200";
                            };
                            //console.log($scope.qrUploadStatus);
                        },
                        function(res){
                            //console.log(res);
                            $scope.qrUploadStatus = 'c';
                            $scope.statusText = "上传失败！请重试";
                            $scope.statusColor = "#942200";
                        },
                        function(res){
                            $scope.qrUploadStatus = 'a';
                            $scope.qrUploadProgress = res;
                            //console.log(res, $scope.qrUploadStatus);
                        }
                    );
                }else{
                    $scope.qrUploadStatus = 'c';
                    $scope.statusText = "请选择有效的文件类型－图片文件";
                    $scope.statusColor = "#942200";    
                }
            };*/

            $scope.errText = "";

            $scope.applyAgent = function(refCode, name, phone, slogan, wechat, email){
                //console.log(name, phone, slogan);
                if($scope.idUploadStatus == 'b'){
                    if(refCode !== undefined){
                        httpServices.getIdByRefCode(refCode).then(
                            function(res){
                                //console.log(res);
                                if(res.err){
                                    $scope.errText = "您输入的并不是有效推荐码";    
                                }else{
                                    if(name !== undefined && phone !== undefined){
                                        if(/^(\+?\d{1,3})(-?\d{2,4})(-?\d{3,4})(-?\d{3,4})$/.test(phone) == true){
                                            httpServices.updateUserInfo(62, phone, name, slogan, wechat, res.id, email).then(
                                                function(res2){
                                                    //console.log(res2);
                                                    $scope.errText = "";
                                                    if(!res2.err){
                                                        $ionicPopup.alert({
                                                            title: '留哪儿',
                                                            template: '恭喜您提交申请成功，我们会尽快审核'
                                                        }).then(
                                                            function(){
                                                                $state.go("app.userApplyStatus");
                                                            }
                                                        );    
                                                    }
                                                }
                                            );
                                        }else{
                                            $scope.errText = "电话号码只能由 +,-,0-9 组成";    
                                        }
                                    }else{
                                        $scope.errText = "姓名与电话号码不能为空";
                                    }    
                                }
                            }
                        );
                    }else{
                        $scope.errText = "推荐码不能为空";    
                    }
                }else{
                    $scope.errText = "请成功上传身份信息文件后再提交";
                }
            };
});
