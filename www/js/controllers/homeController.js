angular.module('homeController',[])
.controller('homeController', function($ionicPopup, $scope, $ionicModal, $ionicScrollDelegate, httpServices, $ionicSlideBoxDelegate,$rootScope, $ionicPosition, $location, $stateParams, localStorage, $sce, $state, $location, $window, wechatShare, $ionicPopup, $timeout) {
  
    wechatShare("留哪儿","http://liunar.com/img/icon.png","更聪明的留学与移民");
    
    $scope.slick = false;
    
    //hide header bar scroll top:
    $scope.shouldHide = true;
    
    $scope.goTo = function(where){
        return $state.go('app.shopnew').then(
            function(){
                $location.hash(where);
            }
        );    
    };
    
    $scope.headerBarControll = function(){
        if($ionicScrollDelegate.getScrollPosition().top > 100){
            $scope.shouldHide = false;
        }else{
            $scope.shouldHide = true;
        }
    }; 
    
    $scope.slideChanged = function(index){
        $scope.slideIndex = index;
    };
    
    httpServices.getHomePosts().then(
        function(res){
            $scope.posts = res;
        }
    );
    
    httpServices.getHomePlaceholder().then(
        function(res){
            console.log(res);
            $scope.placeholderOne = $sce.trustAsHtml(res[1].content_html);
            $scope.questions = $sce.trustAsHtml(res[4].content_html);
            $scope.placeholderTwo = $sce.trustAsHtml(res[2].content_html); 
            $scope.topSlider = $sce.trustAsHtml(res[0].content_html);
            $scope.slickConfig.enabled = true;
            $timeout(
                function(){
                    $scope.slick = true
                }
            );
            console.log($scope.questions);
        }
    );
    
    httpServices.getHomeEvents().then(
        function(res){
            $scope.events = res;
            $scope.now = (new Date().getTime())/1000;
        }
    );
    
    $scope.slickConfig = {
        enabled: false,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 3000,
        dots: true,
        method: {},
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
            }
        }
    };
    
    $scope.grades = {
        "一年级": 1,
        "二年级": 2,
        "三年级": 3,
        "四年级": 4,
        "五年级": 5,
        "六年级": 6,
        "初一": 7,
        "初二": 8,
        "初三": 9,
        "高一": 10,
        "高二": 11,
        "高三": 12,
    };
    
    $scope.countries = {
        "美国": "America",
        "加拿大": "Canada",
        "英国": "England",
        "澳大利亚": "Australia",
        "新西兰": "New Zealand",
        "其他": "Other",
    };
    
    $scope.collectObj = {
        country: "Canada",
        grade: "10",
    }
    
    $scope.collectForm = function(){
        var myPopUp = $ionicPopup.show(
            {
                cssClass: "collectForm",
                scope: $scope,
                title: "中外双总部 8位跨国导师",
                subTitle: "1600名学员成功入读北美 Top100",
                templateUrl: 'templates/collectForm.html',
                buttons: [
                    { 
                        text: '免费获取定制方案',
                        onTap: function(e) {
                            if(!$scope.collectObj.name || !$scope.collectObj.phone){
                                $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '名字与电话不能为空'
                                }).then(
                                    function(res){
                                        $scope.collectObj.name = null;
                                        $scope.collectObj.phone = null;
                                    }
                                );
                                e.preventDefault();
                            }else{
                                $ionicPopup.alert({
                                    title: '留哪儿',
                                    template: '我们将会在24小时内联系您，为您定制方案。'
                                }).then(
                                    function(res){
                                        $scope.collectObj.name = null;
                                        $scope.collectObj.phone = null;
                                    }
                                );      
                            }    
                        }
                    },
                ]
            }
        );
        
        myPopUp.then(function(res){
            console.log(res);    
        });
        
        $scope.closePop = function(){
            myPopUp.close();
        };
    };
})
.controller("tabsController", function($scope, $state){
    function getRegionValue(){
        if(document.cookie.indexOf('region')>-1){
     match = document.cookie.match(new RegExp('region' + '=([^;]+)'));
     if(match && match[1] != null){
          return match[1];
      if (location.href.indexOf(regionValue)== -1 ) {
       //current region and cookie region differs
      }
     }
        }
    }
    $scope.goToHome = function(){
        window.location.href="http://www.liunar.com/"+getRegionValue();
        //$state.go("app.mainHome");
    };
    
    $scope.goToSchool = function(){
        window.location.href="http://www.liunar.com/schools/";
        //$state.go("app.school");
    };
    
    $scope.goToShop = function(){
        window.location.href="http://shop.liunar.com/"+getRegionValue();
        //$state.go("app.shopnew");
    };
    
});
