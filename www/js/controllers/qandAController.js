angular.module('qandAController',[])
.controller('qandAController', function(localStorage, $scope,httpServices,$stateParams,$state,$ionicHistory, $ionicPopup, newQuestion, hotQuestions){
    $scope.hotQuestions = hotQuestions;
    
    $scope.newQuestion = newQuestion;
    
})
.controller('qandAListController', function(localStorage, $scope,httpServices,$stateParams,$state,$ionicHistory, $ionicPopup, initQuestions){
    
    //console.log($stateParams, initQuestions);
    
    $scope.order = $stateParams.order;
    
    if($stateParams.order === "answer_num"){
        $scope.listTitle = "最热问题";
    }else if($stateParams.order === "create_time"){
        $scope.listTitle = "最新问题";
    }
    
    var questionNum = 10;
    
    $scope.questions = initQuestions;

    var ids =[];

    var pushId = initQuestions.forEach(
        function(v,i){
            ids.push(v.id);
        }
    );

    
    $scope.loadMoreQuestions = function(){
        
        questionNum = questionNum + 5;
        //console.log(questionNum);
        httpServices.getQuestions(1,1,$stateParams.order,questionNum).then(
            function(res){
                $scope.questions = res;
                ids = [];
                res.forEach(function(v,i){
                    ids.push(v.id);
                });
                //console.log(res);
                //console.log(ids);
                $scope.$broadcast('scroll.infiniteScrollComplete');
                
            }
        );
    };
    
})
.controller('qandATypeListController', function($scope, $stateParams, httpServices){
    //console.log($stateParams);
    var questionNum = 10;
    $scope.haveMore = true;
    httpServices.getQuestionsByType($stateParams.typeId, questionNum).then(
        function(res){
            //console.log(res);
            $scope.questions = res;
        }
    );
    
    $scope.loadMoreQuestions = function(){
        
        questionNum = questionNum + 5;
        //console.log(questionNum);
        httpServices.getQuestionsByType($stateParams.typeId, questionNum).then(
            function(res){
                $scope.questions = res;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                if(questionNum > res.length){
                    $scope.haveMore = false;
                };
            }
        );
        
        //console.log($scope.haveMore);
    };
    
})
.controller('qandAFormController', function($scope, localStorage, $ionicPopup, httpServices, $state, hotQuestions){
    
    $scope.$on("$ionicView.beforeEnter", function(){
        //console.log("yay");
        if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再提问'
            }).then(
                function(res){
                    $state.go("userLogin",{lastview: "/app/mainhome/QandA/QandAForm"});
                }
            );
        }    
    });
    //ask questions form:
    /*$scope.tags = [];
    
    $scope.addTags = function(tag){
        if($scope.tags.indexOf(tag) === -1){
        $scope.tags.push(tag);
        }else{
            //console.log("exist");
        }
    };
    
    $scope.removeTags = function(tag){
        if($scope.tags.indexOf(tag) !== -1){
        //console.log(tag);
        $scope.tags.splice($scope.tags.indexOf(tag),1);
        }else{
        }
    };*/
    
    $scope.hotQuestions = hotQuestions;
    
    $scope.questionObj = {
        title: "",
        description: "",
        category: "1"
        //tags: $scope.tags
    };
    
    $scope.submitQ = function(){
            if($scope.questionObj.title == "" || $scope.questionObj.title.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '问题不能为空，而且必须多于6个字'
                });  
            }else{
                $scope.questionObj.uid = localStorage.getObject("canbestUserInfo").uid;
                httpServices.postQuestion($scope.questionObj).then(
                    function(res){
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的提问，我们将会在24小时内答复。'
                        }).then(
                            function(){
                                $state.go("app.QandA");
                            }
                        )
                    }
                );
            }        
    };
});