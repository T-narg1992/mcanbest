const concat = require('gulp-concat');  
const rename = require('gulp-rename');  
const uglify = require('gulp-uglify');
const gulp = require('gulp');
const ngAnnotate = require('gulp-ng-annotate');


var need_ng_files = ["../js/**/*.js", "!../js/liunar.js", "!../js/liunar.min.js", "!../js/server_api.js", "!../js/server_api.sample.js", "!../js/ng/**/*.js"];

var ng_dest = "../js/ng";

var jsFiles = "../js/ng/**/*.js";

var dest = "../js";

gulp.task('ng-js', function () {
    return gulp.src(need_ng_files)
        .pipe(ngAnnotate())
        .pipe(gulp.dest(ng_dest));
});

gulp.task('scripts', function() {  
    return gulp.src(jsFiles)
        .pipe(concat('liunar.js'))
        .pipe(gulp.dest(dest))
        .pipe(rename('liunar.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(dest));
});