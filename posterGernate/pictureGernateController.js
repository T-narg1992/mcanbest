const fs = require("fs");
const pdf = require("html-pdf");
const _ = require("underscore");
const options = {
    "directory": "gernates/posters",     
    "border": "0",
    "type": "jpg",
    "quality": "100",
	"zoomFactor": "1",  // allowed units: A3, A4, A5, Legal, Letter, Tabloid 
  	height: "1200px",
	width: "700px",
  	orientation: 'portrait',
};


var createPNG = function(author, avatar, type, title, body, qrcode, expire, callback){
	var html = fs.readFileSync('posterGernate/assets/templateOne.html', 'utf8');
	var compiled = _.template(html);
	var time = new Date(Math.floor(parseInt(expire)*1000)).toDateString();
	var parsedHtml = compiled(
		{
			author: author, 
			avatar: avatar,
			title: title,
			background: type == 1 ? "http://api.liunar.com/posterGernate/assets/imgs/t_1.png" : "http://api.liunar.com/posterGernate/assets/imgs/t_2.png",
			color: type == 1 ? "#88C8B8" : "#E98A61",
			body: body.substring(0, 50)+"...", 
			qrcode: qrcode, 
			expire: time
		}
	);

	pdf.create(parsedHtml, options, function(err, res) {
	  if (err){ 
		  callback({"err": err});
	  }else{
		  if(res && !res.filename){
			  callback({"err": res});
		  }else{
			  callback(res.filename);
		  }
	  }

	});
};

module.exports = {
	createPNG: createPNG
}