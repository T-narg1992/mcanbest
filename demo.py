#encoding=utf-8
import sys
import mysql.connector
import time
import datetime
from dateutil import parser
import urllib
import urllib2
import requests
default_encoding = 'utf-8'
import json
import auth
from yzclient import YZClient


#phone: trades[x].orders.buyer_messages[0].content '手机号码'
#email: trades[x].orders.buyer_messages[1].content '电子邮箱'
#check trades[x].status = "WAIT_BUYER_CONFIRM_GOODS" 
# get trades[x].created > latest time in db


#query = ("SELECT first_name, last_name, hire_date FROM employees "
#"WHERE hire_date BETWEEN %s AND %s")
#
#hire_start = datetime.date(1999, 1, 1)
#hire_end = datetime.date(1999, 12, 31)
#
#cursor.execute(query, (hire_start, hire_end))


from random import choice
from string import ascii_uppercase

def get_random_string(length,root=""):
    return ''.join(choice(ascii_uppercase) for i in range(length))

def save_record(cnx, cursor, sku_id ,prd, code, contact, _type, status, fail_msg, created, retry_time=None): #data connection, data obj, 'phone'/'email', 0/1
   
    record_sent_code = ("INSERT INTO purchase_code (outer_sku_id, prd_title, code, contact, type, status, fail_msg, order_created_time, retry_time) VALUES (%s, %s,%s, %s, %s, %s, %s, %s, %s)")
    cursor.execute(record_sent_code, (sku_id, prd, code, contact, _type, status, fail_msg, created, retry_time))
    cnx.commit()
def getLastSavedOrdertime(cnx, cursor):
    cursor.execute('SELECT order_created_time FROM purchase_code ORDER BY order_created_time DESC LIMIT 1')
    for (order_created_time,) in cursor:
	start_created = str(order_created_time+datetime.timedelta(seconds=1))
	return start_created
def retryErrors(cnx, cursor):
    cursor.execute('SELECT * FROM purchase_code WHERE status = "1" AND retry_time < %s ', (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),) )
    for(outer_sku_id, prd_title, code, contact, type) in cursor:
	print None

def getSMSApi(headers, data):
        
    request = urllib2.Request(url='http://test.liunar.com/api/sms', headers=headers, data=json.dumps(data))
    response = urllib2.urlopen(request)
    resp = json.loads(response.read())
    if resp['message'].find('Permits:10') > -1:
	resp['retry_time'] = str(datetime.datetime.now()+datetime.timedelta(hours=12))
    elif  resp['message'].find('Permits:1') > -1:
	resp['retry_time'] = str(datetime.datetime.now()+datetime.timedelta(minutes=1))
    return resp
def getEmailApi(headers, prd_title, code, contact2):
    title = "春藤公学商品兑换码"
    body_pre = u"<html><p>亲爱的春藤公学用户:</p><p>您购买的商品为："+prd_title+ u"</p><p>领取码为："+code+ u"</p><p>领取方法说明请关注『全球家长营』公众号</p><p>春藤公学团队</p ></html>"
    body = body_pre.encode('utf8')
    data_email = {"recipient": contact2, "title": title,"body": body}
    request = urllib2.Request(url='http://ystage.liunar.com/account/SendEmail', headers=headers, data=json.dumps(data_email))	
    response = urllib2.urlopen(request)
    resp = json.loads(response.read())
    return resp
	
if __name__ == '__main__':
    cnx = mysql.connector.connect(user='alex', password='alexsong',host='www.liunar.com', database='app', charset='utf8')
    cursor = cnx.cursor()
    token = auth.Token(token='b6917fc42ced3793a531f5ddcc6389fc')
    client = YZClient(token)
    params={}
    params['start_created'] = getLastSavedOrdertime(cnx, cursor)
    params['end_created'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    #params['page_size']=20
    #params['page_no']=1
    #params['type']="ALL"
    params['status'] = "WAIT_BUYER_CONFIRM_GOODS"
    #params['store_id'] = 40343044
    #params['start_created'] = "2018-02-01 12:00:00"
    #params['with_childs'] = True
    files=[]
    while True:
        print "querying from "+ params['start_created'] + 'to' + params['end_created']
    	trades = json.loads(client.invoke('youzan.trades.sold.get', '3.0.0', 'GET', params=params, files=files))['response']['trades']
 
    	for i in range(len(trades)):
	    try:
	        if trades[i]['orders'][0]['buyer_messages']:
                    contact1 = trades[i]['orders'][0]['buyer_messages'][0]['content'] #phone
            
	        if trades[i]['orders'][0]['buyer_messages']:
	            contact2 =  trades[i]['orders'][0]['buyer_messages'][1]['content'] #email
	    except IndexError:
	        print i
            prd_title = trades[i]['title']
            created = trades[i]['created']
	    code = get_random_string(7)
	    sku_id = trades[i]['orders'][0]['outer_sku_id']
	    
	    headers = {'Content-Type': 'application/json'}
            if len(contact1) > 0: #if phone
	        data_phone = {"product": prd_title , "code":code, 'phone': contact1}
		res = getSMSApi(headers, data_phone)
	        save_record(cnx, cursor, sku_id, prd_title, code, contact1, 'phone', res['state'], res['message'], created)
	    if len(contact2) > 0: #if email
		res = getEmailApi(headers, prd_title, code, contact2)
	        save_record(cnx, cursor, sku_id, prd_title, code, contact2, 'email', res['code'], res['errmsg'], created)
	    print(sku_id, contact1, contact2, code)
	
	params['end_created'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    	params['start_created'] = getLastSavedOrdertime(cnx, cursor)
	time.sleep(5)
    cursor.close()
    cnx.close()
       
    params={}
    files=[]
         
        
    #print client.invoke('youzan.shop.get', '3.0.0', 'GET', params=params, files=files)

