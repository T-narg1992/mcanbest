angular.module('schoolController',[])
.controller('schoolController', function($scope, $http, httpServices, $ionicPosition, $stateParams){
    //functions => load Content:
    if($stateParams.cateID == 1){
        
        $scope.title = "加拿大大学排名";
    
    }else if($stateParams.cateID == 2){
        
        $scope.title = "加拿大特色院校";
    
    }else if($stateParams.cateID == 3){
        
        $scope.title = "BC省中学排名";
        
    }else if($stateParams.cateID == 4){
        
        $scope.title = "BC省小学排名";
        
    };
    
    var getSchoolsRank = function(){
        //console.log($stateParams);
        httpServices.getSchoolRankList($stateParams.cateID).then(
            function(res){
                $scope.schoolListsRank = res;
                console.log(res);
            }
        );
    }
    
    $scope.$on("$ionicView.beforeEnter", function(){
        getSchoolsRank();
    });
    
})
.controller('campController', function($scope, $http, httpServices, $ionicPosition, $stateParams){
    
    $scope.getCampList = function(){
        
        httpServices.getCampProducts(15).then(
            function(res){
                console.log(res);
                $scope.camplist = res.all;
            }
        );
    };
})