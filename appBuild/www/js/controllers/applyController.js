angular.module('applyController',[])
.controller('applyController', function($scope, $state, $stateParams, $ionicHistory, $ionicPopup, httpServices){
    
    $scope.applyObj = {
        name: "",
        number: "",
        description: "from mobile"
    };
    
    console.log($scope.applyObj);
    
    
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.from = $ionicHistory.viewHistory().backView.url;
        console.log($scope.from, $stateParams);  
        $scope.apply = function(){
            console.log($scope.applyObj.number);
            if($scope.applyObj.name === "" || $scope.applyObj.name.length === 0){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '名字不能为空'
                }).then(
                );
            }else if($scope.applyObj.number === 0 || $scope.applyObj.number === ""){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '请输入有效电话号码'
                }).then(
                );
            }else{
                $scope.applyObj.referrer = $stateParams.pageTitle + "/" + $stateParams.productId + $scope.from;
                console.log($scope.applyObj.referrer);
                httpServices.apply($scope.applyObj).then(
                    function(res){
                        if(res !== 0 ){
                           $ionicPopup.alert({
                                title: '留哪儿',
                                template: '感谢您的申请，我们将会在24小时内联系您！'
                            }).then(
                               function(){
                                   $state.go('app.mainHome');
                               }
                            ); 
                        }
                    }
                );
            }
        }
    });
})