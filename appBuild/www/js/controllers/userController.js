angular.module('userController',[])
.controller('userController', function(httpServices, $scope,localStorage,$state,$ionicPopup,$ionicHistory,$rootScope,$location, $stateParams){  
    var userInit = function(){
        if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){ 
            $state.go("userLogin",{state: $rootScope.shareState});
            //$scope.url = 'templates/userAlreadyLogin.html';
        }else{
            $rootScope.userInfo = localStorage.getObject("canbestUserInfo");
            httpServices.extractUserInfo(localStorage.getObject("canbestUserInfo").uid).then(
                function(res){
                    console.log(res);
                    $scope.totalPoints = res.loginPoints + res.userIntroPoints + res.salePoints + res.sharePoints;
                    $scope.agentType = res.agentType == null ? '普通达人':'认证达人';
                }
            );
            httpServices.extractUserPoints(localStorage.getObject("canbestUserInfo").uid).then(
                function(res){
                    console.log(res);
                    $scope.pointHistory = res;
                    $scope.pointTypes = ['首次微信授权','引进新用户','产品销售','内容分享'];
                }
            );
        }
    };
    
    //console.log($stateParams);
    
    
    $scope.$on("$ionicView.beforeEnter", function(event){
        userInit();
    });
    
    $scope.logout = function(){
      localStorage.clear();
      $state.go("userLogin",{state: $rootScope.shareState});
      console.log("dude", $scope.url);
    };
})











/////////////////
 /*$scope.loginObj = {
        username: "",
        password: ""
    };
    
    $scope.signupObj = {
        username: "",
        password: "",
        confirmPassword: ""
    }
    
    $scope.signup = function(){
        if($scope.signupObj.username !== ""){
            var pattern = /^[a-zA-Z0-9]{4,}$/;
            if(pattern.test($scope.signupObj.username) === true){
                if($scope.signupObj.password !== ""){
                    var passwordPattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/ig;
                    if($scope.signupObj.password.length >=6 && passwordPattern.test($scope.signupObj.password) === true){
                        if($scope.signupObj.password === $scope.signupObj.confirmPassword){
                            //妈的。。。。最后终于。。。。！！！！
                            delete $scope.signupObj.confirmPassword;
                            
                            httpServices.userSignup($scope.signupObj).then(
                                function(res){
                                    console.log(res);
                                    if(res > 0){
                                        
                                        alert("感谢您的注册");
                                        $scope.loginObj = {
                                            username: $scope.signupObj.username,
                                            password: $scope.signupObj.password
                                        };
                                        
                                        $scope.login();
                                    }
                                }
                            );
                            console.log($scope.signupObj);
                        }else{
                            alert("两次密码必须一样");
                        }
                    }
                    else{
                        alert("密码必须至少6位，并且至少1个数字和1个字母的组合且不包含任何特殊符号");
                    }
                }else{
                    alert("密码不能为空");
                }
            }else{
                alert("用户名必须至少4位，并且不包含任何特殊符号");
            }
        }else{
            alert("用户名不能为空");
        }   
    };*/