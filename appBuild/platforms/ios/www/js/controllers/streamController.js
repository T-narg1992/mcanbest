angular.module('streamController',[])
.controller('streamController', function(initList, $scope, $ionicPopup, httpServices){
	var num = 5;
    
    console.log(initList, initList[0].thumbnail);
    
    $scope.stream = initList;
    
    $scope.haveMore = true;
    
	$scope.getMoreStream = function(){
		num += 5;
		httpServices.getStreamList(num).then(
			function(res){
				console.log(res, res.thumbnail);
				$scope.stream = res;
				$scope.$broadcast('scroll.infiniteScrollComplete');
                if(num > res.length){
                    $scope.haveMore = false;
                };
			});
	};
    
    $scope.$on("$ionicView.beforLeave", function(){
        $scope.haveMore = true;
    });

	$scope.tags = {0:"其它", 1:"海外规划", 2:"留学申请", 3:"校园内外", 4:"本地生活", 5:"实习就业", 6:"职业移民"};
	$scope.urlQuestion = "#/app/mainHome/QandA/QandAList/QandADetail/";
	$scope.urlPostDetail = "#/app/mainHome/postDetail/";
});