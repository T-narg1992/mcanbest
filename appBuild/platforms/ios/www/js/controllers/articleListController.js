angular.module('articleListController',[])
.controller('liunarTouTiaoListController', function($scope, httpServices){
    
    var articleNum = 5;
    $scope.getArticleList = function(){
        console.log(articleNum);
        httpServices.getGongLueList(articleNum).then(
            
            function(res){
                console.log(res);
                $scope.articles = res;
            }
        )
    };
    
    $scope.getMoreArticles = function(){
        //console.log(articleNum);
        articleNum = articleNum + 5;
        console.log(articleNum);
        httpServices.getGongLueList(articleNum).then(
            function(res){
                console.log(res);
                $scope.articles = res;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                console.log("scroll complete");
            }
        )
    };
    
    $scope.tags = {0:"其它", 1:"海外规划", 2:"留学申请", 3:"校园内外", 4:"本地生活", 5:"实习就业", 6:"职业移民"};
    
    
    
    
})
.controller('huMaListController', function($scope, httpServices){
    //console.log("huma");
    var num  = 5;
    $scope.$on("$ionicView.beforeEnter", function(){
        $scope.getHuMaList = function(){
            httpServices.getHuMaList(num).then(
                function(res){
                    console.log(res);
                    $scope.humaLists = res;
                }
            )
        };
    });
    
    $scope.$on("$ionicView.beforLeave", function(){
        $scope.haveMore = true;
    });
    
    $scope.haveMore = true; 
    
    $scope.getMoreHuMa = function(){
        //console.log(articleNum);
        num = num + 5;
        
        httpServices.getHuMaList(num).then(
            function(res){
                console.log(res);
                $scope.humaLists = res;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                if(num > res.length){
                    $scope.haveMore = false;
                };
                console.log("scroll complete", $scope.haveMore, num, res.length);
            }
        )
    };
    
    $scope.tags = {0:"其它", 1:"海外规划", 2:"留学申请", 3:"校园内外", 4:"本地生活", 5:"实习就业", 6:"职业移民"};
    
})