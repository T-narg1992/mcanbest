angular.module('agencyController',[])
.controller('agencyController', function($scope, $ionicPopup){
    $scope.QRPopUp = function(){
        $ionicPopup.alert({
            title: "长按二维码识别",
            template:"<div style='padding: 24px;'><img src='https://upload.wikimedia.org/wikipedia/commons/0/0b/QR_code_Wikimedia_Commons_(URL).png' width='100%' /></div>"
        });
    };
})