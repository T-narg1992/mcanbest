angular.module('detailController',[])
.controller('detailController', function(localStorage, $ionicPopup, $scope, httpServices, $rootScope, $stateParams, $sce, $state, $ionicHistory, postDetail, $window, $location, jsSdkStatus){
    
    console.log($window.wx);
    
    console.log(jsSdkStatus);
    
    /*var wx = $window.wx;
    
    var cfg = {
        debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: jsSdkStatus.appId, // 必填，公众号的唯一标识
        timestamp: jsSdkStatus.timestamp, // 必填，生成签名的时间戳
        nonceStr: jsSdkStatus.noncestr, // 必填，生成签名的随机串
        signature: jsSdkStatus.signature,// 必填，签名，见附录1
        jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage']
    };

    wx.config(cfg);

    wx.ready(function(){
        alert("ready: " + $location.$$absUrl + jsSdkStatus.signature + JSON.stringify(cfg) + JSON.stringify(jsSdkStatus));
    });

    wx.error(function(res){
        alert("error: " + $location.$$absUrl + jsSdkStatus.signature + JSON.stringify(res) + JSON.stringify(cfg) + JSON.stringify(jsSdkStatus));
    });

    wx.onMenuShareTimeline({
        title: postDetail.title, // 分享标题
        link: $location.$$absUrl, // 分享链接
        imgUrl: "http://www.liunar.net" + postDetail.thumbnail, // 分享图标
        success: function () { 
            alert("success Post");
        },
        cancel: function () { 
            alert("cancel");
        },
        error: function(){
            alert("error");
        }
    });*/
    
    $scope.$on("$ionicView.beforeEnter", function(){
        console.log($state.current, $state, $ionicHistory.viewHistory(), postDetail);
        if($ionicHistory.viewHistory().backView !== null){
            $scope.from = $ionicHistory.viewHistory().backView.url;
        }else{
            $scope.from = "/app/mainHome";
            $scope.fromShare = true;
        }
    });
        
        $scope.postDetail = postDetail;
        console.log($scope.postDetail);
        
        var desHTML = postDetail.description;
        $scope.description = $sce.trustAsHtml(desHTML);
        var html = postDetail.content_html;
        $scope.postDetailContent = $sce.trustAsHtml(html);
       
    $scope.getPostComments = function(){
       httpServices.getPostDetailsComment($stateParams.postId).then(
            function(res){
                if(res[0]==="error"){
                }else{
                    $scope.comments = res;
                    console.log(res);
                }
            }
        );
    };
    
    $scope.commentObj = {
        row_id : $stateParams.postId,
        content: "",
        module: 1   
    };
    
    $scope.postNewsComment = function(){
        if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $state.go("app.userCenter");    
                }
            );
            //$state.go();
        }else{
            $scope.commentObj.uid = localStorage.getObject("canbestUserInfo").uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.commentObj.content = null;
                                $scope.getPostComments();
                            }
                        )
                    }
                );
            }
        }
    };
    
})