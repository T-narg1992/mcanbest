// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('canbest', ['ionic', 'canbestServer', 'qandAController', 'qandADetailController', 'streamController', 'userController', 'homeController', 'agencyController', 'detailController', 'schoolController', 'schoolDetailController','articleListController','estimateController','liunarProduct','applyController'])

.run(function($http, $ionicPlatform,localStorage,$state, $location, httpServices, pressureTest) {
    //console.log($location);
    
    var urlstring = $location.$$absUrl;
    
    var start = urlstring.indexOf("?code=");
    if(start !== -1){
        start += "?code=".length;
        var end = urlstring.indexOf('&', start);
        
        if(end === -1){
            var code_value = urlstring.substring(start);
        }else{
            var code_value = urlstring.substring(start, end);
        }
        
        httpServices.wechatLogin(code_value).then(
            function(res){
                //alert(res);
                localStorage.setObject("canbestUserInfo", res);
                if(localStorage.getItem("refer") === null){
                    $state.go("app.mainHome");
                }else{
                    $state.go("app.userCenter");
                }
            }
        )
        
    };
    
    /*pressureTest.post();
    
    pressureTest.put();
    
    pressureTest.get();*/
  
    document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
        console.log("get it run!");
        WeixinJSBridge.on('menu:share:appmessage', function(argv){
            /*function shareFriend() {  
               WeixinJSBridge.invoke('sendAppMessage',{  
                "appid": appid,  
                "img_url": imgUrl,  
                "img_width": "200",  
                "img_height": "200",  
                "link": lineLink,  
                "desc": descContent,  
                "title": shareTitle  
               });
            }*/
                                     
            console.log("分享好友");
        });
        WeixinJSBridge.on('menu:share:timeline', function(argv){
            console.log("分享朋友圈");
        });
    });
    
    $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
.directive('elScroll', function($location,$ionicScrollDelegate) {
	return {
		restrict: 'A',
		scope: {
			target:"@elScroll",
			delegate: "@",
			animate: "@"
		},
		link: function(scope, element, attrs) {
		},
		controller: function($scope, $element, $attrs) {
			var shouldAnimate = $scope.animate;
			$scope.scrollTo = function(){
				$location.hash($scope.target);
				var handle = $ionicScrollDelegate.$getByHandle($scope.delegate);
				handle.anchorScroll(shouldAnimate);
			};
			$element.bind("click",function(e){
				e.preventDefault();
				$scope.scrollTo();
			});
		}
    }
})
.directive('map', function(){
    return {
        restrict: 'E',
        replace: true,
        template: '<div></div>',
        scope: {
            'address': "@",
            'mapType': "@"
        },
        link: function(scope, element, attrs) {
            console.log(scope, attrs);
            var geoCoder = new google.maps.Geocoder();

            var mapOptions = {
                zoom: 16,
                mapTypeId: eval(scope.mapType),
                draggable: false,
                disableDefaultUI: true
            };
            
            var viewOptions = {
                pov: {heading: 0, pitch: 0},
                zoom: 1,
                streetViewControl: false
            };
            
            var mapInit = function(){
                var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
                var marker = new google.maps.Marker({
                    map: map,
                });
                geoCoder.geocode(
                {'address': scope.address},
                    function(result, status){
                        console.log(result, status);
                        if(status==="OK"){
                            map.setCenter(result[0].geometry.location);
                            marker.setPosition(result[0].geometry.location);
                        }
                    }
                );
            };
            
            var streetViewInit = function(){
                var panorama = new google.maps.StreetViewPanorama(document.getElementById(attrs.id), viewOptions);
                geoCoder.geocode(
                {'address': scope.address},
                    function(result, status){
                        console.log(result, status);
                        if(status==="OK"){
                            panorama.setPosition(result[0].geometry.location);
                        }
                    }
                );
            }
            
            mapInit();
            
            scope.$watch('mapType', function(a,b){
                if(a){
                    if(a === "streetView"){
                       streetViewInit(); 
                    }else{
                        if(a === undefined || a === ""){
                            mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP; 
                        }else{
                            mapOptions.mapTypeId = eval(a);
                        }
                        mapInit();
                        console.log(a);   
                    }
                }
            });
        }
    };
})
.directive("checkImage", function(){
    return {
        restrict: 'A',
        scope: {},
        link: function(scope, element, attrs){
            //console.log(scope);
            element.bind('error', function(res){
                angular.element(element).attr('class','hide');
            })
        }
    }
})
/*.directive('wechatLogin', function($http){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            //console.log(scope,element,attrs);
            element.bind('click', function(){
                var encodeUrl = encodeURIComponent("http://www.liunar.net");
                
                $http({
                   url: "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx0933bfa899eedcaf&redirect_uri=http%3A%2F%2Fwww.liunar.net&response_type=code&scope=snsapi_userinfo#wechat_redirect",
                   method: "JSONP"
                }).then(
                    function(res){
                        alert(res);
                    }
                );
            });
        }   
    }
})*/
.directive('weiboLogin', function(){
    return {
        restrict: "A",
        scope: {
            login: "@",
            
            
        },
        link: function(scope, element, attrs){
            console.log(attrs);
            element.bind('click', function(){
               WB2.anyWhere(function(W){
                    W.widget.connectButton({
                        id: attrs.id,	
                        //type:"6,2",
                        callback : {
                            login:function(o){	//登录后的回调函数
                                console.log(o);
                            },	
                            logout:function(){	//退出后的回调函数
                            }
                        }
                    });
                }); 
            });
        }
    }
})
.config(function($ionicConfigProvider){
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.tabs.style('standard');
    $ionicConfigProvider.views.transition('ios');
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.navBar.positionPrimaryButtons('left');
    $ionicConfigProvider.navBar.positionSecondaryButtons('right');
})
.config(function($httpProvider){
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.transformRequest.unshift(function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    });  
})
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'homeController'
  })
  .state('app.estimate',{
    url: '/estimate',
    views:{
      'tab-home':{
        templateUrl: "templates/estimate.html",
        controller: "estimateController"
      }
    }
  })
  .state('app.estimateResult',{
    url: '/estimate/estimateResult',
    cache: false,
    views:{
      'tab-home':{
        templateUrl: "templates/estimateResult.html",
        controller: "estimateController",
      }
    }
  })
  .state('app.mainHome', {
    url: '/mainHome',
    views: {
      'tab-home': {
        templateUrl: 'templates/mainHome.html',
        controller: 'homeController'
      }
    }
  })
  .state('app.liunarProduct', {
      url:"/mainHome/liunarProduct/:typeId",
      views: {
          'tab-home': {
            templateUrl: "templates/liunarProduct.html",
            controller: "liunarProductController",
            resolve: {
                  productList: function($stateParams, httpServices){
                      return httpServices.getLiunarProductList($stateParams.typeId);
                  },
                  pastCases: function($stateParams, httpServices){
                      return httpServices.getLiunarPastCase($stateParams.typeId);
                  },
                  schoolList: function($stateParams, httpServices){
                      return httpServices.getSchoolRankList($stateParams.typeId);
                  }
            }
        }
      }
  })
  .state('app.pastCase', {
      url:"/mainHome/liunarProduct/pastCase/:typeId/:caseId",
      views: {
          'tab-home': {
            templateUrl: "templates/liunarProductPastCase.html",
            controller: "liunarPastCaseController",
            resolve: {
                  caseDetail: function($stateParams, httpServices){
                      return httpServices.getLiunarPastCase($stateParams.typeId);
                  },
            }
        }
      }
      
  })
  .state('app.summerCamps', {
      url: "/mainHome/summercamps",
      views: {
          'tab-home': {
              templateUrl: "templates/schoolCamp.html",
              controller:"schoolController"
          }
      }
  })
  .state('app.summerCampsDetail', {
      url: "/mainHome/summercamps/campdetail/:campId",
      views: {
          'tab-home': {
              templateUrl: "templates/schoolCampDetail.html",
              controller: "campDetailController",
              resolve: {
                  campDetail: function($stateParams, httpServices){
                    return httpServices.getCampProductDetail($stateParams.campId); 
                  }
              }
          }
      }
  })
  .state('app.indexPostDetail', {
      url: '/mianHome/indexPostDetail/:placeHolderId',
      views: {
          'tab-home': {
              templateUrl: "templates/indexArticleContainer.html",
              controller: "homeController"
          }
      }
  })
  .state('app.postDetail',{
      url: '/mainHome/postDetail/:postId',
      views: {
          'tab-home': {
             templateUrl: 'templates/postDetail.html',
             controller: 'detailController',
             resolve: {
                postDetail: function($stateParams, httpServices){
                    return httpServices.getPostDetails($stateParams.postId); 
                }
            }
        }
    }
  })
  .state('app.gongLue', {
    url: "/mainHome/gongLue",
    views: {
        'tab-home': {
            templateUrl: "templates/gongLue.html",
            controller: "liunarTouTiaoListController"
        }
    }
  })
  .state('app.huMa', {
    url: "/mainHome/huMa",
    views: {
        'tab-home': {
            templateUrl: "templates/huMa.html",
            controller:"huMaListController"
        }
    }
  })
  .state('app.QandA',{
    url: '/mainHome/QandA',
    views: {
        'tab-home': {
           templateUrl: 'templates/QandA.html',
           controller: "qandAController"
        }
    }
  })
  .state('app.QandAList',{
    url: '/mainHome/QandA/QandAList/:order',
    views: {
        'tab-home': {
           templateUrl: 'templates/QandAList.html',
           controller: "qandAController"
        }
    }
  })
  .state('app.QandAForm',{
    url: '/mainHome/QandA/QandAForm',
    views: {
        'tab-home': {
           templateUrl: 'templates/QandAForm.html',
           controller: "qandAController"
        }
    }
  })
  .state('app.QandADetail',{
    url: "/mainHome/QandA/QandAList/QandADetail/:questionId/:order",
    views: {
        'tab-home': {
            templateUrl: 'templates/QandADetail.html',
            controller: "qandADetailController"
        }
    }
  })
  .state('app.school', {
      url: '/school',
      views: {
        'tab-school': {
          templateUrl: 'templates/school.html',
          controller: 'schoolController'
        }
      }
    })
  .state('app.schoolRank',{
      url: "/schoolRank/:cateID",
      views: {
          'tab-school': {
            templateUrl: 'templates/schoolRank.html',
            controller: "schoolController"
          }
      }
  })
  .state('app.schoolStandard', {
      url: "/schoolStandard/:schoolId",
      views: {
          'tab-school': {
              templateUrl: "templates/schoolStandard.html",
              controller: "schoolDetailController",
              resolve: {
                  schoolDetail: function($stateParams, httpServices){
                      return httpServices.getSchoolDetail($stateParams.schoolId);
                  },
                  schoolComment: function($stateParams, httpServices){
                      return httpServices.getSchoolComment($stateParams.schoolId);
                  }
              }
          }
      }
  })
  .state('app.schoolMoreOne',{
      url: "/schoolMore/:schoolId",
      views: {
          'tab-school': {
              templateUrl: "templates/schoolMoreOne.html",
              controller: "schoolDetailController",
              resolve: {
                  schoolDetail: function($stateParams, httpServices){
                      return httpServices.getSchoolDetail($stateParams.schoolId);
                  },
                  schoolComment: function($stateParams, httpServices){
                      return httpServices.getSchoolComment($stateParams.schoolId);
                  }
              }
          }
      }
  })
  .state('apply', {
      url: "/apply/:pageTitle/:productId",
      templateUrl: "templates/applyForm.html",
      controller: "applyController"
  })
  .state('app.stream',{
      url: "/stream",
      views: {
          'tab-stream': {
            templateUrl: "templates/streams.html",
            controller: "streamController"
          }
      }
  })
  /*.state('userLogin', {
    url: '/userLogin',
    templateUrl: 'templates/userLogin.html',
    controller: "userController"
  })
  .state('userSignup', {
    url: "/userSignup",
    templateUrl: "templates/userSignup.html",
    controller: "userController"
  })*/
  .state('app.userCenter', {
    url: "/userCenter",
    views: {
        'tab-user': {
          templateUrl: "templates/userCenter.html",
          controller: "userController"
        }
    }
  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/mainHome');
})
