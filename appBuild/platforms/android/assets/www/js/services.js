angular.module("canbestServer", [])
.factory('localStorage', function ($window, $ionicHistory, $q) {
    return {
        setItem: function (key, value) {
            $window.localStorage.setItem(key, value);
        },
        getItem: function (key, defaultValue) {
            return $window.localStorage.getItem(key) || null;
        },
        setObject: function (key, obj) {
            $window.localStorage.setItem(key, JSON.stringify(obj));
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage.getItem(key) || 'null');
        },
        clear: function () {
            $window.localStorage.clear();
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
        }
    }
})
.factory('httpServices', function ($window, $http, $q) {
    return {

        urlOne: "http://52.88.94.221/",

        localhost: "http://localhost:8080/",

        urlTwo: "http://canbest.org/",

        urlThree: "http://localhost:8888/beimeibang1205/",

        urlFour: "http://api.liunar.net/",

        wechatLogin: function (code) {

            var deferred = $q.defer();

            var url = this.urlFour + "usercenter/wx/" + code;

            $http({
                url: url,
                method: "GET",
            }).then(
                function (res) {
                    deferred.resolve(res.data);
                });

            return deferred.promise;

        },
        apply: function (obj) {
            var deferred = $q.defer();

            $http({
                url: this.urlTwo + "index.php?s=/home/index/post.html",
                method: "POST",
                data: obj,
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        userLogin: function (obj) {
            var deferred = $q.defer();

            $http({
                url: this.urlOne + "index.php?s=/home/index/apiLoginAuth",
                method: "POST",
                data: obj,
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        getGongLueList: function(limit){
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/list/" + limit.toString(),
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getStreamList: function(limit){
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "discover/list/" + limit.toString(),
                method: "GET"
            }).then(
            function(res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },

        getPostDetails: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "articles/detail/"+ id.toString(),
                    //url: this.urlOne + "index.php?s=News/index/apiGetNewsDetail/id/" + id.toString() + ".html",
                    method: "GET",
                }).then(
                function(res){
                    deferred.resolve(res.data[0]);
                });
            return deferred.promise;
        },

        getPostDetailsComment: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "comments/1/" + id.toString(),
                //url: this.urlOne + "index.php?s=/news/index/apiGetComments&app=news&id=" + id.toString(),
                method: "GET",
            }).then(
            function(res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },
        postComment: function(obj){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "comments",
                    //url:  this.urlOne + "index.php?s=/news/index/apiPostComment",
                method: "POST",
                data: obj
            }).then(
            function(res){
                deferred.resolve(res);
            });
            return deferred.promise;
        },

        getLiunarProductList: function(typeId){
            var deferred = $q.defer();
                $http({
                    //url: this.urlOne + "index.php?s=/issue/index/apiGetRankingList&issue_id=" + schoolCateId.toString(),
                    url: this.urlFour + "products/list/" + typeId.toString(),
                    method: "GET"
                }).then(
                function(res){
                    deferred.resolve(res.data);
                });
                return deferred.promise;
        },
        userSignup: function (obj) {
            var deferred = $q.defer();

            $http({
                url: this.urlOne + "index.php?s=/home/index/apiRegister",
                method: "POST",
                data: obj,
            }).then(
            function (res) {
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },

        getPosts: function () {
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/list",
                //url: this.urlOne + "index.php?s=/news/index/apiGetNewsLists.html",
                method: "GET",
            }).then(
            function (res) {
                //console.log(res);
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },

        getGongLueList: function (limit) {
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/list/" + limit.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        
        getHuMaList: function(limit){
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "articles/list/huma/" + limit.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getStreamList: function (limit) {
            var deferred = $q.defer();

            $http({
                url: this.urlFour + "discover/list/" + limit.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getLiunarProductList: function (typeId) {
            var deferred = $q.defer();
            $http({
                //url: this.urlOne + "index.php?s=/issue/index/apiGetRankingList&issue_id=" + schoolCateId.toString(),
                url: this.urlFour + "products/list/" + typeId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getLiunarPastCase: function (typeId) {
            var deferred = $q.defer();
            $http({
                //url: this.urlOne + "index.php?s=/issue/index/apiGetRankingList&issue_id=" + schoolCateId.toString(),
                url: this.urlFour + "pastcases/list/" + typeId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },
        getCampProducts: function (schoolCateId) {
            var deferred = $q.defer();
            $http({
                url: this.urlOne + "index.php?s=/issue/index/apiGetSchools&issue_id=" + schoolCateId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getCampProductDetail: function (id) {
            var deferred = $q.defer();
            $http({
                url: this.urlOne + "index.php?s=/issue/index/apiGetProductDetail&id=" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getSchoolRankList: function (schoolCateId) {
            var deferred = $q.defer();
            $http({
                //url: this.urlOne + "index.php?s=/issue/index/apiGetRankingList&issue_id=" + schoolCateId.toString(),
                url: this.urlFour + "schools/?type_id=" + schoolCateId.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        getSchoolDetail: function (id) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "schools/" + id.toString(),
                //url: this.urlOne + "index.php?s=/issue/index/apiGetProductDetail&id=" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data[0]);
            }
            );
            return deferred.promise;
        },
        
        getSchoolComment: function(id){
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "comments/2/" + id.toString(),
                //url: this.urlOne + "index.php?s=/news/index/apiGetComments&app=news&id=" + id.toString(),
                method: "GET",
            }).then(
            function(res){
                deferred.resolve(res.data);
            });
            return deferred.promise;
        },

        getQuestions: function (column, data, order, limit) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/question/index/apiGetQuestions&filter_column=" + column + "&filter_data=" + data + "&ordered_by=" + order + "&limit=" + limit,
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        postQuestion: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/question/index/apiPostQuestion",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },

        getQuestionDetail: function (id) {
            var deferred = $q.defer()
            $http({
                url: this.urlTwo + "index.php?s=/question/index/apiGetAnswer&id=" + id.toString(),
                method: "GET"
            }).then(
            function (res) {
                deferred.resolve(res.data);
            }
            );
            return deferred.promise;
        },

        postAnswer: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlTwo + "index.php?s=/question/index/apiPostAnswer",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        },

        postSurveyResult: function (obj) {
            var deferred = $q.defer();
            $http({
                url: this.urlFour + "survey/submit/",
                method: "POST",
                data: obj
            }).then(
            function (res) {
                deferred.resolve(res);
            }
            );
            return deferred.promise;
        }

    }
})
.factory("pressureTest", function ($http) {
    return {
        post: function () {
            $http({
                url: "http://api.liunar.net/articles/user",
                method: "POST",
            }).then(
            function (res) {
                console.log(res);
            }, function (res) {
                console.log(res);
            }
            );
        },

        put: function () {
            $http({
                url: "http://api.liunar.net/articles/user",
                method: "PUT",
            }).then(
            function (res) {
                console.log(res);
            }, function (res) {
                console.log(res);
            }
            );
        },

        get: function () {
            $http({
                url: "http://api.liunar.net/articles/fulllist",
                method: "GET",
            }).then(
            function (res) {
                console.log(res);
            }, function (res) {
                console.log(res);
            }
            );
        }
    }
})
/*.factory("wechatShare", function(){
    console.log();
})*/
