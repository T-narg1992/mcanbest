angular.module('qandAController',[])
.controller('qandAController', function(localStorage, $scope,httpServices,$stateParams,$state,$ionicHistory, $ionicPopup){
    
    //questions playground:
    $scope.getHotQuestions = function(){
        console.log($ionicHistory);
        httpServices.getQuestions(1,1,"answer_num",5).then(
            function(res){
                $scope.hotQuestions = res
                //console.log(res);
            }
        );
    };
    
    $scope.getNewestQuestion = function(){
        httpServices.getQuestions(1,1,"create_time",1).then(
            function(res){
                $scope.newQuestion = res[0];
                //console.log(res);
            }
        );  
    };
    
    var questionNum = 10;
    
    console.log(questionNum);
    $scope.getQuestionByOrder = function(){
        console.log($stateParams.order);
        if($stateParams.order === "answer_num"){
            $scope.listTitle = "最热问题";
        }else if($stateParams.order === "create_time"){
            $scope.listTitle = "最新问题";
        }
        httpServices.getQuestions(1,1,$stateParams.order,questionNum).then(
            function(res){
                $scope.questions = res;
                $scope.order = $stateParams.order;
                //console.log(res);
            }
        );
    };
    
    $scope.loadMoreQuestions = function(){
        
        questionNum = questionNum + 5;
        console.log(questionNum);
        httpServices.getQuestions(1,1,$stateParams.order,questionNum).then(
            function(res){
                $scope.questions = res
                //console.log(res);
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }
        );
    };
    
    //ask questions form:
    /*$scope.tags = [];
    
    $scope.addTags = function(tag){
        if($scope.tags.indexOf(tag) === -1){
        $scope.tags.push(tag);
        }else{
            //console.log("exist");
        }
    };
    
    $scope.removeTags = function(tag){
        if($scope.tags.indexOf(tag) !== -1){
        console.log(tag);
        $scope.tags.splice($scope.tags.indexOf(tag),1);
        }else{
        }
    };*/
    
    $scope.questionObj = {
        title: "",
        description: "",
        //tags: $scope.tags
    };
    
    $scope.submitQ = function(){
        
        if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再提问'
            }).then(
                function(res){
                    $state.go("app.userCenter");
                }
            );
        }else{
            if($scope.questionObj.title == "" || $scope.questionObj.title.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '问题不能为空，而且必须多于6个字'
                });  
            }else{
                $scope.questionObj.uid = localStorage.getObject("canbestUserInfo").uid;
                httpServices.postQuestion($scope.questionObj).then(
                    function(res){
                        $scope.getNewestQuestion();
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的提问，我们将会在24小时内答复。'
                        }).then(
                            function(){
                                $scope.getNewestQuestion();
                                $state.go("app.QandA");
                            }
                        )
                    }
                );
            }
        }
        
    };
})