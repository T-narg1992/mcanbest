angular.module("liunarProduct",[])
.controller("liunarProductController", function($ionicHistory,$scope, $state, $stateParams, productList, pastCases, schoolList){
    console.log($stateParams, productList, pastCases, schoolList);
    if($stateParams.typeId == 1){
        $scope.cate = "知名大学";
    }else if($stateParams.typeId == 2){
        $scope.cate = "特色院校";
    }else if($stateParams.typeId == 3){
        $scope.cate = "低龄留学";
    }
    
    $scope.pastCases = pastCases;
    $scope.productLists = productList;
    $scope.schools = schoolList.slice(0,4);
    $scope.anchor = $stateParams.typeId;
    if($ionicHistory.viewHistory().backView !== null){
            $scope.from = $ionicHistory.viewHistory().backView.url;
        }else{
            $scope.from = "/app/mainHome";
            $scope.fromShare = true;
    }
})
.controller("liunarPastCaseController", function($scope, $state, $stateParams, caseDetail, $ionicHistory){
    console.log(caseDetail);
    for(var i = 0; i < caseDetail.length; i++){
        if(caseDetail[i].id == $stateParams.caseId){
            $scope.case = caseDetail[i];
            break;
        }
    }
    if($ionicHistory.viewHistory().backView !== null){
            $scope.from = $ionicHistory.viewHistory().backView.url;
        }else{
            $scope.from = "/app/mainHome";
            $scope.fromShare = true;
    }
})