angular.module('schoolController',[])
.controller('schoolController', function($scope, $http, httpServices, $ionicPosition, $stateParams){
    //functions => load Content:
    
    $scope.getSchoolsRank = function(){
        console.log($stateParams);
        httpServices.getSchoolRankList($stateParams.cateID).then(
            function(res){
                $scope.schoolListsRank = res; 
                console.log(res);
            }
        );
    };
    
    $scope.getCampList = function(){
        
        httpServices.getCampProducts(15).then(
            function(res){
                console.log(res);
                $scope.camplist = res.all;
            }
        );
    };

})