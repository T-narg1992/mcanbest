angular.module('qandADetailController',[])
.controller('qandADetailController',function(localStorage, $scope, httpServices, $stateParams, $state){
    //getQandA Detail
    $scope.getQuestionDetail = function(){
        httpServices.getQuestionDetail($stateParams.questionId).then(
            function(res){
                //console.log(res);
                $scope.qDetail = res;
            }
        );
        $scope.order = $stateParams.order;
    };
    //Post Answer
    $scope.answerObj = {
        question_id: $stateParams.questionId,
        content: "",
        //uid: localStorage.getObject('canbestUserInfo').uid,
        //commentPerson: null,
    };
    
    $scope.autoFocus = false;
    
    /*$scope.addPerson = function(p){
        if($scope.answerObj.commentPerson === null){
            $scope.answerObj.commentPerson = p;
            $scope.showPerson = true;
        }else if($scope.answerObj.commentPerson !== null){
            if($scope.answerObj.commentPerson === p){
                $scope.answerObj.commentPerson = null;
                $scope.showPerson = false;
            }else{
                $scope.answerObj.commentPerson = p;
                $scope.showPerson = true;
            }
        }
        console.log($scope.answerObj, p);
    };*/
    
    $scope.postAnswer = function(){
        console.log($scope.answerObj);
        
        if(localStorage.getObject("canbestUserInfo") === null){
            $scope.anwserObj.uid = 0;
        }else{
            $scope.answerObj.uid = localStorage.getObject("canbestUserInfo").uid;
        }
        
        httpServices.postAnswer($scope.answerObj).then(
            function(res){
                console.log(res);
                $scope.getQuestionDetail();
            }
        );
        $scope.answerObj.content = null;
    };
})
