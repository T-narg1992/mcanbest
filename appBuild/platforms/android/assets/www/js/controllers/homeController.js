angular.module('homeController',[])
.controller('homeController', function($ionicPopup, $scope, $ionicModal, $ionicScrollDelegate, httpServices, $ionicSlideBoxDelegate,$rootScope, $ionicPosition, $location, $stateParams, localStorage, $sce, $state) {
    
  $rootScope.base_url = "http://52.88.94.221/";
    
  $scope.navTitle = '<h1 class="title"><img class="title-image" src="img/liunarLogo.png"  height="32"/>留哪儿</h1>';
    
  $scope.goToHome = function(){
      $state.go("app.mainHome");
  }
  
  $scope.html = $sce.trustAsHtml($scope.navTitle);
    
  $scope.shouldHide = true;
    
  $scope.headerBarControll = function(){
    //console.log($ionicScrollDelegate.getScrollPosition().top);
    if($ionicScrollDelegate.getScrollPosition().top > 100){
        $scope.shouldHide = false;
    }else{
        $scope.shouldHide = true;
    }
  } 
    
  $scope.slideChanged = function(index){
      $scope.slideIndex = index;
  };
    
  $scope.getPosts = function(){
      httpServices.getPosts().then(
            function(res){
                console.log(res);
                $scope.posts = res;
                localStorage.setItem("refer", 1);
            }
        );
    };
  
  $scope.getArticle = function(){
      console.log($stateParams.placeHolderId);
      $scope.ArticleUrl = "templates/indexArticle" + $stateParams.placeHolderId.toString() + ".html";
  };
  
  $scope.getNewQuestions = function(){
        httpServices.getQuestions(1,1,"create_time",2).then(
            function(res){
                $scope.questions = res;
            }
        );  
    };
    
  $scope.getQuestionDetail = function(id){
        httpServices.getQuestionDetail(id).then(
            function(res){
                //console.log(res);
                $scope.qDetail = res;
            }
        );
        $scope.order = $stateParams.order;
    };

})