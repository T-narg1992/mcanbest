angular.module('streamController',[])
.controller('streamController', function($scope, $ionicPopup, httpServices){
	var num = 5;
	$scope.getAllStream = function(){
		httpServices.getStreamList(num).then(
			function(res){
				console.log(res);
				$scope.stream= res;
				
			}
		)
	};

	$scope.getMoreStream = function(){
		num += 5;
		httpServices.getStreamList(num).then(
			function(res){
				console.log(res);
				$scope.stream = res;
				$scope.$broadcast('scroll.infiniteScrollComplete');

			});
	};

	$scope.tags = {0:"留哪头条",1:"虎妈之家",3:"专家答疑"};
	$scope.urlQuestion = "#/app/mainHome/QandA/QandAList/QandADetail/";
	$scope.urlPostDetail = "#/app/mainHome/postDetail/";

	 // getAllLists();

    
});