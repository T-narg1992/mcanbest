angular.module('articleListController',[])
.controller('liunarTouTiaoListController', function($scope, httpServices){
    
    var articleNum = 5;
    $scope.getArticleList = function(){
        console.log(articleNum);
        httpServices.getGongLueList(articleNum).then(
            
            function(res){
                console.log(res);
                $scope.articles = res;
            }
        )
    };
    
    $scope.getMoreArticles = function(){
        //console.log(articleNum);
        articleNum = articleNum + 5;
        console.log(articleNum);
        httpServices.getGongLueList(articleNum).then(
            function(res){
                console.log(res);
                $scope.articles = res;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                console.log("scroll complete");
            }
        )
    };
})
.controller('huMaListController', function($scope, httpServices){
    //console.log("huma");
    $scope.getHuMaList = function(){
        httpServices.getHuMaList(10).then(
            function(res){
                console.log(res);
                $scope.humaLists = res;
            }
        )
    };
    
})