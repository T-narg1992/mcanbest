angular.module('detailController',[])
.controller('detailController', function(localStorage, $ionicPopup,$scope, httpServices, $rootScope, $stateParams, $sce, $state, $ionicHistory, postDetail, $sanitize){
    //console.log($state.current, $state, $ionicHistory.viewHistory(), postDetail);
    
    $scope.$on("$ionicView.beforeEnter", function(){
        console.log($state.current, $state, $ionicHistory.viewHistory(), postDetail);
        if($ionicHistory.viewHistory().backView !== null){
            $scope.from = $ionicHistory.viewHistory().backView.url;
        }else{
            $scope.from = "/app/mainHome";
            $scope.fromShare = true;
        }
    });
        
        $scope.postDetail = postDetail;
        console.log($scope.postDetail);
        
        var desHTML = postDetail.description;
        $scope.description = $sce.trustAsHtml(desHTML);
        var html = postDetail.content_html;
        $scope.postDetailContent = $sce.trustAsHtml(html);
       
    $scope.getPostComments = function(){
       httpServices.getPostDetailsComment($stateParams.postId).then(
            function(res){
                if(res[0]==="error"){
                }else{
                    $scope.comments = res;
                    console.log(res);
                }
            }
        );
    };
    
    $scope.commentObj = {
        row_id : $stateParams.postId,
        content: "",
        module: 1   
    };
    
    $scope.postNewsComment = function(){
        if(localStorage.getObject("canbestUserInfo") === null || !localStorage.getObject("canbestUserInfo").uid){
            $ionicPopup.alert({
                title: '留哪儿',
                template: '请微信登录之后再评论'
            }).then(
                function(){
                    $state.go("app.userCenter");    
                }
            );
            //$state.go();
        }else{
            $scope.commentObj.uid = localStorage.getObject("canbestUserInfo").uid;
            if($scope.commentObj.content == "" || $scope.commentObj.content.length < 6){
                $ionicPopup.alert({
                    title: '留哪儿',
                    template: '评论不能为空，而且必须多于6个字'
                }); 
            }else{
                httpServices.postComment($scope.commentObj).then(
                    function(res){
                        $ionicPopup.alert({
                            title: '留哪儿',
                            template: '感谢您的评论'
                        }).then(
                            function(res){
                                $scope.commentObj.content = null;
                                $scope.getPostComments();
                            }
                        )
                    }
                );
            }
        }
    };
    
})